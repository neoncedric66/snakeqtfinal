#ifndef PJMCGRADIENTPALETTE_H
#define PJMCGRADIENTPALETTE_H

#include <QObject>
#include <QLinearGradient>

class PJMCGradientPalette : public QObject
{
    Q_OBJECT
    Q_PROPERTY(int width READ width WRITE setWidth NOTIFY widthChanged)
    Q_PROPERTY(QImage* canvas READ canvas)
    Q_PROPERTY(QLinearGradient gradient READ gradient)

public:
    explicit PJMCGradientPalette(QObject *parent = nullptr);

    bool width() const;
    void setWidth(int width);

    QImage* canvas();
    QLinearGradient gradient();

    Q_INVOKABLE void setColorAt(qreal index, const QColor &color);
    Q_INVOKABLE QColor getColorAt(qreal index);

private:
    void paintOnCanvas();

private:
    QImage *canvas_;
    QLinearGradient gradient_;
    int width_;

signals:
    void widthChanged();
    void colorChanged();

public slots:
};

#endif // PJMCGRADIENTPALETTE_H
