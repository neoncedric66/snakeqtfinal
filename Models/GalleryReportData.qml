pragma Singleton
import QtQuick 2.0

Item {
    property string snakechosen: ""
    property var snakevisible: false
    property var speciesknownvisible: true
    property var gallerytext: "Open Gallery"
    property var speciestext: "Species Known?"
}
