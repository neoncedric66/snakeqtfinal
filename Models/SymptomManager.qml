pragma Singleton
import QtQuick 2.0

Item {
    id: root

    signal sMGetSymptomsSuccessful();
    signal sMGetSymptomsFailed(var error);

    property alias symptoms: list_symptoms;

    ListModel {
        id: list_symptoms
    }

    function getSymptoms(){
        var params = {};
        var header = {
            'Authorization': 'Bearer ' + AuthManager.userBearerToken
        };
        RequestManager.get('symptom/find', params, header, {'class':'SymptomManager', 'action':'getSymptoms'});
    }

    /**
        Callback from RequestManager.qml
    */
    Connections {
        ignoreUnknownSignals: true
        target: RequestManager
        onRequestSuccessful: {
            if(userInfo['class'] !== 'SymptomManager') return;

            switch(userInfo['action']){
                case 'getSymptoms':
                getSymptomsSuccessful(response, userInfo);
                break;
                default:
            }
        }
        onRequestFailed: {
            if(userInfo['class'] !== 'SymptomManager') return;

            switch(userInfo['action']){
                case 'getSymptoms':
                getSymptomsFailed(error, userInfo);
                break;
                default:
            }
        }
    }

    function getSymptomsSuccessful(response, userInfo){
        list_symptoms.clear();
        response.forEach(function(aSymptom){
            list_symptoms.append(aSymptom);
        });
        sMGetSymptomsSuccessful();
    }

    function getSymptomsFailed(error, userInfo) {
        sMGetSymptomsFailed(error)
    }
}
