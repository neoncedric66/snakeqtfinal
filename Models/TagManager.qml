pragma Singleton
import QtQuick 2.0

Item {
    id: root

    signal tMGetTagsSuccessful();
    signal tMGetTagsFailed(var error);

    property alias symptoms: list_tags

    ListModel {
        id: list_tags
    }

    function getSymptoms(){
        var params = {};
        var header = {
            'Authorization': 'Bearer ' + AuthManager.userBearerToken
        };
        RequestManager.get('tag/find', params, header, {'class':'TagManager', 'action':'getTags'});
    }

    /**
        Callback from RequestManager.qml
    */
    Connections {
        ignoreUnknownSignals: true
        target: RequestManager
        onRequestSuccessful: {
            if(userInfo['class'] !== 'TagManager') return;

            switch(userInfo['action']){
                case 'getTags':
                getTagsSuccessful(response, userInfo);
                break;
                default:
            }
        }
        onRequestFailed: {
            if(userInfo['class'] !== 'TagManager') return;

            switch(userInfo['action']){
                case 'getTags':
                getTagsFailed(error, userInfo);
                break;
                default:
            }
        }
    }

    function getTagsSuccessful(response, userInfo){
        list_tags.clear();
        response.forEach(function(aTag){
            list_tags.append(aTag);
        });
        tMGetTagsSuccessful();
    }

    function getTagsFailed(error, userInfo) {
        tMGetTagsFailed(error)
    }
}
