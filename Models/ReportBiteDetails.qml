pragma Singleton
import QtQuick 2.0

Item {
    property var currentlocation: false
    property string username: ""
    property var snakechosen: ""
    property var mapbites: reportedbites
    property var body:({})
    property var longitude: ""
    property var latitude: ""
    property var biteurl: ""
    property var imagevis: false
    
    ListModel {
        id: reportedbites
    }
    
}
