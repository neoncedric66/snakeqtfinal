pragma Singleton
import QtQuick 2.0

Item {

    property var snakes: snakePics

    ListModel {
        id: snakePics

        ListElement {
            url: "qrc:/Icons/Snakes/snake1.png"
            number: 1
            name: "Malayan Pit Viper"
            text: "Calloselasma is a monotypic genus created for a venomous pit viper species, C. rhodostoma, which is endemic to Southeast Asia from Thailand to northern Malaysia and on the island of Java. No subspecies are currently recognized.\n\nRetrieved from Wikipedia"
        }
        ListElement {
            url: "qrc:/Icons/Snakes/snake2.png"
            number: 2
            name: "Red Necked Keelback"
        }
        ListElement {
            url: "qrc:/Icons/Snakes/snake3.png"
            number: 3
            name: "Bungarus fasciatus"
        }
        ListElement {
            url: "qrc:/Icons/Snakes/snake4.png"
            number: 4
            name: "Calloselasma rhodostoma"
        }
        ListElement {
            url: "qrc:/Icons/Snakes/snake5.png"
            number: 5
            name: "Cryptelytrops rubeus"
        }
        ListElement {
            url: "qrc:/Icons/Snakes/snake6.png"
            number: 6
            name: "Enhydris enhydris"
        }
        ListElement {
            url: "qrc:/Icons/Snakes/snake7.png"
            number: 7
            name: "Naja naja"
        }
        ListElement {
            url: "qrc:/Icons/Snakes/snake8.png"
            number: 8
            name: "Ophiophagus hannah"
        }
        ListElement {
            url: "qrc:/Icons/Snakes/snake9.png"
            number: 9
            name: "Rhabdophis subminiatus"
        }

    }

}
