pragma Singleton
import QtQuick 2.0

Item {
    id : root
    signal requestSuccessful(var response, var userInfo);
    signal requestFailed(var error, var userInfo);

    property string baseUrl: 'http://snake.sushess.com/';  //'http://68.183.236.185/';

    function postJson (route, params, body, header, userInfo){
        header['Content-type'] = 'application/json';
        performRequest('POST', route, params, body, header, userInfo);
    }
 
    function get (route, params, header, userInfo){
        header['Content-type'] = 'application/json';
        performRequest('GET', route, params, {}, header, userInfo);
    }

    function performRequest (type, route, params, body, header, userInfo){
        var http = new XMLHttpRequest();
        var url = baseUrl + route + formatParams(params);
        http.open(type, url, true);
        Object.keys(header).forEach(function(key) {
            http.setRequestHeader(key, header[key]);
        });
        http.userInfo = userInfo;

        http.onreadystatechange = function() { // Call a function when the state changes.
            if (http.readyState === XMLHttpRequest.DONE) {
                console.log(http.responseText);
                if (http.status == 200) {
                    var response = JSON.parse(http.responseText);
                    console.log(response)
                    console.log(http.userInfo)
                    requestSuccessful(response, http.userInfo);
                } else {
                    var error = '';
                    try{
                        error = JSON.parse(http.responseText);
                    }
                    catch (e){
                        error = http.responseText;
                    }
                    requestFailed(error, http.userInfo)
                }
            }
        }

        if (type === 'POST') http.send(JSON.stringify(body));
        else http.send();
    }

    function formatParams(params){
        if (!params || Object.keys(params).length < 1) return '';
        return '?' + Object
        .keys(params)
        .map(function(key){
            return key+"="+encodeURIComponent(params[key])
        })
        .join('&')
    }

    //    function formatBody(params, type){
    //        switch(type){
    //        case 'json':
    //            return JSON.stringify(params);
    //        default:
    //            return params;
    //        }
    //    }
}
