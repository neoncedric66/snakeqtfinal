pragma Singleton
import QtQuick 2.0

Item {
    property var primary: "#03A9F4"
    property var secondary: "#B3E5FC"
    property var accent: "#BDBDBD"
    property var placedone: 0
    property string mapselector: "Select area on map"
    property string calendarselector: "Choose from calendar"
    property string photoselector: "Take a Photo"
    property var multiple: 0
}
