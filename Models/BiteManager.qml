pragma Singleton
import QtQuick 2.0

Item {
    id: root

    signal bMReportSuccessful();
    signal bMReportFailed(var error);
    signal bMGetMyReportsSuccessful();
    signal bMGetMyReportsFailed(var error);
    signal bMGetRandomReportsSuccessful();
    signal bMGetRandomReportsFailed(var error);
    signal bMDiagnoseSuccessful();
    signal bMDiagnoseFailed(var error);
    signal bMUpdatePatientStatusSuccessful();
    signal bMUpdatePatientStatusFailed(var error);

    property alias myReports: list_myReports;
    property alias randomReports: list_randomReports;

    ListModel {
        id: list_myReports
    }

    ListModel {
        id: list_randomReports
    }

    function report(body) {
        var header = {
            'Authorization': 'Bearer ' + AuthManager.userBearerToken
        }
        RequestManager.postJson('bite/report', {}, body, header, {'class':'BiteManager', 'action':'report'});
    }

    function diagnose(diagnoses) {
        var header = {
            'Authorization': 'Bearer ' + AuthManager
        }
        var body = {
            'diagnoses': diagnoses
        }

        RequestManager.postJson('bite/diagnose', {}, body, header, {'class':'BiteManager', 'action':'diagnose'});
    }

    function updatePatientStatus(statusInfo) {
        var header = {
            'Authorization': 'Bearer ' + AuthManager.userBearerToken
        }
        var body = statusInfo

        RequestManager.postJson('bite/update-patient-status', {}, body, header, {'class':'BiteManager', 'action':'update-patient-status'});
    }

    function getMyReports(){
        var params = {
            'reporter' : UserManager.currentUser['id'],
            'populate' : false
        };
        var header = {
            'Authorization': 'Bearer ' + AuthManager.userBearerToken
        };
        RequestManager.get('bite/find', params, header, {'class':'BiteManager', 'action':'getMyReports'});
    }

    function getRandomReports(body){
        var header = {
            'Authorization': 'Bearer ' + AuthManager.userBearerToken
        }
        RequestManager.postJson('bite/find', {}, body, header, {'class':'BiteManager', 'action':'getRandomReports'});
    }

    /**
        Callback from RequestManager.qml
    */
    Connections {
        ignoreUnknownSignals: true
        target: RequestManager
        onRequestSuccessful: {
            if(userInfo['class'] !== 'BiteManager') return;

            switch(userInfo['action']){
                case 'report':
                reportSuccessful(response, userInfo);
                break;
                case 'diagnose':
                diagnoseSuccessful(response, userInfo);
                break;
                case 'update-patient-status':
                updatePatientStatusSuccessful(response, userInfo);
                break;
                case 'getMyReports':
                getMyReportsSuccessful(response, userInfo);
                break;
                case 'getRandomReports':
                getRandomReportsSuccessful(response, userInfo);
                break;
                default:
            }
        }
        onRequestFailed: {
            if(userInfo['class'] !== 'BiteManager') return;

            switch(userInfo['action']){
                case 'report':
                reportFailed(error, userInfo);
                break;
                case 'diagnose':
                diagnoseFailed(error, userInfo);
                break;
                case 'update-patient-status':
                updatePatientStatusFailed(error, userInfo);
                break;
                case 'getMyReports':
                getMyReportsFailed(error, userInfo);
                break;
                case 'getRandomReports':
                getRandomReportsFailed(error, userInfo);
                break;
                default:
            }
        }
    }

    function reportSuccessful(response, userInfo){
        bMReportSuccessful();
    }

    function reportFailed(error, userInfo) {
        bMReportFailed(error)
    }

    function diagnoseSuccessful(response, userInfo){
        bMDiagnoseSuccessful();
    }

    function diagnoseFailed(error, userInfo) {
        bMDiagnoseFailed(error)
    }

    function updatePatientStatusSuccessful(response, userInfo){
        bMUpdatePatientStatusSuccessful();
    }

    function updatePatientStatusFailed(error, userInfo) {
        bMUpdatePatientStatusFailed(error)
    }


    function getMyReportsSuccessful(response, userInfo){
        list_myReports.clear();

        response.forEach(function (aReport) {
            console.log(aReport)
            var parsetext = JSON.parse(aReport)
            console.log("PARSE: " + parsetext)
            list_myReports.append(aReport);
        });
        console.log(list_myReports)
        bMGetMyReportsSuccessful();
        console.log("SUCCESS!")
    }

    function getMyReportsFailed(error, userInfo){
        bMGetMyReportsFailed(error);
    }

    function getRandomReportsSuccessful(response, userInfo){
        list_randomReports.clear();
        response.forEach(function (aReport) {
            list_randomReports.append(aReport);
        });
        bMGetRandomReportsSuccessful();
    }

    function getRandomReportsFailed(error, userInfo){
        bMGetRandomReportsFailed(error);
    }
}
