pragma Singleton
import QtQuick 2.0

Item {
    id: root
    property var currentUser: ({});


    signal uMRegisterSucessful();
    signal uMRegisterFailed(var error);
    signal uMGetProfileSuccessful();
    signal uMGetProfileFailed(var error);
    signal uMUpdateProfileSuccessful();
    signal uMUpdateProfileFailed(var error);

    function register(body){
        var header = {
        };
        RequestManager.postJson('user/register', {}, body, header, {'class':'UserManager', 'action':'register'});
    }

    function getProfile(){
        var header = {
            'Authorization': 'Bearer ' + AuthManager.userBearerToken
        };
        RequestManager.get('user/profile', {}, header, {'class':'UserManager', 'action':'getProfile'});
    }

    function updateProfile(body){
        var header = {
            'Authorization': 'Bearer ' + AuthManager.userBearerToken
        }
        RequestManager.postJson('user/update', {}, body, header, {'class':'UserManager', 'action':'updateProfile'});
    }

    /**
        Callback from RequestManager.qml
    */
    Connections {
        ignoreUnknownSignals: true
        target: RequestManager
        onRequestSuccessful: {
            if(userInfo['class'] !== 'UserManager') return;

            switch(userInfo['action']){
                case 'register':
                registerSuccessful(response, userInfo);
                break;
                case 'getProfile':
                getProfileSuccessful(response, userInfo);
                break;
                case 'updateProfile':
                updateProfileSuccessful(response, userInfo);
                break;
                default:
            }
        }
        onRequestFailed: {
            if(userInfo['class'] !== 'UserManager') return;

            switch(userInfo['action']){
                case 'register':
                registerFailed(error, userInfo);
                break;
                case 'getProfile':
                getProfileFailed(error, userInfo);
                break;
                case 'updateProfile':
                updateProfileFailed(error, userInfo);
                break;
                default:
            }
        }
    }

    function registerSuccessful(response, userInfo){
        uMRegisterSucessful();
    }

    function registerFailed(error, userInfo){
        uMRegisterFailed(error);
    }

    function getProfileSuccessful(response, userInfo){
        currentUser = response;
        uMGetProfileSuccessful();
    }

    function getProfileFailed(error, userInfo){
        uMGetProfileFailed(error);
    }

    function updateProfileSuccessful(response, userInfo){
        currentUser = response;
        uMUpdateProfileSuccessful();
    }

    function updateProfileFailed(error, userInfo){
        uMUpdateProfileFailed(error);
    }
}
