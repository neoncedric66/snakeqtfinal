pragma Singleton
import QtQuick 2.0


Item {

    property string fail: "fail"

    function xMLReq(url, params, type) {
        var http = new XMLHttpRequest
        http.open(type,url,true)
        http.setRequestHeader("Content-type", "application/x-www-form-urlencoded")
        http.onreadystatechange = function() {
            if (http.readyState == 4 && http.status == 200) {
                console.log(http.responseText)
                console.log(http.status)
                try {
                    var data = JSON.parse(http.responseText)
                    return data
                } catch (TypeError) {
                    console.log("Error, data entered does not fit the requirements")
                    return fail
                }
            } else {
                console.log("Error")
                return fail
            }
        }

        http.send(params)

    }

}
