pragma Singleton
import QtQuick 2.0

Item {
    id: root

    property alias snakes: list_snakes

    signal sMGetSnakesSuccessful();
    signal sMGetSnakesFailed(var error);
    signal sMUploadPhotoSucessful();
    signal sMUploadPhotoFailed(var error);
    signal sMVerifyPhotoSuccessful();
    signal sMVerifyPhotoFailed(var error);

    ListModel{
        id: list_snakes
    }

    function getSnakes(){
        var header = {
            'Authorization': 'Bearer ' + AuthManager.userBearerToken
        }
        RequestManager.get('snake/find', {}, header, {'class':'SnakeManager', 'action':'getSnakes'});
    }

    function uploadPhoto(body){ //snake, snakePart, image file
        //no implementation yet
    }

    function verifyPhoto(photoId){
        var body = {
            'id': photoId
        }
        var header = {
            'Authorization': 'Bearer ' + AuthManager.userBearerToken
        }
        RequestManager.postJson('snake/photo/verify', {}, body, header, {'class':'SnakeManager', 'action':'verifyPhoto'});
    }

    /**
        Callback from RequestManager.qml
    */
    Connections {
        ignoreUnknownSignals: true
        target: RequestManager
        onRequestSuccessful: {
            if(userInfo['class'] !== 'SnakeManager') return;

            switch(userInfo['action']){
                case 'getSnakes':
                getSnakesSuccessful(response, userInfo);
                break;
                case 'uploadPhoto':
                uploadPhotoSuccessful(response, userInfo);
                break;
                case 'verifyPhoto':
                verifyPhotoSuccessful(response, userInfo);
                break;
                default:
            }
        }
        onRequestFailed: {
            if(userInfo['class'] !== 'SnakeManager') return;

            switch(userInfo['action']){
                case 'getSnakes':
                getSnakesFailed(error, userInfo);
                break;
                case 'uploadPhoto':
                uploadPhotoFailed(error, userInfo);
                break;
                case 'verifyPhoto':
                verifyPhotoFailed(error, userInfo);
                break;
                default:
            }
        }
    }

    function getSnakesSuccessful(response, userInfo){
        list_snakes.clear();
        response.forEach(function (aSnake) {
            list_snakes.append(aSnake);
        });
        sMGetSnakesSuccessful();
    }

    function getSnakesFailed (error, userInfo){
        sMGetSnakesFailed(error);
    }

    function uploadPhotoSuccessful(response, userInfo){
        sMUploadPhotoSucessful();
    }

    function uploadPhotoFailed(error, userInfo){
        sMUploadPhotoFailed(error);
    }

    function verifyPhotoSuccessful(response, userInfo){
        sMVerifyPhotoSuccessful();
    }

    function verifyPhotoFailed(error, userInfo){
        sMVerifyPhotoFailed(error);
    }
}
