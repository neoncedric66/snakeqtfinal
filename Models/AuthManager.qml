pragma Singleton
import QtQuick 2.0

Item {
    id: root
    property string appBearerToken: '';
    property string appRefreshToken: '';
    property string userBearerToken: '';
    property string userRefreshToken: '';
    property string clientId: 'FTYBFCJHRY';
    property string clientSecret: 'KFqpxpSidY5llJYciSpMk4L9lQZTAA';

    signal aMAuthAppSuccessful();
    signal aMAuthAppFailed(var error);
    signal aMLoginUserSuccessful();
    signal aMLoginUserFailed(var error);

    function authApp(){
        var body = {
            "grant_type": "client_credentials"
        };
        var authKey = Qt.btoa(root.clientId + ":" + root.clientSecret);
        var header = {
            'Authorization': 'Basic ' + authKey
        }
        RequestManager.postJson('oauth/token', {}, body, header, {'class':'AuthManager', 'action':'authApp'})
    }

    function refreshAuthApp(){
        console.log("Refresh App Auth not yet implemented!");
    }

    function login(username, password){
        var body = {
            "grant_type": "password",
            "username": username,
            "password": password
        };
        var authKey = Qt.btoa(root.clientId + ":" + root.clientSecret);
        var header = {
            'Authorization': 'Basic ' + authKey
        }
        RequestManager.postJson('oauth/token', {}, body, header, {'class':'AuthManager', 'action':'login'})
    }

    function refreshLogin(){
        console.log("Refresh Login not yet implemented!");
    }

    /**
        Callback from RequestManager.qml
    */
    Connections {
        ignoreUnknownSignals: true
        target: RequestManager
        onRequestSuccessful: {
            if(userInfo['class'] !== 'AuthManager') return;

            switch(userInfo['action']){
                case 'authApp':
                authAppSuccessful(response, userInfo);
                break;
                case 'refreshAuthApp':
                refreshAuthAppSuccessful(response, userInfo);
                break;
                case 'login':
                loginSuccessful(response, userInfo);
                break;
                case 'refreshLogin':
                refreshLoginSuccessful(response, userInfo);
                break;
                default:
            }
        }
        onRequestFailed: {
            if(userInfo['class'] !== 'AuthManager') return;

            switch(userInfo['action']){
                case 'authApp':
                authAppFailed(error, userInfo);
                break;
                case 'refreshAuthApp':
                refreshAuthAppFailed(error, userInfo);
                break;
                case 'login':
                loginFailed(error, userInfo);
                break;
                case 'refreshLogin':
                refreshLoginFailed(error, userInfo);
                break;
                default:
            }
        }
    }

    function authAppSuccessful(response, userInfo){
        root.appBearerToken = response['access_token'];
        root.appRefreshToken = response['refresh_token'];
        aMAuthAppSuccessful();
    }

    function authAppFailed(error, userInfo){
        aMAuthAppFailed(error);
    }

    function refreshAuthAppSuccessful(response, userInfo){
        root.appBearerToken = response['access_token'];
        root.appRefreshToken = response['refresh_token'];
        aMAuthAppSuccessful();
    }

    function refreshAuthAppFailed(error, userInfo){
        aMAuthAppFailed(error);
    }

    function loginSuccessful(response, userInfo){
        root.userBearerToken = response['access_token'];
        root.userRefreshToken = response['refresh_token'];
        aMLoginUserSuccessful();
        UserDetails.successlogin = "true"
        console.log(UserDetails.successlogin)
    }

    function loginFailed(error, userInfo){
        aMLoginUserFailed(error);
    }

    function refreshLoginSuccessful(response, userInfo){
        root.userBearerToken = response['access_token'];
        root.userRefreshToken = response['refresh_token'];
        aMLoginUserSuccessful();
    }

    function refreshLoginFailed(error, userInfo){
        aMLoginUserFailed(error);
    }

}
