#include "pjmcgradientpalette.h"
#include <QtQml>
#include <QPainter>
#include <QImage>

PJMCGradientPalette::PJMCGradientPalette(QObject *parent) : QObject(parent)
{
    gradient_ = QLinearGradient(0, 0, 1, 1);
    canvas_ = NULL;
}

bool PJMCGradientPalette::width() const {
    return width_;
}

void PJMCGradientPalette::setWidth(int width){
    width_ = width;
    if (canvas_){
        delete canvas_;
        canvas_ = NULL;
    }
    canvas_ = new QImage(width, 1, QImage::Format_ARGB32);
    gradient_.setFinalStop(width_, 1);
    paintOnCanvas();
    emit widthChanged();
}

QImage* PJMCGradientPalette::canvas(){
    return canvas_;
}
QLinearGradient PJMCGradientPalette::gradient(){
    return gradient_;
}

void PJMCGradientPalette::setColorAt(qreal index, const QColor &color){
    gradient_.setColorAt(index, color);
    paintOnCanvas();
    emit colorChanged();
}

void PJMCGradientPalette::paintOnCanvas(){
    QPainter painter(canvas_);
    painter.setBrush(gradient_);
    painter.setPen(Qt::NoPen);
    painter.fillRect(canvas_->rect(), gradient_);
}

QColor PJMCGradientPalette::getColorAt(qreal index){
    index -= 1;
    if (index > width_)
        return Qt::color0;
    return canvas_->pixel(index, 0);
}


void registerPJMCGradientPalette() {
    qmlRegisterType<PJMCGradientPalette>("PJMCHeatMap", 0, 2, "PJMCGradientPalette");
}

Q_COREAPP_STARTUP_FUNCTION(registerPJMCGradientPalette)

