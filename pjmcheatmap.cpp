#include "pjmcheatmap.h"
#include <QImage>
#include <QColor>
#include <QPainter>
#include <QRadialGradient>
#include <QDebug>
#include <QtQml>

PJMCHeatMap::PJMCHeatMap(QObject *parent) : QObject(parent)
{
    max_ = 1;
    alphaCanvas_ = NULL;
    mainCanvas_ = NULL;
}

PJMCHeatMap::~PJMCHeatMap()
{
    delete alphaCanvas_;
    alphaCanvas_ = NULL;

    delete mainCanvas_;
    mainCanvas_ = NULL;
}

int PJMCHeatMap::opacity(){
    return opacity_;
}

void PJMCHeatMap::setOpacity(int opacity) {
    opacity_ = opacity;
}

int PJMCHeatMap::radius(){
    return radius_;
}

void PJMCHeatMap::setRadius(int radius){
    radius_ = radius;
}

QImage* PJMCHeatMap::mainCanvas(){
    return mainCanvas_;
}

void PJMCHeatMap::createMainCanvas(int width, int height){
    if (mainCanvas_){
        delete mainCanvas_;
        mainCanvas_ = NULL;
    }
    mainCanvas_ = new QImage(width, height, QImage::Format_ARGB32);

    if (alphaCanvas_){
        delete alphaCanvas_;
        alphaCanvas_ = NULL;
    }
    alphaCanvas_ = new QImage(mainCanvas_->size(), QImage::Format_ARGB32);
    alphaCanvas_->fill(QColor(0,0,0,0));

    width_ = width;
    height_ = height;
    data_.resize(width_ * height_);
    data_.fill(0);
}

PJMCGradientPalette* PJMCHeatMap::palette(){
    return palette_;
}

void PJMCHeatMap::setPalette(PJMCGradientPalette *palette)
{
    Q_ASSERT(palette);

    if (palette)
        palette_ = palette;
}

int PJMCHeatMap::increase(int x, int y, int delta)
{
    int index = (y - 1) * width_ + (x - 1);
    data_[index] += delta;
    return data_[index];
}

void PJMCHeatMap::addPoint(int x, int y)
{
    if (x <= 0 || y <= 0 || x > width_ || y > height_)
        return;

    int count = increase(x, y);

    if (max_ < count) {
        max_ = count;
        redraw();
        return;
    }

    drawAlpha(x, y, count);
}

void PJMCHeatMap::redraw()
{
    QColor color(0, 0, 0, 0);
    alphaCanvas_->fill(color);
    mainCanvas_->fill(color);

    int size = data_.size();
    for (int i = 0; i < size; ++i) {
        if (0 == data_[i])
            continue;
        drawAlpha(i % width_ + 1, i / width_ + 1, data_[i], false);
    }
    colorize();
}

int PJMCHeatMap::getCount(int x, int y)
{
    if (x < 0 || y < 0)
        return 0;
    return data_[(y - 1) * width_ + (x - 1)];
}

void PJMCHeatMap::drawAlpha(int x, int y, int count, bool colorize_now)
{
    int alpha = int(qreal(count * 1.0 / max_)*255);
    QRadialGradient gradient(x, y, radius_);
    gradient.setColorAt(0, QColor(0, 0, 0, alpha));
    gradient.setColorAt(1, QColor(0, 0, 0, 0));

    QPainter painter(alphaCanvas_);
    painter.setPen(Qt::NoPen);
    painter.setBrush(gradient);
    painter.drawEllipse(QPoint(x, y), radius_, radius_);

    if (colorize_now)
        colorize(x, y);
}

void PJMCHeatMap::colorize()
{
    colorize(0, 0, width_, height_);
}

void PJMCHeatMap::colorize(int x, int y)
{
    int left = x - radius_;
    int top = y - radius_;
    int right = x + radius_;
    int bottom = y + radius_;
    QColor color;

    if (left < 0)
        left = 0;

    if (top < 0)
        top = 0;

    if (right > width_)
        right = width_;

    if (bottom > height_)
        bottom = height_;

    colorize(left, top, right, bottom);
}

void PJMCHeatMap::colorize(int left, int top, int right, int bottom)
{
    int alpha = 0;
    int finalAlpha = 0;
    QColor color;
    for (int i = left; i < right; ++i) {
        for (int j = top; j < bottom; ++j) {
            alpha = qAlpha(alphaCanvas_->pixel(i, j));
            if (!alpha)
                continue;
            finalAlpha = (alpha < opacity_ ? alpha : opacity_);
            color = palette_->getColorAt(alpha);
            mainCanvas_->setPixel(i, j, qRgba(color.red(),
                                              color.green(),
                                              color.blue(),
                                              finalAlpha));
        }
    }
}

void registerPJMCHeatMap() {
    qmlRegisterType<PJMCHeatMap>("PJMCHeatMap", 0, 2, "PJMCHeatMap");
}

Q_COREAPP_STARTUP_FUNCTION(registerPJMCHeatMap)

