#include "netreq.h"

NetReq::NetReq(QObject *parent) : QObject(parent)
{
//we crewawted the instance of networkaccess manager and said that we are going to connect to its signal finished.
    // whenever finished is emitted,it will call requestFinished
    m_networkManager = new QNetworkAccessManager();
    connect(m_networkManager, SIGNAL(finished(QNetworkReply*)), this, SLOT(requestFinished(QNetworkReply*)));
}



void NetReq::downloadImage(QString imageURL, QString filename){
    QUrl url(imageURL);
    QNetworkRequest request;
    request.setUrl(url);
    QNetworkReply * reply = m_networkManager->get(request); //here we created the request and stored the filename so we sould know what to call the file late
    reply->setProperty("filename",filename);
}

QString NetReq::localFilePath(QString filename){
    return QUrl::fromLocalFile(filename).toString();
}

void NetReq::requestFinished(QNetworkReply*reply){ //this is the implementation of the slot
    if (reply->error() == QNetworkReply::NoError) {
        //if successful

        //save image -> here you are converting the data you received to an image and saving it to the device
        QByteArray response_data = reply->readAll();
        QImage image = QImage::fromData(response_data);
        QString filename = reply->property("filename").toString();
        image.save(filename); //it's up tyou whawt filename you want to you can get it from the url or like a random number or whatever.
        QString path = QUrl::fromLocalFile(filename).toString();
        //here
        // you just need the filename you used to save the data. and then it will get the path for you.
        emit imageDownloaded(path);
    } else {
        //if unsuccessful
        // if you want to emit, failure then it's fine as well.
    }
}
