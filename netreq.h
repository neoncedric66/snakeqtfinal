#ifndef NETREQ_H
#define NETREQ_H

#include <QObject>
#include <QtNetwork>
#include <QNetworkConfiguration>
#include <QImage>

class NetReq : public QObject
{
    Q_OBJECT
public:
    explicit NetReq(QObject *parent = nullptr);
    Q_INVOKABLE void downloadImage(QString imageURL, QString filename); //add q_invokable to functions if you want it accessible in qml
    Q_INVOKABLE QString localFilePath(QString filename);
//    NetReq(QObject *parent = 0);

private:
    QNetworkAccessManager *m_networkManager; //this is the networkaccess manager that we will use

signals: //signals your class emits

    void imageDownloaded(QString path); //path where you saved it. the path? like the qm file?

public slots: //this is where you declare the slot requestFinished
    void requestFinished(QNetworkReply* reply);
//    void ignoreSSLErrors(QNetworkReply* reply,QList errors);
};

//NetReq::NetReq(QObject *parent) :
//    QNetworkAccessManager(parent)
//{
//    QObject::connect(this,SIGNAL(sslErrors(QNetworkReply*,QList)),
//                     this,SLOT(ignoreSSLErrors(QNetworkReply*,QList)));
//}

//void NetReq::ignoreSSLErrors(QNetworkReply* reply,QList errors)
//{
//   reply->ignoreSslErrors(errors);
//}

//END

//class SSLSafeNetworkFactory : public QObject, public QQmlNetworkAccessManagerFactory
//{
//    Q_OBJECT

//public:
//    virtual QNetworkAccessManager *create(QObject *parent);
//};

//QNetworkAccessManager* SSLSafeNetworkFactory::create(QObject *parent)
//{
//    NetReq* manager = new NetReq(parent);
//    return manager;
//}

#endif // NETREQ_H
