import QtQuick 2.0
import QtQuick.Controls 1.2
//import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.2

StackView {
    id: stack

    signal navigationStackUpdated(int depth)

    onCurrentItemChanged: {
        navigationStackUpdated(stack.depth)
        if (currentItem){
            if (typeof currentItem.viewWillAppear !== "undefined"){
                currentItem.viewWillAppear()
            }
        }
    }
    delegate: StackViewDelegate {
        function transitionFinished(properties)
        {
            properties.exitItem.x = 0
            properties.exitItem.opacity = 1

            if (typeof properties.exitItem.viewDidDisappear !== "undefined"){
                properties.exitItem.viewDidDisappear()
            }

            if (typeof properties.enterItem.viewDidAppear !== "undefined"){
                properties.enterItem.viewDidAppear()
            }
        }

        pushTransition: StackViewTransition {
            ParallelAnimation {
                ScriptAction {
                    script: enterItem.opacity = 0
                }
                PropertyAnimation {
                    target: enterItem
                    property: "x"
                    from: enterItem.width
                    to: 0
                }
                PropertyAnimation {
                    target: enterItem
                    property: "opacity"
                    from: 0
                    to: 1
                }
            }
            ParallelAnimation {
                PropertyAnimation {
                    target: exitItem
                    property: "x"
                    from: 0
                    to: -exitItem.width
                }
            }
        }
    }
}

