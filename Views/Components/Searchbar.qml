import QtQuick 2.7
import QtQuick.Controls 2.0
import QtGraphicalEffects 1.0

FocusScope {
    //button padding
    property real buttonTopPadding              : 1
    property real buttonBottomPadding           : 1
    property real buttonLeftPadding             : 7
    property real buttonRightPadding            : 1
    property real buttonIconTopPadding          : 4
    property real buttonIconBottomPadding       : 4
    property real buttonIconLeftPadding         : 5
    property real buttonIconRightPadding        : 5

    property real fontPixelRatioToHeight        : 0.4
    property real buttonWidthHeightPercentage   : 2
    property var border                         : rect_search.border
    property alias buttonBackgroundColor        : rect_btnbg.color
    property alias buttonHoverColor             : rect_btnhover.color
    property alias backgroundColor              : rect_search.color
    property alias bottomShadowColor            : rect_bottomShadow.color
    property alias textColor                    : textInput_search.color
    property alias text                         : textInput_search.text
    property alias placeHolderText              : text_placeholder.text
    property alias buttonIcon                   : img_icon.source
    property alias validator                    : textInput_search.validator

    property string fontFamily                  : "Century Gothic"

    signal search(var text)
    signal searchTextChanged(var text)

    id: rootScope

    Rectangle {
        id: rect_search
        color: "#00000000"
        anchors.fill: parent
        border.color: "#802d2d2d"
        border.width: 1

        Rectangle {
            id: rect_bottomShadow
            color: "#302d2d2d"
            height: 1
            anchors.left: parent.left
            anchors.leftMargin: 0
            anchors.right: parent.right
            anchors.rightMargin: 0
            anchors.bottom: parent.bottom
            anchors.bottomMargin: -1
        }

        Button {
            id: btn_search
            width: height * buttonWidthHeightPercentage
            anchors {
                bottom: parent.bottom
                bottomMargin: buttonBottomPadding
                top: parent.top
                topMargin: buttonTopPadding
                right: parent.right
                rightMargin: buttonRightPadding
            }

            background: Rectangle {
                id: rect_btnbg
                color: "#536176"
                anchors.fill: parent
                Rectangle {
                    id: rect_btnhover
                    color: "#30000000"
                    anchors.fill: parent
                    visible: btn_search.pressed
                }
            }

            contentItem: Item {
                id: item_content
                anchors.fill: parent
                Image {
                    id: img_icon
                    source: "qrc:/Images/search.png"
                    fillMode: Image.PreserveAspectFit
                    anchors {
                        bottom: parent.bottom
                        bottomMargin: buttonIconBottomPadding
                        top: parent.top
                        topMargin: buttonIconTopPadding
                        right: parent.right
                        rightMargin: buttonIconRightPadding
                        left: parent.left
                        leftMargin: buttonIconLeftPadding
                    }
                }
                DropShadow {
                    color: "#302d2d2d"
                    spread: 0.3
                    radius: 1
                    samples: 3
                    verticalOffset: 1
                    horizontalOffset: 1
                    anchors.fill: img_icon
                    transparentBorder: true
                    source: img_icon
                    smooth: true
                    opacity: !btn_search.pressed ? 1 : 0.2
                }
            }
            onClicked: {
                search(rootScope.text)
            }
        }

        TextInput {
            id: textInput_search
            color: "#802d2d2d"
            text: ""
            passwordCharacter: "*"
            echoMode: TextInput.Normal
            font.pixelSize: parent.height * fontPixelRatioToHeight
            font.family: root.fontFamily
            verticalAlignment: TextInput.AlignVCenter
            clip: true
            anchors {
                top: parent.top
                topMargin: 0
                right: btn_search.left
                rightMargin: buttonLeftPadding
                bottom: parent.bottom
                bottomMargin: 0
                left: parent.left
                leftMargin: 8   //we make it fixed. this is so just the text does not reach the border
            }
            validator: RegExpValidator {
                regExp: /^[\w. ]*$/             /** is equal to: /^[a-zA-Z0-9_. ]*$/ **/
            }
            onActiveFocusChanged: {
                if (activeFocus === true){
                    text_placeholder.visible = false
                }else {
                    if (text.length <= 0){
                        text_placeholder.visible = true
                    }
                }
            }
            onTextChanged: {
                searchTextChanged(rootScope.text)
            }

            /** PLACEHOLDER **/
            Text {
                id: text_placeholder
                text: "Search"
                color: textInput_search.color
                anchors.fill: parent
                font.pixelSize:  textInput_search.font.pixelSize
                font.family: root.fontFamily
                verticalAlignment: textInput_search.verticalAlignment
                opacity: 0.7
                elide: Text.ElideRight
            }
        }
    }

}
