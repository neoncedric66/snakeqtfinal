import QtQuick 2.0
import QtQuick.Controls 2.0

Button {
    id: btn_clear
    background: Rectangle{
        anchors.fill: parent
        color: "#00000000"
    }
    contentItem: Rectangle {
        anchors.fill: parent
        color: "#00000000"
    }
}
