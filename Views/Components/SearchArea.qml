import QtQuick 2.0
import QtQuick.Controls 2.0

Item {

    //Determines how large the icon is based on the parent's height
    property real iconContainerWidthPercentage          : 0.96

    //searchbar padding
    property real searchbarTopPadding                   : root.height * 0.28
    property real searchbarBottomPadding                : root.height * 0.28
    property real searchbarRightPadding                 : root.width * 0.019
    property real searchbarLeftPadding                  : root.width * 0

    //icon padding
    property real iconTopPadding                        : rect_icon.height * 0.15
    property real iconBottomPadding                     : rect_icon.height * 0.2
    property real iconLeftPadding                       : rect_icon.width * 0
    property real iconRightPadding                      : rect_icon.width * 0

    property alias backgroundColor                      : rect_bg.color
    property alias iconSource                           : img_icon.source
    property alias searchbar                            : searchbar

    property string fontFamily                          : "Century Gothic"

    id: root

    Rectangle {
        id: rect_bg
        color: "#f0ffff"
        anchors.fill: parent
    }

    Rectangle {
        id: rect_icon
        width: height * iconContainerWidthPercentage
        color: "#00000000"
        anchors.left: parent.left
        anchors.leftMargin: 0
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 0
        anchors.top: parent.top
        anchors.topMargin: 0
        Image {
            id: img_icon
            fillMode: Image.PreserveAspectFit
            source: "qrc:/Images/search-category.png"
            anchors.fill: parent
            anchors.right: parent.right
            anchors.rightMargin: iconRightPadding
            anchors.left: parent.left
            anchors.leftMargin: iconLeftPadding
            anchors.bottom: parent.bottom
            anchors.bottomMargin: iconBottomPadding
            anchors.top: parent.top
            anchors.topMargin: iconTopPadding
        }
    }

    Searchbar {
        id: searchbar
        anchors {
            left: rect_icon.right
            leftMargin: searchbarLeftPadding
            right: parent.right
            rightMargin: searchbarRightPadding
            top: parent.top
            topMargin: searchbarTopPadding
            bottom: parent.bottom
            bottomMargin: searchbarBottomPadding
        }
        placeHolderText: "Search"
        fontFamily: root.fontFamily
        buttonIcon: IMG_LOC + "search.png"
    }
}
