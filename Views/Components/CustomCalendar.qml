import QtQuick 2.2
import QtQuick.Controls 1.2
import QtQuick.Controls.Styles 1.1
import QtQuick.Controls.Private 1.0
import QtQuick 2.7

Calendar {

    frameVisible: false
    weekNumbersVisible: false
    focus: true
    style: CalendarStyle {
        dayDelegate: Item {
            readonly property color sameMonthDateTextColor: "#444"
            readonly property color selectedDateColor: "black"  /*Qt.platform.os === "osx" ? "#3778d0" : systemPalette.highlight*/
            readonly property color selectedDateTextColor: "white"
            readonly property color differentMonthDateTextColor: "#bbb"
            readonly property color invalidDatecolor: "#dddddd"

            Rectangle {
                anchors.fill: parent
                border.color: "transparent"
                color: styleData.date !== undefined && styleData.selected ? selectedDateColor : "transparent"
                anchors.margins: styleData.selected ? -1 : 0
            }


            Label {
                id: dayDelegateText
                text: styleData.date.getDate()
                anchors.centerIn: parent
                color: {
                    var color = invalidDatecolor;
                    if (styleData.valid) {
                        // Date is within the valid range.
                        color = styleData.visibleMonth ? sameMonthDateTextColor : differentMonthDateTextColor;
                        if (styleData.selected) {
                            color = selectedDateTextColor;
                        }
                    }
                    color;
                }
            }
        }
    }

}
