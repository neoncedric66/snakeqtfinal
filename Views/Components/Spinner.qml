import QtQuick 2.4

Item {
    id:root
    anchors.fill                    : parent
    property alias duration         : anim_rotate.duration
    property alias running          : anim_rotate.running
    property alias source           : img_spinner.source
    property string state1          : "HIDDEN"
    property string state2          : "SHOWN"

    Image {
        id: img_spinner
        source: "../../../Images/spinner.png"
        anchors.fill: parent
        fillMode: Image.PreserveAspectFit
        RotationAnimator {
            id:anim_rotate
            target: img_spinner
            from: 0
            to: 360
            loops: Animation.Infinite
            running: true
            duration: duration
        }
        Component.onCompleted: {
            root.state = state1
        }
    }

    states: [
        State {
            name: state1
            PropertyChanges { target: root; opacity: 0 }
        },
        State {
            name: state2
            PropertyChanges { target: root; opacity: 1 }
        }
    ]

    transitions: [
        Transition {
            from: state2
            to: state1

            NumberAnimation {
                target: root
                property: "opacity"
                duration: 600
                easing.type: Easing.InOutCirc
            }
        },
        Transition {
            from: state1
            to: state2
            SequentialAnimation {
                NumberAnimation {
                    target: root
                    property: "opacity"
                    duration: 600
                    easing.type: Easing.InOutCirc
                }
                PauseAnimation {
                    duration: 100
                }

            }
            onRunningChanged: {
                if ((state == state2) && (!running)){
                    root.spin(true)
                }
            }
        }
    ]

//# CONTROLS
    function show () {
        if (root.state !== state2){
            spin(false)
            root.state = state2
        }
    }

    function hide () {
        if (root.state !== state1) {
            spin(false)
            root.state = state1
        }
    }

    function spin (start) {
        if (start === true){
            anim_rotate.start()
        }else {
            anim_rotate.stop()
        }
    }
}
