import QtQuick 2.0
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.3
import QtQuick.Controls.Styles 1.4

Item {

    signal selectOption(string data)

   id: options
    ColumnLayout {
        anchors.fill: parent
        spacing: 10
        ExclusiveGroup {
            id: exclusive
        }
        RadioButton {
            id: option1
            text: "Take photo of snake carcass"
            style: RadioButtonStyle {
                label: Text {
                    id: text1
                    font.pixelSize: 10
                    text: option1.text
                }
            }
            checked: false
            exclusiveGroup: exclusive
            height: parent.height * 0.3
            onCheckedChanged: {
                if (option1.checked === true) {
                    selectOption("Take photo of snake carcass")
                }
            }
        }
        RadioButton {
            id: option2
            style: RadioButtonStyle {
                label: Text {
                    id: text2
                    font.pixelSize: 10
                    text: "Choose snake from gallery"
                }
            }
            checked: false
            exclusiveGroup: exclusive
            height: parent.height * 0.3
            onCheckedChanged: {
                if (option2.checked === true) {
                    selectOption("Choose snake from gallery")
                }
            }
        }
        RadioButton {
            id: option3
            style: RadioButtonStyle {
                label: Text {
                    id: text3
                    font.pixelSize: 10
                    text: "Write description of the snake"
                }
            }
            checked: false
            exclusiveGroup: exclusive
            height: parent.height * 0.3
            onCheckedChanged: {
                if (option3.checked === true) {
                    selectOption("Write description of the snake")
                }
            }
        }
    }
}
