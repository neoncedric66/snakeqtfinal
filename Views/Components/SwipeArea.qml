import QtQuick 2.0

MouseArea {
    property real previousX                 : 0
    property real deltaX                    : 0
    property real previousY                 : 0
    property real deltaY                    : 0
    property bool debug                     : false

    signal swipeRight (real deltaX)
    signal swipeLeft (real deltaX)
    signal swipeUp (real deltaY)
    signal swipeDown (real deltaY)

    signal swipeUpLeft (real deltaX, real deltaY)
    signal swipeUpRight (real deltaX, real deltaY)
    signal swipeDownLeft (real deltaX, real deltaY)
    signal swipeDownRight (real deltaX, real deltaY)

    signal swipeNone()

    id: area_swipe

    Rectangle {
        color: "pink"
        anchors.fill: parent
        visible: debug
    }

    onPressed: {
        previousX = mouseX
        previousY = mouseY
        deltaX = 0
        deltaY = 0
    }

    drag.threshold: 30
    drag.minimumX: -300
    drag.maximumX: 0
    drag.minimumY: 0
    drag.maximumY: 0
    drag.axis: Drag.XAxis
    onMouseXChanged: {
        deltaX = mouseX - previousX
        previousX = mouseX
    }

    onMouseYChanged: {
        deltaY = mouseY - previousY
        previousY = mouseY
    }

    onReleased: {
        //send signals here
        if (drag.axis === Drag.XAndYAxis){
            if (deltaX === 0 && deltaY === 0){
                swipeNone()
            } else if (deltaX > 0 && deltaY < 0) {
                swipeUpRight(deltaX, deltaY)
            } else if (deltaX > 0 && deltaY > 0) {
                swipeDownRight(deltaX, deltaY)
            } else if (deltaX < 0 && deltaY < 0) {
                swipeUpLeft(deltaX, deltaY)
            } else if (deltaX < 0 && deltaY > 0) {
                swipeDownLeft(deltaX, deltaY)
            } else if (deltaX > 0 && deltaY === 0) {
                swipeRight(deltaX)
            } else if (deltaX < 0 && deltaY === 0) {
                swipeLeft(deltaX)
            } else if (deltaX === 0 && deltaY < 0) {    //qt Y increases downward
                swipeUp(deltaY)
            } else if (deltaX === 0 && deltaY > 0) {
                swipeDown(deltaY)
            }
        }else if (drag.axis === Drag.YAxis) {
            if (deltaY < 0) {
                swipeUp(deltaY)
            } else if (deltaY > 0) {
                swipeDown(deltaY)
            } else {
                swipeNone()
            }
        }else if (drag.axis === Drag.XAxis) {
            if (deltaX > 0) {
                swipeRight(deltaX)
            }else if (deltaX < 0) {
                swipeLeft(deltaX)
            }else {
                swipeNone()
            }
        }
    }
}
