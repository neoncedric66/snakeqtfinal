import QtQuick 2.0
import QtQml.Models 2.2
import QtQuick.Controls 1.4
import QtQuick.Controls 2.0
import QtGraphicalEffects 1.0

Rectangle {
    property var buttonWidth                : 15
    property var buttonHeightPercentage     : 0.18
    property var buttonMinHeight            : 70
    property alias button                   : btn_toggle    //override this at your own risk but you have to implement onClicked if you assign a new Button to change state
    property string stateHidden             : "HIDDEN"
    property string stateShown              : "SHOWN"
    property string stateIdle               : "IDLE"
    property real animationDuration         : 600

    signal rectSlideWillShow()
    signal rectSlideWillHide()
    signal rectSlideStateChanged()

    id: rect_slide
    color: "#ffffff"

    MouseArea {
        anchors.fill: parent //so it swallows touch in the area
    }

//    Image {
//        id: taskbarslider
//        source: IMG_LOC + "slider.png"
//        height: 100
//        width: 32
//        fillMode: Image.Stretch
//        y: 175
//        anchors.left: parent.right
//        visible: true
//    }


    Rectangle { //the button width is extra width. it overlaps with over views. (x pos is bigger than rect_slide)
        id: rect_toggleButton
        color: "#00000000"
        width: buttonWidth
        anchors.top : parent.top
        anchors.topMargin: 0
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 0
        anchors.left : parent.right
        anchors.leftMargin: 0

        Button {
            id: btn_toggle
            width: parent.width
            height: Math.max(rect_slide.height * buttonHeightPercentage, buttonMinHeight)  //set a min size
            anchors.verticalCenter: parent.verticalCenter
            anchors.verticalCenterOffset: 0
            visible: parent.width > 0
            contentItem: Rectangle {
                anchors.fill: parent
                color: "gray"
            }
            background: Rectangle {
                color: "#00000000"
                anchors.fill: parent
            }
            onClicked: {
                if (rect_slide.state == stateHidden){
                    goToState(stateShown)
                }else {
                    goToState(stateHidden)
                }
            }
        }
    }

    states: [
        State {
            name: stateHidden
            PropertyChanges { target: rect_slide; x: - rect_slide.width }
        },
        State {
            name: stateShown
            PropertyChanges { target: rect_slide; x: 0 }
        },
        State {
            name: stateIdle
            PropertyChanges { target: rect_slide; x: rect_slide.x }
        }
    ]

    transitions: [
        Transition {
            to: stateHidden

            NumberAnimation {
                id: anim_hide
                target: rect_slide
                properties: "x"
                duration: animationDuration
                to: - rect_slide.width
                easing.type: Easing.OutCubic
            }
        },
        Transition {
            to: stateShown

            NumberAnimation {
                id: anim_show
                target: rect_slide
                properties: "x"
                duration: animationDuration
                to: 0
                easing.type: Easing.OutCubic
            }
        }
    ]

    Component.onCompleted: {
        rect_slide.state = stateHidden
    }

    onWidthChanged: {
        //keep the button at the edge
        if (rect_slide.state == stateHidden){
            rect_slide.x = -rect_slide.width
        }
    }
    onStateChanged: {
        rectSlideStateChanged()
    }

    //FUNCTION
    function goToState(newState) {
        if (rect_slide.state === newState) {
            rect_slide.state = stateIdle        //to start the animation again
        }
        if(newState === stateHidden){
            rectSlideWillHide()
            taskbarslider.opacity = 1
        }else if (newState === stateShown){
            rectSlideWillShow()
            taskbarslider.opacity = 0
        }

        rect_slide.state = newState
    }
}
