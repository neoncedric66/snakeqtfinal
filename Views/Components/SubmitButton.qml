import QtQuick 2.7
import QtQuick.Controls 2.0

Button {
    property string bgColorDefault              : "#3679a9"
    property string bgColorHighlighted          : "#294d69"
    property string textColor                   : "white"
    property real fontPixelRatioToWidth         : 0.08
    property real radius                        : 0
    property string fontFamily                  : "Century Gothic"
    property var vAlign                         : Text.AlignVCenter
    property var hAlign                         : Text.AlignHCenter
    property var lPadding                       : 0
    property var tPadding                       : 0

    id: root
    width: 100
    text: qsTr("Submit")
    highlighted: false
    font.pixelSize: width * fontPixelRatioToWidth
    font.family: root.fontFamily
    background: Rectangle {
        color: root.down ? bgColorHighlighted : bgColorDefault
        radius: root.radius
    }
    contentItem: Text {
        color: textColor
        text: root.text
        font: root.font
        verticalAlignment: vAlign
        horizontalAlignment: hAlign
        leftPadding: lPadding
        topPadding: tPadding
        wrapMode: Text.Wrap
        elide: Text.ElideRight
    }
}
