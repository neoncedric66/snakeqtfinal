import QtQuick 2.0
import QtQuick.Controls 2.0

Item {
    id: root

    property alias buttonWidth              : btn_icon.width
    property alias buttonHeight             : btn_icon.height
    property alias buttonIconSource         : img_buttonIcon.source
    property real borderWidth               : btn_icon.width * 0.03
    property real fontPixelSize             : btn_icon.width * 0.11
    property string buttonTheme             : "#000"
    property string buttonTitle             : "Default"

    Button {
        id: btn_icon

        background: Rectangle {
            id: rect_buttonContent
            color: "#00000000"
            height: btn_icon.height
            width: btn_icon.width
            border.color: buttonTheme
            border.width: borderWidth

            Image {
                id: img_buttonIcon
                height: rect_buttonContent.height * 0.50
                width: rect_buttonContent.width * 0.50
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.top: parent.top
                anchors.topMargin: parent.width * 0.15
            }

            Text {
                id: txt_iconTitle
                text: buttonTitle
                color: buttonTheme
                anchors.bottom: parent.bottom
                anchors.bottomMargin: parent.height * 0.08
                anchors.horizontalCenter: parent.horizontalCenter
                font.pixelSize: fontPixelSize
                font.bold: true
            }
        }
    }


}
