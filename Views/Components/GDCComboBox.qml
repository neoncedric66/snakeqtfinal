import QtQuick 2.7
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import QtQuick.Controls 2.0
//import QtQml.Models 2.1

Item {
    property alias role                                     : column_display.role
    property alias model                                    : tableView.model

    property alias comboBoxText                             : combo_text.text
    property real comboBoxFontPixelRatio                    : 0.6
    property real comboBoxButtonWidthPercentage             : 0.13
    property string comboBoxButtonColor                     : "#202f46"
    property string comboBoxButtonHighlightedColor          : "#111b2b"
    property string comboBoxTextColor                       : "black"
    property string comboBoxColor                           : "white"

    property string selectionBoxRowColor                    : "white"
    property string selectionBoxRowHighlightedColor         : "#302196f3"
    property string selectionBoxTextColor                   : "black"
    property real selectionBoxMaxHeight                     : 700
    property real selectionBoxFontPixelRatio                : 0.6
    property real selectionBoxHeight                        : tableView.model.count * root.selectionBoxRowHeight + root.borderWidth*2
    property real selectionBoxRowHeight                     : rect_comboBox.height

    property string borderColor                             : "#294d69"
    property string fontFamily                              : "Century Gothic"
    property real borderWidth                               : 1

    property string stateHidden                             : "HIDDEN"
    property string stateShown                              : "SHOWN"
    property var comboBoxState                              : rect_selectionArea.state

    //internal
    property real appliedSelectionBoxHeight                 : Math.min(root.selectionBoxHeight, root.selectionBoxMaxHeight)

    signal comboBoxModelChanged()
    signal selectedData(int index, var value)
    signal selectionBoxStatusChanged()

    id: root

    function toggle(){
        btn_combo.checked = !btn_combo.checked
    }

    Rectangle {
        id: rect_comboBox
        anchors.fill: parent
        color: "#000000"
        border.color: root.borderColor
        border.width: root.borderWidth

        Button {
            id: combo_text
            anchors {
                left: parent.left
                leftMargin: root.borderWidth
                top: parent.top
                topMargin: root.borderWidth
                bottom: parent.bottom
                bottomMargin: root.borderWidth
            }
            width: parent.width * (1 - comboBoxButtonWidthPercentage)
            text: "Select"
            font.pixelSize: root.height * root.comboBoxFontPixelRatio
            font.family: root.fontFamily
            background: Rectangle {
                anchors.fill: parent
                color: root.comboBoxColor
            }
            contentItem: Text {
                color: root.comboBoxTextColor
                text: combo_text.text
                font: combo_text.font
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignLeft
                elide: Text.ElideRight
            }
            onClicked: {
                root.toggle()      //propagate the click to the actual combo box button
            }
        }

        Button {
            id: btn_combo
            checkable: true
            anchors {
                left: combo_text.right
                leftMargin: 0
                top: parent.top
                topMargin: root.borderWidth
                bottom: parent.bottom
                bottomMargin: root.borderWidth
                right: parent.right
                rightMargin: root.borderWidth
            }
            background: Rectangle {
                id: rect_comboBg
                color: btn_combo.down ? root.comboBoxButtonHighlightedColor : root.comboBoxButtonColor
            }
            contentItem: Image {
                id: icon
                source: IMG_LOC + "icon-combo.png"
                anchors.fill: parent
                fillMode: Image.PreserveAspectFit
            }
            onCheckedChanged: {
                selectionBoxStatusChanged()
            }
        }
    }

    Rectangle {
        id: rect_selectionArea
        anchors {
            left: root.left
            leftMargin: 0
            right: root.right
            rightMargin: 0
            top: root.bottom
            topMargin: -root.borderWidth
        }
        state: btn_combo.checked ? root.stateShown : root.stateHidden
        color: root.selectionBoxRowColor
        border.color: root.borderColor
        border.width: root.borderWidth

        states: [
            State {
                name: root.stateHidden
                PropertyChanges { target: rect_selectionArea; height: 0 }
            },
            State {
                name: root.stateShown
                PropertyChanges { target: rect_selectionArea; height: appliedSelectionBoxHeight}
            }
        ]

        transitions: [
            Transition {
                to: root.stateHidden
                NumberAnimation {
                    id: anim_hide
                    target: rect_selectionArea
                    properties: "height"
                    duration: 100
                    to: 0
                    easing.type: Easing.InCubic
                }
            },
            Transition {
                to: root.stateShown
                NumberAnimation {
                    id: anim_show
                    target: rect_selectionArea
                    properties: "height"
                    duration: 100
                    to: root.appliedSelectionBoxHeight
                    easing.type: Easing.InCubic
                }
            }
        ]

        TableView {
            id: tableView
            anchors {
                left: parent.left
                leftMargin: root.borderWidth
                right: parent.right
                rightMargin: root.borderWidth
                top: parent.top
                topMargin: root.borderWidth
                bottom: parent.bottom
                bottomMargin: root.borderWidth
            }

            highlightOnFocus: true
            headerVisible: false
            visible: rect_selectionArea.state == root.stateShown ? true : false

            TableViewColumn {
                id: column_display
                role: "name"
            }
            onModelChanged: {
                comboBoxModelChanged()
            }
            itemDelegate: Item {
                id: item_row
                anchors.fill: parent
                Button {
                    id: item_button
                    anchors.fill: parent
                    hoverEnabled: true
                    font.pixelSize: root.height * root.selectionBoxFontPixelRatio
                    font.family: root.fontFamily
                    background: Rectangle {
                        anchors.fill: parent
                        color: item_button.pressed || item_button.hovered ? root.selectionBoxRowHighlightedColor : root.selectionBoxRowColor
                        Rectangle {
                            id: separatorTop
                            anchors{
                                left:parent.left
                                leftMargin: 0
                                right: parent.right
                                rightMargin: 0
                                top: parent.top
                                topMargin: 0
                            }
                            height: styleData.row === 0 ? 0 : root.borderWidth/2
                            color: root.borderColor
                        }
                        Rectangle {
                            id: separator
                            anchors{
                                left:parent.left
                                leftMargin: 0
                                right: parent.right
                                rightMargin: 0
                                top: parent.bottom
                                topMargin: -height
                            }
                            height: styleData.row >= root.model.count-1 ? 0 : root.borderWidth/2
                            color: root.borderColor
                        }
                    }
                    contentItem: Text {
                        id: content
                        padding:7
                        anchors.fill: parent
                        verticalAlignment: Text.AlignVCenter
                        horizontalAlignment: Text.AlignLeft
                        font: item_button.font
                        color: root.selectionBoxTextColor
                        elide: Text.ElideRight
                        text: styleData.value
                    }
                    onClicked: {
                        btn_combo.checked = false
                        tableView.selection.clear()
                        combo_text.text = styleData.value
                        selectedData(styleData.row, styleData.value)
                    }
                }
            }
            rowDelegate: Item {
                height: selectionBoxRowHeight
            }
        }
    }

    onHeightChanged: {
        if (rect_selectionArea.height > 0) {
            rect_selectionArea.height = root.appliedSelectionBoxHeight
        }
    }
}
