import QtQuick 2.0
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import QtQuick 2.7

BusyIndicator {
    id: spinner
    running: true

    //Adjust this to change spinner's position
    anchors.horizontalCenter: parent.horizontalCenter
    y: parent.height * 0.7

//    anchors.centerIn: parent
    width: 44
    height: 44
    style: BusyIndicatorStyle {
        indicator: Image {
            visible: control.running
            source: IMG_LOC + "spinner.png"
            RotationAnimator on rotation {
                running: control.running
                loops: Animation.Infinite
                duration: 1000
                from: 0 ; to: 360
            }
        }
    }
}
