import QtQuick 2.7

Rectangle {
    property alias textInput    : atextInput

    id: rect_barcodeText
    color: "white"
    border.width: 1
    border.color: "black"

    TextInput {
        id: atextInput
        text: "12345678"
        color: "black"
        echoMode: TextInput.Normal
        font.pixelSize: parent.height * 0.5
        font.family: "Century Gothic"
        verticalAlignment: TextInput.AlignVCenter
        horizontalAlignment: TextInput.AlignLeft
        padding:5
        clip: true
        anchors.fill: parent
        validator: RegExpValidator {
            regExp: /^[\w. ]*$/             /** is equal to: /^[a-zA-Z0-9_. ]*$/ **/
        }
    }
}
