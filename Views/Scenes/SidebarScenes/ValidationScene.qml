import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.0
import QtLocation 5.6
import QtPositioning 5.6
import "../../../Models"

Item {
    id: validationmain

    signal homeClicked()
    Rectangle {

        id: validationbackground
        color: ColorScheme.primary
        anchors.fill: parent

    }


    Button {
        id: homeicon
        height: 40
        width: 40

        //Button is 10 pix from the right side and from the top (x value is from bottom left corner so it is dependent on the image's width)
        y: 10
        x: parent.width - width - 10
        background: Image {
            source: IMG_LOC + "homeIcon.png"
            fillMode: Image.Stretch
        }

        //To return back to the home screen
        onClicked: {
            homeClicked()
        }
    }

}
