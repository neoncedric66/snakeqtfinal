import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.0
import QtLocation 5.6
import QtPositioning 5.6
import QtQuick.Controls.Styles 1.4
import QtQuick.Window 2.2
import "../../Components"
import "../../Scenes"
import "../../../Models"

Item {
    id: treatmentmain

    property int size: 8
    property var deleteid: ""

    signal closeList()
    signal finishedLoading()
    signal refreshScene()


    Rectangle {

        id: bitebackground
        color: ColorScheme.primary
        anchors.fill: parent

        Component.onCompleted: {
            makeList()
        }

        //        Button {
        //            anchors.top: parent.top
        //            anchors.left: parent.left
        //            anchors.right: parent.right
        //            height: parent.height * 0.1
        //            background: Rectangle {
        //                color:"red"
        //                anchors.fill: parent
        //            }

        //            onClicked: {
        //                console.log("Req sent")
        //                BiteManager.getMyReports()
        //            }
        //        }

        Rectangle {
            id: borderfill
            anchors.horizontalCenter: listbackground.horizontalCenter
            anchors.verticalCenter: listbackground.verticalCenter
            height: listbackground.height + 6
            width: listbackground.width + 6
            border.color: ColorScheme.accent
            border.width: 3
        }

        Button {
            id: refresh
            anchors.top: parent.top
            anchors.topMargin: 10
            anchors.horizontalCenter: parent.horizontalCenter
            height: parent.height * 0.07
            width: parent.width * 0.3
            background: Rectangle {
                anchors.fill: parent
                color: ColorScheme.accent
                Text {
                    anchors.fill: parent
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    text: "Refresh"
                    color: "black"
                    font.pixelSize: 15
                }
            }
            onClicked: {
                makeList()
            }
        }

        Rectangle {
            id: listbackground
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: parent.top
            anchors.topMargin: 70
            z: borderfill.z + 5
            height: parent.height * 0.8
            width: parent.width * 0.8
            color: ColorScheme.secondary

            ListView {
                id: biterecordsview
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.top: parent.top
                anchors.bottom: parent.bottom
                width: parent.width
                model: biterecords
                clip: true
                delegate: Rectangle {
                    width: listbackground.width
                    height: listbackground.height * 0.5
                    color: ColorScheme.secondary

                    Rectangle {
                        id: bordertop
                        color: ColorScheme.accent
                        anchors.left: parent.left
                        anchors.right: parent.right
                        anchors.top: parent.top
                        height: 3
                        z: grid1.z + 10
                    }

                    Rectangle {
                        id: grid1
                        anchors.top: parent.top
                        anchors.left: parent.left
                        color: ColorScheme.secondary
                        height: parent.height * (1/6)
                        width: parent.width * (1/4)
                        Text {
                            text: "ID:"
                            font.pixelSize: 10
                            anchors.fill: parent
                            horizontalAlignment: Text.AlignHCenter
                            verticalAlignment: Text.AlignVCenter
                        }
                    }

                    Rectangle {
                        id: grid2
                        anchors.top: grid1.bottom
                        anchors.left: parent.left
                        color: ColorScheme.secondary
                        height: parent.height * (1/6)
                        width: parent.width * (1/4)
                        Text {
                            text: "Time Of Bite:"
                            font.pixelSize: 10
                            anchors.fill: parent
                            horizontalAlignment: Text.AlignHCenter
                            verticalAlignment: Text.AlignVCenter
                        }
                    }

                    Rectangle {
                        id: grid3
                        anchors.top: grid2.bottom
                        anchors.left: parent.left
                        color: ColorScheme.secondary
                        height: parent.height * (1/6)
                        width: parent.width * (1/4)
                        Text {
                            text: "Longitude:"
                            font.pixelSize: 10
                            anchors.fill: parent
                            horizontalAlignment: Text.AlignHCenter
                            verticalAlignment: Text.AlignVCenter
                        }
                    }

                    Rectangle {
                        id: grid4
                        anchors.top: grid3.bottom
                        anchors.left: parent.left
                        color: ColorScheme.secondary
                        height: parent.height * (1/6)
                        width: parent.width * (1/4)
                        Text {
                            text: "Latitude:"
                            font.pixelSize: 10
                            anchors.fill: parent
                            horizontalAlignment: Text.AlignHCenter
                            verticalAlignment: Text.AlignVCenter
                        }
                    }

                    Rectangle {
                        id: grid5
                        anchors.top: grid4.bottom
                        anchors.left: parent.left
                        color: ColorScheme.secondary
                        height: parent.height * (1/6)
                        width: parent.width * (1/4)
                        Text {
                            text: "Reporter Username:"
                            font.pixelSize: 10
                            wrapMode: Text.WordWrap
                            anchors.fill: parent
                            horizontalAlignment: Text.AlignHCenter
                            verticalAlignment: Text.AlignVCenter
                        }
                    }

                    Rectangle {
                        id: grid6
                        anchors.top: grid5.bottom
                        anchors.left: parent.left
                        color: ColorScheme.secondary
                        height: parent.height * (1/6)
                        width: parent.width * (1/4)
                        Text {
                            text: "Snake Number:"
                            font.pixelSize: 10
                            wrapMode: Text.WordWrap
                            anchors.fill: parent
                            horizontalAlignment: Text.AlignHCenter
                            verticalAlignment: Text.AlignVCenter
                        }
                    }

                    Rectangle {
                        id: grid7
                        anchors.top: parent.top
                        anchors.left: grid1.right
                        color: ColorScheme.secondary
                        height: parent.height * (1/6)
                        width: parent.width * (1/4)
                        Text {
                            text: id
                            font.pixelSize: 10
                            anchors.fill: parent
                            horizontalAlignment: Text.AlignHCenter
                            verticalAlignment: Text.AlignVCenter
                        }
                    }

                    Rectangle {
                        id: grid8
                        anchors.top: grid7.bottom
                        anchors.left: grid2.right
                        color: ColorScheme.secondary
                        height: parent.height * (1/6)
                        width: parent.width * (1/4)
                        Text {
                            text: timeOfBite.toString().replace(/(..)/g, '$1:').slice(0,-1)
                            font.pixelSize: 10
                            anchors.fill: parent
                            horizontalAlignment: Text.AlignHCenter
                            verticalAlignment: Text.AlignVCenter
                        }
                    }

                    Rectangle {
                        id: grid9
                        anchors.top: grid8.bottom
                        anchors.left: grid3.right
                        color: ColorScheme.secondary
                        height: parent.height * (1/6)
                        width: parent.width * (1/4)
                        Text {
                            text: longitude
                            font.pixelSize: 10
                            anchors.fill: parent
                            horizontalAlignment: Text.AlignHCenter
                            verticalAlignment: Text.AlignVCenter
                        }
                    }

                    Rectangle {
                        id: grid10
                        anchors.top: grid9.bottom
                        anchors.left: grid4.right
                        color: ColorScheme.secondary
                        height: parent.height * (1/6)
                        width: parent.width * (1/4)
                        Text {
                            text: latitude
                            font.pixelSize: 10
                            anchors.fill: parent
                            horizontalAlignment: Text.AlignHCenter
                            verticalAlignment: Text.AlignVCenter
                        }
                    }

                    Rectangle {
                        id: grid11
                        anchors.top: grid10.bottom
                        anchors.left: grid5.right
                        color: ColorScheme.secondary
                        height: parent.height * (1/6)
                        width: parent.width * (1/4)
                        Text {
                            text: username
                            font.pixelSize: 10
                            anchors.fill: parent
                            horizontalAlignment: Text.AlignHCenter
                            verticalAlignment: Text.AlignVCenter
                        }
                    }

                    Rectangle {
                        id: grid12
                        anchors.top: grid11.bottom
                        anchors.left: grid6.right
                        color: ColorScheme.secondary
                        height: parent.height * (1/6)
                        width: parent.width * (1/4)
                        Text {
                            text: snake
                            font.pixelSize: 10
                            anchors.fill: parent
                            horizontalAlignment: Text.AlignHCenter
                            verticalAlignment: Text.AlignVCenter
                        }
                    }

                    Rectangle {
                        id: border
                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.top: parent.top
                        anchors.bottom: parent.bottom
                        width: 3
                        color: ColorScheme.accent
                    }

                    Rectangle {
                        id: grid13
                        anchors.top: parent.top
                        anchors.left: grid7.right
                        color: ColorScheme.secondary
                        height: parent.height * (1/6)
                        width: parent.width * (1/4)
                        Text {
                            text: "Environment:"
                            font.pixelSize: 10
                            anchors.fill: parent
                            horizontalAlignment: Text.AlignHCenter
                            verticalAlignment: Text.AlignVCenter
                        }
                    }

                    Rectangle {
                        id: grid14
                        anchors.top: grid13.bottom
                        anchors.left: grid8.right
                        color: ColorScheme.secondary
                        height: parent.height * (1/6)
                        width: parent.width * (1/4)
                        Text {
                            text: "Circumstance:"
                            font.pixelSize: 10
                            anchors.fill: parent
                            horizontalAlignment: Text.AlignHCenter
                            verticalAlignment: Text.AlignVCenter
                        }
                    }

                    Rectangle {
                        id: grid15
                        anchors.top: grid14.bottom
                        anchors.left: grid9.right
                        color: ColorScheme.secondary
                        height: parent.height * (1/6)
                        width: parent.width * (1/4)
                        Text {
                            text: "Site Of Bite:"
                            font.pixelSize: 10
                            anchors.fill: parent
                            horizontalAlignment: Text.AlignHCenter
                            verticalAlignment: Text.AlignVCenter
                        }
                    }

//                    Rectangle {
//                        id: grid16
//                        anchors.top: grid15.bottom
//                        anchors.left: grid10.right
//                        color: ColorScheme.secondary
//                        height: parent.height * (1/6)
//                        width: parent.width * (1/4)
//                        Text {
//                            text: "Reporter ID:"
//                            font.pixelSize: 10
//                            anchors.fill: parent
//                            horizontalAlignment: Text.AlignHCenter
//                            verticalAlignment: Text.AlignVCenter
//                        }
//                    }

//                    Rectangle {
//                        id: grid17
//                        anchors.top: grid16.bottom
//                        anchors.left: grid11.right
//                        color: ColorScheme.secondary
//                        height: parent.height * (1/6)
//                        width: parent.width * (1/4)
//                        Text {
//                            text: "Reporter Username:"
//                            font.pixelSize: 10
//                            wrapMode: Text.WordWrap
//                            anchors.fill: parent
//                            horizontalAlignment: Text.AlignHCenter
//                            verticalAlignment: Text.AlignVCenter
//                        }
//                    }

                    //                    Rectangle {
                    //                        id: grid18
                    //                        anchors.top: grid17.bottom
                    //                        anchors.left: grid12.right
                    //                        color: ColorScheme.secondary
                    //                        height: parent.height * (1/6)
                    //                        width: parent.width * (1/4)
                    //                        Text {
                    //                            text: "Photo ID:"
                    //                            font.pixelSize: 10
                    //                            anchors.fill: parent
                    //                            horizontalAlignment: Text.AlignHCenter
                    //                            verticalAlignment: Text.AlignVCenter
                    //                        }
                    //                    }

                    Rectangle {
                        id: grid19
                        anchors.top: parent.top
                        anchors.left: grid13.right
                        color: ColorScheme.secondary
                        height: parent.height * (1/6)
                        width: parent.width * (1/4)
                        Text {
                            text: environment
                            font.pixelSize: 10
                            anchors.fill: parent
                            horizontalAlignment: Text.AlignHCenter
                            verticalAlignment: Text.AlignVCenter
                            font.capitalization: Font.Capitalize
                        }
                    }

                    Rectangle {
                        id: grid20
                        anchors.top: grid19.bottom
                        anchors.left: grid14.right
                        color: ColorScheme.secondary
                        height: parent.height * (1/6)
                        width: parent.width * (1/4)
                        Text {
                            text: circumstance
                            font.pixelSize: 10
                            anchors.fill: parent
                            horizontalAlignment: Text.AlignHCenter
                            verticalAlignment: Text.AlignVCenter
                            font.capitalization: Font.Capitalize
                        }
                    }

                    Rectangle {
                        id: grid21
                        anchors.top: grid20.bottom
                        anchors.left: grid15.right
                        color: ColorScheme.secondary
                        height: parent.height * (1/6)
                        width: parent.width * (1/4)
                        Text {
                            text: siteOfBite
                            font.pixelSize: 10
                            anchors.fill: parent
                            horizontalAlignment: Text.AlignHCenter
                            verticalAlignment: Text.AlignVCenter
                            font.capitalization: Font.Capitalize
                        }
                    }

//                    Rectangle {
//                        id: grid22
//                        anchors.top: grid21.bottom
//                        anchors.left: grid16.right
//                        color: ColorScheme.secondary
//                        height: parent.height * (1/6)
//                        width: parent.width * (1/4)
//                        Text {
//                            text: reporter
//                            font.pixelSize: 10
//                            anchors.fill: parent
//                            horizontalAlignment: Text.AlignHCenter
//                            verticalAlignment: Text.AlignVCenter
//                        }
//                    }

//                    Rectangle {
//                        id: grid23
//                        anchors.top: grid22.bottom
//                        anchors.left: grid17.right
//                        color: ColorScheme.secondary
//                        height: parent.height * (1/6)
//                        width: parent.width * (1/4)
//                        Text {
//                            text: username
//                            font.pixelSize: 10
//                            anchors.fill: parent
//                            horizontalAlignment: Text.AlignHCenter
//                            verticalAlignment: Text.AlignVCenter
//                        }
//                    }

                    //                    Rectangle {
                    //                        id: grid24
                    //                        anchors.top: grid23.bottom
                    //                        anchors.left: grid18.right
                    //                        color: ColorScheme.secondary
                    //                        height: parent.height * (1/6)
                    //                        width: parent.width * (1/4)
                    //                        Text {
                    //                            text: photo
                    //                            font.pixelSize: 10
                    //                            anchors.fill: parent
                    //                            horizontalAlignment: Text.AlignHCenter
                    //                            verticalAlignment: Text.AlignVCenter
                    //                        }
                    //                    }

                    Button {
                        id: doctordelete
                        anchors.bottom: parent.bottom
                        anchors.bottomMargin: 15
                        anchors.left: grid13.horizontalCenter
                        anchors.right: grid19.horizontalCenter
                        height: parent.height * 0.15
                        visible: true
                        background: Rectangle {
                            id: deletebutton
                            anchors.fill: parent
                            color: "#E74F4E"
                            Text {
                                anchors.fill: parent
                                text: "Delete"
                                horizontalAlignment: Text.AlignHCenter
                                verticalAlignment: Text.AlignVCenter
                                font.pixelSize: 17
                            }
                        }
                        onClicked: {
                            confirmdiscard.visible = true
                            deleteid = id
                        }

                    }

                    Rectangle {
                        id: cutoff
                        anchors.bottom: parent.bottom
                        anchors.left: parent.left
                        anchors.right: parent.right
                        height: 3
                        color: ColorScheme.accent
                    }

                }
            }

        }
    }

    //    function biteData (filter, attribute) {
    //        var http = new XMLHttpRequest()
    //        var url = "http://localhost:1337/bite/find"
    //        var params = filter + "&populate=false"
    //        http.open("POST",url,true)
    //        http.setRequestHeader("Content-Type","application/x-www-form-urlencoded")
    //        http.setRequestHeader("Authorization", "Bearer " + AuthManager.userBearerToken)
    //        http.onreadystatechange = function() {
    //            console.log(http.readyState)
    //            console.log(http.status)
    //            console.log(http.responseText)
    //            if (http.readyState === 4) {
    //                if (http.status === 200) {
    //                    console.log("connects")
    //                    console.log(http.responseText)
    //                    var parsedata = JSON.parse(http.responseText)
    //                    console.log(parsedata[0].attribute)
    //                }
    //            } else {
    //                console.log("Erorr, cannot connect")
    //            }
    //        }

    //        http.send(params)
    //    }



    ListModel {
        id: biterecords
    }

    function makeList() {

        var http = new XMLHttpRequest()
        var url = RequestManager.baseUrl + "bite/find"
        var params = "&populate=false"
        http.open("POST",url,true)
        http.setRequestHeader("Content-Type","application/x-www-form-urlencoded")
        http.setRequestHeader("Authorization", "Bearer " + AuthManager.userBearerToken)
        http.onreadystatechange = function() {
            if (http.readyState === 4) {
                if (http.status === 200) {
                    //                    console.log("connects")
                    var fulldata = JSON.parse(http.responseText)
                    biterecords.clear()
                    for (var i=0; i < fulldata.length; i ++) {
                        biterecords.append({

                                               "createdAt": fulldata[i].createdAt,
                                               "updatedAt": fulldata[i].updatedAt,
                                               "id": fulldata[i].id,
                                               "longitude": fulldata[i].longitude,
                                               "latitude": fulldata[i].latitude,
                                               "description": fulldata[i].description,
                                               "timeOfBite": fulldata[i].timeOfBite,
                                               "environment": fulldata[i].environment,
                                               "circumstance": fulldata[i].circumstance,
                                               "siteOfBite": fulldata[i].siteOfBite,
                                               "reporter": fulldata[i].reporter,
                                               "snake": fulldata[i].snake,
                                               "photo": fulldata[i].photo,
                                               "username": "",

                                           })

                        //Find the user's username
                        userAttribute("username", fulldata[i].reporter, i)
                    }

                    loading.visible = false
                    console.log(UserDetails.usertype)


                }
            }
        }
        http.send(params)

    }

    function userAttribute(attribute, id, index) {
        var http = new XMLHttpRequest()
        var newurl = RequestManager.baseUrl + "user/find"
        var newparams = "id=" + id
        http.open("POST",newurl,true)
        http.setRequestHeader("Content-Type","application/x-www-form-urlencoded")
        http.setRequestHeader("Authorization", "Bearer " + AuthManager.userBearerToken)
        console.log("reaches here")
        http.onreadystatechange = function() {
            //            console.log(http.readyState)
            //            console.log(http.responseText)
            if (http.readyState === 4) {
                if (http.status === 200) {
                    console.log("CONNECTS")
                    var data = http.responseText
                    var testdata = JSON.parse(data)
                    var userattribute = testdata[0][attribute]
                    biterecords.set(index, {

                                           "username": userattribute

                                       })
                }
            }
        }

        http.send(newparams)
    }

    Rectangle {
        id: loading
        anchors.fill: bitebackground
        color: ColorScheme.primary
        z: bitebackground.z + 100
        visible: true

        AnimatedImage {
            id: loadingspinner
            source: LOAD_LOC + "logo.gif"
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter
            playing: true
            width: bitebackground.width * 0.3 //the pic is default 32x32 size so just make this in multiples of 32
            height: width
            fillMode: Image.PreserveAspectFit
        }

    }

    Rectangle {
        id: confirmdiscard
        anchors {
            verticalCenter: bitebackground.verticalCenter
            horizontalCenter: bitebackground.horizontalCenter
        }
        width: bitebackground.width * 0.9
        height: bitebackground.width * 0.8
        visible: false
        color: "#d4d4d4"
        Rectangle {
            anchors.top: parent.top
            anchors.left: parent.left
            anchors.right: parent.right
            height: parent.height * 0.5
            color: parent.color
            Text {
                text: "Are you sure you want to delete the record?"
                font.pixelSize: 15
                wrapMode: Text.WordWrap
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
            }
        }
        Button {
            id: yes
            anchors.left: parent.left
            anchors.bottom: parent.bottom
            anchors.leftMargin: 10
            anchors.bottomMargin: 10
            width: parent.width * 0.4
            height: parent.width * 0.4
            background: Rectangle {
                Text {
                    text: "Yes"
                    font.pixelSize: 13
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.verticalCenter: parent.verticalCenter
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                    color: "black"
                }
                anchors.fill: parent
                color: "#ccff90"
                border.width: 4
                radius: 4
                border.color: "#64dd17"
            }
            onClicked: {
                var http = new XMLHttpRequest()
                var newurl = RequestManager.baseUrl + "bite/delete"
                var params = "id=" + deleteid
                http.open("POST",newurl,true)
                http.setRequestHeader("Content-Type","application/x-www-form-urlencoded")
                http.setRequestHeader("Authorization", "Bearer " + AuthManager.userBearerToken)

                http.onreadystatechange = function() {
                    if (http.readyState === 4) {
                        if (http.status === 200) {
                            console.log("DELETE")
                            makeList()
//                                        refreshScene()
                        }
                    }
                }
                http.send(params)

                confirmdiscard.visible = false

            }
        }
        Button {
            id: no
            anchors.right: parent.right
            anchors.bottom: parent.bottom
            anchors.rightMargin: 10
            anchors.bottomMargin: 10
            width: parent.width * 0.4
            height: parent.width * 0.4
            background: Rectangle {
                Text {
                    text: "No"
                    font.pixelSize: 13
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.verticalCenter: parent.verticalCenter
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                    color: "white"
                }
                anchors.fill: parent
                color: "#e96868"
                border.width: 4
                radius: 4
                border.color: "#e74f4e"
            }
            onClicked: {
                confirmdiscard.visible = false
            }
        }
    }

//    Rectangle {
//        id: unauthorized
//        anchors.verticalCenter: biterecordsview.verticalCenter
//        anchors.horizontalCenter: biterecordsview.horizontalCenter
//        width: biterecordsview.width * 0.6
//        height: biterecordsview.height * 0.6
//        color: ColorScheme.accent
//        visible: false
//        Rectangle {
//            anchors.top: parent.top
//            anchors.left: parent.left
//            anchors.right: parent.right
//            height: parent.height * 0.3
//            Text {
//                anchors.fill: parent
//                horizontalAlignment: Text.AlignHCenter
//                verticalAlignment: Text.AlignVCenter
//                text: "Unauthorized"
//                color: "black"
//                font.pixelSize: 20
//                font.bold: trie
//            }
//        }
//        Rectangle {
//            anchors.bottom: parent.bottom
//            anchors.left: parent.left
//            anchors.right: parent.right
//            height: parent.height * 0.7
//            Text {
//                anchors.fill: parent
//                horizontalAlignment: Text.AlignHCenter
//                verticalAlignment: Text.AlignVCenter
//                text: "Error, account is unauthorized to delete bite entries."
//                wrapMode: Text.WordWrap
//                color: "black"
//                font.pixelSize: 15
//                font.bold: true
//            }
//        }
//        Button {
//            anchors.right: parent.right
//            anchors.top: parent.top
//            anchors.rightMargin: 5
//            anchors.topMargin: 5
//            height: parent.height * 0.1
//            width: height
//            background: Image {
//                anchors.fill: parent
//                source: IMG_LOC + "cross.png"
//                fillMode: Image.PreserveAspectFit
//            }
//            onClicked: {
//                unauthorized.visible = false
//            }
//        }
//    }

    Button {
        id: homeicon
        height: 40
        width: 40

        //Button is 10 pix from the right side and from the top (x value is from bottom left corner so it is dependent on the image's width)
        y: 10
        x: parent.width - width - 10
        background: Image {
            source: IMG_LOC + "homeIcon.png"
            fillMode: Image.Stretch
        }

        //To return back to the home screen
        onClicked: {
            closeList()
        }
    }
}
