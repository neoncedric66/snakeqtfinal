import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.0
import QtLocation 5.6
import QtPositioning 5.6
import "../../../Models"

Item {
    id: treatmentmain

    signal homeClicked()
    signal goBack()

    Rectangle {

        id: firstaid
        color: ColorScheme.primary
        anchors.fill: parent

        Text {
            id: title
            anchors.top: parent.top
            anchors.topMargin: parent.height * 0.05
            anchors.horizontalCenter: parent.horizontalCenter
            text: "Treatment"
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignTop
            font.pixelSize: 18
        }

        Rectangle {
            id: firstaidbackground
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: parent.top
            anchors.topMargin: 60
            height: parent.height * 0.8
            width: parent.width * 0.9
            color: ColorScheme.secondary
            Flickable {
                id: maintext
                anchors {
                    left: parent.left
                    right: parent.right
                    leftMargin: parent.width * 0.05
                    rightMargin: parent.width * 0.05
                    top: parent.top
                    topMargin: parent.height * 0.05
                }
                height: parent.height * 0.9
                clip: true
                interactive: true
                contentHeight: contentItem.childrenRect.height + 5
                contentWidth: parent.width * 0.9
                Text {
                    id: mainchunk
                    anchors {
                        top: parent.top
                        left: parent.left
                        right: parent.right
                    }
                    text: "If you suspect a snake bite:\n
• Immediately move away from the area where the bite occurred. If the snake is still attached use a stick or tool to make it let go. Sea snake victims need to be moved to dry land to avoid drowning.
• Remove anything tight from around the bitten part of the body (e.g.: rings, anklets, bracelets) as these can cause harm if swelling occurs.
• Reassure the victim. Many snake bites are caused by non-venomous snakes. And even after most venomous snake bites the risk of death is not immediate.
• Immobilize the person completely. Splint the limb to keep it still. Use a makeshift stretcher to carry the person to a place where transport is available to take them to a health facility.
• Never use a tight arterial tourniquet.
• The Australian Pressure Immobilization Bandage (PIB) Method is only recommended for bites by neurotoxic snakes that do not cause local swelling.
• Applying pressure at the bite site with a pressure pad may be suitable in some cases.
• Avoid traditional first aid methods, herbal medicines and other unproven or unsafe forms of first aid.
• Transport the person to a health facility as soon as possible
• Paracetamol may be given for local pain (which can be severe).
• Vomiting may occur, so place the person on their left side in the recovery position.
• Closely monitor airway and breathing and be ready to resuscitate if necessary.\n
For more information, visit:"
                    font.pixelSize: 13
                    verticalAlignment: Text.AlignTop
                    horizontalAlignment: Text.AlignLeft
                    wrapMode: Text.WordWrap
                }
                Text {
                    anchors {
                        top: mainchunk.bottom
                        left: parent.left
                        right: parent.right
                    }
                    font.pixelSize: 13
                    verticalAlignment: Text.AlignTop
                    horizontalAlignment: Text.AlignLeft
                    text: "<a href='http://www.who.int/snakebites/treatment/en/'>WHO Snakebite Treatment</a>"
                    onLinkActivated: Qt.openUrlExternally(link)
                }
            }
        }

    }



    Button {
        id: homeicon
        height: 40
        width: 40

        //Button is 10 pix from the right side and from the top (x value is from bottom left corner so it is dependent on the image's width)
        y: 10
        x: parent.width - width - 10
        background: Image {
            source: IMG_LOC + "homeIcon.png"
            fillMode: Image.Stretch
        }

        //To return back to the home screen
        onClicked: {
            homeClicked()
        }
    }

    Button {
        id: back
        height: 40
        width: 40

        //Button is 10 pix from the right side and from the top (x value is from bottom left corner so it is dependent on the image's width)
        y: 10
        anchors.left: parent.left
        anchors.leftMargin: 10
        background: Image {
            source: IMG_LOC + "backArrow.png"
            fillMode: Image.Stretch
        }

        //To return back to the home screen
        onClicked: {
            goBack()
        }
    }

}
