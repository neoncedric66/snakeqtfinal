import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.0
import QtLocation 5.6
import QtPositioning 5.6
import QtQuick.Controls.Styles 1.4
import QtQuick.Window 2.2
import "../../Components"
import "../../Scenes"
import "../../../Models"

Item {
    id: uploadermain

    signal uploadClose()

    property int phototype: 0
    property int speciesdone: 0
    property int snakepartdone: 0
    property int snakeother: 0
    property int uploaddone: 0
    property int informationdone: 0

    Rectangle {

        id: uploadbackground
        color: ColorScheme.primary
        anchors.fill: parent

        Text {
            anchors.bottom: uploadframe.top
            anchors.bottomMargin: 10
            anchors.top: parent.top
            text: "Upload Photo"
            font.pixelSize: 18
            color: "black"
            anchors.horizontalCenter: parent.horizontalCenter
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }

        Image {
            id: viewimage
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            width: uploadbackground.width * 0.9
            height: uploadbackground.height * 0.65
            visible: false
            source: "file:///" + CameraOutput.photourl
            fillMode: Image.PreserveAspectCrop
            Button {
                anchors.top: parent.top
                anchors.right: parent.right
                anchors.topMargin: parent.height * 0.05
                anchors.rightMargin: parent.width * 0.05
                height: parent.height * 0.1
                width: parent.width * 0.1
                background: Image {
                    anchors.fill: parent
                    source: IMG_LOC + "cancelButton.png"
                    fillMode: Image.PreserveAspectCrop
                }
                onClicked: {
                    viewimage.visible = false
                    uploadframe.visible = true
                    nextpage.visible = true
                    homeicon.visible = true
                }
            }
        }

        Rectangle {
            id: uploadframe
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            width: parent.width * 0.8
            height: parent.height * 0.85
            color: ColorScheme.secondary

            Flickable {
                id: scroller
                anchors {
                    top: parent.top
                    left: parent.left
                    right: parent.right
                }

                ScrollBar.vertical: ScrollBar {
                    parent: scroller.parent
                    anchors.top: parent.top
                    anchors.bottom: parent.bottom
                    anchors.right: parent.right
                    active: true
                    policy: ScrollBar.AlwaysOn
                }

                height: parent.height * 0.95
                clip: true
                interactive: true
                contentHeight: contentItem.childrenRect.height + 5
                contentWidth: parent.width

                Rectangle {
                    id: snaketextfield
                    anchors.top: parent.top
                    anchors.topMargin: 10
                    Text {
                        anchors.fill: parent
                        text: "Snake Species"
                        font.pixelSize: 15
                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.verticalCenter: parent.verticalCenter
                        verticalAlignment: Text.AlignVCenter
                        horizontalAlignment: Text.AlignHCenter
                        wrapMode: Text.WordWrap
                    }
                    anchors.left: parent.left
                    height: uploadframe.height * (1/7)
                    width: parent.width * 0.5
                    color: uploadframe.color
                }

                TextField {
                    id: snake
                    focus: true
                    placeholderText: qsTr("Enter Species")
                    font.pixelSize: 15
                    activeFocusOnPress: true
                    anchors.right: parent.right
                    anchors.top: parent.top
                    anchors.topMargin: 10
                    inputMethodHints: Qt.ImhNoAutoUppercase
                    width: parent.width * 0.4
                    anchors.rightMargin: parent.width * 0.1
                    height: uploadframe.height * (1/7)
                    onEditingFinished: {
                        speciesdone = 1
                    }
                }

                Rectangle {
                    id: parttextfield
                    anchors.top: snaketextfield.bottom
                    anchors.topMargin: 10
                    Text {
                        anchors.fill: parent
                        text: "Part of Snake:"
                        font.pixelSize: 15
                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.verticalCenter: parent.verticalCenter
                        verticalAlignment: Text.AlignVCenter
                        horizontalAlignment: Text.AlignHCenter
                    }
                    anchors.left: parent.left
                    height: uploadframe.height * (1/7)
                    width: parent.width * 0.5
                    color: uploadframe.color
                }

                ComboBox {
                    id: part
                    model: ["General", "Head", "Body", "Tail", "Other"]
                    font.pixelSize: 15
                    currentIndex: -1
                    anchors.right: parent.right
                    anchors.top: snake.bottom
                    anchors.topMargin: 10
                    width: parent.width * 0.4
                    anchors.rightMargin: parent.width * 0.1
                    height: uploadframe.height * (1/7)
                    onCurrentIndexChanged: { 
                        if (part.currentIndex === 4) {
                            partother.height = uploadframe.height * (1/7)
                            partother.visible = true
                        } else {
                            snakepartdone = 1
                            partother.height = 0
                            partother.visible = false
                        }
                    }
                }

                TextField {
                    id: partother
                    focus: true
                    placeholderText: qsTr("Enter Part")
                    font.pixelSize: 15
                    activeFocusOnPress: true
                    anchors.right: parent.right
                    anchors.top: part.bottom
                    anchors.topMargin: 10
                    width: parent.width * 0.4
                    anchors.rightMargin: parent.width * 0.1
                    height: 0
                    visible: false
                    onEditingFinished: {
                        snakeother = 1
                    }
                }

                ComboBox {
                    id: imagetype
                    model: ["Open Gallery", "Take a Photo"]
                    font.pixelSize: 15
                    currentIndex: -1
                    anchors.right: parent.right
                    anchors.top: partother.bottom
                    anchors.topMargin: 10
                    width: parent.width * 0.4
                    anchors.rightMargin: parent.width * 0.1
                    height: uploadframe.height * (1/7)
                    onCurrentIndexChanged: {
                        phototype = imagetype.currentIndex
                        uploader.visible = true
                        if (imagetype.currentIndex === 0) {
                            uploadtext.text = "Open Gallery"
                            uploader.height = uploadframe.height * (1/7)
                            uploader.visible = true
                        } else if (imagetype.currentIndex === 1) {
                            uploadtext.text = "Open Camera"
                            uploader.height = uploadframe.height * (1/7)
                            uploader.visible = true
                        } else {
                            uploadtext.text = ""
                            uploader.height = 0
                            uploader.visible = false
                        }
                    }
                }

                Rectangle {
                    id: imagetextfield
                    y: imagetype.y
                    Text {
                        anchors.fill: parent
                        text: "Upload Image:"
                        font.pixelSize: 15
                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.verticalCenter: parent.verticalCenter
                        verticalAlignment: Text.AlignVCenter
                        horizontalAlignment: Text.AlignHCenter
                    }
                    anchors.left: parent.left
                    height: uploadframe.height * (1/7)
                    width: parent.width * 0.5
                    color: uploadframe.color
                }

                Button {
                    id: uploader
                    anchors.top: imagetype.bottom
                    anchors.right: parent.right
                    height: 0
                    width: parent.width * 0.4
                    anchors.rightMargin: parent.width * 0.1
                    visible: false
                    background: Rectangle {
                        anchors.fill: parent
                        height: uploadframe.height * (1/7)
                        anchors.topMargin: 10
                        width: parent.width * 0.5
                        color: "#e5e5e5"
                        Text {
                            id: uploadtext
                            anchors.fill: parent
                            text: imagetype.currentText
                            font.pixelSize: 14
                            anchors.horizontalCenter: parent.horizontalCenter
                            anchors.verticalCenter: parent.verticalCenter
                            verticalAlignment: Text.AlignVCenter
                            horizontalAlignment: Text.AlignHCenter
                            wrapMode: Text.WordWrap
                        }
                    }
                    onClicked: {
                        uploaddone = 1
                        if (phototype === 0) {
                            //open gallery
                        } else if (phototype === 1) {
                            //open camera
                            console.log("OPEN CAMERA")
                            var cameraScene = SCENE_LOC + "UploadCamera.qml"
                            nav_scene.pushScene(Qt.resolvedUrl(cameraScene),true,{})
                        }
                    }
                }

                Button {
                    id: imagepreview
                    anchors.top: uploader.bottom
                    height: uploadframe.height * CameraOutput.photomultiple
                    anchors.rightMargin: parent.width * 0.1
                    anchors.right: parent.right
                    anchors.topMargin: 10
                    width: uploadframe.width * 0.4
                    visible: CameraOutput.imagevisible

                    background: Rectangle {
                        anchors.fill: parent
                        color: "#e5e5e5"
                        visible: CameraOutput.imagevisible
                        Text {
                            anchors.fill: parent
                            text: "Preview Photo"
                            font.pixelSize: 15
                            visible: CameraOutput.imagevisible
                            anchors.horizontalCenter: parent.horizontalCenter
                            anchors.verticalCenter: parent.verticalCenter
                            verticalAlignment: Text.AlignVCenter
                            horizontalAlignment: Text.AlignHCenter
                            wrapMode: Text.WordWrap
                        }
                    }
                    onClicked: {
                        //stuff when clicked
                        console.log("BITE IMAGE OPEN")
                        viewimage.visible = true
                        uploadframe.visible = false
                        nextpage.visible = false
                        homeicon.visible = false
                    }

                }

            }

        }

        Button {
            id: nextpage
            width:25
            height: 25
            anchors.right: parent.right
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 10
            background: Image {
                source: IMG_LOC + "arrowRightIcon.png"
                fillMode: Image.PreserveAspectCrop
            }
            onClicked: {
                console.log("NEXT")
                if (speciesdone === 1 && snakepartdone === 1 && uploaddone === 1) {
                    informationdone = 1
                } else if (speciesdone === 1 && snakeother === 1 && uploaddone === 1) {
                    informationdone = 1
                } else {
                    //not done
                    invalidinformation.visible = true
                }
                if (informationdone === 1) {
                    var species = snake.getText(0,100)
                    var snakepart = part.currentText.toLowerCase()
                    var photourl = CameraOutput.photourl

                    var body = {
                        "snake": 1, //fix this later
                        "snakePart": snakepart,
                        "image": photourl,
                    }


                    var http = new XMLHttpRequest()
                    var url = RequestManager.baseUrl + "snake/photo/upload"
                    var params =
                    http.open("POST",url,true)
                    http.setRequestHeader("Content-Type","application/json")
                    http.setRequestHeader("Authorization", "Bearer " + AuthManager.userBearerToken)
                    http.onreadystatechange = function() { // Call a function when the state changes.
                        if (http.readyState === XMLHttpRequest.DONE) {
                            console.log(http.responseText)
                            console.log("stuff")
                            CameraOutput.imagevis = false
                            CameraOutput.biteurl =  ""
                            CameraOutput.photomultiple = 0
                        }
                    }
                    http.send(JSON.stringify(body))
                }
            }
        }

        Rectangle {
            id: invalidinformation
            visible: false
            width: uploadframe.width * 0.9
            anchors {
                verticalCenter: uploadframe.verticalCenter
                horizontalCenter: uploadframe.horizontalCenter
            }
            height: width
            color: "#d4d4d4"
            border.width: 0
            border.color: "black"
            Text {
                text: "Not all fields have been<br>entered correctly. Click this message<br>to dismiss it."
                font.pixelSize: 15
                anchors {
                    verticalCenter: parent.verticalCenter
                    horizontalCenter: parent.horizontalCenter
                }
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                color: "black"
            }


            MouseArea {
                anchors.fill: parent
                hoverEnabled: true
                acceptedButtons: Qt.LeftButton | Qt.RightButton
                onEntered: {
                    parent.border.width = 3
                }
                onExited: {
                    parent.border.width = 0
                }
                onClicked: {
                    invalidinformation.visible = false
                }
            }
        }

    }


    Button {
        id: homeicon
        height: 30
        width: 30

        //Button is 10 pix from the right side and from the top (x value is from bottom left corner so it is dependent on the image's width)
        y: 5
        x: parent.width - width - 10
        background: Image {
            source: IMG_LOC + "cross.png"
            fillMode: Image.Stretch
        }

        //To return back to the home screen
        onClicked: {
            uploadClose()
        }
    }
}
