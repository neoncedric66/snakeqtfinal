import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.0
import QtLocation 5.6
import QtPositioning 5.6
import QtQuick.Controls.Styles 1.4
import QtQuick.Window 2.2
import "../../Components"
import "../../Scenes"
import "../../../Models"

Item {
    id: treatmentmain

    property int size: 8
    property var deleteid: ""

    signal closeList()
    signal finishedLoading()
    signal refreshScene()


    Rectangle {

        id: bitebackground
        color: ColorScheme.primary
        anchors.fill: parent

        Component.onCompleted: {
            makeList()
        }

        //        Button {
        //            anchors.top: parent.top
        //            anchors.left: parent.left
        //            anchors.right: parent.right
        //            height: parent.height * 0.1
        //            background: Rectangle {
        //                color:"red"
        //                anchors.fill: parent
        //            }

        //            onClicked: {
        //                console.log("Req sent")
        //                BiteManager.getMyReports()
        //            }
        //        }

        Rectangle {
            id: borderfill
            anchors.horizontalCenter: listbackground.horizontalCenter
            anchors.verticalCenter: listbackground.verticalCenter
            height: listbackground.height + 6
            width: listbackground.width + 6
            border.color: ColorScheme.accent
            border.width: 3
        }

        Rectangle {
            id: title
            color: ColorScheme.primary
            width: parent.width * 0.35
            height: parent.height * 0.07
            anchors.top: parent.top
            anchors.topMargin: 10
            anchors.horizontalCenter: parent.horizontalCenter
            Text {
                id: titletext
                text: ""
                font.pixelSize: 17
                color: "black"
                anchors.fill: parent
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
            }
            Component.onCompleted: {
                if (UserDetails.usertype === "doctor") {
                    titletext.text = "All Bites"
                } else {
                    titletext.text = "Your Bites"
                }
            }
        }

        Button {
            id: refresh
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 15
            anchors.horizontalCenter: parent.horizontalCenter
            height: parent.height * 0.05
            width: parent.width * 0.3
            background: Rectangle {
                anchors.fill: parent
                color: ColorScheme.accent
                Text {
                    anchors.fill: parent
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    text: "Refresh"
                    color: "black"
                    font.pixelSize: 13
                }
            }
            onClicked: {
                loading.visible = true
                biterecords.clear()
                makeList()
            }
        }

        // Edit area for update patient status
        Rectangle {
            id: editfillPatientStatus
            anchors.horizontalCenter: editbackground.horizontalCenter
            anchors.verticalCenter: editbackground.verticalCenter
            height: editPatientStatusBox.height + 6
            width: editPatientStatusBox.width + 6
            visible: false
            border.color: ColorScheme.accent
            border.width: 3
            PatientStatusBox {
                id: editPatientStatusBox
                height: bitebackground.height * 0.6
                width: bitebackground.width * 0.8
                z: 10000000
                visible: false
                color: ColorScheme.secondary
                anchors {
                    horizontalCenter: parent.horizontalCenter
                    verticalCenter: parent.verticalCenter
                }
                onUpdateClicked: {
                    //update
                    console.log("UPDATE STATUS: " + JSON.stringify(statusInfo))
                    BiteManager.updatePatientStatus(statusInfo)

                    //visibility
                    editfillPatientStatus.hideUpdatePatientArea(false)
                }
                onCloseClicked: {
                    editfillPatientStatus.hideUpdatePatientArea(false)
                }
            }

            function showUpdatePatientArea() {
                bitebackground.showDefaultView(false)
                editbackground.visible = false
                editfill.visible = false
                editfillPatientStatus.visible = true
                editPatientStatusBox.visible = true
            }

            function hideUpdatePatientArea() {
                bitebackground.showDefaultView(true)
                editbackground.visible = false
                editfill.visible = false
                editfillPatientStatus.visible = false
                editPatientStatusBox.visible = false
            }

            Connections {
                ignoreUnknownSignals: true
                target: BiteManager
                onBMUpdatePatientStatusSuccessful : {
                    console.log("UPDATE PATIENT STATUS SUCCESS")
                    loading.visible = true
                    biterecords.clear()
                    makeList()
                }
                onBMUpdatePatientStatusFailed: {
                    console.log("UPDATE PATIENT STATUS FAILED")
                }
            }
        }

        function showDefaultView(status) {
            listbackground.visible = status
            biterecordsview.visible = status
            refresh.visible = status
            title.visible = status
            homeicon.visible = status
            borderfill.visible = status
        }

        Rectangle {
            id: editfill
            anchors.horizontalCenter: editbackground.horizontalCenter
            anchors.verticalCenter: editbackground.verticalCenter
            height: editbackground.height + 6
            width: editbackground.width + 6
            visible: false
            border.color: ColorScheme.accent
            border.width: 3
        }

        //Editting panel here
        Rectangle {
            id: editbackground
            height: parent.height * 0.6
            width: parent.width * 0.8
            z: 10000000
            visible: false
            color: ColorScheme.secondary
            anchors {
                horizontalCenter: parent.horizontalCenter
                verticalCenter: parent.verticalCenter
            }

            Button {
                id: proceed
                anchors.bottom: editbackground.bottom
                anchors.bottomMargin: parent.height * 0.05
                height: parent.height * 0.1
                anchors.horizontalCenter: parent.horizontalCenter
                width: parent.width * 0.5
                background: Rectangle {
                    anchors.fill: parent
                    color: "#508030"
                    Text {
                        anchors.fill: parent
                        text: "Proceed"
                        color: "black"
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                        font.pixelSize: 12
                    }
                }
                onClicked: {
                    //proceeding

                    var http = new XMLHttpRequest()


                    //visibility
                    listbackground.visible = true
                    biterecordsview.visible = true
                    refresh.visible = true
                    title.visible = true
                    homeicon.visible = true
                    borderfill.visible = true
                    editbackground.visible = false
                    editfill.visible = false
                }
            }

            Flickable {

                anchors {
                    left: parent.left
                    right: parent.right
                    top: parent.top
                }
                height: parent.height * 0.8
                clip: true
                interactive: true
                contentHeight: contentItem.childrenRect.height + 5
                contentWidth: parent.width

                Rectangle {
                    anchors.fill: parent
                    color: ColorScheme.secondary
                }

                Rectangle {
                    anchors {
                        top: parent.top
                        left: parent.left
                        topMargin: 15
                        leftMargin: parent.width * 0.05
                    }
                    width: parent.width * 0.85
                    color: ColorScheme.secondary
                    Text {
                        anchors.fill: parent
                        horizontalAlignment: Text.AlignLeft
                        verticalAlignment: Text.AlignTop
                        text: "Update Details"
                        color: "black"
                        font.pixelSize: 17
                        font.bold: true
                    }
                }

                Button {
                    id: closer
                    anchors.top: parent.top
                    anchors.right: parent.right
                    anchors.topMargin: 5
                    anchors.rightMargin: 5
                    width: 20
                    height: 20
                    background: Image {
                        anchors.fill: parent
                        source: IMG_LOC + "cancelButton.png"
                        fillMode: Image.PreserveAspectCrop
                        horizontalAlignment: Image.AlignRight
                    }
                    onClicked: {
                        listbackground.visible = true
                        biterecordsview.visible = true
                        refresh.visible = true
                        title.visible = true
                        homeicon.visible = true
                        borderfill.visible = true
                        editbackground.visible = false
                        editfill.visible = false
                    }
                }

                //fields here
                Rectangle {
                    id: venomoustextfield
                    height: editbackground.height * (1/6)
                    width: parent.width * 0.45
                    color: ColorScheme.secondary
                    anchors {
                        left: parent.left
                        top: parent.top
                        topMargin: 40
                    }
                    Text {
                        anchors.fill: parent
                        text: "Venomous:"
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                        color: "black"
                        font.pixelSize: 12
                    }
                }

                ComboBox {
                    id: venomous
                    model: ["Venomous", "Non-Venomous"]
                    font.pixelSize: 12
                    currentIndex: -1
                    anchors {
                        right: parent.right
                        top: parent.top
                        topMargin: 40
                        rightMargin: 5
                    }
                    height: editbackground.height * (1/6)
                    width: parent.width * 0.45
                }

                Rectangle {
                    id: biutetextfield
                    height: editbackground.height * (1/6)
                    width: parent.width * 0.45
                    color: ColorScheme.secondary
                    anchors {
                        left: parent.left
                        top: venomoustextfield.bottom
                        topMargin: 5
                    }
                    Text {
                        anchors.fill: parent
                        text: "Biute:"
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                        color: "black"
                        font.pixelSize: 12
                    }
                }

                ComboBox {
                    id: biute
                    model: ["Envenoming", "Dry Biute"]
                    font.pixelSize: 12
                    currentIndex: -1
                    anchors {
                        right: parent.right
                        top: venomous.bottom
                        topMargin: 5
                        rightMargin: 5
                    }
                    height: editbackground.height * (1/6)
                    width: parent.width * 0.45
                }

                Rectangle {
                    id: envenomationtitle
                    color: ColorScheme.secondary
                    anchors.top: biutetextfield.bottom
                    anchors.left: parent.left
                    anchors.leftMargin: parent.width * 0.05
                    height: editbackground.height * (1/6)
                    width: parent.width * 0.95
                    Text {
                        anchors.fill: parent
                        color: "black"
                        font.pixelSize: 15
                        text: "Signs of Envenomation"
                        horizontalAlignment: Text.AlignLeft
                        verticalAlignment: Text.AlignVCenter
                        font.underline: true
                    }
                }

                Rectangle {
                    id: bruisingtextfield
                    height: editbackground.height * (1/6)
                    width: parent.width * 0.45
                    color: ColorScheme.secondary
                    anchors {
                        left: parent.left
                        top: envenomationtitle.bottom
                        leftMargin: 10
                    }
                    Text {
                        anchors.fill: parent
                        text: "Bruising:"
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                        color: "black"
                        font.pixelSize: 12
                    }
                }

                CheckBox {
                    id: bruising
                    anchors.right: biute.horizontalCenter
                    anchors.verticalCenter: bruisingtextfield.verticalCenter
                    height: editbackground.height * (1/7)
                    width: height
                    checked: false
                }

                Rectangle {
                    id: swellingtextfield
                    height: editbackground.height * (1/6)
                    width: parent.width * 0.45
                    color: ColorScheme.secondary
                    anchors {
                        left: parent.left
                        top: bruisingtextfield.bottom
                        leftMargin: 10
                    }
                    Text {
                        anchors.fill: parent
                        text: "Swelling:"
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                        color: "black"
                        font.pixelSize: 12
                    }
                }

                CheckBox {
                    id: swelling
                    anchors.right: biute.horizontalCenter
                    anchors.verticalCenter: swellingtextfield.verticalCenter
                    height: editbackground.height * (1/7)
                    width: height
                    checked: false
                }

                Rectangle {
                    id: blisteringtextfield
                    height: editbackground.height * (1/6)
                    width: parent.width * 0.45
                    color: ColorScheme.secondary
                    anchors {
                        left: parent.left
                        top: swellingtextfield.bottom
                        leftMargin: 10
                    }
                    Text {
                        anchors.fill: parent
                        text: "Blistering:"
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                        color: "black"
                        font.pixelSize: 12
                    }
                }

                CheckBox {
                    id: blistering
                    anchors.right: biute.horizontalCenter
                    anchors.verticalCenter: blisteringtextfield.verticalCenter
                    height: editbackground.height * (1/7)
                    width: height
                    checked: false
                }

                Rectangle {
                    id: necrosistextfield
                    height: editbackground.height * (1/6)
                    width: parent.width * 0.45
                    color: ColorScheme.secondary
                    anchors {
                        left: parent.left
                        top: blisteringtextfield.bottom
                        leftMargin: 10
                    }
                    Text {
                        anchors.fill: parent
                        text: "Necrosis:"
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                        color: "black"
                        font.pixelSize: 12
                    }
                }

                CheckBox {
                    id: necrosis
                    anchors.right: biute.horizontalCenter
                    anchors.verticalCenter: necrosistextfield.verticalCenter
                    height: editbackground.height * (1/7)
                    width: height
                    checked: false
                }

                Rectangle {
                    id: systemictextfield
                    height: editbackground.height * (1/6)
                    width: parent.width * 0.45
                    color: ColorScheme.secondary
                    anchors {
                        left: parent.left
                        top: necrosistextfield.bottom
                        leftMargin: 10
                    }
                    Text {
                        anchors.fill: parent
                        text: "Systemic Coagulopathy:"
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                        color: "black"
                        font.pixelSize: 12
                    }
                }

                CheckBox {
                    id: systemic
                    anchors.right: biute.horizontalCenter
                    anchors.verticalCenter: systemictextfield.verticalCenter
                    height: editbackground.height * (1/7)
                    width: height
                    checked: false
                }

                Rectangle {
                    id: paralysistextfield
                    height: editbackground.height * (1/6)
                    width: parent.width * 0.45
                    color: ColorScheme.secondary
                    anchors {
                        left: parent.left
                        top: systemictextfield.bottom
                        leftMargin: 10
                    }
                    Text {
                        anchors.fill: parent
                        text: "Paralysis:"
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                        color: "black"
                        font.pixelSize: 12
                    }
                }

                CheckBox {
                    id: paralysis
                    anchors.right: biute.horizontalCenter
                    anchors.verticalCenter: paralysistextfield.verticalCenter
                    height: editbackground.height * (1/7)
                    width: height
                    checked: false
                }
            }
        }

        Rectangle {
            id: listbackground
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: parent.top
            anchors.topMargin: 70
            z: borderfill.z + 5
            height: parent.height * 0.8
            width: parent.width * 0.8
            color: ColorScheme.secondary

            ListView {
                id: biterecordsview
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.top: parent.top
                anchors.bottom: parent.bottom
                width: parent.width
                model: biterecords
                clip: true
                delegate: Rectangle {
                    width: listbackground.width
                    height: listbackground.height * 0.5
                    color: ColorScheme.secondary

                    Rectangle {
                        id: bordertop
                        color: ColorScheme.accent
                        anchors.left: parent.left
                        anchors.right: parent.right
                        anchors.top: parent.top
                        height: 3
                        z: grid1.z + 10
                    }

                    Rectangle {
                        id: grid1
                        anchors.top: parent.top
                        anchors.left: parent.left
                        color: ColorScheme.secondary
                        height: parent.height * (1/6)
                        width: parent.width * (1/4)
                        Text {
                            text: "ID:"
                            font.pixelSize: 10
                            anchors.fill: parent
                            horizontalAlignment: Text.AlignHCenter
                            verticalAlignment: Text.AlignVCenter
                        }
                    }

                    Rectangle {
                        id: grid2
                        anchors.top: grid1.bottom
                        anchors.left: parent.left
                        color: ColorScheme.secondary
                        height: parent.height * (1/6)
                        width: parent.width * (1/4)
                        Text {
                            text: "Time Of Bite:"
                            font.pixelSize: 10
                            anchors.fill: parent
                            horizontalAlignment: Text.AlignHCenter
                            verticalAlignment: Text.AlignVCenter
                        }
                    }

                    Rectangle {
                        id: grid3
                        anchors.top: grid2.bottom
                        anchors.left: parent.left
                        color: ColorScheme.secondary
                        height: parent.height * (1/6)
                        width: parent.width * (1/4)
                        Text {
                            text: "Date of Bite:"
                            font.pixelSize: 10
                            anchors.fill: parent
                            horizontalAlignment: Text.AlignHCenter
                            verticalAlignment: Text.AlignVCenter
                        }
                    }

                    Rectangle {
                        id: grid4
                        anchors.top: grid3.bottom
                        anchors.left: parent.left
                        color: ColorScheme.secondary
                        height: parent.height * (1/6)
                        width: parent.width * (1/4)
                        Text {
                            text: "Longitude:"
                            font.pixelSize: 10
                            anchors.fill: parent
                            horizontalAlignment: Text.AlignHCenter
                            verticalAlignment: Text.AlignVCenter
                        }
                    }

                    Rectangle {
                        id: grid5
                        anchors.top: grid4.bottom
                        anchors.left: parent.left
                        color: ColorScheme.secondary
                        height: parent.height * (1/6)
                        width: parent.width * (1/4)
                        Text {
                            text: "Latitude:"
                            font.pixelSize: 10
                            anchors.fill: parent
                            horizontalAlignment: Text.AlignHCenter
                            verticalAlignment: Text.AlignVCenter
                        }
                    }

                    Rectangle {
                        id: grid6
                        anchors.top: grid5.bottom
                        anchors.left: parent.left
                        color: ColorScheme.secondary
                        height: parent.height * (1/6)
                        width: parent.width * (1/4)
                        Text {
                            text: "Reporter Username:"
                            font.pixelSize: 10
                            wrapMode: Text.WordWrap
                            anchors.fill: parent
                            horizontalAlignment: Text.AlignHCenter
                            verticalAlignment: Text.AlignVCenter
                        }
                    }


                    Rectangle {
                        id: grid7
                        anchors.top: parent.top
                        anchors.left: grid1.right
                        color: ColorScheme.secondary
                        height: parent.height * (1/6)
                        width: parent.width * (1/4)
                        Text {
                            text: id
                            font.pixelSize: 10
                            anchors.fill: parent
                            horizontalAlignment: Text.AlignHCenter
                            verticalAlignment: Text.AlignVCenter
                        }
                    }

                    Rectangle {
                        id: grid8
                        anchors.top: grid7.bottom
                        anchors.left: grid2.right
                        color: ColorScheme.secondary
                        height: parent.height * (1/6)
                        width: parent.width * (1/4)
                        Text {
                            text: {
                                var date = new Date(timeOfBite);
                                return Qt.formatTime(date, 'hh:mm')
                            }
                            wrapMode: Text.WordWrap
                            //                            text: timeOfBite.toString().replace(/(..)/g, '$1:').slice(0,-1)
                            font.pixelSize: 10
                            anchors.fill: parent
                            horizontalAlignment: Text.AlignHCenter
                            verticalAlignment: Text.AlignVCenter
                        }
                    }

                    Rectangle {
                        id: grid9
                        anchors.top: grid8.bottom
                        anchors.left: grid2.right
                        color: ColorScheme.secondary
                        height: parent.height * (1/6)
                        width: parent.width * (1/4)
                        Text {
                            text: {
                                var date = new Date(timeOfBite);
                                return date.toLocaleDateString('en-GB')
                            }
                            wrapMode: Text.WordWrap
                            //                            text: timeOfBite.toString().replace(/(..)/g, '$1:').slice(0,-1)
                            font.pixelSize: 10
                            anchors.fill: parent
                            horizontalAlignment: Text.AlignHCenter
                            verticalAlignment: Text.AlignVCenter
                        }
                    }

                    Rectangle {
                        id: grid10
                        anchors.top: grid9.bottom
                        anchors.left: grid3.right
                        color: ColorScheme.secondary
                        height: parent.height * (1/6)
                        width: parent.width * (1/4)
                        Text {
                            text: (Math.round(longitude * 100) / 100)
                            font.pixelSize: 10
                            anchors.fill: parent
                            horizontalAlignment: Text.AlignHCenter
                            verticalAlignment: Text.AlignVCenter
                        }
                    }

                    Rectangle {
                        id: grid11
                        anchors.top: grid10.bottom
                        anchors.left: grid4.right
                        color: ColorScheme.secondary
                        height: parent.height * (1/6)
                        width: parent.width * (1/4)
                        Text {
                            text: (Math.round(latitude * 100) / 100)
                            font.pixelSize: 10
                            anchors.fill: parent
                            horizontalAlignment: Text.AlignHCenter
                            verticalAlignment: Text.AlignVCenter
                        }
                    }

                    Rectangle {
                        id: grid12
                        anchors.top: grid11.bottom
                        anchors.left: grid5.right
                        color: ColorScheme.secondary
                        height: parent.height * (1/6)
                        width: parent.width * (1/4)
                        Text {
                            text: username
                            font.pixelSize: 10
                            anchors.fill: parent
                            horizontalAlignment: Text.AlignHCenter
                            verticalAlignment: Text.AlignVCenter
                        }
                    }

                    Rectangle {
                        id: border
                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.top: parent.top
                        anchors.bottom: parent.bottom
                        width: 3
                        color: ColorScheme.accent
                    }

                    Rectangle {
                        id: grid13
                        anchors.top: parent.top
                        anchors.left: grid7.right
                        color: ColorScheme.secondary
                        height: parent.height * (1/6)
                        width: parent.width * (1/4)
                        Text {
                            text: "Environment:"
                            font.pixelSize: 10
                            anchors.fill: parent
                            horizontalAlignment: Text.AlignHCenter
                            verticalAlignment: Text.AlignVCenter
                        }
                    }

                    Rectangle {
                        id: grid14
                        anchors.top: grid13.bottom
                        anchors.left: grid8.right
                        color: ColorScheme.secondary
                        height: parent.height * (1/6)
                        width: parent.width * (1/4)
                        Text {
                            text: "Circumstance:"
                            font.pixelSize: 10
                            anchors.fill: parent
                            horizontalAlignment: Text.AlignHCenter
                            verticalAlignment: Text.AlignVCenter
                        }
                    }

                    Rectangle {
                        id: grid15
                        anchors.top: grid14.bottom
                        anchors.left: grid9.right
                        color: ColorScheme.secondary
                        height: parent.height * (1/6)
                        width: parent.width * (1/4)
                        Text {
                            text: "Site Of Bite:"
                            font.pixelSize: 10
                            anchors.fill: parent
                            horizontalAlignment: Text.AlignHCenter
                            verticalAlignment: Text.AlignVCenter
                        }
                    }

                    Rectangle {
                        id: grid16
                        anchors.top: grid15.bottom
                        anchors.left: grid10.right
                        color: ColorScheme.secondary
                        height: parent.height * (1/6)
                        width: parent.width * (1/4)
                        Text {
                            text: "Snake Name:"
                            font.pixelSize: 10
                            wrapMode: Text.WordWrap
                            anchors.fill: parent
                            horizontalAlignment: Text.AlignHCenter
                            verticalAlignment: Text.AlignVCenter
                        }
                    }

                    //                    Rectangle {
                    //                        id: grid16
                    //                        anchors.top: grid15.bottom
                    //                        anchors.left: grid10.right
                    //                        color: ColorScheme.secondary
                    //                        height: parent.height * (1/6)
                    //                        width: parent.width * (1/4)
                    //                        Text {
                    //                            text: "Reporter ID:"
                    //                            font.pixelSize: 10
                    //                            anchors.fill: parent
                    //                            horizontalAlignment: Text.AlignHCenter
                    //                            verticalAlignment: Text.AlignVCenter
                    //                        }
                    //                    }

                    //                    Rectangle {
                    //                        id: grid17
                    //                        anchors.top: grid16.bottom
                    //                        anchors.left: grid11.right
                    //                        color: ColorScheme.secondary
                    //                        height: parent.height * (1/6)
                    //                        width: parent.width * (1/4)
                    //                        Text {
                    //                            text: "Reporter Username:"
                    //                            font.pixelSize: 10
                    //                            wrapMode: Text.WordWrap
                    //                            anchors.fill: parent
                    //                            horizontalAlignment: Text.AlignHCenter
                    //                            verticalAlignment: Text.AlignVCenter
                    //                        }
                    //                    }

                    //                    Rectangle {
                    //                        id: grid18
                    //                        anchors.top: grid17.bottom
                    //                        anchors.left: grid12.right
                    //                        color: ColorScheme.secondary
                    //                        height: parent.height * (1/6)
                    //                        width: parent.width * (1/4)
                    //                        Text {
                    //                            text: "Photo ID:"
                    //                            font.pixelSize: 10
                    //                            anchors.fill: parent
                    //                            horizontalAlignment: Text.AlignHCenter
                    //                            verticalAlignment: Text.AlignVCenter
                    //                        }
                    //                    }

                    Rectangle {
                        id: grid19
                        anchors.top: parent.top
                        anchors.left: grid13.right
                        color: ColorScheme.secondary
                        height: parent.height * (1/6)
                        width: parent.width * (1/4)
                        Text {
                            text: environment
                            font.pixelSize: 10
                            anchors.fill: parent
                            horizontalAlignment: Text.AlignHCenter
                            verticalAlignment: Text.AlignVCenter
                            font.capitalization: Font.Capitalize
                        }
                    }

                    Rectangle {
                        id: grid20
                        anchors.top: grid19.bottom
                        anchors.left: grid14.right
                        color: ColorScheme.secondary
                        height: parent.height * (1/6)
                        width: parent.width * (1/4)
                        Text {
                            text: circumstance
                            font.pixelSize: 10
                            anchors.fill: parent
                            horizontalAlignment: Text.AlignHCenter
                            verticalAlignment: Text.AlignVCenter
                            font.capitalization: Font.Capitalize
                        }
                    }

                    Rectangle {
                        id: grid21
                        anchors.top: grid20.bottom
                        anchors.left: grid15.right
                        color: ColorScheme.secondary
                        height: parent.height * (1/6)
                        width: parent.width * (1/4)
                        Text {
                            text: siteOfBite
                            font.pixelSize: 10
                            anchors.fill: parent
                            horizontalAlignment: Text.AlignHCenter
                            verticalAlignment: Text.AlignVCenter
                            font.capitalization: Font.Capitalize
                        }
                    }

                    Rectangle {
                        id: grid22
                        anchors.top: grid21.bottom
                        anchors.left: grid16.right
                        color: ColorScheme.secondary
                        height: parent.height * (1/6)
                        width: parent.width * (1/4)
                        Text {
                            id: grid22text
                            text: ""
                            font.pixelSize: 10
                            anchors.fill: parent
                            horizontalAlignment: Text.AlignHCenter
                            verticalAlignment: Text.AlignVCenter
                            wrapMode: Text.WordWrap
                        }
                        Component.onCompleted: {
                            if (snake === 2147483647) {
                                grid22text.text = "Unknown"
                            } else {
                                //                                grid12text.text = snake
                                console.log(snake)
                                console.log("LOOOP")
                                for (var i = 0; GalleryScene.snakePics.count; i ++) {

                                    var elemCur = GalleryScene.snakePics.get(i).number
                                    console.log(elemCur)
                                    if (snake === elemCur) {
                                        console.log("HERE HERRE")
                                        var snakeName = GalleryScene.snakePics.get(i).name
                                        var testelem = GalleryScene.snakePics.get(1).name
                                        grid22text.text = snakeName
                                    }
                                }
                                //                                console.log("DONE LOOP")
                                //                                console.log(snakeName)
                                //                                var testelem = GalleryScene.snakePics.get(1).name

                            }
                        }
                    }

                    Button {
                        id: edit
                        anchors.verticalCenter: grid11.verticalCenter
                        anchors.horizontalCenter: grid15.right
                        height: parent.height * (1/8)
                        width: parent.width * (2/5)
                        background: Rectangle {
                            anchors.fill: parent
                            color: "#ffae42"
                            Text {
                                anchors.fill: parent
                                text: "Edit Record"
                                font.pixelSize: 12
                                color: "black"
                                horizontalAlignment: Text.AlignHCenter
                                verticalAlignment: Text.AlignVCenter
                            }
                        }
                        onClicked: {
                            //edits here
                            listbackground.visible = false
                            biterecordsview.visible = false
                            refresh.visible = false
                            title.visible = false
                            homeicon.visible = false
                            editbackground.visible = true
                            borderfill.visible = false
                            editfill.visible = true

                            //reset fields
                            venomous.currentIndex = -1
                            biute.currentIndex = -1
                            bruising.checked = false
                            swelling.checked = false
                            blistering.checked = false
                            necrosis.checked = false
                            systemic.checked = false
                            paralysis.checked = false
                        }
                    }

                    Button {
                        id: updatePatientStatusButton
                        anchors.verticalCenter: grid6.verticalCenter
                        anchors.horizontalCenter: grid15.right
                        height: parent.height * (1/8)
                        width: parent.width * (2/5)
                        background: Rectangle {
                            anchors.fill: parent
                            color: "#ffae42"
                            Text {
                                anchors.fill: parent
                                text: "Update Patient Status"
                                font.pixelSize: 12
                                color: "black"
                                horizontalAlignment: Text.AlignHCenter
                                verticalAlignment: Text.AlignVCenter
                            }
                        }
                        onClicked: {
                            //edits here
                            console.log("UPDATE PATIENT STATUS WITH BITE ID:" + id)
                            console.log("UPDATE PATIENT STATUS WITH BITE STATUS:" + JSON.stringify(patientStatus))
                            editPatientStatusBox.currentBiteId = id
                            editPatientStatusBox.updateValues(patientStatus)
                            editfillPatientStatus.showUpdatePatientArea(true)
                        }
                    }

                    //                    Rectangle {
                    //                        id: grid22
                    //                        anchors.top: grid21.bottom
                    //                        anchors.left: grid16.right
                    //                        color: ColorScheme.secondary
                    //                        height: parent.height * (1/6)
                    //                        width: parent.width * (1/4)
                    //                        Text {
                    //                            text: reporter
                    //                            font.pixelSize: 10
                    //                            anchors.fill: parent
                    //                            horizontalAlignment: Text.AlignHCenter
                    //                            verticalAlignment: Text.AlignVCenter
                    //                        }
                    //                    }

                    //                    Rectangle {
                    //                        id: grid23
                    //                        anchors.top: grid22.bottom
                    //                        anchors.left: grid17.right
                    //                        color: ColorScheme.secondary
                    //                        height: parent.height * (1/6)
                    //                        width: parent.width * (1/4)
                    //                        Text {
                    //                            text: username
                    //                            font.pixelSize: 10
                    //                            anchors.fill: parent
                    //                            horizontalAlignment: Text.AlignHCenter
                    //                            verticalAlignment: Text.AlignVCenter
                    //                        }
                    //                    }

                    //                    Rectangle {
                    //                        id: grid24
                    //                        anchors.top: grid23.bottom
                    //                        anchors.left: grid18.right
                    //                        color: ColorScheme.secondary
                    //                        height: parent.height * (1/6)
                    //                        width: parent.width * (1/4)
                    //                        Text {
                    //                            text: photo
                    //                            font.pixelSize: 10
                    //                            anchors.fill: parent
                    //                            horizontalAlignment: Text.AlignHCenter
                    //                            verticalAlignment: Text.AlignVCenter
                    //                        }
                    //                    }

                    Rectangle {
                        id: cutoff
                        anchors.bottom: parent.bottom
                        anchors.left: parent.left
                        anchors.right: parent.right
                        height: 3
                        color: ColorScheme.accent
                    }

                }
            }

        }
    }

    //    function biteData (filter, attribute) {
    //        var http = new XMLHttpRequest()
    //        var url = "http://localhost:1337/bite/find"
    //        var params = filter + "&populate=false"
    //        http.open("POST",url,true)
    //        http.setRequestHeader("Content-Type","application/x-www-form-urlencoded")
    //        http.setRequestHeader("Authorization", "Bearer " + AuthManager.userBearerToken)
    //        http.onreadystatechange = function() {
    //            console.log(http.readyState)
    //            console.log(http.status)
    //            console.log(http.responseText)
    //            if (http.readyState === 4) {
    //                if (http.status === 200) {
    //                    console.log("connects")
    //                    console.log(http.responseText)
    //                    var parsedata = JSON.parse(http.responseText)
    //                    console.log(parsedata[0].attribute)
    //                }
    //            } else {
    //                console.log("Erorr, cannot connect")
    //            }
    //        }

    //        http.send(params)
    //    }



    ListModel {
        id: biterecords
    }

    function regularList(index, idno) {
        var http = new XMLHttpRequest()
        var newnewurl = RequestManager.baseUrl + "bite/find"
        var newnewparams = "&populate=false&reporter=" + idno
        var userid = UserManager.currentUser.id
        console.log(userid)
        http.open("POST", newnewurl, true)
        http.setRequestHeader("Content-Type","application/x-www-form-urlencoded")
        http.setRequestHeader("Authorization", "Bearer " + AuthManager.userBearerToken)
        http.onreadystatechange = function() {
            console.log(http.responseText)
            if (http.readyState === 4) {
                if (http.status === 200) {
                    console.log("CONNECTS REGULAR LIST")
                    var parsedata = JSON.parse(http.responseText)
                    console.log(parsedata)
                    biterecords.append({

                                           //                                           "createdAt": parsedata[index].createdAt,
                                           //                                           "updatedAt": parsedata[index].updatedAt,
                                           "id": parsedata[index].id,
                                           "longitude": parsedata[index].longitude,
                                           "latitude": parsedata[index].latitude,
                                           "description": parsedata[index].description,
                                           "timeOfBite": parsedata[index].timeOfBite,
                                           "environment": parsedata[index].environment,
                                           "circumstance": parsedata[index].circumstance,
                                           "siteOfBite": parsedata[index].siteOfBite,
                                           "reporter": parsedata[index].reporter,
                                           "snake": parsedata[index].snake,
                                           "photo": parsedata[index].photo,
                                           "username": "",

                                       })
                    //Find the user's username
                    userAttribute("username", parsedata[index].reporter, index)
                }
            } else {
            }
        }
        console.log("SENDING")
        http.send(newnewparams)
    }

    ListModel {
        id: idlist
    }

    function makeList() {

        var http = new XMLHttpRequest()
        var url = RequestManager.baseUrl + "bite/find"
        var params = "&populate=true"
        http.open("POST",url,true)
        http.setRequestHeader("Content-Type","application/x-www-form-urlencoded")
        http.setRequestHeader("Authorization", "Bearer " + AuthManager.userBearerToken)
        http.onreadystatechange = function() {
            if (http.readyState === 4) {
                if (http.status === 200) {
                    //                    console.log("connects")
                    var fulldata = JSON.parse(http.responseText)
                    biterecords.clear()
                    for (var i=0; i < fulldata.length; i ++) {
                        if (UserDetails.usertype === "regular") {
                            userID(UserDetails.username, i)
                        } else {
                            biterecords.append({

                                                   "createdAt": fulldata[i].createdAt,
                                                   "updatedAt": fulldata[i].updatedAt,
                                                   "id": fulldata[i].id,
                                                   "longitude": fulldata[i].longitude,
                                                   "latitude": fulldata[i].latitude,
                                                   "description": fulldata[i].description,
                                                   "timeOfBite": fulldata[i].timeOfBite,
                                                   "environment": fulldata[i].environment,
                                                   "circumstance": fulldata[i].circumstance,
                                                   "siteOfBite": fulldata[i].siteOfBite,
                                                   "reporter": fulldata[i].reporter,
                                                   "snake": fulldata[i].snake,
                                                   "photo": fulldata[i].photo,
                                                   "username": "",
                                                   "patientStatus": fulldata[i].patientStatus
                                               })

                            //Find the user's username

                            userAttribute("username", fulldata[i].reporter, i)
                        }
                    }

                    loading.visible = false
                    console.log(UserDetails.usertype)


                }
            }
        }
        http.send(params)

    }

    function userAttribute(attribute, id, index) {
        var http = new XMLHttpRequest()
        var newurl = RequestManager.baseUrl + "user/find"
        var newparams = "id=" + id
        http.open("POST",newurl,true)
        http.setRequestHeader("Content-Type","application/x-www-form-urlencoded")
        http.setRequestHeader("Authorization", "Bearer " + AuthManager.userBearerToken)
        http.onreadystatechange = function() {
            if (http.readyState === 4) {
                if (http.status === 200) {
                    console.log("CONNECTS")
                    var data = http.responseText
                    var testdata = JSON.parse(data)
                    var userattribute = testdata[0][attribute]
                    biterecords.set(index, {

                                        "username": userattribute

                                    })
                }
            }
        }

        http.send(newparams)
    }

    function userID(number, index) {
        var http = new XMLHttpRequest()
        var newurl = RequestManager.baseUrl + "user/find"
        console.log(number)
        var newparams = "&populate=false&username=" + number
        http.open("POST",newurl,true)
        http.setRequestHeader("Content-Type","application/x-www-form-urlencoded")
        http.setRequestHeader("Authorization", "Bearer " + AuthManager.userBearerToken)
        http.onreadystatechange = function() {
            if (http.readyState === 4) {
                if (http.status === 200) {
                    console.log("CONNECTS")
                    var data = http.responseText
                    var testdata = JSON.parse(data)
                    //                    console.log("HERE")
                    //                    console.log(testdata[0].id)
                    var userattribute = testdata[0].id
                    regularList(index, userattribute)
                }
            }
        }
        http.send(newparams)
    }

    Rectangle {
        id: loading
        anchors.fill: bitebackground
        color: ColorScheme.primary
        z: bitebackground.z + 100
        visible: true
        AnimatedImage {
            id: loadingspinner
            source: LOAD_LOC + "logo.gif"
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter
            playing: true
            width: bitebackground.width * 0.3 //the pic is default 32x32 size so just make this in multiples of 32
            height: width
            fillMode: Image.PreserveAspectFit
        }
    }

    Rectangle {
        id: confirmdiscard
        anchors {
            verticalCenter: bitebackground.verticalCenter
            horizontalCenter: bitebackground.horizontalCenter
        }
        width: bitebackground.width * 0.9
        height: bitebackground.width * 0.8
        visible: false
        color: "#d4d4d4"
        Rectangle {
            anchors.top: parent.top
            anchors.left: parent.left
            anchors.right: parent.right
            height: parent.height * 0.5
            color: parent.color
            Text {
                text: "Are you sure you want to delete the record?"
                font.pixelSize: 15
                wrapMode: Text.WordWrap
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
            }
        }
        Button {
            id: yes
            anchors.left: parent.left
            anchors.bottom: parent.bottom
            anchors.leftMargin: 10
            anchors.bottomMargin: 10
            width: parent.width * 0.4
            height: parent.width * 0.4
            background: Rectangle {
                Text {
                    text: "Yes"
                    font.pixelSize: 13
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.verticalCenter: parent.verticalCenter
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                    color: "black"
                }
                anchors.fill: parent
                color: "#ccff90"
                border.width: 4
                radius: 4
                border.color: "#64dd17"
            }
            onClicked: {
                var http = new XMLHttpRequest()
                var newurl = RequestManager.baseUrl + "bite/delete"
                var params = "id=" + deleteid
                http.open("POST",newurl,true)
                http.setRequestHeader("Content-Type","application/x-www-form-urlencoded")
                http.setRequestHeader("Authorization", "Bearer " + AuthManager.userBearerToken)

                http.onreadystatechange = function() {
                    if (http.readyState === 4) {
                        if (http.status === 200) {
                            console.log("DELETE")
                            makeList()
                            //                                        refreshScene()
                        }
                    }
                }
                http.send(params)

                confirmdiscard.visible = false

            }
        }
        Button {
            id: no
            anchors.right: parent.right
            anchors.bottom: parent.bottom
            anchors.rightMargin: 10
            anchors.bottomMargin: 10
            width: parent.width * 0.4
            height: parent.width * 0.4
            background: Rectangle {
                Text {
                    text: "No"
                    font.pixelSize: 13
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.verticalCenter: parent.verticalCenter
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                    color: "white"
                }
                anchors.fill: parent
                color: "#e96868"
                border.width: 4
                radius: 4
                border.color: "#e74f4e"
            }
            onClicked: {
                confirmdiscard.visible = false
            }
        }
    }

    //    Rectangle {
    //        id: unauthorized
    //        anchors.verticalCenter: biterecordsview.verticalCenter
    //        anchors.horizontalCenter: biterecordsview.horizontalCenter
    //        width: biterecordsview.width * 0.6
    //        height: biterecordsview.height * 0.6
    //        color: ColorScheme.accent
    //        visible: false
    //        Rectangle {
    //            anchors.top: parent.top
    //            anchors.left: parent.left
    //            anchors.right: parent.right
    //            height: parent.height * 0.3
    //            Text {
    //                anchors.fill: parent
    //                horizontalAlignment: Text.AlignHCenter
    //                verticalAlignment: Text.AlignVCenter
    //                text: "Unauthorized"
    //                color: "black"
    //                font.pixelSize: 20
    //                font.bold: true
    //            }
    //        }
    //        Rectangle {
    //            anchors.bottom: parent.bottom
    //            anchors.left: parent.left
    //            anchors.right: parent.right
    //            height: parent.height * 0.7
    //            Text {
    //                anchors.fill: parent
    //                horizontalAlignment: Text.AlignHCenter
    //                verticalAlignment: Text.AlignVCenter
    //                text: "Error, account is unauthorized to delete bite entries."
    //                wrapMode: Text.WordWrap
    //                color: "black"
    //                font.pixelSize: 15
    //                font.bold: true
    //            }
    //        }
    //        Button {
    //            anchors.right: parent.right
    //            anchors.top: parent.top
    //            anchors.rightMargin: 5
    //            anchors.topMargin: 5
    //            height: parent.height * 0.1
    //            width: height
    //            background: Image {
    //                anchors.fill: parent
    //                source: IMG_LOC + "cross.png"
    //                fillMode: Image.PreserveAspectFit
    //            }
    //            onClicked: {
    //                unauthorized.visible = false
    //            }
    //        }
    //    }

    Button {
        id: homeicon
        height: 40
        width: 40

        //Button is 10 pix from the right side and from the top (x value is from bottom left corner so it is dependent on the image's width)
        y: 10
        x: parent.width - width - 10
        background: Image {
            source: IMG_LOC + "homeIcon.png"
            fillMode: Image.Stretch
        }

        //To return back to the home screen
        onClicked: {
            closeList()
        }
    }
}
