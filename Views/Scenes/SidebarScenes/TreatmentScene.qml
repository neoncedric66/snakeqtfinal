import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.0
import QtLocation 5.6
import QtPositioning 5.6
import "../../../Models"

Item {
    id: treatmentmain

    signal homeClicked()
    signal mapOpen()
    signal listOpen()
    signal openList()
    signal firstAid()
    Rectangle {

        id: settingsBackground
        color: ColorScheme.primary
        anchors.fill: parent

        Rectangle {
            id: buttonsbackground
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: parent.top
            anchors.topMargin: 60
            height: parent.height * 0.8
            width: parent.width * 0.8
            color: ColorScheme.secondary

            Button {
                id: database
                anchors.left: parent.left
                anchors.top: parent.top
                anchors.topMargin: parent.height * 0.06
                anchors.leftMargin: parent.width * 0.03
                width: parent.width * 0.45
                height: parent.height * 0.88
                background: Image {
                    anchors.fill: parent
                    source: TREAT_LOC + "databasebutton.png"
                    fillMode: Image.PreserveAspectFit
                }
                onClicked: {
//                    listOpen()
                }
            }

            Button {
                id: map
                anchors.right: parent.right
                anchors.top: parent.top
                anchors.topMargin: parent.height * 0.06
                anchors.rightMargin: parent.width * 0.03
                width:  parent.width * 0.45
                height: parent.height * 0.4
                background: Image {
                    anchors.fill: parent
                    source: TREAT_LOC + "mapbutton.png"
                    fillMode: Image.PreserveAspectFit
                }
                onClicked: {
                    mapOpen()
                }
            }

            Button {
                id: firstaid
                anchors.right: parent.right
                anchors.bottom: parent.bottom
                anchors.bottomMargin: parent.height * 0.06
                anchors.rightMargin: parent.width * 0.03
                width:  parent.width * 0.45
                height: parent.height * 0.4
                background: Image {
                    anchors.fill: parent
                    source: TREAT_LOC + "firstaidbutton.png"
                    fillMode: Image.PreserveAspectFit
                }
                onClicked: {
                    firstAid()
                }
            }

        }

//        Component.onCompleted: {
//            if (UserDetails.usertype === "doctor") {
//                // something
//                bitelist.visible = true
//            } else {
//                // nothing
//                bitelist.visible = false
//            }
//        }

//        ListView {
//            id: bitelist
//            model: ReportBiteDetails.mapbites
//            anchors.top: parent.top
//            anchors.bottom: buttonsbackground.top
//            anchors.horizontalCenter: parent.horizontalCenter
//            width: parent.width * 0.5
//            visible: false
//            delegate: Rectangle {
//                color: ColorScheme.primary
//                anchors {
//                    left: parent.left
//                    right: parent.right
//                    top: parent.top
//                    bottom: parent.bottom
//                }
//                anchors.topMargin: 5
//                Text {
//                    id: first
//                    anchors.top: parent.top
//                    font.pixelSize: 10
//                    color: "black"
//                    text: "Bite No.: " + ReportBiteDetails.mapbites.number
//                }
//                Text {
//                    anchors.top: first.bottom
//                    font.pixelSize: 10
//                    color: "black"
//                    text: "Reported by: " + ReportBiteDetails.mapbites.name
//                }
//            }
//        }

    }



    Button {
        id: homeicon
        height: 40
        width: 40

        //Button is 10 pix from the right side and from the top (x value is from bottom left corner so it is dependent on the image's width)
        y: 10
        x: parent.width - width - 10
        background: Image {
            source: IMG_LOC + "homeIcon.png"
            fillMode: Image.Stretch
        }

        //To return back to the home screen
        onClicked: {
            homeClicked()
        }
    }

}
