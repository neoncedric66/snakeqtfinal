import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.0
import QtLocation 5.6
import QtPositioning 5.6
import QtQuick.Controls.Styles 1.4
import QtQuick.Window 2.2
import "../../Components"
import "../../Scenes"
import "../../../Models"

Rectangle {
    property var currentBiteId
    signal updateClicked(var statusInfo)
    signal closeClicked
    id: root

    function updateValues(statusInfo) {
        if (!statusInfo) {
            resetValues()
            return
        }
        deadBox.checked = statusInfo.isDead
        accommodationBox.currentIndex = statusInfo.isAdmitted ? 0 : 1
        hospitalField.text = statusInfo.isAdmitted ? statusInfo.admissionHospital : statusInfo.referredHospital
        var types = ["general", "regional", "national", "other"] //must match order in display below
        hospitalTypeBox.currentIndex = types.indexOf(statusInfo.isAdmitted ? statusInfo.admissionHospitalType : statusInfo.referredHospitalType)
    }
    function resetValues() {
        deadBox.checked = false
        accommodationBox.currentIndex = -1
        hospitalField.text = ""
        hospitalTypeBox.currentIndex = -1
    }

    Button {
        id: proceed
        anchors.bottom: parent.bottom
        anchors.bottomMargin: parent.height * 0.05
        height: parent.height * 0.1
        anchors.horizontalCenter: parent.horizontalCenter
        width: parent.width * 0.5
        background: Rectangle {
            anchors.fill: parent
            color: "#508030"
            Text {
                anchors.fill: parent
                text: "Update"
                color: "black"
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                font.pixelSize: 12
            }
        }
        onClicked: {
            var statusInfo = {}
            statusInfo.isDead = deadBox.checked
            statusInfo.isAdmitted = accommodationBox.currentText == "Admitted" ? true : false
            statusInfo.isReferred = accommodationBox.currentText == "Referred" ? true : false
            statusInfo.referredHospital = statusInfo.isAdmitted ? "" : hospitalField.text
            statusInfo.referredHospitalType = statusInfo.isAdmitted ? "" : hospitalTypeBox.currentText.toLowerCase()
            statusInfo.admissionHospital = statusInfo.isAdmitted ? hospitalField.text : ""
            statusInfo.admissionHospitalType = statusInfo.isAdmitted ? hospitalTypeBox.currentText.toLowerCase() : ""
            statusInfo.bite = currentBiteId
            updateClicked(statusInfo)
        }
    }

    Button {
        id: closer
        anchors.top: parent.top
        anchors.right: parent.right
        anchors.topMargin: 5
        anchors.rightMargin: 5
        width: 20
        height: 20
        z:100000
        background: Image {
            anchors.fill: parent
            source: IMG_LOC + "cancelButton.png"
            fillMode: Image.PreserveAspectCrop
            horizontalAlignment: Image.AlignRight
        }
        onClicked: {
            closeClicked()
        }
    }

    Flickable {
        anchors {
            left: parent.left
            right: parent.right
            top: parent.top
        }
        height: parent.height * 0.8
        clip: true
        interactive: true
        contentHeight: contentItem.childrenRect.height + 5
        contentWidth: parent.width

        Text {
            anchors {
                top: parent.top
                left: parent.left
                topMargin: 20
                leftMargin: parent.width * 0.05
            }
            height: root.height * (1/8)
            width: parent.width * 0.85
            anchors.fill: parent
            horizontalAlignment: Text.AlignLeft
            verticalAlignment: Text.AlignTop
            text: "Update Patient Status"
            color: "black"
            font.pixelSize: 17
            font.bold: true
        }

        // Accommodation
        Text {
            id: accommodationLabel
            height: root.height * (1/6)
            width: parent.width * 0.3
            anchors {
                left: parent.left
                top: parent.top
                topMargin: 60
                leftMargin: 10
            }
            text: "Accommodation:"
            horizontalAlignment: Text.AlignLeft
            verticalAlignment: Text.AlignVCenter
            color: "black"
            font.pixelSize: 12
        }

        ComboBox {
            id: accommodationBox
            model: ["Admitted", "Referred"]
            font.pixelSize: 12
            currentIndex: 0
            anchors {
                right: parent.right
                top: parent.top
                topMargin: 60
                rightMargin: 10
            }
            height: root.height * (1/6)
            width: parent.width * 0.55
        }

        // Hospital Type
        Text {
            id: hospitalTypeLabel
            height: root.height * (1/6)
            width: parent.width * 0.3
            anchors {
                left: accommodationLabel.left
                top: accommodationBox.bottom
            }
            text: "Hospital Type:"
            horizontalAlignment: Text.AlignLeft
            verticalAlignment: Text.AlignVCenter
            color: "black"
            font.pixelSize: 12
        }

        ComboBox {
            id: hospitalTypeBox
            model: ["General", "Regional", "National", "Other"]
            font.pixelSize: 12
            currentIndex: 0
            anchors {
                right: parent.right
                top: accommodationBox.bottom
                topMargin: 10
                rightMargin: 10
            }
            height: root.height * (1/6)
            width: parent.width * 0.55
        }

        // Hospital
        Text {
            id: hospitalLabel
            height: root.height * (1/6)
            width: parent.width * 0.3
            anchors {
                left: hospitalTypeLabel.left
                top: hospitalTypeBox.bottom
            }
            text: "Hospital/Clinic"
            horizontalAlignment: Text.AlignLeft
            verticalAlignment: Text.AlignVCenter
            color: "black"
            font.pixelSize: 12
        }

        TextField {
            id: hospitalField
            placeholderText: qsTr("Hospital name")
            font.pixelSize: 12
            activeFocusOnPress: true
            anchors {
                right: parent.right
                top: hospitalTypeBox.bottom
                topMargin: 10
                rightMargin: 10
            }
            height: root.height * (1/8)
            width: parent.width * 0.55
            inputMethodHints: Qt.ImhNoAutoUppercase
            background: Rectangle {
                anchors.fill: parent
                color: ColorScheme.secondary
            }
            onEditingFinished: {
                console.log(hospitalField.text)
            }
        }


        // Dead
        Text {
            id: deadLabel
            height: root.height * (1/6)
            width: parent.width * 0.3
            anchors {
                left: hospitalLabel.left
                top: hospitalLabel.bottom
            }
            text: "Is Dead?"
            horizontalAlignment: Text.AlignLeft
            verticalAlignment: Text.AlignVCenter
            color: "black"
            font.pixelSize: 12
        }

        CheckBox {
            id: deadBox
            anchors.left: hospitalField.left
            anchors.verticalCenter: deadLabel.verticalCenter
            height: root.height * (1/7)
            width: height
            checked: false
        }

        Rectangle {
            id: filler
            color: ColorScheme.secondary
            anchors.top: deadLabel.bottom
            anchors.left: parent.left
            anchors.right: parent.right
            height: 10
        }
    }
}
