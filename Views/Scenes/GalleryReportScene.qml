                                                                                                                import QtQuick 2.7
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.0
import QtLocation 5.6
import QtPositioning 5.6
import QtQuick.Controls.Styles 1.4
import QtQuick.Window 2.2
import SortFilterProxyModel 0.2
import QtQml.Models 2.1

import "../Components"
import "../Scenes"
import "../../Models"


Item {
    id: gallerymain

    signal closeGalleryReport()
    property int imageshown: 0
    property string imageurl: ""
    property int photonumber: 0 //number of photos initially shown = photonumber + photoappend
    property int photoappend: 1 //number of photos added each time
    property string magimageurl: ""

    property int relatedsnake: 0

    property string largeimage: ""

    //Function for generating snake list
    function urllist() {
        console.log("makelist")
        var http = new XMLHttpRequest()
        var url = RequestManager.baseUrl + "snake/photo/find"
        var params = "status=pending,verified&populate=false"
        http.open("GET",url,true)
        http.setRequestHeader("Content-Type","application/x-www-form-urlencoded")
        http.setRequestHeader("Authorization", "Bearer " + AuthManager.userBearerToken)
        http.onreadystatechange = function() {
            if (http.readyState === 4) {
                if (http.status === 200) {
                    console.log("CONNECTS")
                    var fulldata = JSON.parse(http.responseText)
                    snakePics.clear()
                    for (var i=0; i < fulldata.length; i ++) {
                        snakePics.append({
                                             "url": fulldata[i].source,
                                             "name": fulldata[i].snake,
                                             "index": fulldata[i].snake,
                                             "number": fulldata[i].id
                                         })
                        console.log(fulldata[i].snake)
                    }
                    snakename()
                }
            } else {
                console.log("DOESNT WORK")
            }
        }
        console.log("SENDING")
        http.send(params)
    }

    //function for replacing id number with proper snake name
    function snakename() {
        console.log("makesnakelist")
        var http = new XMLHttpRequest()
        var newurl = RequestManager.baseUrl + "snake/photo/find"
        var newparams = ""
        http.open("GET",newurl,true)
        http.setRequestHeader("Content-Type","application/x-www-form-urlencoded")
        http.setRequestHeader("Authorization", "Bearer " + AuthManager.userBearerToken)
        http.onreadystatechange = function() {
            if (http.readyState === 4) {
                if (http.status === 200) {
                    console.log("CONNECTS")
                    var parsedata = JSON.parse(http.responseText)
                    for (var i=0; i < snakePics.length; i ++) {
                        for (var z=0; z < parsedata.length; z ++) {
                            console.log(parsedata[z].name)
                            if (snakePics[i].name === parsedata[z].id) {
                                snakePics[i].name = parsedata[z].name
                            } else {
                                //nothing
                            }
                        }
                    }
                    //show the gallery
                    console.log(snakePics.count)
                    loading.visible = false
                    gallerybackground.visible = true
//                    uploader.visible = true
                    closereport.visible = true
                    refreshbutton.visible = true
                }
            } else {
                console.log("DOESNT WORK")
            }
        }
        console.log("SENDING")
        http.send(newparams)
    }

    //HTTPRequest to retrieve related images
    function relatedlist(skip, limit, snake) {
        loadingmain.visible = true
        var http = new XMLHttpRequest()
        var url = RequestManager.baseUrl + "snake/photo/find"
        var params = "populate=false&skip="+skip+"&limit="+limit+"&snake="+snake
        console.log(params)
        http.open("POST",url,true)
        http.setRequestHeader("Content-Type","application/x-www-form-urlencoded")
        http.setRequestHeader("Authorization", "Bearer " + AuthManager.userBearerToken)
        http.onreadystatechange = function() {
            if (http.readyState === 4) {
                if (http.status === 200) {
                    console.log("CONNECTS")
                    var fulldata = JSON.parse(http.responseText)
                    console.log(fulldata)
                    if (fulldata.length !== 0) {
                        for (var i=0; i < fulldata.length; i ++) {
                            console.log(fulldata[i].id)
                            relatedimageslist.append({
                                                         "url": fulldata[i].source,
                                                         "number": fulldata[i].number,
                                                         "name": fulldata.snake
                                                     })
                        }
                        loadingmain.visible = false
                        photonumber = photonumber + photoappend
                    } else if (fulldata.length === 0) {
                        photonumber = photonumber
                        loadingmain.visible = false
                    }
                    refresh.visible = true
                }
            } else {
                console.log("DOESNT WORK")
            }
        }
        console.log("SENDING")
        http.send(params)
    }

    ListModel {
        id: snakePics

        //        ListElement {
        //            url: "qrc:/Icons/Snakes/snake1.png"
        //            number: 1
        //            name: "Malayan Pit Viper"
        //        }
        //        ListElement {
        //            url: "qrc:/Icons/Snakes/snake2.png"
        //            number: 2
        //            name: "Red Necked Keelback"
        //        }
        //        ListElement {
        //            url: "qrc:/Icons/Snakes/snake3.png"
        //            number: 3
        //            name: "Bungarus fasciatus"
        //        }
        //        ListElement {
        //            url: "qrc:/Icons/Snakes/snake4.png"
        //            number: 4
        //            name: "Calloselasma rhodostoma"
        //        }
        //        ListElement {
        //            url: "qrc:/Icons/Snakes/snake5.png"
        //            number: 5
        //            name: "Cryptelytrops rubeus"
        //        }
        //        ListElement {
        //            url: "qrc:/Icons/Snakes/snake6.png"
        //            number: 6
        //            name: "Enhydris enhydris"
        //        }
        //        ListElement {
        //            url: "qrc:/Icons/Snakes/snake7.png"
        //            number: 7
        //            name: "Naja naja"
        //        }
        //        ListElement {
        //            url: "qrc:/Icons/Snakes/snake8.png"
        //            number: 8
        //            name: "Ophiophagus hannah"
        //        }
        //        ListElement {
        //            url: "qrc:/Icons/Snakes/snake9.png"
        //            number: 9
        //            name: "Rhabdophis subminiatus"
        //        }

    }

    ListModel {
        id: snakeNames

        //Keep adding List Elements when more pictures are added
        ListElement {
            name: "Malayan Pit Viper"
            text: "Calloselasma is a monotypic genus created for a venomous pit viper species, C. rhodostoma, which is endemic to Southeast Asia from Thailand to northern Malaysia and on the island of Java. No subspecies are currently recognized.\n\nRetrieved from Wikipedia"
        }

        ListElement {
            name: "Red Necked Keelback"
            text: "Rhabdophis subminiatus, commonly called the red-necked keelback, is a species of venomous snake in the family Colubridae. The species is endemic to Asia.\n\nRhabdophis subminiatus is a rear-fanged species and was previously thought to be harmless. However, following one fatal and several near-fatal envenomations, the toxicity of its venom was investigated. As a result, it has recently been reclassified as a dangerous species. Rear-fanged snakes need to bite and hold on, or repeatedly bite, to have any effect on humans. A chewing action facilitates envenomation as the venom ducts open to fangs that are externally grooved (not hollow) and are posterior in the oral cavity.R. subminiatus has enlarged and non-grooved teeth. R. subminiatus has two enlarged teeth in the back of the snake’s jaw. Located in the upper jaw is a gland known as the Duvernoy's glands which produces an extremely venomous secretion.\n\nRetrieved from Wikipedia"
        }

        ListElement {
            name: "Bungarus fasciatus"
            text: "Identifying characteristics\n\n Snake venom is relatively large, usually longer than 1m. Head large and short, less distinguishable from the neck, relatively small eyes and round, usually heavy body, short tail, round tail end, the middle of the back has a very prominent. Rows of scales are spine-shaped, larger than lateral scales. The body is black and interlaced with yellow, alternately rounded.\n\nBiology/Ecology:A common venomous snake, one of the most common venomous snakes in the lowlands, midlands and mountains. Living in the jungle or in places close to human habitation, they are more commonly found in highland terrain with water, live in rat caves or abandoned caves in the fields, mounds, riverbanks, garden, bamboo, pond. In the cold dry season they often hide alone, sometimes two to three individuals in a cave, sometimes living with both frogs. Scorpion stalks are evolved throughout the year and are often molten in the cave. They feed at night, catch other snakes, sometimes eat lizards. In northern Vietnam, hot cobia begin to mate in caves beginning in January or February, laying eggs in caves in May and June, laying an average of 9 eggs (4 to 16 eggs). Mother Snake has the ability to pull eggs to guard. During this time, the mother still has to guard the eggs just to go for prey. Young in July or August usually 30-35 cm long. Snakebite is very slow during daytime, less biting, but when bitten by a snake, it can die, because snake venom is very poisonous, toxicity is four times higher than the snake.\n\nDistribution:\n Inland: Widely distributed in the delta, midland and mountainous areas. \n Northeastern India, Nepal, Bangladesh, Brunei, Parussalam, Bhutan, Myanmar, South China, Laos, Thailand, Cambodia, Malaysia, Singapore, Indonesia (Sumatra, Java, Borneo"
        }

        ListElement {
            name: "Calloselasma rhodostoma"
            text: "Identifying characteristics:\n\n This reptile has a length of about 100cm with nine solid and balanced scales at the top of the head. Pointed and pointed upwards. The nose extends from the eyes to the muzzle. The body is not very thick, the scales smooth. The pattern on the body consists of 19 to 31 dark triangular markings on the base of deep reddish or dark red.\n\nBiology/Ecology:\n As a nightlife and prefer high humidity, use the tail to swing to attract prey. Their food is frogs, frogs, some other amphibians, sometimes they even eat rodents, birds and other snakes. often attacking prey unexpectedly. Prisoners of 13 to 30 hit and guarded during 5-7 weeks of incubation. Snakes 13 to 20cm long look like mature snakes. This terrestrial snake prefers dry, lowland forests but has been found at 2000 m altitude. There are venom and the ability to kill.\n\nDistribution:\n\n Vietnam: Ninh Thuan (Phan Rang, Vinh Hoa), Song Be (Ben Cat, Loc Ninh, Thu Dau Mot), Ba Ria - Vung Tau, Tay Ninh, Dong Nai (Bien Hoa, Long Binh, Xuan Loc), An Giang (Phu Vinh) \n\n World:  This snake species is found throughout Thailand and the northern states of the Malay Peninsula, Laos, Cambodia, India."
        }

        ListElement {
            name: "Cryptelytrops rubeus"
            text: "Identifying characteristics:\n\nMedium sized snout to anal (SVL) up to 51 cm; back flanks, with 21 scales around the body; Abdominal scales; 52 - 74 scales under the tail; brown eyes; green trunks, white striped children running from the nose along the ribs to the anus, males with a red strip below the white stripes. Head larger than the neck, nostrils away from the eye socket. \n\n Biology/Ecology: \n Reptiles are often found in good evergreen forest. Appears around puddles in the forest to look for food that is frogs ... and only appears in the rainy season. Live the same habitat with the red-tailed Trimeresurus albolabris. The species is derived from a Latin term 'rubens' meaning red. The venomous snake is dangerous with cellular venom, which can be deadly. \n\n Distribution:  \n New reptiles were discovered in 2011 at Cat Sa National Park, Vinh Cuu Nature Reserve, Dong Nai Province and Ho Chi Minh City. Specimens of this species are collected in lowland forest habitats up to about 500 m above sea level."
        }

        ListElement {
            name: "Enhydris enhydris"
            text: "Identifying characteristics:  \n\nThis medium-length snake has a distinctively colored head. Cylindrical body with smooth scales. The upper body is gray or olive with brown stripes on the back and lighter stripes separated by the black stripes on each side. Brown head with light colored lines towards the muzzle. The belly is white or yellow with a line in the center of dark color or a circle of dots. \n\n Biology/Ecology: \n Their food is mainly fish. Each puppy lays 4-18 pups. Snake 14 cm long like adult snake. They are a serpentine snake that is active during the day and is found in freshwater areas of lowland, ponds and swamps.\n\n Distribution: \n\n  Vietnam:  From Phu Yen to Minh Hai all have this species distribution. \n\n  World:Most reside in the tropical regions of Southeast Asia, including the southern part of Thailand 170 North, the Malay Peninsula and Singapore."
        }

        ListElement {
            name: "Naja naja"
            text: "Identifying characteristics: \n\nA large snake about 1 meter in length, up to 2 meters long. The head is broad and slightly flat, irrespective of neck, with a pair of hooks that grow in front of the upper jaw, which can be erect. The back is yellowish green, dark brown or black or solid color, or there are patterned strips such as single or double luminous stripes. Patterns in the neck are of two types, see clearly when the cobra neck, the neck is always horizontal, to the sides. \n\n Biology/Ecology:  \n Chinese cobras are hidden in rat caves, caves in fields, villages, gardens, dikes, mounds, under trees in bushes. In many cases they crawl into the rat cave, swallowing the host and taking over the cave. They eat at night, eat mostly rat but eat lizard snake, toad and frog. Young snakes mainly feed on frogs. in North Vietnam The Chinese snake or Chinese snake has the most crowded glass frame year-round in August and the months in the winter season (December, January and February) mate in late April and early May, Lay eggs in June (mainly in June) from 6 to 20 eggs / litter. The Mother Snake wraps around the eggs to protect them. The Eggs hatch after 50 - 57 days. \n\n Distribution: \n\n Inland: Widely distributed in the delta, midland and mountainous areas. There are many in northern Vietnam that may extend south to Quang Binh, Quang Tri. From Danang back, Chinese snakes have become extremely scarce. \n\n  World: South China, Laos."
        }

        ListElement {
            name: "Ophiophagus hannah"
            text: " Identifying characteristics: \n\nThe largest size is about 3m - 4m, sometimes up to 5m. It is capable of neck, but not enlarged by normal cobra. The head is relatively short, slightly flat, somewhat differentiated from the neck. The body is small, tapering backwards, with long tail. Adult snakes are green or brown, sometimes with black lead. The top of the head has a triangular scab, with a triangular top pointing towards the tail. Young individuals with black back with many bright horizontal lines, in the neck with a yellow V-shaped back. \n\n  Biology/Ecology: \n They live in the midlands and in the mountains, live in the forests, in the hills, even in the bushes of the village, and live in the caves under the trees in the forest, by the stream. They climb trees and swim well, but often live on the ground. Eating during the day and at night, the food mainly consists of other snakes or sometimes even lizards. Leprosy breeds about 20 to 30 eggs per litter in April / May, in the nest with many leaves or flakes, eggs are both snake father and mother guard. Snakes are about 40-50 centimeters in length, with many yellow scales, with a yellow V-shaped back. Snake Tiger is a fierce and aggressive snake that attacks people actively. When attacking, they usually build up in front of the body. Their height depends on their size and length. \n\n Distribution: \n\n In the country:  Cao Bang, Lao Cai, Vinh Phuc, Ha Tay, Phu Tho, Bac Giang, Quang Ninh, Ninh Binh, Ha Tinh, Quang Binh, Dak Lak, Lam Dong, Phu Yen, Ninh Thuan, Gia Lai, Kontum, Tay Ninh, Dong Nai, Ba Ria - Vung Tau. \n\n Bangladesh, East India, Nepal, Myanmar, South China, Laos, Thailand, Cambodia, Malaysia, Indonesia (Sumatra, Java, Borneo) and the Philippines."
        }

        ListElement {
            name: "Rhabdophis subminiatus"
            text: "Identifying characteristics: \n\nThe snake is small, with a length of about 130 cm, with a long, distinctive long neck. The round nose, located in the middle of the nose, divides the junction between the two nasal plates near the line between the two frontal plates. The forehead is longer, broader, smaller than the top, shorter than the muzzle. has a slightly longer cheeks on top of the second, above the forearm. There are 8 edges on each side, 3 on the left, 4 on the eye, and 8 on the bottom 10 on each side. The triangular triangle is broader than the long one. Scales of 19 rows of edges with obliterated outer scales, 160 - 167 sheets of abdominal scales, scales under the tail of 84 - 86 plates, broken anal. \n\n The body is dark green or dark gray, the head is dark. The front half of the body usually has black veins, unevenly on the back and sides of the body, the white edge, the back of the neck with or without black rings, the neck is yellowish and brown in the young very clear and faded when the head into. White chin and throat. \n\n Biology/Ecology: \n This daytime snake prefers low-lying soils in the forest and hills of up to 1,780m. Their main food is frog and toad. Each time from 5 to 17 eggs, incubation time for eggs hatched from 8 to 10 weeks; Long snake 13 - 19cm. This species has been found to be poisonous when it is snake bite severe symptoms. From 1997 to date, according to the data of Cho Ray hospital, there were 24 cases reported by the small Rhabdophis subminiatus. This snake causes severe consequences; victims of bites will have symptoms such as: blood clotting disorder like snake bite. There were two deaths from snakes in 2009 and 2011. There is no serum in the world for this species. \n\n Distribution: \n\n  Vietnam:  This species is almost entirely populated in Vietnam from Cao Bang to Kien Giang. \n\n World: Myanmar, China, Thailand, Laos, Cambodia, Malaysia."
        }

    }

    SortFilterProxyModel {
        id: aProxy
        sourceModel: snakePics //assign the original ListModel to SortFilterProxyModel
        filters: [
            AnyOf {
                RegExpFilter {
                    id: filter
                    roleName: "name"
                    pattern: ""         //update this pattern whenever the textinput value changed
                    caseSensitivity: Qt.CaseInsensitive
                }
            }
        ]
    }

    //    Component {
    //        id: textDelegate
    //        Row {
    //            spacing: 10
    //            Text { text: name }
    //        }
    //    }

    //    ListView {
    //        anchors.fill: parent
    //        model: aProxy               //assign the SortFilterProxyModel to the list view
    //        delegate: textDelegate
    //    }

    Rectangle {
        id: gallerybackground
        color: ColorScheme.primary
        anchors.fill: parent

        Component.onCompleted: {
            urllist()
        }

        Image {
            id: largeimage
            visible: false
            height: width
            width: parent.width * 0.7
            anchors {
                verticalCenter: parent.verticalCenter
                horizontalCenter: parent.horizontalCenter
            }
            source: magimageurl
            fillMode: Image.PreserveAspectFit
        }

        Button {
            id: magimageclose
            visible: false
            anchors.bottom: largeimage.top
            anchors.bottomMargin: 5
            anchors.right: largeimage.right
            anchors.rightMargin: 5
            height: 30
            width: 30
            background: Image {
                anchors.fill: parent
                source: IMG_LOC + "cancelButton.png"
                fillMode: Image.PreserveAspectFit
            }
            onClicked: {
                description.visible = true
                relatedimages.visible = true
                homeicon.visible = true
                largeimage.visible = false
                magimageclose.visible = false
            }
        }

        Rectangle {
            id: gallerytitle
            anchors.top: parent.top
            anchors.topMargin: 40
            anchors.horizontalCenter: gallerybackground.horizontalCenter
            height: parent.height * 0.05
            width: parent.width * 0.7
            color: parent.color
            Text {
                anchors.fill: parent
                text: "Gallery"
                font.pixelSize: 20
                font.family: "Open Sans"
                font.bold: true
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
            }
        }

        Rectangle {
            id: searchbar
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: gallerytitle.bottom
            anchors.topMargin: 10
            //            x: 0
            //            y: 0
            height: parent.height * 0.05
            width: parent.width * 0.7
            z: photoslider.z + 10000

            Rectangle {
                id: textinputContainer
                color: "white"
                anchors.top: parent.top
                anchors.bottom: parent.bottom
                width: parent.width * 0.85
                height: parent.height
                anchors.left: parent.left
                TextInput {
                    id: input
                    anchors.fill: parent
                    color: "black"
                    echoMode: TextInput.Normal
                    font.pixelSize: height * 0.5
                    font.family: "Century Gothic"
                    verticalAlignment: TextInput.AlignVCenter
                    horizontalAlignment: TextInput.AlignLeft
                    padding: 5
                    clip: true
                    text: ""
                    validator: RegExpValidator {
                        regExp: /^[\w. ]*$/             /** is equal to: /^[a-zA-Z0-9_. ]*$/ **/
                    }
                    onTextChanged: {
                        filter.pattern = input.text
                    }
                }
            }
            Button {
                id: searchbutton
                anchors.right: parent.right
                anchors.top: parent.top
                anchors.bottom: parent.bottom
                width: parent.width * 0.15
                height: parent.height

                background: Rectangle {
                    color: "#CDDC39"
                    Image {
                        anchors.fill: parent
                        source: IMG_LOC + "MagIcon.png"
                        fillMode: Image.PreserveAspectCrop
                    }
                }

                onClicked: {
                    input.focus = false
                }
            }
        }

        Rectangle {
            id: galleryframe
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: searchbar.bottom
            anchors.topMargin: 10
            height: parent.height * 0.7
            width: parent.width * 0.8
            color: ColorScheme.secondary

            GridView {
                id: photoslider
                anchors {
                    left: parent.left
                    right: parent.right
                    top: parent.top
                    bottom: parent.bottom
                }

                ScrollBar.vertical: ScrollBar {
                    id: galleryscroll
                    parent: photoslider.parent
                    anchors.top: parent.top
                    anchors.bottom: parent.bottom
                    anchors.right: parent.right
                    active: true
                    policy: ScrollBar.AlwaysOn
                }

                anchors.leftMargin: 5
                cellWidth: galleryframe.width * 0.3
                cellHeight: cellWidth
                model: aProxy
                flow: GridView.FlowLeftToRight
                clip: true
                delegate: Item {
                    width: photoslider.cellWidth
                    height: photoslider.cellHeight
                    Button {
                        anchors {
                            left: parent.left
                            right: parent.right
                            top: parent.top
                            bottom: parent.bottom
                        }
                        anchors.leftMargin: 5
                        background: Image {
                            source: RequestManager.baseUrl + url /* IMG_LOC + "cross.png"*/
                            anchors.fill: parent
                            fillMode: Image.PreserveAspectFit
                            width: photoslider.cellWidth; height: photoslider.cellHeight
                        }
                        onClicked: {
                            homeicon.visible = true
                            photoslider.visible = false
                            searchbar.visible = false
                            gallerytitle.visible = false
                            description.visible = true
                            magimage.visible = true
                            snaketext.visible = true
                            scroller.visible = true
                            galleryscroll.visible = false
                            right.visible = true
                            left.visible = true
                            close.visible = true
                            galleryframe.visible = false
                            relatedimages.visible = true
                            relatedscroll.visible = true
                            closereport.visible = false
                            refreshbutton.visible = false

                            imageshown = number
                            imageurl = RequestManager.baseUrl + url
                            relatedlist(photonumber, photoappend, imageshown)
                            console.log(photonumber)
                        }
                    }
                }
            }
        }

    }

    //Rectangle and objects to cover gallery
    Rectangle {
        id: description
        width: gallerybackground.width * 0.8
        height: gallerybackground.height * 0.5
        anchors.horizontalCenter: gallerybackground.horizontalCenter
        anchors.top: parent.top
        anchors.topMargin: gallerybackground.height * 0.06
        z: galleryframe.z + 1000

        visible: false
        color: ColorScheme.secondary

        Rectangle {
            id: snaketext
            anchors.horizontalCenter: description.horizontalCenter
            anchors.top: description.top
            anchors.topMargin: 20
            anchors.bottomMargin: 0
            Text {
                anchors.fill: parent
                text: snakeNames.get(imageshown - 1).name
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                font.pixelSize: 18
                color: "black"
            }
        }

        Button {
            id: magimage
            anchors.left: parent.left
            anchors.leftMargin: parent.width * 0.05
            anchors.verticalCenter: description.verticalCenter
            width: galleryframe.width * 0.45
            height: width
            background: Image {
                id: image
                anchors.fill: parent
                source: imageurl /*SNAKE_LOC + "snake" + imageshown + ".png"*/
                fillMode: Image.PreserveAspectFit
                visible: true
            }
            onClicked: {
                description.visible = false
                relatedimages.visible = false
                homeicon.visible = false
                largeimage.visible = true
                magimageurl = image.source
                magimageclose.visible = true
            }
        }

        ScrollView {
            id: scroller
            anchors.right: parent.right
            anchors.rightMargin: parent.width * 0.05
            anchors.verticalCenter: magimage.verticalCenter
            width: description.width * 0.4
            height: description.height * 0.6
            clip: true
            TextArea {
                anchors.top: parent.top
                height: parent.height
                anchors.left: parent.left
                anchors.right: parent.right
                text: snakeNames.get(imageshown-1).text
                wrapMode: Text.WordWrap
                font.pixelSize: 10
                readOnly: true
                enabled: true
                z: swipe_area.z + 1
                color: "black"

                background: Rectangle {
                    anchors.top: parent.top
                    anchors.left: parent.left
                    anchors.right: parent.right
                    anchors.bottom: parent.bottom
                    color: "white"
                }
            }
        }

        Button {
            id: confirmchoice
            anchors.bottom: parent.bottom
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.bottomMargin: 10
            height: parent.height * 0.09
            width: parent.width * 0.5
            Rectangle {
                id: choicebox
                anchors.fill: parent
                color: "#CCFF90"
                Text {
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.horizontalCenter: parent.horizontalCenter
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    text: "Confirm Choice?"
                    color: "#212121"
                    font.pixelSize: 12
                }
            }
            onClicked: {
                description.visible = false
                closeGalleryReport()
                ReportBiteDetails.snakechosen = snakeNames.get(imageshown - 1).index
                GalleryReportData.snakechosen = snakeNames.get(imageshown - 1).name
                GalleryReportData.gallerytext = "Choose Another"
                GalleryReportData.speciesknownvisible = false
                GalleryReportData.snakevisible = true
                GalleryReportData.speciestext = "Species Chosen:"
                console.log(GalleryReportData.snakechosen)
            }

        }

    }

    Rectangle {
        id: relatedimages
        anchors.top: description.bottom
        anchors.topMargin: gallerybackground.height * 0.05
        height: gallerybackground.height * 0.34
        width: description.width
        visible: false
        color: ColorScheme.secondary
        anchors.horizontalCenter: gallerybackground.horizontalCenter
        Rectangle {
            id: relatedtitle
            anchors.horizontalCenter: relatedimages.horizontalCenter
            anchors.top: relatedimages.top
            anchors.topMargin: 20
            anchors.bottomMargin: 0
            Text {
                anchors.fill: parent
                text: "Related Images"
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                font.pixelSize: 18
                color: "black"
            }
        }

        //temporary list with the elements
        ListModel {
            id: relatedimageslist
        }

        Rectangle {
            id: loadingmain
            anchors {
                left: relatedimages.left
                right: relatedimages.right
                top: relatedtitle.bottom
                topMargin: 15
                bottom: relatedimages.bottom
            }
            z: relatedphotos.z + 100
            visible: false
            color: ColorScheme.secondary
            AnimatedImage {
                id: loadingspinnermain
                source: LOAD_LOC + "logo.gif"
                anchors.verticalCenter: parent.verticalCenter
                anchors.horizontalCenter: parent.horizontalCenter
                playing: true
                width: relatedimages.width * 0.5 //the pic is default 32x32 size so just make this in multiples of 32
                height: width
                fillMode: Image.PreserveAspectFit
            }
        }

        //Grid with related images
        GridView {
            id: relatedphotos
            anchors {
                left: parent.left
                right: parent.right
                top: relatedtitle.bottom
                topMargin: 15
                bottom: parent.bottom
                bottomMargin: parent.height * 0.2
            }
            ScrollBar.vertical: ScrollBar {
                id: relatedscroll
                parent: relatedphotos.parent
                anchors.top: relatedphotos.top
                anchors.bottom: parent.bottom
                anchors.bottomMargin: parent.height * 0.2
                anchors.right: parent.right
                active: true
                visible: false
                policy: ScrollBar.AlwaysOn
            }
            anchors.leftMargin: 5
            cellWidth: relatedimages.width * 0.3
            cellHeight: cellWidth
            model: relatedimageslist

            flow: GridView.FlowLeftToRight
            clip: true
            delegate: Item {
                width: relatedphotos.cellWidth
                height: relatedphotos.cellHeight
                Button {
                    anchors {
                        left: parent.left
                        right: parent.right
                        top: parent.top
                        bottom: parent.bottom
                    }
                    anchors.leftMargin: 5
                    background: Image {
                        source: RequestManager.baseUrl + url /*IMG_LOC + "cross.png"*/
                        anchors.fill: parent
                        fillMode: Image.PreserveAspectFit
                        width: relatedphotos.cellWidth; height: relatedphotos.cellHeight
                    }
                    onClicked: {
                    }
                }
            }
        }

        Button {
            id: refresh
            anchors.top: relatedphotos.bottom
            anchors.topMargin: parent.height *0.05
            anchors.horizontalCenter: parent.horizontalCenter
            height: parent.height * 0.1
            width: parent.width * 0.5
            Rectangle {
                anchors.fill: parent
                color: "#CCFF90"
                Text {
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.horizontalCenter: parent.horizontalCenter
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    text: "Refresh"
                    color: "#212121"
                    font.pixelSize: 12
                }
            }
            onClicked: {
                //                addphotos(photoappend)
                refresh.visible = false
                relatedlist(photonumber, photoappend, imageshown)
                loadingmain.visible = true
                console.log(photonumber)
            }
        }

    }

    Button {
        id: refreshbutton
        height: parent.height * 0.05
        width: parent.width * 0.4
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottom: parent.bottom
        anchors.bottomMargin: parent.height * (0.02)
        visible: false
        background: Rectangle {
            color: ColorScheme.accent
            anchors.fill: parent
            Text {
                anchors.fill: parent
                text: "Refresh"
                color: "black"
                font.pixelSize: 13
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
            }
        }
        onClicked: {
            loading.visible = true
            refreshbutton.visible = false
            urllist()
        }
    }

    Rectangle {
        id: loading
        anchors.fill: gallerybackground
        color: ColorScheme.primary
        z: gallerybackground.z + 100
        visible: true

        AnimatedImage {
            id: loadingspinner
            source: LOAD_LOC + "logo.gif"
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter
            playing: true
            width: gallerybackground.width * 0.3 //the pic is default 32x32 size so just make this in multiples of 32
            height: width
            fillMode: Image.PreserveAspectFit
        }

    }

    Button {
        id: homeicon
        height: 40
        width: 40
        visible: false

        //Button is 10 pix from the right side and from the top (x value is from bottom left corner so it is dependent on the image's width)
        y: 6
        x: parent.width - width - 10
        background: Text {
            text: "Back"
            color: "black"
            font.pixelSize: 15
        }

        //To return back to the home screen
        onClicked: {
            description.visible = false
            gallerybackground.height = gallerymain.height * 0.85
            homeicon.visible = true
            photoslider.visible = true
            searchbar.visible = true
            gallerytitle.visible = true
            galleryscroll.visible = true
            galleryframe.visible = true
            relatedimages.visible = false
            relatedscroll.visible = false
            homeicon.visible = false
            closereport.visible = true
            photonumber = 0
            refreshbutton.visible = true
            relatedimageslist.clear()
        }
    }

    Button {
        id: closereport
        height: 40
        width: 40
        visible: false

        //Button is 10 pix from the right side and from the top (x value is from bottom left corner so it is dependent on the image's width)
        y: 6
        x: parent.width - width - 10
        background: Text {
            text: "Close"
            color: "black"
            font.pixelSize: 15
        }
        onClicked: {
            closeGalleryReport()
        }
    }

}


