import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.0
import QtLocation 5.6
import QtPositioning 5.6
import QtQuick.Controls.Styles 1.4
import QtQuick.Window 2.2
import QtQml 2.2
import "../Components"
import "../Scenes"
import "../../Models"

Item {
    id: reportmain

    //    property var namedone: 0
    //    property var agedone: 0
    //    property var genderdone: 0
    //    property var genderotherdone: 0
    //    property var occupationdone: 0
    property var datedone: 0
    property var timedone: 0
    property var warddone: 0
    property var communedone: 0
    property var districtdone: 0
    property var provincedone: 0
    property var environmentdone: 0
    property var environmentotherdone: 0
    property var circumstancesdone: 0
    property var circumstancesotherdone: 0
    property var dominantdone: 0
    property var siteofbitedone: 0
    property var siteofbiteotherdone: 0
    property var informationvalid: 0
    property var newmarker: false
    property var locale: Qt.locale()
    property date currentTime: new Date()
    property var phototaken: 0
    property var timeofbite: ""
    property var finaltime: ""
    property var snakephoto: ""
    property var temptimebite: ""
    property var tempdominant: -1
    property var timeString: ""
    property date tempdatebite: new Date()
    property date temptempdatebite: new Date()
    property string environment: ""
    property string circumstance: ""
    property string bitesite: ""
    signal sliderChange()
    signal homeClicked()
    signal reportedBite()
    signal mapOpenBite()
    signal chooseMap()

    Component.onCompleted: {
        ColorScheme.placedone = 0
    }

    Image {
        id: biteimage
        width: reportmain.width * 0.9
        height: reportmain.height * 0.65
        z: mainquestions.z + 100
        anchors.horizontalCenter: reportmain.horizontalCenter
        anchors.verticalCenter: reportmain.verticalCenter
        visible: false
        source: "file:///" + ReportBiteDetails.biteurl
        fillMode: Image.PreserveAspectCrop
        Button {
            anchors.top: parent.top
            anchors.right: parent.right
            anchors.topMargin: parent.height * 0.05
            anchors.rightMargin: parent.width * 0.05
            height: parent.height * 0.1
            width: parent.width * 0.1
            background: Image {
                anchors.fill: parent
                source: IMG_LOC + "cancelButton.png"
                fillMode: Image.PreserveAspectCrop
            }
            onClicked: {
                biteimage.visible = false
                mainquestions.visible = true
                nextpage.visible = true
                homeicon.visible = true
            }
        }
    }

    CustomCalendar {
        id: calendar
        width: reportmain.width * 0.9
        height: reportmain.height * 0.65
        z: mainquestions.z + 100
        anchors.horizontalCenter: reportmain.horizontalCenter
        anchors.verticalCenter: reportmain.verticalCenter
        visible: false
        onClicked: {
            temptempdatebite = date
        }
    }

    Rectangle {
        id: calendarborder
        visible: false
        anchors.bottom: calendar.top
        z: mainquestions.z + 100
        anchors.horizontalCenter: calendar.horizontalCenter
        height: reportmain.height * 0.07
        width: calendar.width
        color: "#e5e5e5"
        Button {
            id: closer
            anchors.right: parent.right
            anchors.verticalCenter: parent.verticalCenter
            height: parent.height * 0.8
            width: calendar.width * 0.2
            background: Image {
                source: IMG_LOC + "cancelButton.png"
                fillMode: Image.PreserveAspectFit
                anchors.fill: parent
            }
            onClicked: {
                calendar.visible = false
                calendarborder.visible = false
                calendarproceed.visible = false
                mainquestions.visible = true
                nextpage.visible = true
                homeicon.visible = true
            }
        }
    }

    Rectangle {
        id: calendarproceed
        visible: false
        anchors.top: calendar.bottom
        z: mainquestions.z + 100
        anchors.horizontalCenter: calendar.horizontalCenter
        height: reportmain.height * 0.07
        width: calendar.width
        color: "#e5e5e5"
        Button {
            id: choosedate
            anchors.right: parent.right
            anchors.verticalCenter: parent.verticalCenter
            height: parent.height * 0.8
            width: calendar.width * 0.2
            background: Image {
                source: IMG_LOC + "proceed.png"
                fillMode: Image.PreserveAspectFit
                anchors.fill: parent
            }
            onClicked: {
                ColorScheme.calendarselector = "Choose another from calendar"
                chosendate.visible = true
                chosendate.height = mainquestions.height * (1/7)
                tempdatebite = temptempdatebite
                calendar.visible = false
                calendarborder.visible = false
                mainquestions.visible = true
                nextpage.visible = true
                calendarproceed.visible = false
                homeicon.visible = true
                datedone = 1
            }
        }
    }

    Rectangle {
        id: editbackground
        height: parent.height * 0.6
        width: parent.width * 0.8
        z: 10000000
        visible: false
        color: ColorScheme.secondary
        anchors {
            horizontalCenter: parent.horizontalCenter
            verticalCenter: parent.verticalCenter
        }

        Button {
            id: proceed
            anchors.bottom: editbackground.bottom
            anchors.bottomMargin: parent.height * 0.05
            height: parent.height * 0.1
            anchors.horizontalCenter: parent.horizontalCenter
            width: parent.width * 0.5
            background: Rectangle {
                anchors.fill: parent
                color: "#508030"
                Text {
                    anchors.fill: parent
                    text: "Proceed"
                    color: "black"
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    font.pixelSize: 12
                }
            }
            onClicked: {
                //proceeding
                finishedEdit()
            }
        }

        Flickable {

            anchors {
                left: parent.left
                right: parent.right
                top: parent.top
            }
            height: parent.height * 0.8
            clip: true
            interactive: true
            contentHeight: contentItem.childrenRect.height + 5
            contentWidth: parent.width

            Rectangle {
                anchors.fill: parent
                color: ColorScheme.secondary
            }

            Rectangle {
                id: title
                anchors {
                    top: parent.top
                    left: parent.left
                    topMargin: 5
                    leftMargin: 10
                }
                width: parent.width * 0.85
                height: 65
                Text {
                    anchors.fill: parent
                    horizontalAlignment: Text.AlignLeft
                    verticalAlignment: Text.AlignVCenter
                    text: "Update Details"
                    color: "black"
                    font.pixelSize: 17
                    font.bold: true
                }
                color: ColorScheme.secondary
            }

            Button {
                id: close
                anchors.verticalCenter: title.verticalCenter
                anchors.right: parent.right
                anchors.rightMargin: 15
                width: 20
                height: 20
                background: Image {
                    anchors.fill: parent
                    source: IMG_LOC + "cancelButton.png"
                    fillMode: Image.PreserveAspectCrop
                    horizontalAlignment: Image.AlignRight
                }
                onClicked: {
                    editbackground.visible = false
                    mainquestions.visible = true
                    homeicon.visible = true
                    nextpage.visible = true
                }
            }

            Rectangle {
                id: timevalidatetext
                anchors.top: title.bottom
                anchors.topMargin: 10
                anchors.left: parent.left
                height: mainquestions.height * (1/7)
                width: parent.width * 0.5
                color: mainquestions.color
                Text {
                    anchors.fill: parent
                    text: "Time:"
                    font.pixelSize: 15
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.verticalCenter: parent.verticalCenter
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                    wrapMode: Text.WordWrap
                }
            }

            Rectangle {
                id: timevalidate
                y: timevalidatetext.y
                anchors.right: parent.right
                height: mainquestions.height * (1/7)
                width: parent.width * 0.5
                color: mainquestions.color
                Text {
                    anchors.fill: parent
                    text: {
                        var date = new Date(timeofbite);
                        return Qt.formatTime(date, 'hh:mm')
                    }
                    font.pixelSize: 15
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.verticalCenter: parent.verticalCenter
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                    wrapMode: Text.WordWrap
                }
            }

            Rectangle {
                id: datevalidatetext
                anchors.top: timevalidatetext.bottom
                anchors.topMargin: 10
                anchors.left: parent.left
                height: mainquestions.height * (1/7)
                width: parent.width * 0.5
                color: mainquestions.color
                Text {
                    anchors.fill: parent
                    text: "Date:"
                    font.pixelSize: 15
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.verticalCenter: parent.verticalCenter
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                    wrapMode: Text.WordWrap
                }
            }

            Rectangle {
                id: datevalidate
                y: datevalidatetext.y
                anchors.right: parent.right
                height: mainquestions.height * (1/7)
                width: parent.width * 0.5
                color: mainquestions.color
                Text {
                    anchors.fill: parent
                    text:{
                        var date = new Date(timeofbite);
                        return date.toLocaleDateString('en-GB')
                    }
                    font.pixelSize: 15
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.verticalCenter: parent.verticalCenter
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                    wrapMode: Text.WordWrap
                }
            }

            Rectangle {
                id: environmentvalidatetext
                anchors.top: datevalidatetext.bottom
                anchors.topMargin: 10
                anchors.left: parent.left
                height: mainquestions.height * (1/7)
                width: parent.width * 0.5
                color: mainquestions.color
                Text {
                    anchors.fill: parent
                    text: "Environment:"
                    font.pixelSize: 15
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.verticalCenter: parent.verticalCenter
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                    wrapMode: Text.WordWrap
                }
            }

            Rectangle {
                id: environmentvalidate
                y: environmentvalidatetext.y
                anchors.right: parent.right
                height: mainquestions.height * (1/7)
                width: parent.width * 0.5
                color: mainquestions.color
                Text {
                    anchors.fill: parent
                    text: environment
                    font.pixelSize: 15
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.verticalCenter: parent.verticalCenter
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                    wrapMode: Text.WordWrap
                    font.capitalization: Font.Capitalize
                }
            }

            Rectangle {
                id: circumstancevalidatetext
                anchors.top: environmentvalidatetext.bottom
                anchors.topMargin: 10
                anchors.left: parent.left
                height: mainquestions.height * (1/7)
                width: parent.width * 0.5
                color: mainquestions.color
                Text {
                    anchors.fill: parent
                    text: "Circumstance:"
                    font.pixelSize: 15
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.verticalCenter: parent.verticalCenter
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                    wrapMode: Text.WordWrap
                }
            }

            Rectangle {
                id: circumstancevalidate
                y: circumstancevalidatetext.y
                anchors.right: parent.right
                height: mainquestions.height * (1/7)
                width: parent.width * 0.5
                color: mainquestions.color
                Text {
                    anchors.fill: parent
                    text: circumstance
                    font.pixelSize: 15
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.verticalCenter: parent.verticalCenter
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                    wrapMode: Text.WordWrap
                    font.capitalization: Font.Capitalize
                }
            }

            Rectangle {
                id: bitesitevalidatetext
                anchors.top: circumstancevalidatetext.bottom
                anchors.topMargin: 10
                anchors.left: parent.left
                height: mainquestions.height * (1/7)
                width: parent.width * 0.5
                color: mainquestions.color
                Text {
                    anchors.fill: parent
                    text: "Bite Side:"
                    font.pixelSize: 15
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.verticalCenter: parent.verticalCenter
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                    wrapMode: Text.WordWrap
                }
            }

            Rectangle {
                id: bitesitevalidate
                y: bitesitevalidatetext.y
                anchors.right: parent.right
                height: mainquestions.height * (1/7)
                width: parent.width * 0.5
                color: mainquestions.color
                Text {
                    anchors.fill: parent
                    text: bitesite
                    font.pixelSize: 15
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.verticalCenter: parent.verticalCenter
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                    wrapMode: Text.WordWrap
                    font.capitalization: Font.Capitalize
                }
            }

            Rectangle {
                id: dominantvalidatetext
                anchors.top: bitesitevalidatetext.bottom
                anchors.topMargin: 10
                anchors.left: parent.left
                height: mainquestions.height * (1/7)
                width: parent.width * 0.5
                color: mainquestions.color
                Text {
                    anchors.fill: parent
                    text: "Site of Bite:"
                    font.pixelSize: 15
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.verticalCenter: parent.verticalCenter
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                    wrapMode: Text.WordWrap
                }
            }

            Rectangle {
                id: doinantvalidate
                y: dominantvalidatetext.y
                anchors.right: parent.right
                height: mainquestions.height * (1/7)
                width: parent.width * 0.5
                color: mainquestions.color
                Text {
                    anchors.fill: parent
                    text: {
                        if (tempdominant === 1) {
                            return "Yes"
                        } else if (tempdominant === 0) {
                            return "No"
                        }
                    }
                    font.pixelSize: 15
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.verticalCenter: parent.verticalCenter
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                    wrapMode: Text.WordWrap
                }
            }

        }
    }

    Rectangle {

        id: reportfill
        color: ColorScheme.primary
        height: parent.height
        width: parent.width
        anchors.fill: parent

        Rectangle {

            id: mainquestions
            width: parent.width * 0.8
            height: parent.height * 0.85
            anchors {
                verticalCenter: parent.verticalCenter
                horizontalCenter: parent.horizontalCenter
            }
            color: ColorScheme.secondary

            Flickable {
                id: scroller
                anchors {
                    top: parent.top
                    left: parent.left
                    right: parent.right
                }

                ScrollBar.vertical: ScrollBar {
                    parent: scroller.parent
                    anchors.top: parent.top
                    anchors.bottom: parent.bottom
                    anchors.right: parent.right
                    active: true
                    policy: ScrollBar.AlwaysOn
                }


                height: parent.height
                clip: true
                interactive: true
                contentHeight: contentItem.childrenRect.height + 5
                contentWidth: parent.width


                Rectangle {
                    id: usecurrenttime
                    anchors.top: parent.top
                    anchors.topMargin: 10
                    Text {
                        anchors.fill: parent
                        text: "Use Current Time?"
                        font.pixelSize: 15
                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.verticalCenter: parent.verticalCenter
                        verticalAlignment: Text.AlignVCenter
                        horizontalAlignment: Text.AlignHCenter
                        wrapMode: Text.WordWrap
                    }
                    anchors.left: parent.left
                    height: mainquestions.height * (1/7)
                    width: parent.width * 0.5
                    color: mainquestions.color
                }

                CheckBox {
                    id: currenttime
                    anchors.top: parent.top
                    anchors.topMargin: 10
                    anchors.horizontalCenter: placeknown.horizontalCenter
                    height: mainquestions.height * (1/7)
                    width: height
                    checked: false
                    onCheckStateChanged: {
                        if (checked === true) {
                            timetextfield.visible = false
                            time.visible = false
                            chosendate.visible = false
                            datetextfield.visible = false
                            date.visible = false
                            timetextfield.height = 0
                            time.height = 0
                            datetextfield.height = 0
                            date.height = 0
                            timedone = 1
                            datedone = 1

                            timeString = currentTime.toTimeString()

                            console.log(currentTime.valueOf())
                            finaltime = currentTime.valueOf()
                            var dateString = currentTime.toDateString()
                            console.log(dateString)

                        } else if (checked === false) {
                            timetextfield.visible = true
                            time.visible = true
                            datetextfield.visible = true
                            //                            chosendate.visible = true
                            date.visible = true
                            timetextfield.height = mainquestions.height * (1/7)
                            time.height = mainquestions.height * (1/7)
                            datetextfield.height = mainquestions.height * (1/7)
                            date.height = mainquestions.height * (1/7)
                            timedone = 0
                            datedone = 0
                        }
                    }
                }

                Rectangle {
                    id: timetextfield
                    anchors.top: usecurrenttime.bottom
                    anchors.topMargin: 10
                    Text {
                        anchors.fill: parent
                        text: "Time of Bite:"
                        font.pixelSize: 15
                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.verticalCenter: parent.verticalCenter
                        verticalAlignment: Text.AlignVCenter
                        horizontalAlignment: Text.AlignHCenter
                    }
                    anchors.left: parent.left
                    height: mainquestions.height * (1/7)
                    width: parent.width * 0.5
                    color: mainquestions.color
                }

                TextField {
                    id: time
                    focus: true
                    placeholderText: qsTr("HH:MM")
                    inputMethodHints: Qt.ImhTime
                    font.pixelSize: 15
                    activeFocusOnPress: true
                    anchors.right: parent.right
                    anchors.top: currenttime.bottom
                    anchors.topMargin: 10
                    width: parent.width * 0.4
                    anchors.rightMargin: parent.width * 0.1
                    height: mainquestions.height * (1/7)
                    validator: RegExpValidator {regExp: /^\d{2}[./:]\d{2}$/}
                    onEditingFinished: {
                        timedone = 1
                    }
                }

                Rectangle {
                    id: datetextfield
                    anchors.top: timetextfield.bottom
                    anchors.topMargin: 10
                    Text {
                        anchors.fill: parent
                        text: "Date of Bite:"
                        font.pixelSize: 15
                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.verticalCenter: parent.verticalCenter
                        verticalAlignment: Text.AlignVCenter
                        horizontalAlignment: Text.AlignHCenter
                    }
                    anchors.left: parent.left
                    height: mainquestions.height * (1/7)
                    width: parent.width * 0.5
                    color: mainquestions.color
                }

                Rectangle {
                    id: chosendate
                    anchors.top: time.bottom
                    anchors.horizontalCenter: time.horizontalCenter
                    anchors.topMargin: 10
                    height: 0
                    visible: false
                    color: "#e5e5e5"
                    Text {
                        id: datetextbox
                        anchors.fill: parent
                        text: {
                            if (currenttime.checked === true) {
                                return ""
                            } else {
                                tempdatebite.toLocaleDateString("en-GB")
                            }
                        }
                        color: "black"
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                        wrapMode: Text.WordWrap
                        font.pixelSize: 15
                    }
                }

                Button {
                    id: date
                    anchors.right: parent.right
                    anchors.top: chosendate.visible ? chosendate.bottom : time.bottom
                    anchors.topMargin: 10
                    width: parent.width * 0.4
                    anchors.rightMargin: parent.width * 0.1
                    height: mainquestions.height * (1/7)
                    background: Rectangle {
                        anchors.fill: parent
                        color: "#e5e5e5"
                        Text {
                            anchors.fill: parent
                            text: ColorScheme.calendarselector
                            font.pixelSize: 13
                            anchors.horizontalCenter: parent.horizontalCenter
                            anchors.verticalCenter: parent.verticalCenter
                            verticalAlignment: Text.AlignVCenter
                            wrapMode: Text.WordWrap
                            horizontalAlignment: Text.AlignHCenter
                        }
                    }
                    onClicked: {
                        calendar.visible = true
                        calendarborder.visible = true
                        calendarproceed.visible = true
                        mainquestions.visible = false
                        nextpage.visible = false
                        homeicon.visible = false
                    }
                }

                Rectangle {
                    id: placetextfield
                    y: placeknown.y
                    anchors.topMargin: 10
                    Text {
                        anchors.fill: parent
                        text: "Place of Bite:"
                        font.pixelSize: 15
                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.verticalCenter: parent.verticalCenter
                        verticalAlignment: Text.AlignVCenter
                        horizontalAlignment: Text.AlignHCenter
                    }
                    anchors.left: parent.left
                    height: mainquestions.height * (1/7)
                    width: parent.width * 0.5
                    color: mainquestions.color
                }

                Rectangle {
                    id: blankfill1
                    anchors.top: placetextfield.bottom
                    anchors.topMargin: ward.visible ? 40:0
                    anchors.left: parent.left
                    height: ward.visible? mainquestions.height * (4/7):0
                    width: parent.width * 0.5
                    color: mainquestions.color
                }

                ComboBox {
                    id: placeknown
                    model: ["Current Location", "Locate on Map", "Not Known"]
                    font.pixelSize: 13
                    currentIndex: -1
                    anchors.right: parent.right
                    anchors.top: date.visible ? date.bottom:currenttime.bottom
                    anchors.topMargin: 10
                    width: parent.width * 0.4
                    anchors.rightMargin: parent.width * 0.1
                    height: mainquestions.height * (1/7)
                    onCurrentIndexChanged: {

                        if (placeknown.currentIndex === 0) {
                            ColorScheme.placedone = 1
                            ReportBiteDetails.currentlocation = true
                            console.log(ReportBiteDetails.currentlocation)
                            mapchooser.height = 0
                            mapchooserfiller.height = 0
                            mapchooser.visible = false
                            mapchooser.visible = false
                        }

                        if (placeknown.currentIndex === 1) {
                            //choose on map
                            mapchooser.height = mainquestions.height * (1/7)
                            ReportBiteDetails.currentlocation = false
                            mapchooserfiller.height = mainquestions.height * (1/7)
                            mapchooser.visible = true
                            mapchooser.visible = true
                        }

                        if (placeknown.currentIndex === 2) {
                            //invis
                            mapchooser.height = 0
                            mapchooserfiller.height = 0
                            ReportBiteDetails.currentlocation = false
                            mapchooser.visible = false
                            mapchooser.visible = false
                        }
                    }
                }

                Rectangle {
                    id: mapchooserfiller
                    anchors.top: placetextfield.bottom
                    anchors.topMargin: 10
                    anchors.left: parent.left
                    height: mainquestions.height * (1/7)
                    width: parent.width * 0.5
                    color: mainquestions.color
                    visible: false
                }

                Button {
                    id: mapchooser
                    anchors.top: placeknown.bottom
                    anchors.topMargin: 10
                    anchors.right: parent.right
                    anchors.rightMargin: parent.width * 0.1
                    height: mainquestions.height * (1/7)
                    width: parent.width * 0.4
                    visible: false
                    background: Rectangle {
                        color: "#e5e5e5"
                        Text {
                            anchors.fill: parent
                            horizontalAlignment: Text.AlignHCenter
                            verticalAlignment: Text.AlignVCenter
                            text: ColorScheme.mapselector
                            wrapMode: Text.WordWrap
                            color: "black"
                            font.pixelSize: 13
                        }
                    }
                    onClicked: {
                        chooseMap()
                    }
                }

                TextField {
                    id: ward
                    focus: true
                    placeholderText: qsTr("Ward")
                    font.pixelSize: 15
                    activeFocusOnPress: true
                    anchors.right: parent.right
                    anchors.top: placeknown.bottom
                    anchors.topMargin: ward.visible ? 10:0
                    width: parent.width * 0.4
                    anchors.rightMargin: parent.width * 0.1
                    height: 0
                    visible: false
                    onEditingFinished: {
                        warddone = 1
                    }
                }

                TextField {
                    id: commune
                    focus: true
                    placeholderText: qsTr("Commune")
                    font.pixelSize: 15
                    activeFocusOnPress: true
                    anchors.right: parent.right
                    anchors.top: ward.bottom
                    anchors.topMargin: ward.visible ? 10:0
                    width: parent.width * 0.4
                    anchors.rightMargin: parent.width * 0.1
                    height: 0
                    visible: false
                    onEditingFinished: {
                        communedone = 1
                    }
                }

                TextField {
                    id: district
                    focus: true
                    placeholderText: qsTr("District/Town")
                    font.pixelSize: 15
                    activeFocusOnPress: true
                    anchors.right: parent.right
                    anchors.top: commune.bottom
                    anchors.topMargin: ward.visible ? 10:0
                    width: parent.width * 0.4
                    anchors.rightMargin: parent.width * 0.1
                    height: 0
                    visible: false
                    onEditingFinished: {
                        districtdone = 1
                    }
                }

                TextField {
                    id: provincialcode
                    focus: true
                    placeholderText: qsTr("Province Code")
                    font.pixelSize: 15
                    activeFocusOnPress: true
                    anchors.right: parent.right
                    anchors.top: district.bottom
                    anchors.topMargin: ward.visible ? 10:0
                    width: parent.width * 0.4
                    anchors.rightMargin: parent.width * 0.1
                    height: 0
                    visible: false
                    onEditingFinished: {
                        provincedone = 1
                    }
                }

                ComboBox {
                    id: environmentfield
                    model: ["Garden", "Field", "Rice Paddy", "Plantation", "House", "Not Known", "Other"]
                    font.pixelSize: 13
                    currentIndex: -1
                    anchors.right: parent.right
                    anchors.top: mapchooser.visible ? mapchooser.bottom:placeknown.bottom
                    anchors.topMargin: 10
                    width: parent.width * 0.4
                    anchors.rightMargin: parent.width * 0.1
                    height: mainquestions.height * (1/7)

                    onCurrentIndexChanged: {
                        environmentdone = 1
                        if (environment.currentIndex === 6) {
                            environmentother.height = mainquestions.height * (1/7)
                            environmentother.visible = true
                        } else {
                            environmentother.height = 0
                            environmentother.visible = false
                        }
                    }
                }

                Rectangle {
                    id: environmenttextfield
                    y: environmentfield.y
                    anchors.topMargin: 10
                    Text {
                        anchors.fill: parent
                        text: "Environment:"
                        font.pixelSize: 15
                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.verticalCenter: parent.verticalCenter
                        verticalAlignment: Text.AlignVCenter
                        horizontalAlignment: Text.AlignHCenter
                    }
                    anchors.left: parent.left
                    height: mainquestions.height * (1/7)
                    width: parent.width * 0.5
                    color: mainquestions.color
                }

                TextField {
                    id: environmentother
                    focus: true
                    placeholderText: qsTr("Other (specify)")
                    font.pixelSize: 15
                    activeFocusOnPress: true
                    anchors.right: parent.right
                    anchors.top: environmentfield.bottom
                    anchors.topMargin: 10
                    width: parent.width * 0.4
                    anchors.rightMargin: parent.width * 0.1
                    height: 0
                    visible: false
                    onEditingFinished: {
                        environmentotherdone = 1
                    }
                }

                Rectangle {
                    id: environmentothertextfield
                    anchors.top: environmenttextfield.bottom
                    anchors.topMargin: 10
                    anchors.left: parent.left
                    width: parent.width * 0.5
                    color: mainquestions.color
                    height: mainquestions.height * (1/7)
                }

                Rectangle {
                    id: circumstancestextfield
                    anchors.top: environmentother.visible? environmentothertextfield.bottom:environmenttextfield.bottom
                    anchors.left: parent.left
                    height: mainquestions.height * (1/7)
                    anchors.topMargin: 10
                    width: parent.width * 0.5
                    color: mainquestions.color
                    Text {
                        anchors.fill: parent
                        text: "Circumstances:"
                        font.pixelSize: 15
                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.verticalCenter: parent.verticalCenter
                        verticalAlignment: Text.AlignVCenter
                        horizontalAlignment: Text.AlignHCenter
                    }
                }

                ComboBox {
                    id: circumstances
                    model: ["Working", "Walking", "Recreational", "Catching Snakes", "Unknown", "Other"]
                    font.pixelSize: 15
                    currentIndex: -1
                    anchors.right: parent.right
                    anchors.top: environmentother.visible ? environmentother.bottom:environmentfield.bottom
                    anchors.topMargin: 10
                    width: parent.width * 0.4
                    anchors.rightMargin: parent.width * 0.1
                    height: mainquestions.height * (1/7)
                    onCurrentIndexChanged: {
                        circumstancesdone = 1
                        if (circumstances.currentIndex === 5) {
                            circumstancesother.height = mainquestions.height * (1/7)
                            circumstancesother.visible = true
                        } else {
                            circumstancesother.height = 0
                            circumstancesother.visible = false
                        }
                    }
                }

                Rectangle {
                    id: circumstancesothertextfield
                    anchors.top: circumstancestextfield.bottom
                    anchors.left: parent.left
                    height: mainquestions.height * (1/7)
                    anchors.topMargin: 10
                    width: parent.width * 0.5
                    color: mainquestions.color
                }

                TextField {
                    id: circumstancesother
                    focus: true
                    placeholderText: qsTr("Other (specify)")
                    font.pixelSize: 15
                    activeFocusOnPress: true
                    anchors.right: parent.right
                    anchors.top: circumstances.bottom
                    anchors.topMargin: 10
                    width: parent.width * 0.4
                    anchors.rightMargin: parent.width * 0.1
                    height: 0
                    visible: false
                    onEditingFinished: {
                        circumstancesotherdone = 1
                    }
                }

                Rectangle {
                    id: siteofbitetextfield
                    anchors.top: circumstancesother.visible? circumstancesothertextfield.bottom:circumstancestextfield.bottom
                    anchors.left: parent.left
                    height: mainquestions.height * (1/7)
                    anchors.topMargin: 10
                    width: parent.width * 0.5
                    color: mainquestions.color
                    Text {
                        anchors.fill: parent
                        text: "Site of Bite:"
                        font.pixelSize: 15
                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.verticalCenter: parent.verticalCenter
                        verticalAlignment: Text.AlignVCenter
                        horizontalAlignment: Text.AlignHCenter
                    }
                }

                ComboBox {
                    id: siteofbite
                    model: ["Hand", "Foot", "Unknown", "Other"]
                    font.pixelSize: 15
                    currentIndex: -1
                    anchors.right: parent.right
                    anchors.top: circumstancesother.visible ? circumstancesother.bottom:circumstances.bottom
                    anchors.topMargin: 10
                    width: parent.width * 0.4
                    anchors.rightMargin: parent.width * 0.1
                    height: mainquestions.height * (1/7)
                    onCurrentIndexChanged: {
                        siteofbitedone = 1
                        if (siteofbite.currentIndex === 3) {
                            siteofbiteother.height = mainquestions.height * (1/7)
                            siteofbiteother.visible = true
                            siteofbiteother.topMargin = 10
                        } else {
                            siteofbiteother.height = 0
                            siteofbiteother.visible = false
                            siteofbiteother.topMargin = 10
                        }
                    }
                }

                TextField {
                    id: siteofbiteother
                    focus: true
                    placeholderText: qsTr("Other (specify)")
                    font.pixelSize: 15
                    activeFocusOnPress: true
                    anchors.right: parent.right
                    anchors.top: siteofbite.bottom
                    anchors.topMargin: 10
                    width: parent.width * 0.4
                    anchors.rightMargin: parent.width * 0.1
                    height: 0
                    visible: false
                    onEditingFinished: {
                        siteofbiteotherdone = 1
                    }
                }

                Rectangle {
                    id: siteofbiteothertextfield
                    height: siteofbiteother.visible ? mainquestions.height * (1/7):0
                    width: parent.width * 0.5
                    anchors.top: siteofbitetextfield.bottom
                    anchors.left: parent.left
                    color: mainquestions.color
                    anchors.topMargin: siteofbiteother.visible ? 10:0
                }

                ComboBox {
                    id: sitedominant
                    model: ["Dominant", "Non-dominant"]
                    font.pixelSize: 15
                    currentIndex: -1
                    anchors.right: parent.right
                    anchors.top: siteofbiteother.visible ? siteofbiteother.bottom : siteofbite.bottom
                    anchors.topMargin: 10
                    width: parent.width * 0.4
                    anchors.rightMargin: parent.width * 0.1
                    height: mainquestions.height * (1/7)
                    onCurrentIndexChanged: {
                        dominantdone = 1
                    }
                }

                Rectangle {
                    id: sitedominanttextfield
                    anchors.top: siteofbiteother.visible? siteofbiteothertextfield.bottom:siteofbitetextfield.bottom
                    anchors.left: parent.left
                    height: mainquestions.height * (1/7)
                    anchors.topMargin: 10
                    width: parent.width * 0.5
                    color: mainquestions.color
                    Text {
                        anchors.fill: parent
                        text: "Bite Side:"
                        font.pixelSize: 15
                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.verticalCenter: parent.verticalCenter
                        verticalAlignment: Text.AlignVCenter
                        horizontalAlignment: Text.AlignHCenter
                    }
                }

                Rectangle {
                    id: identificationtextfield
                    anchors.top: sitedominanttextfield.bottom
                    anchors.left: parent.left
                    height: mainquestions.height * (1/7)
                    anchors.topMargin: 10
                    width: parent.width * 0.5
                    color: mainquestions.color
                    Text {
                        anchors.fill: parent
                        text: "Identification Page:"
                        font.pixelSize: 15
                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.verticalCenter: parent.verticalCenter
                        verticalAlignment: Text.AlignVCenter
                        horizontalAlignment: Text.AlignHCenter
                    }
                }

                Button {
                    id: identification
                    anchors.right: parent.right
                    anchors.top: sitedominant.bottom
                    height: mainquestions.height * (1/7)
                    width: parent.width * 0.4
                    anchors.rightMargin: parent.width * 0.1
                    anchors.topMargin: 10
                    background: Rectangle {
                        anchors.fill: parent
                        height: mainquestions.height * (1/7)
                        anchors.topMargin: 10
                        width: parent.width * 0.5
                        color: "#e5e5e5"
                        Text {
                            anchors.fill: parent
                            text: "Identification"
                            font.pixelSize: 14
                            anchors.horizontalCenter: parent.horizontalCenter
                            anchors.verticalCenter: parent.verticalCenter
                            verticalAlignment: Text.AlignVCenter
                            horizontalAlignment: Text.AlignHCenter
                            wrapMode: Text.WordWrap
                        }
                    }
                    onClicked: {
                        //stuff when clicked
                        var identificationScene = SCENE_LOC + "IdentificationPage.qml"
                        nav_scene.pushScene(Qt.resolvedUrl(identificationScene),false,{})
                    }
                }

                Rectangle {
                    id: filler
                    anchors.top: identification.bottom
                    anchors.left: parent.left
                    anchors.right: parent.right
                    height: 10
                    color: mainquestions.color
                }

                //Code for taking a photo of the bite
                //                Rectangle {
                //                    id: photoofbitetextfield
                //                    anchors.top: sitedominanttextfield.bottom
                //                    anchors.left: parent.left
                //                    height: mainquestions.height * (1/7)
                //                    anchors.topMargin: 10
                //                    width: parent.width * 0.5
                //                    color: mainquestions.color
                //                    Text {
                //                        anchors.fill: parent
                //                        text: "Photo of Bite:"
                //                        font.pixelSize: 15
                //                        anchors.horizontalCenter: parent.horizontalCenter
                //                        anchors.verticalCenter: parent.verticalCenter
                //                        verticalAlignment: Text.AlignVCenter
                //                        horizontalAlignment: Text.AlignHCenter
                //                    }
                //                }

                //                Button {
                //                    id: imagepreview
                //                    anchors.top: sitedominant.bottom
                //                    height: mainquestions.height * ColorScheme.multiple
                //                    anchors.rightMargin: parent.width * 0.1
                //                    anchors.right: parent.right
                //                    anchors.topMargin: 10
                //                    width: parent.width * 0.4
                //                    visible: ReportBiteDetails.imagevis
                //                    background: Rectangle {
                //                        anchors.fill: parent
                //                        height: mainquestions.height * (1/7)
                //                        anchors.topMargin: 10
                //                        width: parent.width * 0.5
                //                        color: "#e5e5e5"
                //                        Text {
                //                            anchors.fill: parent
                //                            text: "Preview Photo"
                //                            font.pixelSize: 15
                //                            anchors.horizontalCenter: parent.horizontalCenter
                //                            anchors.verticalCenter: parent.verticalCenter
                //                            verticalAlignment: Text.AlignVCenter
                //                            horizontalAlignment: Text.AlignHCenter
                //                            wrapMode: Text.WordWrap
                //                        }
                //                    }
                //                    onClicked: {
                //                        //stuff when clicked
                //                        console.log("BITE IMAGE OPEN")
                //                        biteimage.visible = true
                //                        mainquestions.visible = false
                //                        nextpage.visible = false
                //                        homeicon.visible = false
                //                    }
                //                }

                //                Button {
                //                    id: photoofbite
                //                    anchors.top: imagepreview.bottom
                //                    anchors.right: parent.right
                //                    height: mainquestions.height * (1/7)
                //                    width: parent.width * 0.4
                //                    anchors.rightMargin: parent.width * 0.1
                //                    anchors.topMargin: imagepreview.visible ? 10:0
                //                    background: Rectangle {
                //                        anchors.fill: parent
                //                        height: mainquestions.height * (1/7)
                //                        anchors.topMargin: 10
                //                        width: parent.width * 0.5
                //                        color: "#e5e5e5"
                //                        Text {
                //                            anchors.fill: parent
                //                            text: ColorScheme.photoselector
                //                            font.pixelSize: 14
                //                            anchors.horizontalCenter: parent.horizontalCenter
                //                            anchors.verticalCenter: parent.verticalCenter
                //                            verticalAlignment: Text.AlignVCenter
                //                            horizontalAlignment: Text.AlignHCenter
                //                            wrapMode: Text.WordWrap
                //                        }
                //                    }
                //                    onClicked: {
                //                        //stuff when clicked
                //                        console.log("Open Camera")
                //                        var cameraScene = SCENE_LOC + "ReportBiteCamera.qml"
                //                        nav_scene.pushScene(Qt.resolvedUrl(cameraScene),true,{})
                //                    }

                //                }



                //Code for gallery identification
                //                Rectangle {
                //                    id: speciesknowntextfield
                //                    Text {
                //                        anchors.fill: parent
                //                        text:  GalleryReportData.speciestext
                //                        font.pixelSize: 15
                //                        anchors.horizontalCenter: parent.horizontalCenter
                //                        anchors.verticalCenter: parent.verticalCenter
                //                        verticalAlignment: Text.AlignVCenter
                //                        horizontalAlignment: Text.AlignHCenter
                //                    }

                //                    color: mainquestions.color
                //                    anchors.left: parent.left
                //                    y: speciesknown.y
                //                    anchors.topMargin: 10
                //                    width: parent.width * 0.5
                //                    height: mainquestions.height * (1/7)
                //                }

                //                ComboBox {
                //                    id: speciesknown
                //                    model: ["Known", "Unknown"]
                //                    font.pixelSize: 15
                //                    currentIndex: 1
                //                    anchors.right: parent.right
                //                    anchors.top: photoofbite.bottom
                //                    anchors.topMargin: 10
                //                    width: parent.width * 0.4
                //                    anchors.rightMargin: parent.width * 0.1
                //                    height: mainquestions.height * (1/7)
                //                    visible: GalleryReportData.speciesknownvisible
                //                    Component.onCompleted:  {
                //                        speciesknown.visible = true
                //                    }

                //                    onCurrentIndexChanged: {
                //                        if (speciesknown.currentText === "Known") {
                //                            gallery.visible = true
                //                            gallery.height = mainquestions.height * (1/7)
                //                        } else {
                //                            gallery.visible = false
                //                            gallery.height = 0
                //                        }
                //                    }
                //                    onAccepted: {
                //                        if (speciesknown.currentText === "Known") {
                //                            gallery.visible = true
                //                            gallery.height = mainquestions.height * (1/7)
                //                        } else {
                //                            gallery.visible = false
                //                            gallery.height = 0
                //                        }
                //                    }
                //                    onFocusChanged: {
                //                        if (speciesknown.currentText === "Known") {
                //                            gallery.visible = true
                //                            gallery.height = mainquestions.height * (1/7)
                //                        } else {
                //                            gallery.visible = false
                //                            gallery.height = 0
                //                        }
                //                    }
                //                    onCurrentTextChanged: {
                //                        if (speciesknown.currentText === "Known") {
                //                            gallery.visible = true
                //                            gallery.height = mainquestions.height * (1/7)
                //                        } else {
                //                            gallery.visible = false
                //                            gallery.height = 0
                //                        }
                //                    }
                //                }

                //                Rectangle {
                //                    id: snakechosenbox
                //                    visible: GalleryReportData.snakevisible
                //                    anchors.fill: speciesknown
                //                    color: "#BDBDBD"
                //                    Text {
                //                        id: snakechosentext
                //                        anchors.fill: parent
                //                        text: GalleryReportData.snakechosen
                //                        color: "black"
                //                        horizontalAlignment: Text.AlignHCenter
                //                        verticalAlignment: Text.AlignVCenter
                //                        wrapMode: Text.WordWrap
                //                        font.pixelSize: 15
                //                    }
                //                }


                //                Button {
                //                    id: gallery
                //                    anchors.right: parent.right
                //                    anchors.rightMargin: parent.width * 0.1
                //                    anchors.top: speciesknown.bottom
                //                    anchors.topMargin: 10
                //                    height: 0
                //                    width: parent.width * 0.4
                //                    visible: false
                //                    background: Rectangle {
                //                        anchors.fill: parent
                //                        Text {
                //                            anchors.fill: parent
                //                            horizontalAlignment: Text.AlignHCenter
                //                            verticalAlignment: Text.AlignVCenter
                //                            text: GalleryReportData.gallerytext
                //                            wrapMode: Text.WordWrap
                //                        }
                //                    }
                //                    onClicked: {
                //                        var galleryReport = SCENE_LOC + "GalleryReportScene.qml"
                //                        nav_scene.pushScene(Qt.resolvedUrl(galleryReport),false,{})
                //                    }
                //                }



            }
        }


        Button {
            id: homeicon
            height: 30
            width: 30

            //Button is 10 pix from the right side and from the top (x value is from bottom left corner so it is dependent on the image's width)
            y: 10
            x: parent.width - width - 10
            background: Image {
                source: IMG_LOC + "homeIcon.png"
                fillMode: Image.Stretch
            }

            //To return back to the home screen
            onClicked: {
                confirmdiscard.visible = true
            }
        }

        Rectangle {
            id: confirmdiscard
            anchors {
                verticalCenter: mainquestions.verticalCenter
                horizontalCenter: mainquestions.horizontalCenter
            }
            width: mainquestions.width * 0.9
            height: mainquestions.width * 0.8
            visible: false
            color: "#d4d4d4"
            Rectangle {
                anchors.top: parent.top
                anchors.left: parent.left
                anchors.right: parent.right
                height: parent.height * 0.5
                color: parent.color
                Text {
                    text: "Are you sure you want to exit?<br>All changes will be discarded."
                    font.pixelSize: 15
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.verticalCenter: parent.verticalCenter
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                }
            }
            Button {
                id: yes
                anchors.left: parent.left
                anchors.bottom: parent.bottom
                anchors.leftMargin: 10
                anchors.bottomMargin: 10
                width: parent.width * 0.4
                height: parent.width * 0.4
                background: Rectangle {
                    Text {
                        text: "Yes"
                        font.pixelSize: 13
                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.verticalCenter: parent.verticalCenter
                        verticalAlignment: Text.AlignVCenter
                        horizontalAlignment: Text.AlignHCenter
                        color: "black"
                    }
                    anchors.fill: parent
                    color: "#ccff90"
                    border.width: 4
                    radius: 4
                    border.color: "#64dd17"
                }
                onClicked: {
                    homeClicked()
                    ColorScheme.photoselector = "Take a Photo"
                    ReportBiteDetails.imagevis = false
                    ReportBiteDetails.biteurl =  ""
                    ColorScheme.multiple = 0
                }
            }
            Button {
                id: no
                anchors.right: parent.right
                anchors.bottom: parent.bottom
                anchors.rightMargin: 10
                anchors.bottomMargin: 10
                width: parent.width * 0.4
                height: parent.width * 0.4
                background: Rectangle {
                    Text {
                        text: "No"
                        font.pixelSize: 13
                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.verticalCenter: parent.verticalCenter
                        verticalAlignment: Text.AlignVCenter
                        horizontalAlignment: Text.AlignHCenter
                        color: "white"
                    }
                    anchors.fill: parent
                    color: "#e96868"
                    border.width: 4
                    radius: 4
                    border.color: "#e74f4e"
                }
                onClicked: {
                    confirmdiscard.visible = false
                }
            }
        }


        Button {
            id: nextpage
            width:25
            height: 25
            anchors.right: parent.right
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 10
            background: Image {
                source: IMG_LOC + "arrowRightIcon.png"
                fillMode: Image.PreserveAspectCrop
            }
            onClicked: {
                if (/*namedone === 1 && agedone === 1 && genderdone === 1 && occupationdone === 1 &&*/ datedone === 1 && timedone === 1 && ColorScheme.placedone === 1 && environmentdone === 1 && circumstancesdone === 1 && siteofbitedone === 1 && dominantdone === 1) {
                    if (ward.visible === true) {
                        if (warddone === 1 && communedone === 1 && districtdone === 1 && provincedone === 1){
                            if (environmentother.visible === true) {
                                if (environmentotherdone === 1) {
                                    if (circumstancesother.visible === true) {
                                        if (circumstancesotherdone === 1) {
                                            if (siteofbiteother.visible === true) {
                                                if (siteofbiteotherdone === 1) {
                                                    //INSERT DONE
                                                    informationvalid = 1
                                                }
                                            } else {
                                                //INSERT DONE
                                                informationvalid = 1
                                            }
                                        }
                                    } else if (siteofbiteother.visible === true) {
                                        if (siteofbiteotherdone === 1) {
                                            //INSERT DONE
                                            informationvalid = 1
                                        }
                                    } else {
                                        //INSERT DONE
                                        informationvalid = 1
                                    }
                                }
                            }
                        }
                    } else if (environmentother.visible === true) {
                        if (environmentotherdone === 1) {
                            if (circumstancesother.visible === true) {
                                if (circumstancesotherdone === 1) {
                                    if (siteofbiteother.visible === true) {
                                        if (siteofbiteotherdone === 1) {
                                            //INSERT DONE
                                            informationvalid = 1
                                        }
                                    } else {
                                        //INSERT DONE
                                        informationvalid = 1
                                    }
                                }
                            } else if (siteofbiteother.visible === true) {
                                if (siteofbiteotherdone === 1) {
                                    //INSERT DONE
                                    informationvalid = 1
                                }
                            } else {
                                //INSERT DONE
                                informationvalid = 1
                            }
                        }
                    } else if (ward.visible === true) {
                        if (warddone === 1 && communedone === 1 && districtdone === 1 && provincedone === 1){
                            if (environmentother.visible === true) {
                                if (environmentotherdone === 1) {
                                    if (circumstancesother.visible === true) {
                                        if (circumstancesotherdone === 1) {
                                            if (siteofbiteother.visible === true) {
                                                if (siteofbiteotherdone === 1) {
                                                    //INSERT DONE
                                                    informationvalid = 1
                                                }
                                            } else {
                                                //INSERT DONE
                                                informationvalid = 1
                                            }
                                        }
                                    } else if (siteofbiteother.visible === true) {
                                        if (siteofbiteotherdone === 1) {
                                            //INSERT DONE
                                            informationvalid = 1
                                        }
                                    } else {
                                        //INSERT DONE
                                        informationvalid = 1
                                    }
                                }
                            } else if (circumstancesother.visible === true) {
                                if (circumstancesotherdone === 1) {
                                    if (siteofbiteother.visible === true) {
                                        if (siteofbiteotherdone === 1) {
                                            //INSERT DONE
                                            informationvalid = 1
                                        }
                                    } else {
                                        //INSERT DONE
                                        informationvalid = 1
                                    }
                                }
                            } else if (siteofbiteother.visible === true) {
                                if (siteofbiteotherdone === 1) {
                                    //INSERT DONE
                                    informationvalid = 1
                                }
                            } else {
                                //INSERT DONE
                                informationvalid = 1
                            }
                        }
                    } else if (environmentother.visible === true) {
                        if (environmentotherdone === 1) {
                            if (circumstancesother.visible === true) {
                                if (circumstancesotherdone === 1) {
                                    if (siteofbiteother.visible === true) {
                                        if (siteofbiteotherdone === 1) {
                                            //INSERT DONE
                                            informationvalid = 1
                                        }
                                    } else {
                                        //INSERT DONE
                                        informationvalid = 1
                                    }
                                }
                            } else if (siteofbiteother.visible === true) {
                                if (siteofbiteotherdone === 1) {
                                    //INSERT DONE
                                    informationvalid = 1
                                }
                            } else {
                                //INSERT DONE
                                informationvalid = 1
                            }
                        }
                    } else {
                        //INSERT DONE
                        informationvalid = 1
                    }
                } else {
                    console.log("Not all fields entered")
                }

                //After all the necessary validation checks, insert the code to be exectued ->
                if (informationvalid === 1) {
                    console.log(IdentificationDetails.descriptiontext)
                    //edit background visible here
                    editbackground.visible = true
                    mainquestions.visible = false
                    homeicon.visible = false
                    nextpage.visible = false

                    //Preparing variables
                    if (currenttime.checked === true) {
                        timeofbite = finaltime
                    } else {
                        temptimebite = time.getText(0,100)
                        var year = tempdatebite.getFullYear()
                        var month = tempdatebite.getMonth()
                        var date = tempdatebite.getDate()
                        var hours = temptimebite.slice(0,2)
                        var minutes = temptimebite.slice(3,5)
                        var tempdate = new Date()
                        tempdate.setHours(hours, minutes, 0)
                        tempdate.setFullYear(year, month, date)

                        console.log(tempdate)
                        timeofbite = tempdate.valueOf()
                    }

                    //Insert other code to send the data once all the data is valid
                    environment = environmentfield.currentText.toLowerCase()
                    circumstance = circumstances.currentText.toLowerCase()
                    bitesite = siteofbite.currentText.toLowerCase()

                    if (sitedominant.currentIndex === 0) {
                        tempdominant = 1
                    } else if (sitedominant.currentIndex === 1) {
                        tempdominant = 0
                    }

                    console.log("IDENTIFICATION DETAILS OPTION CHOSEN")
                    console.log(IdentificationDetails.optionchosen)

                    if (IdentificationDetails.optionchosen === 1) {
                        //camera
                        IdentificationDetails.descriptiontext = "N/A"
                        snakephoto = ""
                    } else if (IdentificationDetails.optionchosen === 2) {
                        snakephoto = ReportBiteDetails.snakechosen
                        IdentificationDetails.descriptiontext = "N/A"
                    } else if (IdentificationDetails.optionchosen === 3) {
                        snakephoto = ""
                    }
                } else {
                    //Tells user not all fields entered
                    invalidinformation.visible = true
                }

            }
        }

        Rectangle {
            id: invalidinformation
            visible: false
            width: mainquestions.width * 0.9
            anchors {
                verticalCenter: mainquestions.verticalCenter
                horizontalCenter: mainquestions.horizontalCenter
            }
            height: width
            color: "#d4d4d4"
            border.width: 0
            border.color: "black"
            Text {
                text: "Not all fields have been<br>entered correctly. Click this message<br>to dismiss it."
                font.pixelSize: 15
                anchors {
                    verticalCenter: parent.verticalCenter
                    horizontalCenter: parent.horizontalCenter
                }
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                color: "black"
            }


            MouseArea {
                anchors.fill: parent
                hoverEnabled: true
                acceptedButtons: Qt.LeftButton | Qt.RightButton
                onEntered: {
                    parent.border.width = 3
                }
                onExited: {
                    parent.border.width = 0
                }
                onClicked: {
                    invalidinformation.visible = false
                }
            }
        }

    }

    function finishedEdit() {
        console.log("DESCRIPTION")
        console.log(IdentificationDetails.descriptiontext)
        if (snakephoto === "") {
            ReportBiteDetails.body = {
                "longitude": "",
                "latitude": "",
                "description": IdentificationDetails.descriptiontext,
                "timeOfBite": timeofbite,
                "environment": environment,
                "circumstance": circumstance,
                "siteOfBite": bitesite,
                "isAtDominantSide": tempdominant,
            }
        } else if (IdentificationDetails.optionchosen === 1) {
            ReportBiteDetails.body = {
                "longitude": "",
                "latitude": "",
                "description": IdentificationDetails.descriptiontext,
                "timeOfBite": timeofbite,
                "snake": snakephoto,
                "environment": environment,
                "circumstance": circumstance,
                "siteOfBite": bitesite,
                "photos": "1",
                "image": IdentificationDetails.camerapath,
                "isAtDominantSide": tempdominant,
            }
        } else {
            ReportBiteDetails.body = {
                "longitude": "",
                "latitude": "",
                "description": IdentificationDetails.descriptiontext,
                "timeOfBite": timeofbite,
                "snake": snakephoto,
                "environment": environment,
                "circumstance": circumstance,
                "siteOfBite": bitesite,
                "isAtDominantSide": tempdominant,
            }
        }



        console.log("HERE")
        mapOpenBite()
    }

    //    Connections {
    //        ignoreUnknownSignals: true
    //        target: ReportBiteCamera
    //        onPhotoChosen: {
    //            photoofbite.visible = false
    //            photodone.visible = true
    //        }
    //    }


}
