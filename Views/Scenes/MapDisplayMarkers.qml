import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.0
import QtLocation 5.6
import QtPositioning 5.6
import QtQuick.Controls.Styles 1.4
import "../Components"
import "../../Models"

Item {
    id: root



    property alias newmap: newmap
    property MapCircle mapCircle
    property var initialLoad: true
    property var markerCoordinate: "14.0583,108.2772" //myPositionSource.position.coordinate
    property var startCoordinate: "14.0583,108.2772"

    property var longitudetext: ""
    property var latitudetext: ""
    property var timeOfBitetext: ""
    property var snaketext: ""

    signal closeMap()

    Component.onCompleted: {
        //function here
        loading.visible = true
        makeMarkers()
    }

    Rectangle {
        id: loading
        anchors.fill: root
        color: ColorScheme.primary
        z: root.z + 100
        visible: true

        AnimatedImage {
            id: loadingspinner
            source: LOAD_LOC + "logo.gif"
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter
            playing: true
            width: root.width * 0.3 //the pic is default 32x32 size so just make this in multiples of 32
            height: width
            fillMode: Image.PreserveAspectFit
        }

    }

    Rectangle {
        anchors.fill: parent
        color: "white"
    }

    //Test to retrieve position
    PositionSource {
        active: true
        onPositionChanged: {
        }
    }


    Plugin {
        id: mapPlugin
        name: "osm" // "mapboxgl", "esri", ...
        PluginParameter {
            name: "osm.useragent"
            value: "Qt Maps Example Application"
        }
    }

    //My position source
    PositionSource {
        id: myPositionSource
        active: true
        updateInterval: 4000
        onPositionChanged: {
            console.log("POSITION: " + myPositionSource.position.coordinate.latitude + "," + myPositionSource.position.coordinate.longitude)

            if (myPositionSource.position.latitudeValid && myPositionSource.position.longitudeValid){
                if (initialLoad) {
                    newmap.center = myPositionSource.position.coordinate
                    initialLoad = false
                }

                //                myMarker.coordinate = myPositionSource.position.coordinate
            } else {
                console.log("invalid position: " + myPositionSource.sourceError)
            }
        }
    }

    Map {
        id: newmap
        anchors.fill: parent
        plugin: mapPlugin
        zoomLevel: 16
        gesture.enabled: true // allows you to zoom in and out using pinch touch

        Component.onCompleted: {
            //Example on how to instantiate QMLObject on runtime
            mapCircle = Qt.createQmlObject('import QtLocation 5.6; MapCircle{}',newmap)
            mapCircle.center = myPositionSource.position.coordinate
            mapCircle.radius = 30
            mapCircle.color = "#c0461414"
            mapCircle.border.color = "#000000"
            mapCircle.border.width = 2
            newmap.addMapItem(mapCircle)
        }


        //Searching places and displaying results in map
        PlaceSearchModel {
            id: searchModel
            plugin: mapPlugin
            searchTerm: ""
            searchArea: QtPositioning.circle(newmap.center,1000)
            Component.onCompleted: {
                update()
            }
        }

        MapItemView {
            id:mapViewSearchPlace
            model: searchModel
            delegate: MapQuickItem {
                coordinate: place.location.coordinate
                anchorPoint.x: image.width * 0.5
                anchorPoint.y: image.height
                sourceItem: Rectangle {
                    anchors.fill: parent
                    id: itemView
                    Column {
                        Image{id:image; source: IMG_LOC + "placeindicator.png"}
                        Text {
                            id: myAddress
                            //                            text: place.location.address.text
                            visible: false
                        }
                    }
                    Button {        //Changed to Button; for some reason ClearButton or any custom qml for button is not being rendered on ios
                        id: markerButton
                        width: image.width
                        height: image.height
                        anchors.top: parent.top
                        anchors.left: parent.left
                        background: Rectangle{
                            anchors.fill: parent
                            color: "#00000000"
                        }
                        contentItem: Rectangle {
                            anchors.fill: parent
                            color: "#00000000"
                        }
                        onPressed: {
                            console.log("Marker:" + place.location.address.text)
                            myAddress.visible = !myAddress.visible
                        }
                    }
                }
            }
        }

    }

    /** ----------- Components ----------------*/

    Rectangle {
        id: returnPositionButtonFill
        anchors.fill: returnPositionButton
        color: "#bee3db"
        radius: 20
    }

    Button{
        id: returnPositionButton
        width: 40
        height: 40
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 10
        background: Image {
            source: IMG_LOC + "currentIcon.png"
            fillMode: Image.Stretch
        }
        onClicked: {
            newmap.center = myPositionSource.position.coordinate
        }
    }

    //function to draw markers from bite list

    function makeMarkers() {

        var http = new XMLHttpRequest()
        var url = RequestManager.baseUrl + "bite/find"
        var params = "&populate=false"
        http.open("POST",url,true)
        http.setRequestHeader("Content-Type","application/x-www-form-urlencoded")
        http.setRequestHeader("Authorization", "Bearer " + AuthManager.userBearerToken)
        http.onreadystatechange = function() {
            if (http.readyState === 4) {
                if (http.status === 200) {
                    //                    console.log("connects")
                    var fulldata = JSON.parse(http.responseText)
                    for (var i=0; i < fulldata.length; i ++) {
                        userID(UserDetails.username, i)
                    }

                    closeLoading()

                }
            }
        }
        http.send(params)
    }

    function closeLoading() {
        loading.visible = false
    }

    function userID(number, index) {
        var http = new XMLHttpRequest()
        var newurl = RequestManager.baseUrl + "user/find"
        console.log("NUMBER HERE")
        console.log(number)
        var newparams = "&populate=false&username=" + number
        http.open("POST",newurl,true)
        http.setRequestHeader("Content-Type","application/x-www-form-urlencoded")
        http.setRequestHeader("Authorization", "Bearer " + AuthManager.userBearerToken)
        http.onreadystatechange = function() {
            if (http.readyState === 4) {
                if (http.status === 200) {
                    //                    console.log("CONNECTS")
                    var data = http.responseText
                    var testdata = JSON.parse(data)
                    //                    console.log("HERE")
                    //                    console.log(testdata[0].id)
                    var userattribute = testdata[0].id
                    drawmarkers(index, userattribute)
                }
            }
        }
        http.send(newparams)
    }

    function drawmarkers (index, idno) {
        var http = new XMLHttpRequest()
        var newnewurl = RequestManager.baseUrl + "bite/find"
        console.log(idno)
        var newnewparams = "&populate=false&reporter" + idno
        http.open("GET", newnewurl, true)
        http.setRequestHeader("Content-Type","application/json")
        http.setRequestHeader("Authorization", "Bearer " + AuthManager.userBearerToken)
        http.onreadystatechange = function() {
            if (http.readyState === 4) {
                if (http.status === 200) {
                    console.log("CONNECTS DRAWMARKERS")
                    var parsedata = JSON.parse(http.responseText)
                    for (var x=0; x < parsedata.length; x++) {
                        console.log(http.responseText)
                        var mapMarker = mapItemComponent.createObject(newmap, {})
                        newmap.addMapItem(mapMarker)
                        console.log(newmap.toCoordinate(parsedata[x].latitude, parsedata[x].longitude))
                        var point = QtPositioning.coordinate(parsedata[x].latitude, parsedata[x].longitude)
                        console.log(point)
                        //                        var point = newmap.toCoordinate(parsedata[x].latitude, parsedata[x].longitude)
                        mapMarker.coordinate = point
                        mapMarker.longitude = parsedata[x].longitude
                        mapMarker.latitude = parsedata[x].latitude
                        mapMarker.timeOfBite = parsedata[x].timeOfBite
                        mapMarker.snakename = parsedata[x].snake.name
                        console.log(parsedata[x].snake.name)
                        console.log(mapMarker.snakename)
                    }
                }
            } else {
                //doesnt work
            }
        }
        http.send(newnewparams)
    }

    Component {
        id: mapItemComponent
        MapQuickItem {
            id: bitemarker

            property var longitude: ""
            property var latitude: ""
            property var timeOfBite: ""
            property var snakename: ""


            anchorPoint.x: image2.width * 0.5
            anchorPoint.y: image2.height * 0.5

            sourceItem: Button {
                onClicked: {
                    biteinfobox.visible = true
                    root.longitudetext = bitemarker.longitude
                    root.latitudetext = bitemarker.latitude
                    root.timeOfBitetext = bitemarker.timeOfBite
                    root.snaketext = bitemarker.snakename
                    console.log(root.snaketext)

                }
                background: Image {
                    id: image2
                    source: IMG_LOC + "marker2.png"
                }
            }

            coordinate: myPositionSource.position.coordinate
            visible: true
        }
    }



    Rectangle {
        id: biteinfobox
        visible: false
        width: newmap.width * 0.8
        height: width
        anchors.horizontalCenter: newmap.horizontalCenter
        anchors.verticalCenter: newmap.verticalCenter
        color: ColorScheme.primary

        Rectangle {
            id: boxtitle
            anchors.top: parent.top
            anchors.left: parent.left
            anchors.right: parent.right
            color: parent.color
            height: parent.height * 0.25
            Text {
                anchors.fill: parent
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                font.pixelSize: 18
                color: "black"
                text: "Marker Information"
            }
        }

        Rectangle {
            id: longtext
            anchors.top: boxtitle.bottom
            anchors.left: parent.left
            anchors.right: parent.right
            color: parent.color
            height: parent.height * 0.17
            anchors.topMargin: parent.height * 0.04
            Text {
                anchors.top: parent.top
                anchors.bottom: parent.bottom
                anchors.left: parent.left
                width: parent.width * 0.5
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                font.pixelSize: 15
                color: "black"
                text: "Longitude: "
            }
            Text {
                anchors.top: parent.top
                anchors.bottom: parent.bottom
                anchors.right: parent.right
                width: parent.width * 0.5
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                font.pixelSize: 15
                color: "black"
                text: (Math.round(longitudetext * 100) / 100)
            }
        }

        Rectangle {
            id: lattext
            anchors.top: longtext.bottom
            anchors.left: parent.left
            anchors.right: parent.right
            color: parent.color
            height: parent.height * 0.17
            Text {
                anchors.top: parent.top
                anchors.bottom: parent.bottom
                anchors.left: parent.left
                width: parent.width * 0.5
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                font.pixelSize: 15
                color: "black"
                text: "Latitude: "
            }
            Text {
                anchors.top: parent.top
                anchors.bottom: parent.bottom
                anchors.right: parent.right
                width: parent.width * 0.5
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                font.pixelSize: 15
                color: "black"
                text: (Math.round(latitudetext * 100) / 100)
            }
        }

        Rectangle {
            id: timetext
            anchors.top: lattext.bottom
            anchors.left: parent.left
            anchors.right: parent.right
            color: parent.color
            height: parent.height * 0.17
            Text {
                anchors.top: parent.top
                anchors.bottom: parent.bottom
                anchors.left: parent.left
                width: parent.width * 0.5
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                font.pixelSize: 15
                color: "black"
                text: "Time Of Bite: "
            }
            Text {
                anchors.top: parent.top
                anchors.bottom: parent.bottom
                anchors.right: parent.right
                width: parent.width * 0.5
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                font.pixelSize: 15
                color: "black"
                text: {
                    var date = new Date(timeOfBitetext);
                    return date.toLocaleDateString('en-GB')
                }
            }
        }

        Rectangle {
            id: snakedisplaytext
            anchors.top: timetext.bottom
            anchors.left: parent.left
            anchors.right: parent.right
            color: parent.color
            height: parent.height * 0.17
            Text {
                anchors.top: parent.top
                anchors.bottom: parent.bottom
                anchors.left: parent.left
                width: parent.width * 0.5
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                font.pixelSize: 15
                color: "black"
                text: "Snake: "
            }
            Text {
                anchors.top: parent.top
                anchors.bottom: parent.bottom
                anchors.right: parent.right
                width: parent.width * 0.5
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                font.pixelSize: 15
                color: "black"
                text: root.snaketext
                wrapMode: Text.WordWrap
            }
        }

        Button {
            id: close
            anchors.top: parent.top
            anchors.right: parent.right
            height: parent.height * 0.1
            width: height
            anchors.topMargin: parent.height * 0.05
            anchors.rightMargin: parent.width * 0.05
            background: Image {
                source: IMG_LOC + "cancelButton.png"
                anchors.fill: parent
                fillMode: Image.PreserveAspectCrop
            }
            onClicked: {
                biteinfobox.visible = false
            }
        }

    }


    /** ---------- Non Map Related -----------*/

    //Map Info
    Rectangle {
        id: rect_info
        width: parent.width
        height: childrenRect.height * 1.5
        anchors.right: parent.right
        color: "#e6f5ff"  //old color was "#95ffffff" - semi transparent
        opacity: 0.65

        Button {
            id: closemap
            anchors.right: parent.right
            anchors.top: parent.top
            width: 40
            height: width
            background: Image {
                anchors.fill: parent
                source: IMG_LOC + "cross.png"
                fillMode: Image.PreserveAspectFit
            }
            onClicked: {
                closeMap()
            }
        }

        TextField {
            id: textInput_searchPlace
            anchors.verticalCenter: searchbutton.verticalCenter
            anchors.left: rect_info.left
            anchors.leftMargin: 10
            width: 150
            placeholderText: "Enter Text Here"
            color: "#461414"
            height: searchbutton.height
            font.pixelSize: searchbutton.text.font.pixelSize /*(root.width * 0.02 > 10) ? root.width * 0.02 : 10*/
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            onEditingFinished: {
                searchModel.searchTerm = textInput_searchPlace.text
                searchModel.update()
                textInput_searchPlace.placeholderText = "Search the Map"
                textInput_searchPlace.focus = false
            }
        }
        Button {
            id: searchbutton
            anchors.top: parent.top
            anchors.topMargin: 5
            font.pixelSize: 13
            anchors.left: textInput_searchPlace.right
            anchors.leftMargin: 10
            text: "Search"
            onClicked: {
                searchModel.searchTerm = textInput_searchPlace.text
                searchModel.update()
            }
        }
        Button {
            id: filterbutton
            anchors.top: textInput_searchPlace.bottom
            anchors.topMargin: 5
            font.pixelSize: 13
            anchors.left: rect_info.left
            anchors.leftMargin: 10
            text: "Filters"
            onClicked: {
                //filter
            }
        }
    }


}
