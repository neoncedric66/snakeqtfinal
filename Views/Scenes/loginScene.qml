import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.0
import QtLocation 5.6
import QtPositioning 5.6
import "../Components"
import "../Scenes"
import "../../Models"
import PJMCHeatMap 0.2

Item {
    id: loginpagemain

    property var usernamedone: 0
    property var passworddone: 0
    property var usernamelength: ""
    property var passwordlength: ""
    property var validated: 0

    signal validInfo()
    signal registerPage()

    Rectangle {
        id: loginfill
        anchors.fill: parent
        color: ColorScheme.primary
    }

    Image {
        id: logo
        source: IMG_LOC + "companylogo.png"
        width: parent.width * 0.4
        height: width
        fillMode: Image.PreserveAspectFit
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottom: username.top
        anchors.bottomMargin: 20
        z: logintext.z + 100000
    }

    Rectangle {
        id: logintext
        anchors.bottom: usernametextfield.top
        anchors.bottomMargin: 45
        anchors.horizontalCenter: parent.horizontalCenter
        width: parent.width * 0.8
        height: parent.height * 0.1
        color: ColorScheme.primary
    }

    //Rectangle to background the login text fields
    Rectangle {
        id: loginbackground
        anchors.top: logintext.bottom
        anchors.horizontalCenter: parent.horizontalCenter
        height: parent.height * 0.65
        width: logintext.width
        color: ColorScheme.primary
    }

    Image {
        id: usernametextfield
        anchors.bottom: parent.verticalCenter
        anchors.bottomMargin: 65
        anchors.right: username.left
        anchors.rightMargin: 10
        width: parent.width * 0.1
        height: parent.height * 0.1
        //        source: "file:TPV_people.jpg"
        source: LOGIN_LOC + "username.png"
        fillMode: Image.PreserveAspectFit
    }

    TextField {
        id: username
        placeholderText: qsTr("Enter name")
        font.pixelSize: 15
        activeFocusOnPress: true
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottom: parent.verticalCenter
        anchors.bottomMargin: 65
        width: parent.width * 0.45
        height: parent.height * 0.1
        inputMethodHints: Qt.ImhNoAutoUppercase
        //        validator: RegExpValidator {regExp: /^[a-zA-Z ]*$/}
        background: Rectangle {
            id: usernamehighlight
            anchors.fill: parent
            color: "white"
        }
        onEditingFinished: {
            usernamelength = getText(0,1000)
            if (usernamelength === "") {
                usernamedone = 0
            } else {
                usernamedone = 1
                usernamehighlight.color = "white"
            }
        }
    }

    Image {
        id: passwordtextfield
        anchors.top: usernametextfield.top
        anchors.topMargin: 90
        anchors.right: username.left
        anchors.rightMargin: 10
        width: parent.width * 0.1
        height: parent.height * 0.1
        source: LOGIN_LOC + "password.png"
        fillMode: Image.PreserveAspectFit
    }

    TextField {
        id: password
        placeholderText: qsTr("Enter password")
        font.pixelSize: 15
        activeFocusOnPress: true
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: username.top
        anchors.topMargin: 90
        width: parent.width * 0.45
        echoMode: TextInput.Password
        height: parent.height * 0.1
        background: Rectangle {
            id: passwordhighlight
            anchors.fill: parent
            color: "white"
        }
        onEditingFinished: {
            passwordlength = getText(0,1000)
            if (passwordlength === "") {
                passworddone = 0
            } else {
                passworddone = 1
                passwordhighlight.color = "white"
            }
        }
    }

    Rectangle {
        id: errormessage
        anchors.top: password.bottom
        anchors.topMargin: 10
        anchors.horizontalCenter: password.horizontalCenter
        height: parent.height * 0.06
        width: parent.width * 0.3
        color: ColorScheme.primary
        visible: false
        Text {
            font.pixelSize: 15
            text: "Wrong Credentials"
            color: "#D9321F"
            anchors.fill: parent
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }
    }

    Rectangle {
        id: loading
        anchors.fill: loginpagemain
        color: ColorScheme.primary
        z: logintext.z + 100
        visible: false

        AnimatedImage {
            id: loadingspinner
            source: LOAD_LOC + "logo.gif"
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter
            playing: true
            width: loginpagemain.width * 0.3 //the pic is default 32x32 size so just make this in multiples of 32
            height: width
            fillMode: Image.PreserveAspectFit
        }

    }

    Button {
        id: nextpage
        anchors.top: password.bottom
        anchors.topMargin: 30
        anchors.horizontalCenter: parent.horizontalCenter
        height: parent.height * 0.15
        width: parent.width * 0.65 //0.75
        background: Image {
            anchors.fill: parent
            source: LOGIN_LOC + "loginbutton.png"
            fillMode: Image.PreserveAspectFit
        }
        onClicked: {
            console.log(passworddone)
            console.log(usernamedone)
            usernamelength = username.getText(0,1000)
            passwordlength = password.getText(0,1000)



            if (passworddone == 1 && usernamedone === 1) {
                //cont stuff after all fields entered correctly
                usernamelength = username.getText(0,10000)
                ReportBiteDetails.username = usernamelength
                console.log(ReportBiteDetails.username)
                errormessage.visible = true

                loading.visible = true
                password.visible = false
                username.visible = false
                nextpage.visible = false
                register.visible = false
                logo.visible = false

                //                var http = new XMLHttpRequest()
                //                var url = "http://68.183.236.185/oauth/client/create/"         //change IP address later - localhost is for computer IP address is for phone
                //                var params = "name=Snake App&redirectURI=www.test.com"
                //                //            var url = "http://localhost:1337/user"
                //                //                var data = {"username":usernamelength,"password":passwordlength}

                //                //            http.setRequestHeader("Authorization", "92yIbHNMqdPzfd4N30rC6MilmqWgeVLzPvm4hdy9RdhVMkJQAGJY1IdIXhvhjUiA9BjwkFepLUHUC7Xi") //When you add tokens to your api, you will need to set the authorization of the content-type

                //                http.open("POST",url,true)
                //                //                http.setRequestHeader("Content-type", "application/x-www-form-urlencoded")

                //                http.setRequestHeader("Content-type", "application/x-www-form-urlencoded")
                //                //                http.setRequestHeader("Postman-Token", "7fdc13d9-3628-4add-9830-8a3456142940")



                //                //                console.log(params)
                //                //                //                console.log(http.status)

                //                http.onreadystatechange = function() {
                //                    if (http.readyState === 4) {
                //                        if (http.status === 200) {
                //                            var data = JSON.parse(http.responseText)
                //                            UserDetails.access_token = data.access_token
                //                            console.log("OK")
                //                            validInfo()
                //                        } else {
                //                            console.log("Error recieved, status is: " + http.status)
                //                            usernamehighlight.color = "#ff6961"
                //                            passwordhighlight.color = "#ff6961"
                //                            username.remove(0,100)
                //                            password.remove(0,100)
                //                            loading.visible = false
                //                            password.visible = true
                //                            username.visible = true
                //                            nextpage.visible = true
                //                            register.visible = true
                //                            logo.visible = true
                //                            errormessage.visible = true
                //                        }
                //                    } else {
                //                        console.log("Error, unable to connect, Ready state is: " + http.readyState)
                //                    }
                //                }


                //                http.send(params)
                //            }



                var data = AuthManager.login(usernamelength, passwordlength)
                console.log(data)

            } else if (passworddone == 1 && usernamedone == 0) {
                //highlight only username text field
                usernamehighlight.color = "#ff6961"
            } else if (usernamedone == 1 && passworddone == 0) {
                //highlight only password text field
                passwordhighlight.color = "#ff6961"
            } else if (passworddone == 0 && usernamedone == 0){
                usernamehighlight.color = "#ff6961"
                passwordhighlight.color = "#ff6961"
            }
        }
    }

    Button {
        id: register
        anchors.horizontalCenter: nextpage.horizontalCenter
        anchors.top: nextpage.bottom
        anchors.topMargin: 10
        width: nextpage.width
        height: loginfill.height * 0.15
        background: Image {
            anchors.fill: parent
            source: LOGIN_LOC + "register.png"
            fillMode: Image.PreserveAspectCrop
        }
        onClicked: {
            registerPage()
        }
    }

    //    Timer {
    //        id: timer
    //        interval: 2000
    //        repeat: false
    //        onTriggered: {
    //            console.log("end")
    //            if (UserDetails.successlogin === "true") {
    //                console.log(UserDetails.successlogin)
    //                validInfo()
    //            } else {
    //                console.log(UserDetails.successlogin)
    //            }
    //        }
    //    }

    Connections {
        ignoreUnknownSignals: true
        target: AuthManager
        onAMLoginUserSuccessful: {
            UserManager.getProfile();

            //            var http = new XMLHttpRequest()
            //            var newurl = "http://68.183.236.185/user/find"
            //            var newparams = "username=" + usernamelength
            //            http.open("POST",newurl,true)
            //            http.setRequestHeader("Content-Type","application/x-www-form-urlencoded")
            //            http.setRequestHeader("Authorization", "Bearer " + AuthManager.userBearerToken)

            //            http.onreadystatechange = function() {
            //                if (http.readyState === 4) {
            //                    if (http.status === 200) {
            //                        console.log("CONNECTS")
            //                        var data = http.responseText
            //                        var testdata = JSON.parse(data)
            //                        var userattribute = testdata[0].type
            //                        UserDetails.usertype = userattribute
            //                        UserDetails.username = usernamelength
            //                        console.log(UserDetails.usertype)
            //                        validInfo()
            //                    }
            //                }
            //            }

            //            http.send(newparams)

        }
        onAMLoginUserFailed: {
            reset()
        }
    }

    Connections {
        ignoreUnknownSignals: true
        target: UserManager
        onUMGetProfileSuccessful: {
            UserDetails.usertype = UserManager.currentUser.type
            UserDetails.username = usernamelength
            console.log(UserDetails.usertype)
            validInfo()
        }
        onUMGetProfileFailed: {
            reset()
        }
    }

    function reset() {
        errormessage.visible = true
        username.remove(0,1000)
        password.remove(0,1000)
        usernamedone = 0
        passworddone = 0
        loading.visible = false
        password.visible = true
        username.visible = true
        nextpage.visible = true
        register.visible = true
        logo.visible = true
    }

    PJMCGradientPalette{
        id: palette
        width: parent.width
        Component.onCompleted: {
            palette.setColorAt(0.45, Qt.blue);
            palette.setColorAt(0.55, Qt.cyan);
            palette.setColorAt(0.65, Qt.green);
            palette.setColorAt(0.85, Qt.yellow);
            palette.setColorAt(1.0, Qt.red);
        }
    }

    PJMCHeatMap{
        id: heatmap
        opacity: 128
        radius: 60
        palette: palette
        Component.onCompleted: {
            createMainCanvas(1000, 1000)
        }
        onMainCanvasChanged: {

        }
    }

}

