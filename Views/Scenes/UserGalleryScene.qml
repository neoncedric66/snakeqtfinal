import QtQuick 2.7
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.0
import QtLocation 5.6
import QtPositioning 5.6
import QtQuick.Controls.Styles 1.4
import QtQuick.Window 2.2

import "../Components"
import "../Scenes"


Item {
    id: usergallerymain

    Rectangle {

        id: usergallerybackground
        color: "#4682B4"
        anchors.fill: parent


        Text {
            id: gallerytitle
            anchors.bottom: galleryframe.top
            anchors.bottomMargin: 10
            anchors.horizontalCenter: galleryframe.horizontalCenter
            text: "User Gallery"
            font.pixelSize: 15
            font.family: "Open Sans"
            font.bold: true
            font.underline: true
        }

        Rectangle {
            id: galleryframe
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter
            height: parent.height * 0.7
            width: parent.width * 0.8
            color: "#B4CDCD"
        }
    }
}
