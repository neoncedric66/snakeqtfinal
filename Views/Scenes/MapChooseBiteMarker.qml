import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.0
import QtLocation 5.6
import QtPositioning 5.6
import QtQuick.Controls.Styles 1.4
import "../Components"
import "../../Models"

Item {
    id: root

    property alias map: map
    property MapCircle mapCircle
    property var initialLoad: true
    property var aTaskBar
    property var markerCoordinate: "14.0583,108.2772" //myPositionSource.position.coordinate
    property var startCoordinate: "14.0583,108.2772"
    property var latitude: ""
    property var longitude: ""
    property var point: ""
    signal closeMarkerMap()

    Component.onCompleted: {
        latitude = myPositionSource.position.coordinate.latitude
        longitude = myPositionSource.position.coordinate.longitude
    }


    Rectangle {
        anchors.fill: parent
        color: "white"
    }

    //Test to retrieve position
    PositionSource {
        active: true
        onPositionChanged: {
            console.log(latitude)
            console.log(longitude)
            latitude = myPositionSource.position.coordinate.latitude
            longitude = myPositionSource.position.coordinate.latitude
        }
    }


    Plugin {
        id: mapPlugin
        name: "osm" // "mapboxgl", "esri", ...
        PluginParameter {
            name: "osm.useragent"
            value: "Qt Maps Example Application"
        }
    }

    //My position source
    PositionSource {
        id: myPositionSource
        active: true
        updateInterval: 4000
        onPositionChanged: {
            console.log("POSITION: " + myPositionSource.position.coordinate.latitude + "," + myPositionSource.position.coordinate.longitude)

            if (myPositionSource.position.latitudeValid && myPositionSource.position.longitudeValid){
                if (initialLoad) {
                    map.center = myPositionSource.position.coordinate
                    initialLoad = false
                }

                //                myMarker.coordinate = myPositionSource.position.coordinate
            } else {
                console.log("invalid position: " + myPositionSource.sourceError)
            }
        }
    }

    Map {
        id: map
        anchors.fill: parent
        plugin: mapPlugin
        zoomLevel: 16
        gesture.enabled: true // allows you to zoom in and out using pinch touch

//        Component.onCompleted: {
//            //Example on how to instantiate QMLObject on runtime
//            mapCircle = Qt.createQmlObject('import QtLocation 5.6; MapCircle{}',map)
//            mapCircle.center = myPositionSource.position.coordinate
//            mapCircle.radius = 30
//            mapCircle.color = "#c0461414"
//            mapCircle.border.color = "#000000"
//            mapCircle.border.width = 2
//            map.addMapItem(mapCircle)
//            console.log("checked")
//        }

        //To receive touch in map area
        MouseArea {
            anchors.fill: parent
            onClicked: {
                point = map.toCoordinate(Qt.point(mouse.x,mouse.y))
                console.log(point.longitude)
                console.log(point.latitude)
                marker.visible = true
                marker.coordinate = point
            }
        }


        //Red pin
        MapQuickItem {  //For simplicity, we just used one map item and update it's position upon selection, but you may create a mapquickitem for every selected location
            id:marker
            anchorPoint.x: image3.width * 0.5
            anchorPoint.y: image3.height
            sourceItem: Image {
                id: image3
                source: IMG_LOC + "marker.png"
            }
            visible: false
            coordinate: myPositionSource.position.coordinate
        }


        //Searching places and displaying results in map
        PlaceSearchModel {
            id: searchModel
            plugin: mapPlugin
            searchTerm: ""
            searchArea: QtPositioning.circle(map.center,1000)
            Component.onCompleted: {
                update()
            }
        }

        MapItemView {
            id:mapViewSearchPlace
            model: searchModel
            delegate: MapQuickItem {
                coordinate: place.location.coordinate
                anchorPoint.x: image.width * 0.5
                anchorPoint.y: image.height
                sourceItem: Rectangle {
                    anchors.fill: parent
                    id: itemView
                    Column {
                        Image{id:image; source: IMG_LOC + "placeindicator.png"}
                        Text {
                            id: myAddress
                            //                            text: place.location.address.text
                            visible: false
                        }
                    }
                    Button {        //Changed to Button; for some reason ClearButton or any custom qml for button is not being rendered on ios
                        id: markerButton
                        width: image.width
                        height: image.height
                        anchors.top: parent.top
                        anchors.left: parent.left
                        background: Rectangle{
                            anchors.fill: parent
                            color: "#00000000"
                        }
                        contentItem: Rectangle {
                            anchors.fill: parent
                            color: "#00000000"
                        }
                        onPressed: {
                            console.log("Marker:" + place.location.address.text)
                            myAddress.visible = !myAddress.visible
                        }
                    }
                }
            }
        }

    }

    //Helper function to update route waypoints
    function updateRouteQuery(startCoordinate, endCoordinate) {
        routeQuery.clearWaypoints()
        routeQuery.addWaypoint(startCoordinate)
        routeQuery.addWaypoint(endCoordinate)
        routeQuery.travelModes = RouteQuery.CarTravel
        routeQuery.routeOptimizations = RouteQuery.FastestRoute
        routeModel.update()
    }

    /** ----------- Components ----------------*/
    //Component used to draw the pink pins
    Component {
        id: mapItemComponent
        MapQuickItem {
            id:marker
            anchorPoint.x: image2.width * 0.5
            anchorPoint.y: image2.height
            sourceItem: Image {
                id: image2
                source: IMG_LOC + "marker2.png"
            }
            visible: true
        }
    }

    //Component used to draw the route
    Component {
        id: routeDelegate
        MapRoute {
            route: routeData
            line.color: "#461414"
            line.width: 4
            smooth: true
            opacity: 0.9
        }
    }

    //Component used to return user to start position (will change later to make it users location)

    Rectangle {
        id: returnPositionButtonFill
        anchors.fill: returnPositionButton
        color: "#bee3db"
        radius: 20
    }

    Button{
        id: returnPositionButton
        width: 40
        height: 40
        anchors.left: parent.left
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 10
        background: Image {
            source: IMG_LOC + "currentIcon.png"
            fillMode: Image.Stretch
        }
        onClicked: {
            map.center = myPositionSource.position.coordinate
        }
        //        Component.onCompleted: {
        //            console.log(taskBar)
        //        }
    }

    Button {
        id: proceed
        width: 40
        height: 40
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 10
        background: Image {
            fillMode: Image.Stretch
            source: IMG_LOC + "proceed.png"
        }
        onClicked: {
            closeMarkerMap()
            ColorScheme.placedone = 1
            console.log(marker.coordinate.longitude)
            console.log(marker.coordinate.latitude)
            var testlongitude = marker.coordinate.longitude
            var testlatitude = marker.coordinate.latitude
            ReportBiteDetails.latitude = testlatitude
            ReportBiteDetails.longitude = testlongitude
            ColorScheme.mapselector = "Choose another area on the map"
        }
    }



    /** ---------- Non Map Related -----------*/

    //Map Info
    Rectangle {
        id: rect_info
        width: parent.width
        height: childrenRect.height * 1.5
        anchors.right: parent.right
        color: "#e6f5ff"  //old color was "#95ffffff" - semi transparent
        opacity: 0.65
        //        CheckBox {
        //            id: cb_setPin
        //            anchors.right: parent.right
        //            anchors.top: parent.top
        //            checked: false
        //            text: "Add Pins"
        //            onCheckStateChanged: {
        //            }
        //        }
        //        ShadowText {
        //            id: text_pinCoordinate
        //            anchors.top: cb_setPin.bottom
        //            anchors.right: cb_setPin.right
        //            text: ""
        //            visible: false
        //            color: "#461414"
        //            font.pixelSize: (root.width * 0.02 > 10) ? root.width * 0.02 : 10
        //            horizontalAlignment: Text.Right
        //            layer.enabled: false
        //        }

        Button {
            id: close
            anchors.right: parent.right
            anchors.top: parent.top
            width: 40
            height: width
            background: Image {
                anchors.fill: parent
                source: IMG_LOC + "cross.png"
                fillMode: Image.PreserveAspectFit
            }
            onClicked: {
                closeMarkerMap()
            }
        }

        TextField {
            id: textInput_searchPlace
            anchors.verticalCenter: searchbutton.verticalCenter
            anchors.left: rect_info.left
            anchors.leftMargin: 10
            width: 150
            placeholderText: "Enter Text Here"
            color: "#461414"
            height: searchbutton.height
            font.pixelSize: searchbutton.text.font.pixelSize /*(root.width * 0.02 > 10) ? root.width * 0.02 : 10*/
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            onEditingFinished: {
                searchModel.searchTerm = textInput_searchPlace.text
                searchModel.update()
                textInput_searchPlace.placeholderText = "Enter Text Here"
                textInput_searchPlace.focus = false
            }
        }
        Button {
            id: searchbutton
            anchors.top: parent.top
            anchors.topMargin: 5
            font.pixelSize: 13
            anchors.left: textInput_searchPlace.right
            anchors.leftMargin: 10
            text: "Search"
            onClicked: {
                searchModel.searchTerm = textInput_searchPlace.text
                searchModel.update()
            }
        }
    }
}
