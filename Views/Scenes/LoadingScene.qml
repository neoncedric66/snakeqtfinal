import QtQuick 2.0
import "../Components"
import "../../Models"

Item {
    id: root
    property bool appReady: false;
    property bool animationFinished: false;

    //images of the loading IconButton

//    property variant images : [
//        LOAD_LOC + "sprite_0.png",
//        LOAD_LOC + "sprite_1.png",
//        LOAD_LOC + "sprite_2.png",
//        LOAD_LOC + "sprite_3.png",
//        LOAD_LOC + "sprite_4.png",
//        LOAD_LOC + "sprite_5.png",
//        LOAD_LOC + "sprite_6.png",
//        LOAD_LOC + "sprite_7.png",
//        LOAD_LOC + "sprite_8.png",
//        LOAD_LOC + "sprite_9.png",
//        LOAD_LOC + "sprite_10.png",
//        LOAD_LOC + "sprite_11.png",
//        LOAD_LOC + "sprite_12.png",
//        LOAD_LOC + "sprite_13.png",
//        LOAD_LOC + "sprite_14.png",
//        LOAD_LOC + "sprite_15.png",
//        LOAD_LOC + "sprite_16.png",
//        LOAD_LOC + "sprite_17.png",
//        LOAD_LOC + "sprite_18.png",
//        LOAD_LOC + "sprite_19.png",
//        LOAD_LOC + "sprite_20.png",
//        LOAD_LOC + "sprite_21.png",
//        LOAD_LOC + "sprite_22.png",
//        LOAD_LOC + "sprite_23.png",
//        LOAD_LOC + "sprite_24.png",
//        LOAD_LOC + "sprite_25.png",
//        LOAD_LOC + "sprite_26.png",
//        LOAD_LOC + "sprite_27.png",
//        LOAD_LOC + "sprite_28.png",
//        LOAD_LOC + "sprite_29.png",
//        LOAD_LOC + "sprite_30.png",
//        LOAD_LOC + "sprite_31.png",
//    ]

//    property int spriteno: 0

    signal finishedLoading()

    Rectangle {
        id:  rect_bg
        anchors.fill: parent
        color: ColorScheme.primary //"#4682B4"
    }

    Image {
        id: companyLogo
        width: 200
        height: 200
        fillMode: Image.PreserveAspectCrop
        source: IMG_LOC + "companylogo.png"
        anchors {
            horizontalCenter: parent.horizontalCenter
        }
        y: parent.height * 0.27
    }

//    Image {
//        id: oucruLogo
//        width: 90
//        height: width * (739/860)
//        fillMode: Image.Stretch
//        source: IMG_LOC + "oucrulogo.png"
//        anchors.right: parent.right
//        anchors.bottom: parent.bottom
//    }

//    Image {
//        id: bislogo
//        width: oucruLogo.height
//        height: width
//        fillMode: Image.Stretch
//        source: IMG_LOC + "bislogo.png"
//        anchors.left: parent.left
//        anchors.bottom: parent.bottom
//        anchors.leftMargin: 10
//        anchors.bottomMargin: 10
//    }

    Timer {
        id: loadingTimer
        interval: 0 // set to 0 for development purposes /*5000*/
        repeat: false
        running: true
        onTriggered: {
            loadingSuccessful()
        }
    }
    //  Create a gap to seperate the logo and the spinner

//    CustomBusyIndicator {
//        id: spinner
//        running: true
//        anchors.top: companyLogo.bottom
//        anchors.topMargin: 10
//    }

    AnimatedImage {
        id: loadingspinner
        source: LOAD_LOC + "logo.gif"
        anchors.top: companyLogo.bottom
//        anchors.topMargin: 10
        anchors.horizontalCenter: parent.horizontalCenter
        playing: true
        width: 128 //the pic is default 32x32 size so just make this in multiples of 32
        height: width
        fillMode: Image.PreserveAspectFit
    }


    function loadingSuccessful() {
        loadingspinner.playing = false;
        finishedLoading()
    }


    //test to download images
//    Component.onCompleted: {
//        NET_REQ.downloadImage("http://thepublicvoice.org/wordpress/wp-content/uploads/2016/01/TPV-people-hp.jpg","TPV_people.jpg");
//    }
//    Connections {
//        target: NET_REQ
//        onImageDownloaded:{
//            console.log("DOWNLOAD")
//            console.log(path)
//        }
//    }
}
