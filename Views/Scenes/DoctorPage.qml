import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.0
import QtLocation 5.6
import QtPositioning 5.6
import QtQuick.Controls.Styles 1.4
import QtQuick.Window 2.2
import QtMultimedia 5.8
import "../../Models"
import "../Components"
import "../Scenes"

Item {

    id: doctormain

    signal homeClicked()
    signal openBites()

    Rectangle {

        id: doctorBackground
        color: ColorScheme.primary
        anchors.fill: parent

        Rectangle {
            id: doctorframe
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter
            width: parent.width * 0.8
            height: parent.height * 0.8
            color: ColorScheme.secondary

            Button {
                anchors.top: parent.top
                anchors.left: parent.left
                anchors.topMargin: 5
                anchors.leftMargin: 5
                width: parent.width * 0.5
                height: width
                background: Rectangle {
                    anchors.fill: parent
                    color: ColorScheme.accent
                    Text {
                        anchors.fill: parent
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                        text: "Validate reported bites"
                        color: "black"
                        font.pixelSize: 17
                        wrapMode: Text.WordWrap
                    }
                }
                onClicked: {
                    openBites()
                }
            }

        }

    }


    Button {
        id: homeicon
        height: 40
        width: 40

        //Button is 10 pix from the right side and from the top (x value is from bottom left corner so it is dependent on the image's width)
        y: 10
        x: parent.width - width - 10
        background: Image {
            source: IMG_LOC + "homeIcon.png"
            fillMode: Image.Stretch
        }

        //To return back to the home screen
        onClicked: {
            homeClicked()
        }
    }

}
