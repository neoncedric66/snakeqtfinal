import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.0
import QtLocation 5.6
import QtPositioning 5.6
import "../Components"
import "../Scenes"
import "../../Models"

Item {
    id: registerpagemain

    property var usernamedone: 0
    property var passworddone: 0
    property var confirmpassworddone: 0
    property var firstnamedone: 0
    property var lastnamedone: 0
    property var birthdatedone: 0
    property var phonenumberdone: 0
    property var emaildone: 0
    property var statusdone: 0
    property var typedone: 0
    property var genderdone: 0
    property var firstpassword: ""
    property var secondpassword: ""
    property var genderotherdone: 0
    property var informationvalid: 1
    property var keydone: 0
    property string gendertext: ""
    property string keytext: ""
    property var params: ""
    property var occupationdone: 0

    signal finishedLoading()
    signal finishedRegister()

    Rectangle {
        id: registerfill
        anchors.fill: parent
        color: ColorScheme.primary
    }

    Rectangle {
        id: registertext
        anchors.top: parent.top
        anchors.topMargin: 45
        anchors.horizontalCenter: parent.horizontalCenter
        width: parent.width * 0.8
        height: parent.height * 0.1
        color: ColorScheme.primary
        Text {
            anchors.fill: parent
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            text: "Register"
            color: "black"
            font.bold: true
            font.pixelSize: 20
        }
        Button {
            anchors.top: parent.top
            anchors.right: parent.right
            anchors.topMargin: 5
            anchors.rightMargin: 5
            height: parent.height * 0.4
            width: height
            background: Rectangle {
                anchors.fill: parent
                color: ColorScheme.primary
                Image {
                    anchors.fill: parent
                    source: IMG_LOC + "cancelButton.png"
                    fillMode: Image.PreserveAspectFit
                }
            }

            onClicked: {
                //change back to login
                finishedLoading()
            }
        }
    }

    //Rectangle to background the register text fields
    Rectangle {
        id: registerbackground
        anchors.top: registertext.bottom
        anchors.horizontalCenter: parent.horizontalCenter
        height: parent.height * 0.7
        width: registertext.width
        color: ColorScheme.secondary

        //Rectangle for error messages
        Rectangle {
            id: error
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter
            height: parent.height * 0.6
            width: parent.width * 0.8
            z: scroller.z + 10
            visible: false
            color: ColorScheme.secondary
            Rectangle {
                id: toperror
                anchors.top: parent.top
                anchors.left: parent.left
                anchors.right: parent.right
                height: parent.height * 0.3
                color: ColorScheme.accent
                Text {
                    anchors.fill: parent
                    text: "Error"
                    font.family: "Open Sans"
                    font.bold: true
                    font.pixelSize: 25
                    color: "black"
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                }
            }

            Rectangle {
                anchors.top: toperror.bottom
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.leftMargin: parent.width * 0.2
                anchors.rightMargin: parent.width * 0.2
                anchors.bottom: parent.bottom
                color: parent.color
                Text {
                    anchors.fill: parent
                    text: "Passwords do not match"
                    wrapMode: Text.WordWrap
                    font.family: "Open Sans"
                    font.pixelSize: 20
                    color: "black"
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                }
            }

            Button {
                anchors.top: parent.top
                anchors.right: parent.right
                height: parent.height * 0.15
                width: height
                Image {
                    anchors.fill: parent
                    source: IMG_LOC + "cancelButton.png"
                    fillMode: Image.PreserveAspectFit
                }
                onClicked: {
                    error.visible = false
                    confirmpassword.remove(0,1000)
                    password.remove(0,1000)
                    secondpassword = ""
                    firstpassword = ""
                }
            }

        }

        //Flickable that contains all the details the user needs to input in order to register

        Flickable {
            id: scroller
            anchors {
                verticalCenter: parent.verticalCenter
                left: parent.left
                right: parent.right
            }
            height: parent.height * 0.9
            clip: true
            interactive: true
            contentHeight: contentItem.childrenRect.height + 5
            contentWidth: parent.width

            TextField {
                id: username
                placeholderText: qsTr("Enter username")

                font.pixelSize: 15
                activeFocusOnPress: true
                anchors.right: parent.right
                anchors.top: parent.top
                width: parent.width * 0.55
                anchors.topMargin: 10
                anchors.rightMargin: parent.width * 0.1
                height: registerbackground.height * (1/7)
                inputMethodHints: Qt.ImhNoAutoUppercase
                onEditingFinished: {
                    usernamedone = 1
                }
            }

            Rectangle {

                id: usernametextfield
                Text {
                    anchors.fill: parent
                    text: "Username:"
                    font.pixelSize: 15
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.verticalCenter: parent.verticalCenter
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                }

                color: registerbackground.color
                anchors.left: parent.left
                anchors.top: parent.top
                anchors.topMargin: 10
                width: parent.width * 0.35
                height: registerbackground.height * (1/7)

            }

            TextField {
                id: password
                placeholderText: qsTr("Enter password")
                font.pixelSize: 15
                activeFocusOnPress: true
                anchors.right: parent.right
                anchors.top: username.bottom
                anchors.topMargin: 10
                width: parent.width * 0.55
                echoMode: TextInput.Password
                anchors.rightMargin: parent.width * 0.1
                height: registerbackground.height * (1/7)
                onEditingFinished: {
                    passworddone = 1
                    confirmpassword.visible = true
                    firstpassword = getText(0,1000)
                    confirmpassword.visible = true
                    confirmpasswordtextfield.visible = true
                    confirmpassword.focus = true
                    confirmpassword.height = registerbackground.height * (1/7)
                    confirmpasswordtextfield.height = registerbackground.height * (1/7)
                }
            }

            Rectangle {

                id: passwordtextfield
                Text {
                    anchors.fill: parent
                    text: "Password:"
                    font.pixelSize: 15
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.verticalCenter: parent.verticalCenter
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                }
                color: registerbackground.color
                anchors.left: parent.left
                anchors.topMargin: 10
                anchors.top: usernametextfield.bottom
                width: parent.width * 0.35
                height: registerbackground.height * (1/7)
            }

            TextField {
                id: confirmpassword
                placeholderText: qsTr("Enter password")
                font.pixelSize: 15
                activeFocusOnPress: true
                anchors.right: parent.right
                anchors.top: password.bottom
                anchors.topMargin: 10
                width: parent.width * 0.55
                echoMode: TextInput.Password
                visible: false
                anchors.rightMargin: parent.width * 0.1
                height: 0
                onEditingFinished: {
                    if (passworddone === 1) {
                        secondpassword = getText(0,1000)
                        if (secondpassword === firstpassword) {
                            confirmpassworddone = 1
                            console.log(passworddone)
                            console.log(confirmpassworddone)
                        } else {
                            confirmpassworddone = 0
                            console.log(passworddone)
                            console.log(confirmpassworddone)
                            error.visible = true
                            confirmpassword.focus = false
                        }
                    }
                }
            }

            Rectangle {

                id: confirmpasswordtextfield
                Text {
                    anchors.fill: parent
                    text: "Confirm\nPassword:"
                    font.pixelSize: 15
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                }
                color: registerbackground.color
                anchors.left: parent.left
                anchors.topMargin: 10
                anchors.top: passwordtextfield.bottom
                width: parent.width * 0.35
                height: 0
                visible: false
            }

            TextField {
                id: firstname
                placeholderText: qsTr("First name")
                font.pixelSize: 15
                activeFocusOnPress: true
                anchors.right: parent.right
                anchors.top: confirmpassword.bottom
                anchors.topMargin: confirmpassword.visible? 10:0
                width: parent.width * 0.55
                inputMethodHints: Qt.ImhNoAutoUppercase
                anchors.rightMargin: parent.width * 0.1
                height: registerbackground.height * (1/7)
                onEditingFinished: {
                    firstnamedone = 1
                }
            }

            Rectangle {

                id: firstnametextfield
                Text {
                    anchors.fill: parent
                    text: "First Name:"
                    font.pixelSize: 15
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.verticalCenter: parent.verticalCenter
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                    wrapMode: Text.WordWrap
                }
                color: registerbackground.color
                anchors.left: parent.left
                anchors.topMargin:  confirmpasswordtextfield.visible? 10:0
                anchors.top: confirmpasswordtextfield.bottom
                width: parent.width * 0.35
                height: registerbackground.height * (1/7)
            }

            TextField {
                id: lastname

                placeholderText: qsTr("Last name")
                font.pixelSize: 15
                activeFocusOnPress: true
                anchors.right: parent.right
                anchors.top: firstname.bottom
                anchors.topMargin: 10
                width: parent.width * 0.55
                inputMethodHints: Qt.ImhNoAutoUppercase
                anchors.rightMargin: parent.width * 0.1
                height: registerbackground.height * (1/7)
                onEditingFinished: {
                    lastnamedone = 1
                }
            }

            Rectangle {

                id: lastnametextfield
                Text {
                    anchors.fill: parent
                    text: "Last Name:"
                    font.pixelSize: 15
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.verticalCenter: parent.verticalCenter
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                    wrapMode: Text.WordWrap
                }
                color: registerbackground.color
                anchors.left: parent.left
                anchors.topMargin: 10
                anchors.top: firstnametextfield.bottom
                width: parent.width * 0.35
                height: registerbackground.height * (1/7)
            }

            ComboBox {
                id: gender
                model: ["Male", "Female"/*, "Other"*/]
                font.pixelSize: 15
                currentIndex: -1
                anchors.right: parent.right
                anchors.top: lastname.bottom
                width: parent.width * 0.55
                anchors.rightMargin: parent.width * 0.1
                height: registerbackground.height * (1/7)
                anchors.topMargin: 10
                onCurrentIndexChanged: {
                    genderdone = 1
//                    if (gender.currentIndex === 2) {
//                        genderother.height = registerbackground.height * (1/7)
//                        genderother.visible = true
//                        genderothertextfield.height = registerbackground.height * (1/7)
//                        genderothertextfield.visible = true
//                    } else {
//                        genderother.height = 0
//                        genderother.visible = false
//                        genderothertextfield.height = 0
//                        genderothertextfield.visible = false
//                    }
                }
            }

            Rectangle {

                id: gendertextfield
                Text {
                    anchors.fill: parent
                    text: "Gender:"
                    font.pixelSize: 15
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.verticalCenter: parent.verticalCenter
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                    wrapMode: Text.WordWrap
                }
                color: registerbackground.color
                anchors.left: parent.left
                anchors.top: lastnametextfield.bottom
                width: parent.width * 0.35
                anchors.topMargin: 10
                height: registerbackground.height * (1/7)
            }

            TextField {
                id: genderother
                anchors.top: gender.bottom
                anchors.topMargin: 10
                height: 0
                width: parent.width * 0.55
                anchors.right: parent.right
                inputMethodHints: Qt.ImhNoAutoUppercase
                anchors.rightMargin: parent.width * 0.1
                placeholderText: qsTr("Enter Other Gender")
                activeFocusOnPress: true
                visible: false
                onEditingFinished: {
                    genderotherdone = 1
                }
            }

            Rectangle {
                id: genderothertextfield
                anchors.top: gendertextfield.bottom
                anchors.topMargin: 10
                anchors.left: parent.left
                height: 0
                visible: false
                width: parent.width * 0.35
                color: registerbackground.color
                Text {
                    anchors.fill: parent
                    text: "Other\nGender:"
                    font.pixelSize: 15
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.verticalCenter: parent.verticalCenter
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                    wrapMode: Text.WordWrap
                }
            }



            TextField {
                id: birthdate

                placeholderText: qsTr("(DDMMYYYY)")
//                inputMethodHints: Qt.ImhTime
                font.pixelSize: 15
                activeFocusOnPress: true
                anchors.right: parent.right
                anchors.top: genderother.bottom
                width: parent.width * 0.55
                anchors.topMargin: genderother.visible? 10:0
                anchors.rightMargin: parent.width * 0.1
                height: registerbackground.height * (1/7)
//                validator: RegExpValidator {regExp: /^\d{8}$/}
                onEditingFinished: {
                    birthdatedone = 1
                }
            }

            Rectangle {

                id: birthdatetextfield
                Text {
                    anchors.fill: parent
                    text: "Date of Birth:"
                    font.pixelSize: 15
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.verticalCenter: parent.verticalCenter
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                    wrapMode: Text.WordWrap
                }
                color: registerbackground.color
                anchors.left: parent.left
                anchors.top: genderothertextfield.bottom
                width: parent.width * 0.35
                anchors.topMargin: genderothertextfield.visible? 10:0
                height: registerbackground.height * (1/7)
            }

            TextField {
                id: phonenumber

                placeholderText: qsTr("Phone No")
                font.pixelSize: 15
                activeFocusOnPress: true
                anchors.right: parent.right
                anchors.top: birthdate.bottom
                anchors.topMargin: 10
                width: parent.width * 0.55
                anchors.rightMargin: parent.width * 0.1
                height: registerbackground.height * (1/7)
                onEditingFinished: {
                    phonenumberdone = 1
                }
            }

            Rectangle {

                id: phonenumbertextfield
                Text {
                    anchors.fill: parent
                    text: "Phone Number:"
                    font.pixelSize: 15
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.verticalCenter: parent.verticalCenter
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                    wrapMode: Text.WordWrap
                }
                color: registerbackground.color
                anchors.left: parent.left
                anchors.top: birthdatetextfield.bottom
                width: parent.width * 0.35
                anchors.topMargin: 10
                height: registerbackground.height * (1/7)
            }

            TextField {
                id: email

                placeholderText: qsTr("Email")
                validator: RegExpValidator {regExp: /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/}
                font.pixelSize: 15
                activeFocusOnPress: true
                anchors.right: parent.right
                anchors.top: phonenumber.bottom
                anchors.topMargin: 10
                inputMethodHints: Qt.ImhNoAutoUppercase
                width: parent.width * 0.55
                anchors.rightMargin: parent.width * 0.1
                height: registerbackground.height * (1/7)
                onEditingFinished: {
                    emaildone = 1
                }
            }

            Rectangle {

                id: emailtextfield
                Text {
                    anchors.fill: parent
                    text: "Email:"
                    font.pixelSize: 15
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.verticalCenter: parent.verticalCenter
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                    wrapMode: Text.WordWrap
                }
                color: registerbackground.color
                anchors.left: parent.left
                anchors.top: phonenumbertextfield.bottom
                width: parent.width * 0.35
                anchors.topMargin: 10
                height: registerbackground.height * (1/7)
            }

            TextField {
                id: occupation

                placeholderText: qsTr("Occupation")
                font.pixelSize: 15
                activeFocusOnPress: true
                anchors.right: parent.right
                anchors.top: email.bottom
                anchors.topMargin: 10
                width: parent.width * 0.55
                anchors.rightMargin: parent.width * 0.1
                height: registerbackground.height * (1/7)
                inputMethodHints: Qt.ImhNoPredictiveText
                onEditingFinished: {
                    occupationdone = 1
                }
            }

            Rectangle {

                id: occupationtextfield
                Text {
                    anchors.fill: parent
                    text: "Occupation:"
                    font.pixelSize: 15
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.verticalCenter: parent.verticalCenter
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                    wrapMode: Text.WordWrap
                }
                color: registerbackground.color
                anchors.left: parent.left
                anchors.top: emailtextfield.bottom
                width: parent.width * 0.35
                anchors.topMargin: 10
                height: registerbackground.height * (1/7)
            }

            ComboBox {
                id: type
                model: ["Doctor", "Regular"]
                font.pixelSize: 15
                currentIndex: 1
                anchors.right: parent.right
                anchors.top: occupation.bottom
                anchors.topMargin: 10
                width: parent.width * 0.55
                anchors.rightMargin: parent.width * 0.1
                height: registerbackground.height * (1/7)
                onCurrentIndexChanged: {

                    if (type.currentIndex === 0) {
                        //things

                        key.visible = true
                        keytextfield.visible = true
                        keydone = 0
                        key.height = registerbackground.height * (1/7)
                        keytextfield.height = registerbackground.height * (1/7)

                    } else {
                        //no things

                        key.visible = false
                        keytextfield.visible = false
                        keydone = 1
                        key.height = 0
                        keytextfield.height = 0

                    }

                    typedone = 1
                }
            }

            Rectangle {

                id: typetextfield
                Text {
                    anchors.fill: parent
                    text: "Type of Account:"
                    font.pixelSize: 15
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.verticalCenter: parent.verticalCenter
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                    wrapMode: Text.WordWrap
                }
                color: registerbackground.color
                anchors.left: parent.left
                anchors.topMargin: 10
                anchors.top: occupationtextfield.bottom
                width: parent.width * 0.35
                height: registerbackground.height * (1/7)
            }

            TextField {
                id: key
                placeholderText: qsTr("Key")
                activeFocusOnPress: true
                anchors.right: parent.right
                anchors.top: type.bottom
                anchors.topMargin: 10
                visible: false
                width: parent.width * 0.55
                anchors.rightMargin: parent.width * 0.1
                height: 0
                onEditingFinished: {
                    keydone = 1
                }
            }

            Rectangle {
                id: keytextfield
                Text {
                    anchors.fill: parent
                    text: "Enter Key:"
                    font.pixelSize: 15
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.verticalCenter: parent.verticalCenter
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                    wrapMode: Text.WordWrap
                }
                color: registerbackground.color
                anchors.left: parent.left
                visible: false
                anchors.topMargin: 10
                anchors.top: typetextfield.bottom
                width: parent.width * 0.35
                height: 0
            }

            Rectangle {
                id: filler
                anchors.top: keytextfield.bottom
                height: 5
                color: ColorScheme.secondary
                width: parent.width
            }

        }

    }

    Rectangle {
        id: continuerectangle
        anchors.top: registerbackground.bottom
        anchors.horizontalCenter: parent.horizontalCenter
        width: parent.width * 0.8
        height: parent.height * 0.1
        color: ColorScheme.primary
        Button {
            id: nextpage
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            height: parent.height * 0.8
            width: parent.width * 0.5
            background: Image {
                anchors.fill: parent
                source: LOGIN_LOC + "register.png"
                fillMode: Image.PreserveAspectFit
            }
            onClicked: {
                //Send after registering
                console.log(usernamedone)
                console.log(passworddone)
                console.log(firstnamedone)
                console.log(lastnamedone)
                console.log(birthdatedone)
                console.log(phonenumberdone)
                console.log(emaildone)

                if (usernamedone === 1 && occupationdone === 1 && passworddone === 1 && confirmpassworddone === 1 && firstnamedone === 1 && lastnamedone === 1 && birthdatedone === 1 && phonenumberdone === 1 && emaildone === 1) {
                    if (key.visible === true) {
                        if (keydone === 1) {
                            if (genderother.visible === true) {
                                if (genderotherdone === 1) {
                                    informationvalid = 1
                                }
                            } else {
                                if (genderdone === 1) {
                                    informationvalid = 1
                                }
                            }
                        }
                    } else {
                        if (typedone === 1) {
                            informationvalid = 1
                        }
                    }
                } else {
                    informationvalid = 0
                }

                if (informationvalid === 1) {

                    //Gets the text from the all the text fields from the register scene
                    var usernametext = username.getText(0,1000)
                    var passwordtext = password.getText(0,1000)
                    var firstnametext = firstname.getText(0,1000)
                    var lastnametext = lastname.getText(0,1000)
                    var birthdatetext = birthdate.getText(0,1000)
                    var phonenumbertext = phonenumber.getText(0,1000)
                    var emailtext = email.getText(0,1000)
                    var typetext = (type.currentText).toLowerCase()
                    var occupationtext = occupation.getText(0,1000)

                    if (genderother.visible === true) {
                        gendertext = genderother.getText(0,1000)
                    } else {
                        gendertext = (gender.currentText).toLowerCase()
                    }

                    if (key.visible === true) {
                        keytext = key.getText(0,1000)
                    }

                    //Register the user then send to home page
                    var http = new XMLHttpRequest()
                    var url = RequestManager.baseUrl + "user/register/"

                    //Remove occupation = student when done

                    if (key.visible === false) {
                        params = "username="+usernametext+"&password="+passwordtext+"&firstName="+firstnametext+"&lastName="+lastnametext+"&birthDate="+birthdatetext+"&phoneNumber="+phonenumbertext+"&email="+emailtext+"&type="+typetext+"&occupation="+occupationtext+"&sex="+gendertext
                    } else if (key.visible === true) {
                        params = "username="+usernametext+"&password="+passwordtext+"&firstName="+firstnametext+"&lastName="+lastnametext+"&birthDate="+birthdatetext+"&phoneNumber="+phonenumbertext+"&email="+emailtext+"&type="+typetext+"&code="+keytext+"&occupation="+occupationtext+"&sex="+gendertext
                    }

                    http.open("POST",url,true)
                    http.setRequestHeader("Content-type", "application/x-www-form-urlencoded")

                    http.onreadystatechange = function() {
                        if (http.readyState === 4 && http.status === 200) {
                            console.log(http.responseText)
                            console.log(http.readyState)
                            console.log(http.status)
                            console.log("Works")
                            finishedRegister()
                        } else {
                            console.log("Error")
                        }
                    }

                    console.log(params)
                    console.log(http.readyState)
                    console.log(http.responseText)

                    http.send(params)


                } else {
                    //User has not entered all the fields properly
                    console.log("Not done")
                }

            }
        }
    }


    Rectangle {
        id: invalidinformation
        visible: false
        width: registerbackground.width * 0.9
        anchors {
            verticalCenter: registerbackground.verticalCenter
            horizontalCenter: registerbackground.horizontalCenter
        }
        height: width
        color: "#d4d4d4"
        border.width: 0
        border.color: "black"
        Text {
            text: "Not all fields have been<br>entered correctly. Click this message<br>to dismiss it."
            font.pixelSize: 15
            anchors {
                verticalCenter: parent.verticalCenter
                horizontalCenter: parent.horizontalCenter
            }
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            color: "black"
        }
        MouseArea {
            anchors.fill: parent
            hoverEnabled: true
            acceptedButtons: Qt.LeftButton | Qt.RightButton
            onEntered: {
                parent.border.width = 3
            }
            onExited: {
                parent.border.width = 0
            }
            onClicked: {
                invalidinformation.visible = false
            }
        }
    }

    Rectangle {
        id: loading
        anchors.fill: registerpagemain
        color: "#4682B4"
        z: registertext.z + 100
        visible: false

        AnimatedImage {
            id: loadingspinner
            source: LOAD_LOC + "logo.gif"
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter
            playing: true
            width: registerpagemain.width * 0.3 //the pic is default 32x32 size so just make this in multiples of 32
            height: width
            fillMode: Image.PreserveAspectFit
        }

    }

}
