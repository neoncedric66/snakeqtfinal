import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.0
import QtLocation 5.6
import QtPositioning 5.6
import QtQuick.Controls.Styles 1.4
import QtQuick.Window 2.2
import "../Components"
import "../Scenes"
import "../../Models"

Item {
    id: main
    signal logOut()
    signal listOpen()

    Rectangle {
        id: mainfill
        anchors.fill: parent
        color: ColorScheme.primary

        Button {
            id: logout
            width: parent.width *  0.25
            height: width * 0.4
            background: Image {
                anchors.fill: parent
                source: IMG_LOC + "logout.png"
                fillMode: Image.PreserveAspectCrop
            }
            anchors.top: mainfill.top
            anchors.topMargin: 15
            anchors.right: mainfill.right
            anchors.rightMargin: 15
            onClicked: {
                logOut()
                ReportBiteDetails.username = ""
                ReportBiteDetails.currentlocation = false
                GalleryReportData.gallerytext = "Open Gallery:"
                GalleryReportData.speciestext = "Species Known?"
                GalleryReportData.snakevisible = false
                GalleryReportData.speciesknownvisible = true
                UserDetails.usertype = ""
                UserDetails.access_token = ""
            }
        }

        Button {
            id: settings
            width: parent.width * 0.1
            height: width
            background: Image {
                anchors.fill: parent
                source: SLIDE_LOC + "Setting.png"
                fillMode: Image.PreserveAspectCrop
            }
            anchors.top: mainfill.top
            anchors.topMargin: 15
            anchors.left: mainfill.left
            anchors.leftMargin: 15
            onClicked: {
                var settingPage = SIDE_LOC + "SettingsScene.qml"
                nav_scene.pushScene(Qt.resolvedUrl(settingPage),false,{})
                nav_scene.isNavigationBarOverlay = false //so that the stack storage will not be positioned under the navigation bar
                nav_scene.navigationBar.color = "#000000" //used to be efa151
                nav_scene.navigationBar.height = 0
                slide_menu.visible = true
                swipe_area.visible = true
            }
        }

    }

//    Rectangle {
//        id: doctorpatient
//        anchors {
//            verticalCenter: parent.verticalCenter
//            horizontalCenter: parent.horizontalCenter
//        }
//        height: parent.height * 0.25
//        width: parent.width * 0.75
//        visible: false
//        z: scrollerpage.z + 1000
//        color: ColorScheme.secondary
//        Button {
//            id: doctor
//            anchors.left: parent.left
//            anchors.verticalCenter: parent.verticalCenter
//            width: parent.width * 0.4
//            height: width
//            anchors.leftMargin: 10
//            background: Rectangle {
//                anchors.fill: parent
//                color: ColorScheme.accent
//                Text {
//                    text: "Doctor"
//                    anchors.horizontalCenter: parent.horizontalCenter
//                    anchors.verticalCenter: parent.verticalCenter
//                    horizontalAlignment: Text.AlignHCenter
//                    verticalAlignment: Text.AlignVCenter
//                }
//            }
//            onClicked: {
//                var reportBiteDoctor = SCENE_LOC + "ReportBiteSceneDoctor.qml"
//                nav_scene.pushScene(Qt.resolvedUrl(reportBiteDoctor),false,{})
//                doctorpatient.visible = false
//                slide_menu.visible = true
//                logout.visible = true
//            }
//        }
//        Button {
//            id: patient
//            anchors.right: parent.right
//            anchors.verticalCenter: parent.verticalCenter
//            width: parent.width * 0.4
//            height: width
//            anchors.rightMargin: 10
//            background: Rectangle {
//                anchors.fill: parent
//                color: ColorScheme.accent
//                Text {
//                    text: "Patient"
//                    anchors.horizontalCenter: parent.horizontalCenter
//                    anchors.verticalCenter: parent.verticalCenter
//                    horizontalAlignment: Text.AlignHCenter
//                    verticalAlignment: Text.AlignVCenter
//                }
//            }
//            onClicked: {
//                var reportBitePatient = SCENE_LOC + "ReportBiteScene.qml"
//                nav_scene.pushScene(Qt.resolvedUrl(reportBitePatient),false,{})
//                doctorpatient.visible = false
//                slide_menu.visible = true
//                logout.visible = true
//            }
//        }
//        Rectangle {
//            id: close
//            anchors.bottom: parent.top
//            width: parent.width
//            height: parent.height * 0.1
//            color: parent.color
//            Button {
//                anchors.right: parent.right
//                anchors.rightMargin: 5
//                anchors.top: parent.top
//                anchors.topMargin: 10
//                width: parent.width * 0.08
//                height: width
//                background: Image {
//                    anchors.fill: parent
//                    source: IMG_LOC + "cancelButton.png"
//                    fillMode: Image.PreserveAspectFit
//                }
//                onClicked: {
//                    doctorpatient.visible = false
//                    slide_menu.visible = true
//                    logout.visible = true
//                }
//            }

//        }
//    }

    Image {
        id: titlepic
        source: IMG_LOC + "companylogo.png"
        fillMode: Image.PreserveAspectFit
        anchors.top: parent.top
        anchors.topMargin: 10
        anchors.horizontalCenter: parent.horizontalCenter
        height: parent.height * 0.25
        width: height
    }


    Rectangle {
        id: slide_menu
        anchors.top: titlepic.bottom
        anchors.topMargin: 5
        anchors.horizontalCenter: parent.horizontalCenter
        height: parent.height * 0.7
        width: parent.width * 0.7
        color: ColorScheme.secondary
        Rectangle {
            id: scrollerpage
            anchors {
                top: parent.top
                left: parent.left
                right: parent.right
            }
            height: parent.height
            //            clip: true
            //            interactive: true
            //            contentWidth: parent.width
            //            contentHeight: contentItem.childrenRect.height + 5
            visible: true

            Button  {

                id: reportBite
                anchors.top: parent.top
                background: Rectangle {
                    color: ColorScheme.secondary
                    border.color: mainfill.color
                    anchors.fill: parent
                    border.width: 0
                    Image {
                        source: SLIDE_LOC + "Report.png"
                        fillMode: Image.PreserveAspectFit
                        anchors.right: parent.right
                        anchors.top: parent.top
                        anchors.bottom: parent.bottom
                        anchors.rightMargin: 10
                        width: parent.height
                        anchors.topMargin: 30
                        anchors.bottomMargin: 30
                    }

                    Text {
                        text: "Report a Bite"
                        font.pixelSize: 20
                        color: "#212121"
                        anchors.leftMargin: 30
                        anchors.left: parent.left
                        anchors.verticalCenter: parent.verticalCenter
                    }

                    MouseArea {
                        anchors.fill: parent
                        hoverEnabled: true
                        acceptedButtons: Qt.LeftButton | Qt.RightButton
                        //                        onEntered: {
                        //                            parent.border.width = 3
                        //                        }
                        //                        onExited: {
                        //                            parent.border.width = 0
                        //                        }
                        onClicked: {
                            var reportBitePatient = SCENE_LOC + "ReportBiteScene.qml"
                            nav_scene.pushScene(Qt.resolvedUrl(reportBitePatient),false,{})
                            //Rectangle shown
//                            doctorpatient.visible = true
//                            slide_menu.visible = false
//                            logout.visible = false
                        }
                    }
                }

                //So when mouse hovers over the button, its border turns white

                height: slide_menu.height * 0.2
                anchors.left: parent.left
                anchors.right: parent.right
            }

            //            Button {
            //                id: validationPage
            //                background: Rectangle {
            //                    color: ColorScheme.secondary
            //                    border.color: "white"
            //                    anchors.fill: parent
            //                    border.width: 0
            //                    Image {
            //                        source: SLIDE_LOC + "DataValidation.png"
            //                        fillMode: Image.PreserveAspectFit
            //                        anchors.right: parent.right
            //                        anchors.top: parent.top
            //                        anchors.bottom: parent.bottom
            //                        anchors.rightMargin: 10
            //                        width: parent.height
            //                        anchors.topMargin: 30
            //                        anchors.bottomMargin: 30
            //                    }

            //                    Text {
            //                        text: "Data Validation"
            //                        font.pixelSize: 20
            //                        color: "#212121"
            //                        anchors.left: parent.left
            //                        anchors.leftMargin: 30
            //                        anchors.verticalCenter: parent.verticalCenter
            //                    }

            //                    MouseArea {
            //                        anchors.fill: parent
            //                        hoverEnabled: true
            //                        acceptedButtons: Qt.LeftButton | Qt.RightButton
            //                        //                        onEntered: {
            //                        //                            parent.border.width = 3
            //                        //                        }
            //                        //                        onExited: {
            //                        //                            parent.border.width = 0
            //                        //                        }
            //                        onClicked: {

            //                            var validationscene = SIDE_LOC + "ValidationScene.qml"
            //                            nav_scene.pushScene(Qt.resolvedUrl(validationscene),false,{})
            //                            nav_scene.isNavigationBarOverlay = false //so that the stack storage will not be positioned under the navigation bar
            //                            nav_scene.navigationBar.color = "#000000" //used to be efa151
            //                            nav_scene.navigationBar.height = 0
            //                            slide_menu.visible = true
            //                            swipe_area.visible = true
            //                        }
            //                    }
            //                }
            //                height: slide_menu.height * 0.2
            //                anchors.top: reportBite.bottom
            //                anchors.left: parent.left
            //                anchors.right: parent.right
            //            }

            Button {
                id: treatmentPage
                background: Rectangle {
                    color: ColorScheme.secondary
                    border.color: "white"
                    anchors.fill: parent
                    border.width: 0
                    Image {
                        source: SLIDE_LOC + "Treatment.png"
                        fillMode: Image.PreserveAspectFit
                        anchors.right: parent.right
                        anchors.top: parent.top
                        anchors.bottom: parent.bottom
                        anchors.rightMargin: 10
                        width: parent.height
                        anchors.topMargin: 30
                        anchors.bottomMargin: 30
                    }

                    Text {
                        text: "Treatment"
                        font.pixelSize: 20
                        color: "#212121"
                        anchors.left: parent.left
                        anchors.leftMargin: 30
                        anchors.verticalCenter: parent.verticalCenter
                    }

                    MouseArea {
                        anchors.fill: parent
                        hoverEnabled: true
                        acceptedButtons: Qt.LeftButton | Qt.RightButton
                        //                        onEntered: {
                        //                            parent.border.width = 3
                        //                        }
                        //                        onExited: {
                        //                            parent.border.width = 0
                        //                        }
                        onClicked: {

                            var treatmentscene = SIDE_LOC + "TreatmentScene.qml"
                            nav_scene.pushScene(Qt.resolvedUrl(treatmentscene),false,{})
                            nav_scene.isNavigationBarOverlay = false //so that the stack storage will not be positioned under the navigation bar
                            nav_scene.navigationBar.color = "#000000" //used to be efa151
                            nav_scene.navigationBar.height = 0
                            slide_menu.visible = true
                            swipe_area.visible = true
                        }
                    }
                }
                height: slide_menu.height * 0.2
                anchors.top: reportBite.bottom
                anchors.left: parent.left
                anchors.right: parent.right
            }

            Button {
                id: galleryPage
                background: Rectangle {
                    color: ColorScheme.secondary
                    border.color: "white"
                    anchors.fill: parent
                    border.width: 0
                    Image {
                        source: SLIDE_LOC + "Gallery.png"
                        fillMode: Image.PreserveAspectFit
                        anchors.right: parent.right
                        anchors.top: parent.top
                        anchors.bottom: parent.bottom
                        anchors.rightMargin: 10
                        width: parent.height
                        anchors.topMargin: 30
                        anchors.bottomMargin: 30
                    }

                    Text {
                        text: "Gallery"
                        font.pixelSize: 20
                        color: "#212121"
                        anchors.left: parent.left
                        anchors.leftMargin: 30
                        anchors.verticalCenter: parent.verticalCenter
                    }

                    MouseArea {
                        anchors.fill: parent
                        hoverEnabled: true
                        acceptedButtons: Qt.LeftButton | Qt.RightButton
                        //                        onEntered: {
                        //                            parent.border.width = 3
                        //                        }
                        //                        onExited: {
                        //                            parent.border.width = 0
                        //                        }
                        onClicked: {

                            var galleryscene = SCENE_LOC + "GalleryScene.qml"
                            nav_scene.pushScene(Qt.resolvedUrl(galleryscene),false,{})
                            nav_scene.isNavigationBarOverlay = false //so that the stack storage will not be positioned under the navigation bar
                            nav_scene.navigationBar.color = "#000000" //used to be efa151
                            nav_scene.navigationBar.height = 0
                            slide_menu.visible = true
                            swipe_area.visible = true
                        }
                    }
                }
                height: slide_menu.height * 0.2
                anchors.top: treatmentPage.bottom
                anchors.left: parent.left
                anchors.right: parent.right
            }

            Button {
                id: newsPage
                background: Rectangle {
                    color: ColorScheme.secondary
                    border.color: "white"
                    anchors.fill: parent
                    border.width: 0
                    Image {
                        source: SLIDE_LOC + "News.png"
                        fillMode: Image.PreserveAspectFit
                        anchors.right: parent.right
                        anchors.top: parent.top
                        anchors.bottom: parent.bottom
                        anchors.rightMargin: 10
                        width: parent.height
                        anchors.topMargin: 30
                        anchors.bottomMargin: 30
                    }

                    Text {
                        text: "News Feed"
                        font.pixelSize: 20
                        color: "#212121"
                        anchors.left: parent.left
                        anchors.leftMargin: 30
                        anchors.verticalCenter: parent.verticalCenter
                    }

                    MouseArea {
                        anchors.fill: parent
                        hoverEnabled: true
                        acceptedButtons: Qt.LeftButton | Qt.RightButton
                        //                        onEntered: {
                        //                            parent.border.width = 3
                        //                        }
                        //                        onExited: {
                        //                            parent.border.width = 0
                        //                        }
                        onClicked: {
                            listOpen()
                            //                            var newsscene = SIDE_LOC + "NewsScene.qml"
                            //                            nav_scene.pushScene(Qt.resolvedUrl(newsscene),false,{})
                            //                            nav_scene.isNavigationBarOverlay = false //so that the stack storage will not be positioned under the navigation bar
                            //                            nav_scene.navigationBar.color = "#000000" //used to be efa151
                            //                            nav_scene.navigationBar.height = 0
                            //                            slide_menu.visible = true
                            //                            swipe_area.visible = true
                        }
                    }
                }
                height: slide_menu.height * 0.2
                anchors.top: galleryPage.bottom
                anchors.left: parent.left
                anchors.right: parent.right
            }

            Button {
                id: helpPage
                background: Rectangle {
                    color: ColorScheme.secondary
                    border.color: "white"
                    anchors.fill: parent
                    border.width: 0
                    Image {
                        source: SLIDE_LOC + "Help.png"
                        fillMode: Image.PreserveAspectFit
                        anchors.right: parent.right
                        anchors.top: parent.top
                        anchors.bottom: parent.bottom
                        anchors.rightMargin: 10
                        width: parent.height
                        anchors.topMargin: 30
                        anchors.bottomMargin: 30
                    }

                    Text {
                        text: "Help"
                        font.pixelSize: 20
                        color: "#212121"
                        anchors.left: parent.left
                        anchors.leftMargin: 30
                        anchors.verticalCenter: parent.verticalCenter
                    }

                    MouseArea {
                        anchors.fill: parent
                        hoverEnabled: true
                        acceptedButtons: Qt.LeftButton | Qt.RightButton
                        //                        onEntered: {
                        //                            parent.border.width = 3
                        //                        }
                        //                        onExited: {
                        //                            parent.border.width = 0
                        //                        }
                        onClicked: {

                            var helpscene = SIDE_LOC + "HelpScene.qml"
                            nav_scene.pushScene(Qt.resolvedUrl(helpscene),false,{})
                            nav_scene.isNavigationBarOverlay = false //so that the stack storage will not be positioned under the navigation bar
                            nav_scene.navigationBar.color = "#000000" //used to be efa151
                            nav_scene.navigationBar.height = 0
                            slide_menu.visible = true
                            swipe_area.visible = true
                        }
                    }
                }
                height: slide_menu.height * 0.2
                anchors.top: newsPage.bottom
                anchors.left: parent.left
                anchors.right: parent.right
                visible: false
            }

            Button {
                id: doctorPage
                background: Rectangle {
                    color: ColorScheme.secondary
                    anchors.fill: parent
                    //                    Image {
                    //                        source: SLIDE_LOC + "Help.png"
                    //                        fillMode: Image.PreserveAspectFit
                    //                        anchors.right: parent.right
                    //                        anchors.top: parent.top
                    //                        anchors.bottom: parent.bottom
                    //                        anchors.rightMargin: 10
                    //                        width: parent.height
                    //                        anchors.topMargin: 30
                    //                        anchors.bottomMargin: 30
                    //                    }

                    Text {
                        text: "Doctor's Page"
                        font.pixelSize: 20
                        color: "#212121"
                        anchors.left: parent.left
                        anchors.leftMargin: 20
                        anchors.verticalCenter: parent.verticalCenter
                    }

                    MouseArea {
                        anchors.fill: parent
                        hoverEnabled: true
                        acceptedButtons: Qt.LeftButton | Qt.RightButton
                        //                        onEntered: {
                        //                            parent.border.width = 3
                        //                        }
                        //                        onExited: {
                        //                            parent.border.width = 0
                        //                        }
                        onClicked: {

                            var doctorscene = SCENE_LOC + "DoctorPage.qml"
                            nav_scene.pushScene(Qt.resolvedUrl(doctorscene),false,{})
                            nav_scene.isNavigationBarOverlay = false //so that the stack storage will not be positioned under the navigation bar
                            nav_scene.navigationBar.color = "#000000" //used to be efa151
                            nav_scene.navigationBar.height = 0
                            slide_menu.visible = true
                            swipe_area.visible = true
                        }
                    }
                }
                height: slide_menu.height * 0.2
                anchors.top: newsPage.bottom
                anchors.left: parent.left
                anchors.right: parent.right
                visible: false

                Component.onCompleted: {
                    if (UserDetails.usertype === "doctor") {
                        doctorPage.visible = true
                        helpPage.visible = false
                    } else {
                        doctorPage.visible = false
                        helpPage.visible = true
                    }
                }

            }

        }
    }

}
