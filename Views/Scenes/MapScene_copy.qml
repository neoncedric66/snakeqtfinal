import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.0
import QtLocation 5.6
import QtPositioning 5.6
import QtQuick.Controls.Styles 1.4
import "../Components"
import "../../Models"

Item {
    id: root



    property alias map: map
    property MapCircle mapCircle
    property var initialLoad: true
    property var aTaskBar
    property var markerCoordinate: "14.0583,108.2772" //myPositionSource.position.coordinate
    property var startCoordinate: "14.0583,108.2772"
    property var latitude: ""
    property var longitude: ""
    signal closeMap()

    Component.onCompleted: {
        ReportBiteDetails.body.latitude = latitude
        ReportBiteDetails.body.longitude = longitude
//        ReportBiteDetails.body.latitude = "10"
//        ReportBiteDetails.body.longitude = "10"
        latitude = myPositionSource.position.coordinate.latitude
        longitude = myPositionSource.position.coordinate.longitude
        console.log(latitude)
        console.log(longitude)
        BiteManager.report(ReportBiteDetails.body)
    }


    Rectangle {
        anchors.fill: parent
        color: "white"
    }

    //Test to retrieve position
    PositionSource {
        active: true
        onPositionChanged: {
            ReportBiteDetails.body.latitude = latitude
            ReportBiteDetails.body.longitude = longitude
            console.log(latitude)
            console.log(longitude)
            latitude = myPositionSource.position.coordinate.latitude
            longitude = myPositionSource.position.coordinate.latitude
       }
    }


    Plugin {
        id: mapPlugin
        name: "osm" // "mapboxgl", "esri", ...
        PluginParameter {
            name: "osm.useragent"
            value: "Qt Maps Example Application"
        }
    }

    //My position source
    PositionSource {
        id: myPositionSource
        active: true
        updateInterval: 4000
        onPositionChanged: {
            console.log("POSITION: " + myPositionSource.position.coordinate.latitude + "," + myPositionSource.position.coordinate.longitude)

            if (myPositionSource.position.latitudeValid && myPositionSource.position.longitudeValid){
                if (initialLoad) {
                    map.center = myPositionSource.position.coordinate
                    initialLoad = false
                }

                //                myMarker.coordinate = myPositionSource.position.coordinate
            } else {
                console.log("invalid position: " + myPositionSource.sourceError)
            }
        }
    }

    Map {
        id: map
        anchors.fill: parent
        plugin: mapPlugin
        zoomLevel: 16
        gesture.enabled: true // allows you to zoom in and out using pinch touch

        Component.onCompleted: {
            //Example on how to instantiate QMLObject on runtime
            mapCircle = Qt.createQmlObject('import QtLocation 5.6; MapCircle{}',map)
            mapCircle.center = myPositionSource.position.coordinate
            mapCircle.radius = 30
            mapCircle.color = "#c0461414"
            mapCircle.border.color = "#000000"
            mapCircle.border.width = 2
            map.addMapItem(mapCircle)
            console.log("checked")
        }

        //        //To receive touch in map area
        //        MouseArea {
        //            anchors.fill: parent
        //            onClicked: {
        //                console.log("Clicked: " + mouse.x + ":" + mouse.y)
        //                var point = map.toCoordinate(Qt.point(mouse.x,mouse.y))
        //                text_pinCoordinate.text = "(" + point.latitude + "," + point.longitude + ")"
        //                console.log("Translated: " + point.latitude + ":" + point.longitude)
        //                if (cb_setPin.checked) {    //Adding new Pink Pins
        //                    var mapMarker = mapItemComponent.createObject(map,{}) //Example of instantating Component on runtime
        //                    if (mapMarker){
        //                        map.addMapItem(mapMarker)
        //                        mapMarker.coordinate = point
        //                    }
        //                } else { //Updating Red Pin Position
        //                    marker.visible = true
        //                    marker.coordinate = point
        //                    updateRouteQuery(myMarker.coordinate, marker.coordinate)
        //                }
        //            }
        //        }


        //        //Marker for current coordinate
        //        MapQuickItem {
        //            id: myMarker
        //            //            anchorPoint.x: image2.width * 0.5
        //            //            anchorPoint.y: image2.height * 0.5
        //            sourceItem: Image {
        //                id: image2
        //                source: IMG_LOC + "mycoordinate.png"
        //            }
        //            coordinate: myPositionSource.position.coordinate
        //            visible: true

        //        }

        //                //Red pin
        //                MapQuickItem {  //For simplicity, we just used one map item and update it's position upon selection, but you may create a mapquickitem for every selected location
        //                    id:marker
        //                    anchorPoint.x: image3.width * 0.5
        //                    anchorPoint.y: image3.height
        //                    sourceItem: Image {
        //                        id: image3
        //                        source: IMG_LOC + "marker.png"
        //                    }
        //                    visible: false
        //                    coordinate: myPositionSource.position.coordinate
        //                }

        //                Rectangle {
        //                    id:markertext
        //                    width: children.width
        //                    height: children.height
        //                    anchors.horizontalCenter: marker.horizontalCenter
        //                    anchors.bottom: marker.top
        //                    anchors.bottomMargin: 15
        //                    color: "#757575"
        //                    z: map.z + 1000
        //                    visible: false
        //                    Text {
        //                        anchors.fill: parent
        //                        id: markerusername
        //                        horizontalAlignment: Text.AlignHCenter
        //                        verticalAlignment: Text.AlignVCenter
        //                        text: "Username:\n" + ReportBiteDetails.username
        //                        font.pixelSize: 10
        //                        color: "black"
        //                        font.bold: true
        //                    }
        //                }



        //Finding and displaying route
        RouteModel {            //RouteModel is where you get the route information
            id:routeModel
            plugin: mapPlugin
            query: RouteQuery{
                id: routeQuery
            }
            onStatusChanged: {
                if (status === RouteModel.Ready){
                    switch(count) {
                    case 0:
                        console.log("No Route")
                        break
                    case 1:
                        console.log("Found Routes")
                    }
                } else if (status === RouteModel.Error) {
                    console.log("Route Error")
                }
            }
        }
        MapItemView {
            model: routeModel
            delegate: routeDelegate //RouteDelegate interprets the information and draws it
        }


        //Searching places and displaying results in map
        PlaceSearchModel {
            id: searchModel
            plugin: mapPlugin
            searchTerm: ""
            searchArea: QtPositioning.circle(map.center,1000)
            Component.onCompleted: {
                update()
            }
        }

        MapItemView {
            id:mapViewSearchPlace
            model: searchModel
            delegate: MapQuickItem {
                coordinate: place.location.coordinate
                anchorPoint.x: image.width * 0.5
                anchorPoint.y: image.height
                sourceItem: Rectangle {
                    anchors.fill: parent
                    id: itemView
                    Column {
                        Image{id:image; source: IMG_LOC + "placeindicator.png"}
                        Text {
                            id: myAddress
                            //                            text: place.location.address.text
                            visible: false
                        }
                    }
                    Button {        //Changed to Button; for some reason ClearButton or any custom qml for button is not being rendered on ios
                        id: markerButton
                        width: image.width
                        height: image.height
                        anchors.top: parent.top
                        anchors.left: parent.left
                        background: Rectangle{
                            anchors.fill: parent
                            color: "#00000000"
                        }
                        contentItem: Rectangle {
                            anchors.fill: parent
                            color: "#00000000"
                        }
                        onPressed: {
                            console.log("Marker:" + place.location.address.text)
                            myAddress.visible = !myAddress.visible
                        }
                    }
                }
            }
        }

    }

    //Helper function to update route waypoints
    function updateRouteQuery(startCoordinate, endCoordinate) {
        routeQuery.clearWaypoints()
        routeQuery.addWaypoint(startCoordinate)
        routeQuery.addWaypoint(endCoordinate)
        routeQuery.travelModes = RouteQuery.CarTravel
        routeQuery.routeOptimizations = RouteQuery.FastestRoute
        routeModel.update()
    }

    /** ----------- Components ----------------*/
    //Component used to draw the pink pins
    Component {
        id: mapItemComponent
        MapQuickItem {
            id:marker
            anchorPoint.x: image2.width * 0.5
            anchorPoint.y: image2.height
            sourceItem: Image {
                id: image2
                source: IMG_LOC + "marker2.png"
            }
            visible: true
        }
    }

    Component {
        id: mapItemText
        MapQuickItem {
            id:markertext
            sourceItem: Rectangle {
                width: children.width
                height: children.height
                anchors.horizontalCenter: marker.horizontalCenter
                anchors.bottom: marker.top
                anchors.bottomMargin: 15
                color: "#757575"
                z: map.z + 1000
                visible: false
                Text {
                    anchors.fill: parent
                    id: markerusername
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    text: "Username:\n" + ReportBiteDetails.username
                    font.pixelSize: 10
                    color: "black"
                    font.bold: true
                }
            }
        }
    }

    //Component used to draw the route
    Component {
        id: routeDelegate
        MapRoute {
            route: routeData
            line.color: "#461414"
            line.width: 4
            smooth: true
            opacity: 0.9
        }
    }

    //Component used to return user to start position (will change later to make it users location)

    Rectangle {
        id: returnPositionButtonFill
        anchors.fill: returnPositionButton
        color: "#bee3db"
        radius: 20
    }

    Button{
        id: returnPositionButton
        width: 40
        height: 40
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 10
        background: Image {
            source: IMG_LOC + "currentIcon.png"
            fillMode: Image.Stretch
        }
        onClicked: {
            map.center = myPositionSource.position.coordinate
        }
        //        Component.onCompleted: {
        //            console.log(taskBar)
        //        }
    }



    /** ---------- Non Map Related -----------*/

    //Map Info
    Rectangle {
        id: rect_info
        width: parent.width
        height: childrenRect.height * 1.5
        anchors.right: parent.right
        color: "#e6f5ff"  //old color was "#95ffffff" - semi transparent
        opacity: 0.65
        //        CheckBox {
        //            id: cb_setPin
        //            anchors.right: parent.right
        //            anchors.top: parent.top
        //            checked: false
        //            text: "Add Pins"
        //            onCheckStateChanged: {
        //            }
        //        }
        //        ShadowText {
        //            id: text_pinCoordinate
        //            anchors.top: cb_setPin.bottom
        //            anchors.right: cb_setPin.right
        //            text: ""
        //            visible: false
        //            color: "#461414"
        //            font.pixelSize: (root.width * 0.02 > 10) ? root.width * 0.02 : 10
        //            horizontalAlignment: Text.Right
        //            layer.enabled: false
        //        }

        Button {
            id: close
            anchors.right: parent.right
            anchors.top: parent.top
            width: 40
            height: width
            background: Image {
                anchors.fill: parent
                source: IMG_LOC + "cross.png"
                fillMode: Image.PreserveAspectFit
            }
            onClicked: {
                closeMap()
            }
        }

        TextField {
            id: textInput_searchPlace
            anchors.verticalCenter: searchbutton.verticalCenter
            anchors.left: rect_info.left
            anchors.leftMargin: 10
            width: 150
            placeholderText: "Enter Text Here"
            color: "#461414"
            height: searchbutton.height
            font.pixelSize: searchbutton.text.font.pixelSize /*(root.width * 0.02 > 10) ? root.width * 0.02 : 10*/
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            onEditingFinished: {
                searchModel.searchTerm = textInput_searchPlace.text
                searchModel.update()
                textInput_searchPlace.placeholderText = "Enter Text Here"
                textInput_searchPlace.focus = false
            }
        }
        Button {
            id: searchbutton
            anchors.top: parent.top
            anchors.topMargin: 5
            font.pixelSize: 13
            anchors.left: textInput_searchPlace.right
            anchors.leftMargin: 10
            text: "Search"
            onClicked: {
                searchModel.searchTerm = textInput_searchPlace.text
                searchModel.update()
            }
        }
    }

    ListModel {
        id: reportedbites
    }



    //    ListView {
    //        id: myListView
    //        model: mysampleList
    //        delegate: Text {
    //            text: name
    //        }
    //        width: listButton.width
    //        anchors.bottom: rect_info.bottom
    //        anchors.top:listButton.bottom
    //        anchors.horizontalCenter: listButton.horizontalCenter
    //    }

    //    ListModel {
    //        id: mysampleList
    //    }

    //    Button {
    //        id: listButton
    //        width: 100
    //        height: 20
    //        anchors.left: sendButton.right
    //        contentItem: Rectangle {
    //            color: "red"
    //            anchors.fill: parent
    //            Text {
    //                text: qsTr("Get users!")
    //                verticalAlignment: Text.AlignVCenter
    //                horizontalAlignment: Text.AlignHCenter
    //                anchors.fill: parent
    //            }
    //        }
    //        background: Rectangle {
    //            color: "#00000000"
    //        }

    //        onClicked: {
    //            var http = new XMLHttpRequest()
    //            var url = "http://192.168.1.167:1337/user"           //for requesting a specific user
    ////            var url = "http://localhost:1337/user"

    //            http.open("GET",url,true)
    //            http.setRequestHeader("Content-type", "application/x-www-form-urlencoded")
    ////            http.setRequestHeader("Authorization", "92yIbHNMqdPzfd4N30rC6MilmqWgeVLzPvm4hdy9RdhVMkJQAGJY1IdIXhvhjUiA9BjwkFepLUHUC7Xi") //When you add tokens to your api, you will need to set the authorization of the content-type

    //            http.onreadystatechange = function() {
    //                if (http.readyState === XMLHttpRequest.DONE && http.status === 200) {
    //                    console.log(http.responseText)
    //                    var jsonResponse = JSON.parse(http.responseText)
    //                    mysampleList.clear()
    //                    if (isArray(jsonResponse)){
    //                        for(var i=0; i< jsonResponse.length; i++) {
    //                            mysampleList.append({"name":jsonResponse[i].name})
    //                        }
    //                    }else {
    //                        mysampleList.append({"name":jsonResponse.name})
    //                    }
    //                }
    //            }
    //            http.send()
    //        }
    //    }

    //    function isArray(what) {
    //        return Object.prototype.toString.call(what) === '[object Array]';
    //    }


//        Button {
//            id: sendButton
//            width: 100
//            height: 20
//            anchors.left: parent.left
//            anchors.leftMargin: 35
//            contentItem: Rectangle {
//                color: "pink"
//                anchors.fill: parent
//                Text {
//                    text: qsTr("Send Test User!")
//                    verticalAlignment: Text.AlignVCenter
//                    horizontalAlignment: Text.AlignHCenter
//                    anchors.fill: parent
//                }
//            }
//            background: Rectangle {
//                color: "#00000000"
//            }

//            onClicked: {
//                if (nameInput.text === "" || usernameInput.text === "" || passwordInput.text === "") {
//                    console.log("can't add user")
//                    return
//                }

//                var http = new XMLHttpRequest()
//                var url = "http://192.168.1.167:1337/user"
//                var postData = '{"name":' + '"' + nameInput.text + '"' + ', "username":' + '"' + usernameInput.text + '"' + ', "password": ' + '"' + passwordInput.text + '"' + '}'

//                http.open("POST",url,true)
//                http.setRequestHeader("Content-type", "application/json")
//                http.setRequestHeader("Content-length", postData.length)
//    //            http.setRequestHeader("Authorization", "92yIbHNMqdPzfd4N30rC6MilmqWgeVLzPvm4hdy9RdhVMkJQAGJY1IdIXhvhjUiA9BjwkFepLUHUC7Xi") //When you add tokens to your api, you will need to set the authorization of the content-type

//                http.onreadystatechange = function() {
//                    if (http.readyState === XMLHttpRequest.DONE && http.status === 200) {
//                        console.log(http.responseText)
//                        var jsonResponse = JSON.parse(http.responseText)
//                        mysampleList.clear()
//                        for(var i=0; i< jsonResponse.length; i++) {
//                            mysampleList.append({"name":jsonResponse[i].name})
//                        }

//                    }else {
//                        console.log(http.responseText)
//                    }
//                }
//                http.send(postData)
//            }
//        }

    //    TextField {
    //        id: nameInput
    //        width: sendButton.width
    //        height: 30
    //        anchors.left: sendButton.left
    //        anchors.top: sendButton.bottom
    //        anchors.topMargin: 10
    //        background: Rectangle {
    //            color: "pink"
    //        }
    //        placeholderText: "name"
    //    }

    //    TextField {
    //        id: usernameInput
    //        width: nameInput.width
    //        height: nameInput.height
    //        anchors.left: sendButton.left
    //        anchors.top: nameInput.bottom
    //        anchors.topMargin: 10
    //        background: Rectangle {
    //            color: "pink"
    //        }
    //        placeholderText: "username"
    //    }

    //    TextField {
    //        id: passwordInput
    //        width: nameInput.width
    //        height: nameInput.height
    //        anchors.left: sendButton.left
    //        anchors.top: usernameInput.bottom
    //        anchors.topMargin: 10
    //        background: Rectangle {
    //            color: "pink"
    //        }
    //        placeholderText: "password"
    //    }
}
