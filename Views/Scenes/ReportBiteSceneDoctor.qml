import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.0
import QtLocation 5.6
import QtPositioning 5.6
import QtQuick.Controls.Styles 1.4
import QtQuick.Window 2.2
import QtQml 2.2
import "../Components"
import "../Scenes"
import "../../Models"



Item {
    id: reportmain

    signal sliderChange()
    signal homeClicked()
    signal reportedBite()
    signal mapOpenBite()

    Rectangle {

        id: reportfill
        color: ColorScheme.primary
        height: parent.height
        width: parent.width
        anchors.fill: parent

        Rectangle {

            id: mainquestions
            width: parent.width * 0.8
            height: parent.height * 0.85
            anchors {
                verticalCenter: parent.verticalCenter
                horizontalCenter: parent.horizontalCenter
            }
            color: ColorScheme.secondary

            Flickable {
                id: scroller
                anchors {
                    top: parent.top
                    left: parent.left
                    right: parent.right
                }
                height: parent.height * 0.95
                clip: true
                interactive: true
                contentHeight: contentItem.childrenRect.height + 5
                contentWidth: parent.width

                Rectangle {
                    id: speciesknowntextfield
                    Text {
                        anchors.fill: parent
                        text:  GalleryReportData.speciestext
                        font.pixelSize: 15
                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.verticalCenter: parent.verticalCenter
                        verticalAlignment: Text.AlignVCenter
                        horizontalAlignment: Text.AlignHCenter
                    }

                    color: mainquestions.color
                    anchors.left: parent.left
                    anchors.top: parent.top
                    anchors.topMargin: 10
                    width: parent.width * 0.5
                    height: mainquestions.height * (1/7)
                }

                ComboBox {
                    id: speciesknown
                    model: ["Known", "Unknown"]
                    font.pixelSize: 15
                    currentIndex: 1
                    anchors.right: parent.right
                    anchors.top: parent.top
                    anchors.topMargin: 10
                    width: parent.width * 0.4
                    anchors.rightMargin: parent.width * 0.1
                    height: mainquestions.height * (1/7)
                    visible: GalleryReportData.speciesknownvisible
                    onCurrentIndexChanged: {
                        if (speciesknown.currentText === "Known") {
                            gallery.visible = true
                        } else {
                            gallery.visible = false
                        }
                    }
                    onAccepted: {
                        if (speciesknown.currentText === "Known") {
                            gallery.visible = true
                        } else {
                            gallery.visible = false
                        }
                    }
                    onFocusChanged: {
                        if (speciesknown.currentText === "Known") {
                            gallery.visible = true
                        } else {
                            gallery.visible = false
                        }
                    }
                    onCurrentTextChanged: {
                        if (speciesknown.currentText === "Known") {
                            gallery.visible = true
                        } else {
                            gallery.visible = false
                        }
                    }
                }

                Rectangle {
                    id: snakechosenbox
                    visible: GalleryReportData.snakevisible
                    anchors.fill: speciesknown
                    color: "#BDBDBD"
                    Text {
                        anchors.fill: parent
                        text: GalleryReportData.snakechosen
                        color: "black"
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                        wrapMode: Text.WordWrap
                        font.pixelSize: 15
                    }
                }


                Button {
                    id: gallery
                    anchors.right: parent.right
                    anchors.rightMargin: parent.width * 0.1
                    anchors.top: speciesknown.bottom
                    anchors.topMargin: 10
                    height: mainquestions.height * (1/7)
                    width: parent.width * 0.4
                    visible: false
                    background: Rectangle {
                        anchors.fill: parent
                        Text {
                            anchors.fill: parent
                            horizontalAlignment: Text.AlignHCenter
                            verticalAlignment: Text.AlignVCenter
                            text: GalleryReportData.gallerytext
                        }
                    }
                    onClicked: {
                        var galleryReport = SCENE_LOC + "GalleryReportScene.qml"
                        nav_scene.pushScene(Qt.resolvedUrl(galleryReport),false,{})
                    }
                }

            }
        }

        Button {
            id: nextpage
            width:25
            height: 25
            anchors.right: parent.right
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 10
            background: Image {
                source: IMG_LOC + "arrowRightIcon.png"
                fillMode: Image.PreserveAspectCrop
            }
            onClicked: {
                if (GalleryReportData.snakechosen === "") {
                    //nothing
                } else {
                    mapOpenBite()
                    console.log(ReportBiteDetails.currentlocation)
                }

            }
        }


        Button {
            id: homeicon
            height: 30
            width: 30

            //Button is 10 pix from the right side and from the top (x value is from bottom left corner so it is dependent on the image's width)
            y: 10
            x: parent.width - width - 10
            background: Image {
                source: IMG_LOC + "homeIcon.png"
                fillMode: Image.Stretch
            }

            //To return back to the home screen
            onClicked: {
                confirmdiscard.visible = true
            }
        }

        Rectangle {
            id: confirmdiscard
            anchors {
                verticalCenter: mainquestions.verticalCenter
                horizontalCenter: mainquestions.horizontalCenter
            }
            width: mainquestions.width * 0.9
            height: mainquestions.width * 0.8
            visible: false
            color: "#d4d4d4"
            Rectangle {
                anchors.top: parent.top
                anchors.left: parent.left
                anchors.right: parent.right
                height: parent.height * 0.5
                color: parent.color
                Text {
                    text: "Are you sure you want to exit?<br>All changes will be discarded."
                    font.pixelSize: 15
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.verticalCenter: parent.verticalCenter
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                }
            }
            Button {
                id: yes
                anchors.left: parent.left
                anchors.bottom: parent.bottom
                anchors.leftMargin: 10
                anchors.bottomMargin: 10
                width: parent.width * 0.4
                height: parent.width * 0.4
                background: Rectangle {
                    Text {
                        text: "Yes"
                        font.pixelSize: 13
                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.verticalCenter: parent.verticalCenter
                        verticalAlignment: Text.AlignVCenter
                        horizontalAlignment: Text.AlignHCenter
                        color: "black"
                    }
                    anchors.fill: parent
                    color: "#ccff90"
                    border.width: 4
                    radius: 4
                    border.color: "#64dd17"
                }
                onClicked: {
                    homeClicked()
                    confirmdiscard.visible = false
                    GalleryReportData.snakevisible = false
                    GalleryReportData.speciesknownvisible = true
                    GalleryReportData.gallerytext = "Open Gallery"
                    GalleryReportData.speciestext = "Species Known?"
                }
            }
            Button {
                id: no
                anchors.right: parent.right
                anchors.bottom: parent.bottom
                anchors.rightMargin: 10
                anchors.bottomMargin: 10
                width: parent.width * 0.4
                height: parent.width * 0.4
                background: Rectangle {
                    Text {
                        text: "No"
                        font.pixelSize: 13
                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.verticalCenter: parent.verticalCenter
                        verticalAlignment: Text.AlignVCenter
                        horizontalAlignment: Text.AlignHCenter
                        color: "white"
                    }
                    anchors.fill: parent
                    color: "#e96868"
                    border.width: 4
                    radius: 4
                    border.color: "#e74f4e"
                }
                onClicked: {
                    confirmdiscard.visible = false
                }
            }
        }

    }



}
