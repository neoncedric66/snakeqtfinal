import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.0
import QtLocation 5.6
import QtPositioning 5.6
import QtQuick.Controls.Styles 1.4
import QtQuick.Window 2.2
import QtMultimedia 5.8
import "../../Models"


Item {
    id: bitecameramain
    signal cameraDone()
    property alias photoPreview: photoPreview.source


    signal photoChosen()

    Camera {
        id: bitecamera
        imageProcessing.whiteBalanceMode: CameraImageProcessing.WhiteBalanceFlash
        exposure {
            exposureCompensation: -1.0
            exposureMode: Camera.ExposurePortrait
        }
        flash.mode: Camera.FlashRedEyeReduction
        imageCapture {
            onImageCaptured: {
                photoPreview.source = preview  // Show the preview in an Image
                bitecamera.stop()
                CameraOutput.preview = preview
                CameraOutput.requestId = requestId
            }
            onImageSaved: {
                //how to send photo
                console.log("PHOTO PATH HERE")
                console.log(bitecamera.imageCapture.capturedImagePath)
            }
        }
        position: Camera.BackFace
    }

    VideoOutput {
        source: bitecamera
        anchors.fill: parent
        autoOrientation: true
        focus : visible // to receive focus and capture key events when visible
    }

    Image {
        id: photoPreview
        anchors.fill: parent
        fillMode: Image.PreserveAspectFit
    }

    Button {
        id: captureimage
        width: 70
        height: 70
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 20
        background: Rectangle {
            color: "#e5e5e5"
            radius: 5
            Rectangle {
                anchors.verticalCenter: parent.verticalCenter
                anchors.horizontalCenter: parent.horizontalCenter
                width: 50
                height: 50
                radius: 10
                color: ColorScheme.accent
            }
        }
        onClicked: {
            bitecamera.imageCapture.capture()
            captureimage.visible = false
            cancelImage.visible = true
            chooseImage.visible = true
            closeCamera.visible = false
        }
    }

    Button {
        id: chooseImage
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.rightMargin: 10
        anchors.bottomMargin: 10
        height: 50
        width: 50
        background: Rectangle {
            anchors.fill: parent
            color: "white"
            Image {
                anchors.fill: parent
                fillMode: Image.Stretch
                source: IMG_LOC + "arrowRightIcon.png"
            }
        }

        visible: false
        onClicked: {
            //choose photo send photoPreview to database once it is set up
            photoChosen()
            cameraDone()
            console.log("stuff")
            CameraOutput.photoselector = "Take another photo"
            CameraOutput.photourl =  bitecamera.imageCapture.capturedImagePath
            CameraOutput.photomultiple = (1/7)
            CameraOutput.imagevisible = true
        }
    }

    Button {
        id: cancelImage
        anchors.left: parent.left
        anchors.bottom: parent.bottom
        anchors.leftMargin: 10
        anchors.bottomMargin: 10
        width: 50
        height: 50
        background: Rectangle {
            anchors.fill: parent
            color: "white"
            Image {
                anchors.fill: parent
                fillMode: Image.Stretch
                source: IMG_LOC + "cancelButton.png"
            }
        }

        visible: false
        onClicked: {
            //cancel photo
            confirmdiscard.visible = true
        }
    }

    Rectangle {
        id: confirmdiscard
        anchors {
            verticalCenter: parent.verticalCenter
            horizontalCenter: parent.horizontalCenter
        }
        width: parent.width * 0.8
        height: parent.width * 0.6
        visible: false
        color: "#d4d4d4"
        Rectangle {
            anchors.top: parent.top
            anchors.left: parent.left
            anchors.right: parent.right
            height: parent.height * 0.5
            color: parent.color
            Text {
                text: "Are you sure you want<br> to discard the image?"
                font.pixelSize: 15
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
            }
        }
        Button {
            id: yes
            anchors.left: parent.left
            anchors.bottom: parent.bottom
            anchors.leftMargin: 10
            anchors.bottomMargin: 10
            width: parent.width * 0.4
            height: parent.width * 0.4
            background: Rectangle {
                Text {
                    text: "Yes"
                    font.pixelSize: 13
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.verticalCenter: parent.verticalCenter
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                    color: "black"
                }
                anchors.fill: parent
                color: "#ccff90"
                border.width: 4
                radius: 4
                border.color: "#64dd17"
            }
            onClicked: {
                bitecamera.start()
                photoPreview.source = ""
                cancelImage.visible = false
                chooseImage.visible = false
                captureimage.visible = true
                closeCamera.visible = true
                confirmdiscard.visible = false
            }
        }
        Button {
            id: no
            anchors.right: parent.right
            anchors.bottom: parent.bottom
            anchors.rightMargin: 10
            anchors.bottomMargin: 10
            width: parent.width * 0.4
            height: parent.width * 0.4
            background: Rectangle {
                Text {
                    text: "No"
                    font.pixelSize: 13
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.verticalCenter: parent.verticalCenter
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                    color: "white"
                }
                anchors.fill: parent
                color: "#e96868"
                border.width: 4
                radius: 4
                border.color: "#e74f4e"
            }
            onClicked: {
                confirmdiscard.visible = false
            }
        }
    }

    Button {
        id: closeCamera
        anchors.left: parent.left
        anchors.top: parent.top
        anchors.leftMargin: 10
        anchors.topMargin: 10
        width: 50
        height: 50
        background: Rectangle {
            anchors.fill: parent
            color: "white"
            Image {
                anchors.fill: parent
                fillMode: Image.Stretch
                source: IMG_LOC + "cancelButton.png"
            }
        }
        visible: true
        onClicked: {
            cameraDone()
            taskBar.visible = true
        }
    }

}
