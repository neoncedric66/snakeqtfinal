import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.0
import QtLocation 5.6
import QtPositioning 5.6
import QtQuick.Controls.Styles 1.4
import QtQuick.Window 2.2
import "../Components"
import "../Scenes"
import "../../Models"



Item {

    property alias slide_menu: slide_menu
    signal logOut()

    // Colors

    id: item_main


    //Main scene (stack scene)
    NavigationScene {
        id: nav_scene
        anchors.fill: parent
        Component.onCompleted: {
            //            var mapScene = SCENE_LOC + "MapScene.qml"
            //            nav_scene.pushScene(Qt.resolvedUrl(mapScene),false,{aTaskBar:taskBar})
            var openScene = SCENE_LOC + "TestMainScene.qml"
            nav_scene.pushScene(Qt.resolvedUrl(openScene),false)
            nav_scene.isNavigationBarOverlay = false //so that the stack storage will be positioned under the navigation bar
            nav_scene.navigationBar.color = "#efa151" // "#000000" used to be efa151
            nav_scene.navigationBar.height = 0
            nav_scene.navigationBar.anchors.bottom = nav_scene.top
        }
    }

    Connections {
        ignoreUnknownSignals: true
        target: nav_scene.navigationStack.currentItem
        onFinishedRegister: {
            console.log("Connects")
            nav_scene.popCurrentScene(false)
            var openScene = SCENE_LOC + "TestMainScene.qml"
            nav_scene.pushScene(Qt.resolvedUrl(openScene),false)
        }
        onOpenBites: {
            var validationscene = SIDE_LOC + "BiteListDoctors.qml"
            nav_scene.pushScene(Qt.resolvedUrl(validationscene), false)
        }
        onCloseBites: {
            nav_scene.popCurrentScene(false)
        }
        onUploadClose: {
            nav_scene.popCurrentScene(false)
        }
        onHomeClicked: {
            nav_scene.popToRootScene(false)
        }
        onCameraDone: {
            nav_scene.popCurrentScene(false)
        }
        onFirstAid: {
            var firstaid = SIDE_LOC + "FirstAid.qml"
            nav_scene.pushScene(Qt.resolvedUrl(firstaid),false)
        }
        onGoBack: {
            nav_scene.popCurrentScene(false)
        }

        onMapOpen: {
            var mapMarkerScene = SCENE_LOC + "MapDisplayMarkers.qml"
            nav_scene.pushScene(Qt.resolvedUrl(mapMarkerScene),false)
        }
        onListOpen: {
            var listScene = SIDE_LOC + "BiteList.qml"
            //            var listScene = SIDE_LOC + "BiteListDoctors.qml"
            console.log("Open Scene")
            nav_scene.pushScene(Qt.resolvedUrl(listScene), false)
        }
        onCloseList: {
            nav_scene.popCurrentScene(false)
        }
        onMapOpenBite: {
            nav_scene.popCurrentScene(false )
            var mapScene = SCENE_LOC + "MapScene.qml"
            //            var mapMarkerScene = SCENE_LOC + "MapDisplayMarkers.qml"
            nav_scene.pushScene(Qt.resolvedUrl(mapScene),false)
        }
        onCloseMap: {
            nav_scene.popToRootScene(false)
            GalleryReportData.snakevisible = false
            GalleryReportData.speciesknownvisible = true
            GalleryReportData.gallerytext = "Open Gallery"
            GalleryReportData.speciestext = "Species Known?"
        }
        onChooseMap: {
            var mapMarkerScene = SCENE_LOC + "MapChooseBiteMarker.qml"
            nav_scene.pushScene(Qt.resolvedUrl(mapMarkerScene),false)
        }
        onCloseMarkerMap: {
            //FIX HERE WHEN MARKER CHOSEN
            nav_scene.popCurrentScene(false)
        }

        onCloseGallery: {
            nav_scene.popCurrentScene(false)
        }
        onCloseGalleryReport: {
            nav_scene.popCurrentScene(false)
        }
        onChoiceMade: {
            nav_scene.popCurrentScene(false)
        }
        onLogOut: {
            //logout
            logOut()
        }
    }




    //Side Overlay Button
    Rectangle {
        anchors.top: nav_scene.top
        anchors.topMargin: nav_scene.navigationBar.height
        anchors.bottom: nav_scene.bottom
        anchors.left: nav_scene.left
        anchors.right: nav_scene.right
        color: "#80000000"
        visible: slide_menu.state === slide_menu.stateShown
        Button {
            id: btn_dismissSideMenu
            width: slide_menu.state === slide_menu.stateHidden ? 0 : (parent.width - slide_menu.width)
            anchors {
                top: parent.top
                bottom: parent.bottom
                right: parent.right
            }
            background: Rectangle {
                anchors.fill: parent
                color: "#00000000"
            }
            onClicked: {
                slide_menu.goToState(slide_menu.stateHidden)
            }
        }

        NumberAnimation on opacity {
            id: animation_fadeIn
            from: 0
            to: 1
            duration: 160
        }
        onVisibleChanged: {
            if (visible === true){
                animation_fadeIn.start()
            }
        }
    }

    //    SwipeArea {
    //        id: swipe_area
    //        anchors.top: nav_scene.top
    //        anchors.topMargin: nav_scene.navigationBar.height
    //        anchors.bottom: nav_scene.bottom
    //        visible: false
    //        debug: false    //set to true to see area covered
    //        width: {
    //            if (slide_menu.state === slide_menu.stateHidden) {
    //                return slide_menu.width * 0.10
    //            }
    //            return slide_menu.width * 1.10
    //        }
    //        drag.target: slide_menu
    //        drag.axis: Drag.XAxis
    //        drag.minimumX: -slide_menu.width
    //        drag.maximumX: 0
    //        onSwipeRight: {
    //            slide_menu.goToState(slide_menu.stateShown)
    //        }
    //        onSwipeLeft: {
    //            slide_menu.goToState(slide_menu.stateHidden)
    //        }
    //        onSwipeNone: {
    //            //if equal, assign the same state
    //            slide_menu.goToState(slide_menu.state)

    //        }
    //    }

    RectSlide {
        id: slide_menu
        color: "#000000"
        opacity: 0.4
        anchors.top: nav_scene.top
        anchors.topMargin: nav_scene.navigationBar.height
        width: parent.width * 0.8
        height: parent.height
        state: slide_menu.stateHidden
        buttonWidth: 0 //hide the button beside it
        animationDuration: 40
        visible: false
        onRectSlideStateChanged: {
            if (slide_menu.state == slide_menu.stateShown) {
                nav_scene.navigationBar.height = 0
                scrollerpage.visible = true
            } else {
                nav_scene.navigationBar.height = parent.height * 0.75
                scrollerpage.visible = false
            }
        }

        Image {
            id: taskbarslider
            source: IMG_LOC + "slider.png"
            height: 100
            width: 32
            fillMode: Image.Stretch
            anchors.verticalCenter: parent.verticalCenter
            anchors.left: parent.right
            visible: true
        }

        Flickable {
            id: scrollerpage
            anchors {
                top: parent.top
                left: parent.left
                right: parent.right
            }
            height: parent.height
            clip: true
            interactive: true
            contentWidth: parent.width
            contentHeight: contentItem.childrenRect.height + 5
            visible: false

            Button  {

                id: reportBite
                anchors.top: parent.top
                background: Rectangle {
                    color: "#000000"
                    border.color: "white"
                    anchors.fill: parent
                    border.width: 0
                    Image {
                        source: SLIDE_LOC + "Setting.png"
                        fillMode: Image.PreserveAspectFit
                        anchors.right: parent.right
                        anchors.top: parent.top
                        anchors.bottom: parent.bottom
                        anchors.rightMargin: 10
                        width: parent.height
                        anchors.topMargin: 30
                        anchors.bottomMargin: 30
                    }

                    Text {
                        text: "Report a Bite"
                        font.pixelSize: 20
                        color: "white"
                        anchors.leftMargin: 30
                        anchors.left: parent.left
                        anchors.verticalCenter: parent.verticalCenter
                    }

                    MouseArea {
                        anchors.fill: parent
                        hoverEnabled: true
                        acceptedButtons: Qt.LeftButton | Qt.RightButton
                        //                        onEntered: {
                        //                            parent.border.width = 3
                        //                        }
                        //                        onExited: {
                        //                            parent.border.width = 0
                        //                        }
                        onClicked: {

                            var reportBiteVAR = SCENE_LOC + "ReportBiteScene.qml"
                            nav_scene.pushScene(Qt.resolvedUrl(reportBiteVAR),false,{})
                            nav_scene.isNavigationBarOverlay = false //so that the stack storage will not be positioned under the navigation bar
                            nav_scene.navigationBar.color = "#000000" //used to be efa151
                        }
                    }
                }

                //So when mouse hovers over the button, its border turns white

                height: slide_menu.height * 0.2
                anchors.left: parent.left
                anchors.right: parent.right
            }

            Button  {

                id: settingsPage
                anchors.top: reportBite.bottom
                background: Rectangle {
                    color: "#000000"
                    border.color: "white"
                    anchors.fill: parent
                    border.width: 0
                    Image {
                        source: SLIDE_LOC + "Setting.png"
                        fillMode: Image.PreserveAspectFit
                        anchors.right: parent.right
                        anchors.top: parent.top
                        anchors.bottom: parent.bottom
                        anchors.rightMargin: 10
                        width: parent.height
                        anchors.topMargin: 30
                        anchors.bottomMargin: 30
                    }

                    Text {
                        text: "Settings"
                        font.pixelSize: 20
                        color: "white"
                        anchors.leftMargin: 30
                        anchors.left: parent.left
                        anchors.verticalCenter: parent.verticalCenter
                    }

                    MouseArea {
                        anchors.fill: parent
                        hoverEnabled: true
                        acceptedButtons: Qt.LeftButton | Qt.RightButton
                        //                        onEntered: {
                        //                            parent.border.width = 3
                        //                        }
                        //                        onExited: {
                        //                            parent.border.width = 0
                        //                        }
                        onClicked: {

                            var settingScene = SIDE_LOC + "SettingsScene.qml"
                            nav_scene.pushScene(Qt.resolvedUrl(settingScene),false,{})
                            nav_scene.isNavigationBarOverlay = false //so that the stack storage will not be positioned under the navigation bar
                            nav_scene.navigationBar.color = "#000000" //used to be efa151
                            nav_scene.navigationBar.height = 0

                        }
                    }
                }

                //So when mouse hovers over the button, its border turns white

                height: slide_menu.height * 0.2
                anchors.left: parent.left
                anchors.right: parent.right
            }

            Button {
                id: validationPage
                background: Rectangle {
                    color: "#000000"
                    border.color: "white"
                    anchors.fill: parent
                    border.width: 0
                    Image {
                        source: SLIDE_LOC + "DataValidation.png"
                        fillMode: Image.PreserveAspectFit
                        anchors.right: parent.right
                        anchors.top: parent.top
                        anchors.bottom: parent.bottom
                        anchors.rightMargin: 10
                        width: parent.height
                        anchors.topMargin: 30
                        anchors.bottomMargin: 30
                    }

                    Text {
                        text: "Data Validation"
                        font.pixelSize: 20
                        color: "white"
                        anchors.left: parent.left
                        anchors.leftMargin: 30
                        anchors.verticalCenter: parent.verticalCenter
                    }

                    MouseArea {
                        anchors.fill: parent
                        hoverEnabled: true
                        acceptedButtons: Qt.LeftButton | Qt.RightButton
                        //                        onEntered: {
                        //                            parent.border.width = 3
                        //                        }
                        //                        onExited: {
                        //                            parent.border.width = 0
                        //                        }
                        onClicked: {

                            var validationscene = SIDE_LOC + "ValidationScene.qml"
                            nav_scene.pushScene(Qt.resolvedUrl(validationscene),false,{})
                            nav_scene.isNavigationBarOverlay = false //so that the stack storage will not be positioned under the navigation bar
                            nav_scene.navigationBar.color = "#000000" //used to be efa151
                            nav_scene.navigationBar.height = 0

                        }

                    }
                }
                height: slide_menu.height * 0.2
                anchors.top: settingsPage.bottom
                anchors.left: parent.left
                anchors.right: parent.right
            }

            Button {
                id: treatmentPage
                background: Rectangle {
                    color: "#000000"
                    border.color: "white"
                    anchors.fill: parent
                    border.width: 0
                    Image {
                        source: SLIDE_LOC + "Treatment.png"
                        fillMode: Image.PreserveAspectFit
                        anchors.right: parent.right
                        anchors.top: parent.top
                        anchors.bottom: parent.bottom
                        anchors.rightMargin: 10
                        width: parent.height
                        anchors.topMargin: 30
                        anchors.bottomMargin: 30
                    }

                    Text {
                        text: "Treatment"
                        font.pixelSize: 20
                        color: "white"
                        anchors.left: parent.left
                        anchors.leftMargin: 30
                        anchors.verticalCenter: parent.verticalCenter
                    }

                    MouseArea {
                        anchors.fill: parent
                        hoverEnabled: true
                        acceptedButtons: Qt.LeftButton | Qt.RightButton
                        //                        onEntered: {
                        //                            parent.border.width = 3
                        //                        }
                        //                        onExited: {
                        //                            parent.border.width = 0
                        //                        }
                        onClicked: {

                            var treatmentscene = SIDE_LOC + "TreatmentScene.qml"
                            nav_scene.pushScene(Qt.resolvedUrl(treatmentscene),false,{})
                            nav_scene.isNavigationBarOverlay = false //so that the stack storage will not be positioned under the navigation bar
                            nav_scene.navigationBar.color = "#000000" //used to be efa151
                            nav_scene.navigationBar.height = 0

                        }

                    }
                }
                height: slide_menu.height * 0.2
                anchors.top: validationPage.bottom
                anchors.left: parent.left
                anchors.right: parent.right
            }

            Button {
                id: galleryPage
                background: Rectangle {
                    color: "#000000"
                    border.color: "white"
                    anchors.fill: parent
                    border.width: 0
                    Image {
                        source: SLIDE_LOC + "Gallery.png"
                        fillMode: Image.PreserveAspectFit
                        anchors.right: parent.right
                        anchors.top: parent.top
                        anchors.bottom: parent.bottom
                        anchors.rightMargin: 10
                        width: parent.height
                        anchors.topMargin: 30
                        anchors.bottomMargin: 30
                    }

                    Text {
                        text: "Gallery"
                        font.pixelSize: 20
                        color: "white"
                        anchors.left: parent.left
                        anchors.leftMargin: 30
                        anchors.verticalCenter: parent.verticalCenter
                    }

                    MouseArea {
                        anchors.fill: parent
                        hoverEnabled: true
                        acceptedButtons: Qt.LeftButton | Qt.RightButton
                        //                        onEntered: {
                        //                            parent.border.width = 3
                        //                        }
                        //                        onExited: {
                        //                            parent.border.width = 0
                        //                        }
                        onClicked: {

                            var galleryscene = SCENE_LOC + "GalleryScene.qml"
                            nav_scene.pushScene(Qt.resolvedUrl(galleryscene),false,{})
                            nav_scene.isNavigationBarOverlay = false //so that the stack storage will not be positioned under the navigation bar
                            nav_scene.navigationBar.color = "#000000" //used to be efa151
                            nav_scene.navigationBar.height = 0

                        }
                    }
                }
                height: slide_menu.height * 0.2
                anchors.top: treatmentPage.bottom
                anchors.left: parent.left
                anchors.right: parent.right
            }

            Button {
                id: newsPage
                background: Rectangle {
                    color: "#000000"
                    border.color: "white"
                    anchors.fill: parent
                    border.width: 0
                    Image {
                        source: SLIDE_LOC + "News.png"
                        fillMode: Image.PreserveAspectFit
                        anchors.right: parent.right
                        anchors.top: parent.top
                        anchors.bottom: parent.bottom
                        anchors.rightMargin: 10
                        width: parent.height
                        anchors.topMargin: 30
                        anchors.bottomMargin: 30
                    }

                    Text {
                        text: "News Feed"
                        font.pixelSize: 20
                        color: "white"
                        anchors.left: parent.left
                        anchors.leftMargin: 30
                        anchors.verticalCenter: parent.verticalCenter
                    }

                    MouseArea {
                        anchors.fill: parent
                        hoverEnabled: true
                        acceptedButtons: Qt.LeftButton | Qt.RightButton
                        //                        onEntered: {
                        //                            parent.border.width = 3
                        //                        }
                        //                        onExited: {
                        //                            parent.border.width = 0
                        //                        }
                        onClicked: {

                            var newsscene = SIDE_LOC + "NewsScene.qml"
                            nav_scene.pushScene(Qt.resolvedUrl(newsscene),false,{})
                            nav_scene.isNavigationBarOverlay = false //so that the stack storage will not be positioned under the navigation bar
                            nav_scene.navigationBar.color = "#000000" //used to be efa151
                            nav_scene.navigationBar.height = 0

                        }
                    }
                }
                height: slide_menu.height * 0.2
                anchors.top: galleryPage.bottom
                anchors.left: parent.left
                anchors.right: parent.right
            }

            Button {
                id: helpPage
                background: Rectangle {
                    color: "#000000"
                    border.color: "white"
                    anchors.fill: parent
                    border.width: 0
                    Image {
                        source: SLIDE_LOC + "Help.png"
                        fillMode: Image.PreserveAspectFit
                        anchors.right: parent.right
                        anchors.top: parent.top
                        anchors.bottom: parent.bottom
                        anchors.rightMargin: 10
                        width: parent.height
                        anchors.topMargin: 30
                        anchors.bottomMargin: 30
                    }

                    Text {
                        text: "Help"
                        font.pixelSize: 20
                        color: "white"
                        anchors.left: parent.left
                        anchors.leftMargin: 30
                        anchors.verticalCenter: parent.verticalCenter
                    }

                    MouseArea {
                        anchors.fill: parent
                        hoverEnabled: true
                        acceptedButtons: Qt.LeftButton | Qt.RightButton
                        //                        onEntered: {
                        //                            parent.border.width = 3
                        //                        }
                        //                        onExited: {
                        //                            parent.border.width = 0
                        //                        }
                        onClicked: {

                            var reportbitescene = SIDE_LOC + "HelpScene.qml"
                            nav_scene.pushScene(Qt.resolvedUrl(reportbitescene),true,{})
                            nav_scene.isNavigationBarOverlay = false //so that the stack storage will not be positioned under the navigation bar
                            nav_scene.navigationBar.color = "#000000" //used to be efa151

                        }
                    }
                }
                height: slide_menu.height * 0.2
                anchors.top: newsPage.bottom
                anchors.left: parent.left
                anchors.right: parent.right
            }

        }


    }
}
