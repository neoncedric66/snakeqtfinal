import QtQuick 2.7
import QtQuick.Controls 2.0
import "../Components"

Item {
    property alias navigationBar: rect_navBar
    property bool isNavigationBarOverlay: true
    property int navigationDepth: storage.depth
    property alias navigationStack: storage

    id: root
    Rectangle {
        id: rect_bg
        anchors.fill: parent
        color: "black"
    }

    NavigationStack {
        id: storage
        anchors{
            left: parent.left
            right: parent.right
            bottom: parent.bottom
            top: (isNavigationBarOverlay ? parent.top :  rect_navBar.bottom)
        }
        Component.onCompleted: {
        }
    }

    Rectangle {
        id: rect_navBar
        anchors{
            left: parent.left
            right: parent.right
            top: parent.top
        }
        height: parent.height * 0.075
        color: "#80000000"
        ClearButton {
            onPressed: {
                console.log("do nothing! fix to absorb touch for navbar")
            }
        }
    }

    //Navigation Actions
    function pushScene(scene,animated,properties){
        if (navigationDepth < 1) {
            replaceRootScene(scene,animated,properties)
        }else {
            storage.push({item:scene, immediate:!animated, properties: properties})
        }
    }

    function popCurrentScene(animated){
        storage.pop({immediate:!animated})
    }

    function replaceRootScene(scene,animated,properties) {
        storage.pop(null)
        storage.push({item:scene, replace:true, immediate:!animated, properties: properties})
    }

    function popToRootScene(animated) {
        storage.pop({item:null, immediate:!animated})
    }


    //NavigationBar
    function setNavigationBarOverlayMode(status){
        isNavigationBarOverlay = status
    }

    function addNavigationBarComponent(component, params){
        var data = component.createObject(rect_navBar, params)
        if(data === null){
            console.log("Failed to create object! " + component)
        }
        return data
    }

    function addNavigationBarQml(qmlFile, params){
        var component = Qt.createComponent(qmlFile)
        return addNavigationBarComponent(component,params)
    }



}
