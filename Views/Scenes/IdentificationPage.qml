import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.0
import QtLocation 5.6
import QtPositioning 5.6
import "../../Models"
import "../Components"

Item {
    id: identificationmain

    signal goBack()
    property int option: 0

    MouseArea {
        id: descriptionback
        anchors.fill: identificationmain
        onClicked: {
            console.log("OUTSIDE CLICKED")
            description.focus = false
        }
    }

    Rectangle {

        id: identification
        color: ColorScheme.primary
        anchors.fill: parent

        Rectangle {
            id: title
            color: ColorScheme.primary
            width: parent.width * 0.35
            height: parent.height * 0.07
            anchors.top: parent.top
            anchors.topMargin: 10
            anchors.horizontalCenter: parent.horizontalCenter
            Text {
                id: titletext
                text: "Select Identification Option:"
                font.pixelSize: 17
                color: "black"
                anchors.fill: parent
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
            }
        }

        Rectangle {
            id: identificationoption
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: parent.top
            anchors.topMargin: 60
            height: parent.height * 0.5
            width: parent.width * 0.8
            color: ColorScheme.secondary

            MultipleChoice {
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter
                height: parent.height * 0.8
                z: descriptionback.z + 100
                width: parent.width * 0.8
                onSelectOption: {
                    console.log(data)
                    if (data === "Take photo of snake carcass") {
                        console.log("1")
                        option1.visible = true
                        option2.visible = false
                        option3.visible = false
                        GalleryReportData.snakechosen = ""
                        GalleryReportData.snakevisible = false
                        GalleryReportData.speciesknownvisible = true
                    } else if (data === "Choose snake from gallery") {
                        console.log("2")
                        option1.visible = false
                        option2.visible = true
                        option3.visible = false
                    } else if (data === "Write description of the snake") {
                        console.log("3")
                        option1.visible = false
                        option2.visible = false
                        option3.visible = true
                        GalleryReportData.snakechosen = ""
                        GalleryReportData.snakevisible = false
                        GalleryReportData.speciesknownvisible = true
                    }
                }
            }

        }

        Rectangle {
            id: identificationaction
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: identificationoption.bottom
            anchors.topMargin: parent.height * 0.05
            height: parent.height * 0.3
            width: parent.width * 0.8
            color: ColorScheme.secondary

            //All options related to taking photo of the carcass
            Item {
                id: option1
                anchors.fill: parent
                visible: false
                Rectangle {
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.left: parent.left
                    anchors.leftMargin: parent.width * 0.025
                    height: parent.height * 0.3
                    width: parent.width * 0.45
                    color: identificationaction.color
                    Text {
                        anchors.fill: parent
                        font.pixelSize: 12
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                        text: "Take Photo:"
                    }
                }

                Button {
                    id: camera
                    z: descriptionback.z + 100
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.right: parent.right
                    anchors.rightMargin: parent.width * 0.025
                    height: parent.height * 0.3
                    width: parent.width * 0.45
                    background: Rectangle {
                        anchors.fill: parent
                        color: ColorScheme.accent
                        Text {
                            anchors.fill: parent
                            text: "Camera"
                            font.pixelSize: 12
                            color: "black"
                            horizontalAlignment: Text.AlignHCenter
                            verticalAlignment: Text.AlignVCenter
                        }
                    }
                    onClicked: {
                        //open camera
                        console.log("Open Camera")
                        var cameraScene = SCENE_LOC + "UploadCamera.qml"
                        nav_scene.pushScene(Qt.resolvedUrl(cameraScene),true,{})
                        option = 1
                        IdentificationDetails.optionchosen = 1
                    }
                }
            }

            //All options related to choosing the snake from the gallery
            Item {
                id: option2
                visible: false
                anchors.fill: parent
                Rectangle {
                    id: gallerytextfield
                    anchors.top: parent.top
                    anchors.topMargin: parent.height * 0.15
                    anchors.left: parent.left
                    anchors.leftMargin: parent.width * 0.025
                    height: parent.height * 0.3
                    width: parent.width * 0.45
                    color: identificationaction.color
                    Text {
                        anchors.fill: parent
                        font.pixelSize: 12
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                        text: "Open Gallery:"
                    }
                }
                Button {
                    id: gallery
                    z: descriptionback.z + 100
                    anchors.top: parent.top
                    anchors.topMargin: parent.height * 0.15
                    anchors.right: parent.right
                    anchors.rightMargin: parent.width * 0.025
                    height: parent.height * 0.3
                    width: parent.width * 0.45
                    background: Rectangle {
                        anchors.fill: parent
                        color: ColorScheme.accent
                        Text {
                            anchors.fill: parent
                            text: GalleryReportData.gallerytext
                            font.pixelSize: 12
                            color: "black"
                            horizontalAlignment: Text.AlignHCenter
                            verticalAlignment: Text.AlignVCenter
                        }
                    }
                    onClicked: {
                        //open gallery
                        var galleryScene = SCENE_LOC + "GalleryReportScene.qml"
                        nav_scene.pushScene(Qt.resolvedUrl(galleryScene),true,{})
                        option = 2
                        IdentificationDetails.optionchosen = 2
                    }
                }

                Rectangle {
                    id: snakechosenbox
                    visible: GalleryReportData.snakevisible
                    anchors.top: gallery.bottom
                    anchors.horizontalCenter: gallery.horizontalCenter
                    anchors.topMargin: parent.height * 0.1
                    height: parent.height * 0.3
                    width: parent.width * 0.45
                    color: "#BDBDBD"
                    Text {
                        id: snakechosentext
                        anchors.fill: parent
                        text: GalleryReportData.snakechosen
                        color: "black"
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                        wrapMode: Text.WordWrap
                        font.pixelSize: 12
                    }
                }

                Rectangle {
                    id: snakechosentextfield
                    anchors.top: gallerytextfield.bottom
                    anchors.horizontalCenter: gallerytextfield.horizontalCenter
                    width: parent.width * 0.45
                    height: parent.height * 0.3
                    anchors.topMargin: parent.height * 0.1
                    visible: GalleryReportData.snakevisible
                    color: ColorScheme.secondary
                    Text {
                        anchors.fill: parent
                        text: "Snake Chosen:"
                        font.pixelSize: 12
                        color: "black"
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                    }
                }
            }

            //All options related to writing the description of the snake
            Item {
                id: option3
                visible: false
                anchors.fill: parent
                Rectangle {
                    id: descriptiontextfield
                    anchors.top: parent.top
                    anchors.horizontalCenter: parent.horizontalCenter
                    height: parent.height * 0.3
                    width: parent.width * 0.5
                    color: identificationaction.color
                    Text {
                        anchors.fill: parent
                        font.pixelSize: 14
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                        text: "Description"
                    }
                }
                TextArea {
                    id: description
                    z: descriptionback.z + 100
                    anchors.top: descriptiontextfield.bottom
                    anchors.topMargin: parent.height * 0.025
                    anchors.horizontalCenter: parent.horizontalCenter
                    height: parent.height * 0.6
                    width: parent.width * 0.9
                    font.pixelSize: 10
                    text: ""
                    inputMethodHints: Qt.ImhNoAutoUppercase
                    onEditingFinished: {
                        option = 3
                        IdentificationDetails.optionchosen = 3
                    }
                }
            }

        }

    }

    Button {
        id: nextpage
        width: 25
        height: 25
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 10
        background: Image {
            source: IMG_LOC + "arrowRightIcon.png"
            fillMode: Image.PreserveAspectCrop
        }
        Component.onCompleted: {
            console.log(option)
        }
        onClicked: {
            console.log("HERE")
            IdentificationDetails.optionchosen = option
            console.log(IdentificationDetails.optionchosen)
            if (option === 1) {
                IdentificationDetails.optionchosen = option
            } else if (option === 2) {
                IdentificationDetails.optionchosen = option
            } else if (option === 3) {
                console.log("OPTION 3 CHOSEN")
                IdentificationDetails.optionchosen = option
                var descrtext = description.getText(0,100000)
                IdentificationDetails.descriptiontext = descrtext
                console.log(IdentificationDetails.descriptiontext)
            }

            goBack()
            CameraOutput.imagevis = false
            CameraOutput.biteurl =  ""
            CameraOutput.photomultiple = 0
        }
    }

    Button {
        id: back
        height: 25
        width: 25

        //Button is 10 pix from the right side and from the top (x value is from bottom left corner so it is dependent on the image's width)
        y: 10
        x: parent.width - width - 10
        background: Image {
            source: IMG_LOC + "backArrow.png"
            fillMode: Image.Stretch
        }

        //To return back to the home screen
        onClicked: {
            goBack()
        }
    }

}
