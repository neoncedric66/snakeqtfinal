import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.0
import QtLocation 5.6
import QtPositioning 5.6
import 'Models'

ApplicationWindow {
    visible: true
    width: /*640*/ 720 * (1/2)
    height: /*480*/ 1480 * (1/2) //Scaled down the height and width of the computer application to match mobile phone resolution
    title: qsTr("Snake App")


    Loader {
        id:sceneLoader
        focus:true
        anchors.fill: parent
        anchors.centerIn: parent
        property bool valid: item !== null
        source : SCENE_LOC + "LoadingScene.qml"
    }

    Connections {
        ignoreUnknownSignals: true
        target: sceneLoader.valid ? sceneLoader.item : null
        onLogOut: {
            //Signal from LoadingScene.qml and registerScene.qml
            sceneLoader.source = SCENE_LOC + "loginScene.qml"
        }
    }


    Connections {
        ignoreUnknownSignals: true
        target: sceneLoader.valid ? sceneLoader.item : null
        onFinishedLoading: {
            //Signal from LoadingScene.qml and registerScene.qml
//            sceneLoader.source = SCENE_LOC + "TestMainScene.qml"
            sceneLoader.source = SCENE_LOC + "loginScene.qml"
        }
    }

    Connections {
        ignoreUnknownSignals: true
        target: sceneLoader.valid ? sceneLoader.item : null
        onFinishedRegister: {
            console.log("Connects")
            sceneLoader.source = SCENE_LOC + "loginScene.qml"
        }
    }

    Connections {
        ignoreUnknownSignals: true
        target: sceneLoader.valid ? sceneLoader.item : null
        onValidInfo: {
            //Signal from loginScene.qml
            sceneLoader.source = SCENE_LOC + "MainScene.qml"
//            sceneLoader.source = SCENE_LOC + "ReportBiteScene.qml"
        }
    }

    Connections {
        ignoreUnknownSignals: true
        target: sceneLoader.valid ? sceneLoader.item : null
        onRegisterPage: {
            //Signal from loginScene.qml
            sceneLoader.source = SCENE_LOC + "registerScene.qml"
        }
    }



}
