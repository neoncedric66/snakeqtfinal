module.exports = {
  apps : [
    {
      name        : 'snake-core',
      script      : './app.js',
      instances   : '1',
      exec_mode   : 'cluster',
      watch       : false,
      env: {
        'NODE_ENV': 'development',
      },
      env_production : {
        'NODE_ENV': 'production'
      },
      env_staging : {
        'NODE_ENV': 'staging'
      },
      env_docker : {
        'NODE_ENV': 'docker'
      },
    },
  ]
};
