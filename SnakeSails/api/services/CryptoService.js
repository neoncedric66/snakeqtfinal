const bcrypt = require('bcrypt');
const md5 = require("crypto-js/md5");
const parley = require('parley');
const crypto = require('crypto');
var hasher = require('object-hash');

module.exports = {
    oHash: function (obj) {
        return hasher(obj);
    },
    md5: function (data) {
        return md5(data);
    },

    bHash: function (data) {
        var deferred = parley(function (done) {
            var cryptError = ErrorService.errorList.E_CRYPT_FAIL;
            if (data === undefined) {
                return done(ErrorService.createError(cryptError, '[' + 'undefined parameter' + ']'));
            }
            bcrypt.hash(data, 10, function (err, hash) {
                if (err) {
                    return done(ErrorService.createError(cryptError, '[' + err.message + ']'));
                }
                return done(undefined, hash);
            });
        });
        return deferred;
    },
    bCompare: function (data, hash) {
        var deferred = parley(function (done) {
            var cryptError = ErrorService.errorList.E_CRYPT_FAIL;
            if (data === undefined) {
                return done(ErrorService.createError(cryptError, '[' + 'undefined parameter' + ']'));
            }
            bcrypt.compare(data, hash, function (err, res) {
                if (err) {
                    return done(ErrorService.createError(cryptError, '[' + err.message + ']'));
                }
                return done(undefined, res);
            });
        });
        return deferred;
    },

    cEncrypt: function (data) {
        var deferred = parley(function (done) {
            var cryptError = ErrorService.errorList.E_CRYPT_FAIL;
            if (data === undefined) {
                return done(ErrorService.createError(cryptError, '[' + 'undefined parameter' + ']'));
            }
            let algorithm = sails.config.appSettings.algorithmType;
            let password = Buffer.from(sails.config.appSettings.secretKey, 'hex');
            let iv = Buffer.from(sails.config.appSettings.ivKey, 'hex');

            var cipher = crypto.createCipheriv(algorithm, password, iv);
            var encrypted = cipher.update(data, 'utf8', 'hex');
            encrypted += cipher.final('hex');

            return done(undefined, encrypted);
        });
        return deferred;
    },

    cDecrypt: function (data) {
        var deferred = parley(function (done) {
            var cryptError = ErrorService.errorList.E_CRYPT_FAIL;
            if (data === undefined) {
                return done(ErrorService.createError(cryptError, '[' + 'undefined parameter' + ']'));
            }
            let algorithm = sails.config.appSettings.algorithmType;
            let password = Buffer.from(sails.config.appSettings.secretKey, 'hex');
            let iv = Buffer.from(sails.config.appSettings.ivKey, 'hex');

            var decipher = crypto.createDecipheriv(algorithm, password, iv);
            var decrypted = decipher.update(data, 'hex', 'utf8');
            decrypted += decipher.final('utf8');

            return done(undefined, decrypted);
        });
        return deferred;
    }
};
