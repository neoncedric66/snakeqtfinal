var parley = require("parley");

module.exports = {
  report: function(inputs, db) {
    return imp_bite.create(inputs, db);
  },
  find: function(inputs, db) {
    return imp_bite.find(inputs, db);
  },
  respond: function(inputs, db) {
    return imp_bite.respond(inputs, db);
  },
  delete: function(inputs, db) {
    return imp_bite.delete(inputs, db);
  },
  update: function(inputs, db) {
    return imp_bite.update(inputs, db);
  }
};

var imp_bite = {
  create: function(inputs, db) {
    var deferred = parley(async function(done) {
      try {
        let [extractedInput] = await RequestService.extractValues(inputs, [
          "reporter",
          "longitude",
          "latitude",
          "description",
          "timeOfBite",
          "snake",
          "environment",
          "circumstance",
          "siteOfBite",
          "isAtDominantSide",
          "photos"
        ]);
        let hash = CryptoService.oHash(extractedInput);
        _.assign(extractedInput, { hash: hash });
        let [foundSnake] = await Snake.find({ id: extractedInput.snake }).usingConnection(db);
        if (!foundSnake) { throw ErrorService.createError( ErrorService.errorList.E_NOT_EXIST, "snake"); }
        let countDupBite = await Bite.count({ hash: hash }).usingConnection(db);
        if (countDupBite > 0) { throw ErrorService.createError( ErrorService.errorList.E_ALREADY_EXIST ); }
        let [foundUser] = await User.find({ id: extractedInput.reporter, status: "active" }).usingConnection(db); //reporter is an active user
        if (!foundUser) { throw ErrorService.createError(ErrorService.errorList.E_NOT_EXIST, "reporter" ); }
        // let [foundPhoto] = await Photo.find({ id: extractedInput.photo }).usingConnection(db);
        // if (!foundPhoto) { throw ErrorService.createError(ErrorService.errorList.E_NOT_EXIST, "photo" ); }
        //create bite
        let createdBite = await Bite.create(_.omit(extractedInput, ['photos'])).usingConnection(db).meta({ fetch: true });

        //assign photos to bites
        let assignedPhotos = [];
        let photosToAdd = extractedInput.photos;
        if (photosToAdd !== undefined) {
          photosToAdd = photosToAdd.split(',').map(Number).filter(Boolean);
          for (aPhoto of photosToAdd) {
            aPhotoObj = {
              photo: aPhoto,
              bite: createdBite.id
            };
            let [foundPhoto] = await Photo.find({ id: aPhotoObj.photo }).usingConnection(db);
            if (!foundPhoto) { throw ErrorService.createError(ErrorService.errorList.E_NOT_EXIST, "photos" ); }
            let createdBitePhoto = await BitePhoto.
            create(aPhotoObj).usingConnection(db).meta({
              fetch: true
            });
            assignedPhotos.push(createdBitePhoto);
          }
        }

        let mainPhoto = null
        if (assignedPhotos.length > 0 ){
          mainPhoto = Photo.find({id: assignedPhotos[0].photo}).usingConnection(db);
        }
        //email reported bite
        if (sails.config.appSettings.shouldEmailBite){
          await imp_bite.emailBite({ bite: createdBite, reporter: foundUser, snake: foundSnake, photo:mainPhoto },db);
        }

        //upate results
        createdBite.photos = assignedPhotos
        return done(undefined, createdBite);
      } catch (err) {
        return done(ErrorService.createCustomError(err.code, err.message));
      }
    });
    return deferred;
  },
  find: function(inputs, db) {
    var deferred = parley(async function(done) {
      try {
        let {
          reporter,
          coordinate,
          timeOfBite,
          snake,
          id,
          environment,
          circumstance,
          siteOfBite,
          custom
        } = inputs;

        let { skip, limit, sort, populate, populateAttributes } = inputs;
        //Create the query based on the parameters passed.
        var query = {};
        var where = { or: [] };

        // if request is post, expect custom input data
        if (custom !== undefined && custom.length) {
          where.or = custom.filter(function(obj) { return Object.keys(obj).length; });
        }

        if (reporter !== undefined && reporter.length) {
          where.or.push({reporter: reporter.split(",").map(Number).filter(Boolean)});
        }
        if (snake != undefined && snake.length) {
          where.or.push({snake: snake.split(",").map(Number).filter(Boolean)});
        }
        if (timeOfBite != undefined && timeOfBite.length) {
          where.or.push({timeOfBite: timeOfBite.split(",").map(Number).filter(Boolean)});
        }
        if (environment != undefined && environment.length) {
          where.or.push({environment: environment.split(",").map(String).filter(Boolean)});
        }
        if (circumstance != undefined && circumstance.length) {
          where.or.push({circumstance: circumstance.split(",").map(String).filter(Boolean)});
        }
        if (siteOfBite != undefined && siteOfBite.length) {
          where.or.push({siteOfBite: siteOfBite.split(",").map(String).filter(Boolean)});
        }
        if (id != undefined && id.length) {
          where.or.push({id: id.split(",").map(Number).filter(Boolean)});
        }
        if (coordinate != undefined && coordinate.length) {
          let coordinates = coordinate.split(",").map(String).filter(Boolean);
          for (let pair of coordinates) {
            let longlat = pair.split(":").map(String).filter(Boolean);
            if (longlat.length > 1) {
              where.or.push({ longitude: longlat[0], latitude: longlat[1] });
            }
          }
        }

        query.where = where.or.length > 0 ? where : {};

        if (skip !== undefined && skip > -1) {
          query.skip = skip;
        }
        if (limit !== undefined && limit > -1) {
          query.limit = limit;
        }
        if (sort !== undefined) {
          if (["ASC", "DESC"].includes(sort.toUpperCase())) {
            query.sort = "id " + sort.toUpperCase();
          }
        }

        let queryAction = Bite.find(query);

        if (populate && populateAttributes) {
          for (let attribute of populateAttributes) {
            queryAction.populate(attribute);
          }
        }

        let foundData = await queryAction;

        return done(undefined, foundData);
      } catch (err) {
        return done(ErrorService.createCustomError(err.code, err.message));
      }
    });
    return deferred;
  },
  update: function(inputs, db) {
    var deferred = parley(async function(done) {
      try {
        let { id } = inputs;
        let countBite = await Bite.count({ id: id }).usingConnection(db);
        if (countBite < 1) {
          throw ErrorService.createError(ErrorService.errorList.E_NOT_EXIST,"id");
        }
        let updatedBite = Bite.update({ id: id }).set(_.omit(inputs, ['id', 'user'])).usingConnection(db).meta({ fetch: true });
        return done(undefined, updatedBite);
      } catch (err) {
        return done(ErrorService.createCustomError(err.code, err.message));
      }
    });
    return deferred;
  },
  delete: function(inputs, db) {
    var deferred = parley(async function(done) {
      try {
        //find bite exist
        let { id } = inputs;
        let countBite = await Bite.count({ id: id }).usingConnection(db);
        if (countBite < 1) {throw ErrorService.createError(ErrorService.errorList.E_NOT_EXIST,"id");}
        //delete bite
        let deletedBite = await Bite.destroy({ id: id }).usingConnection(db).meta({ fetch: true });
        return done(undefined, deletedBite);
      } catch (err) {
        return done(ErrorService.createCustomError(err.code, err.message));
      }
    });
    return deferred;
  },
  emailBite: function(inputs, db) {
    var deferred = parley(async function(done) {
      try {
        // let doctors = await User.find({type: "doctor",status: "active"}).usingConnection(db);
        // doctors = doctors.map(a => a.email);
        // console.log("inputs: " + inputs);

        var emailTemplate = parley(function(done2) {
          var fs = require("fs");
          fs.readFile(
            sails.config.appSettings.templateBiteReport,
            "utf8",
            function(err, data) {
              if (err) {
                return done2(err);
              }
              return done2(undefined, data);
            }
          );
        });
        let emailBody = await emailTemplate;
        emailBody = emailBody.replace("data_reporter",inputs.reporter.firstName + " " + inputs.reporter.lastName);
        emailBody = emailBody.replace("data_snake", inputs.snake.name);
        emailBody = emailBody.replace("data_siteOfBite",inputs.bite.siteOfBite);
        emailBody = emailBody.replace("data_circumstance",inputs.bite.circumstance);
        emailBody = emailBody.replace("data_environment",inputs.bite.environment);
        if (inputs.photo){
          emailBody = emailBody.replace("data_photo",inputs.photo.source);
        }

        // let emailBody = JSON.stringify({
        //   reporter: inputs.reporter.firstName + " " + inputs.reporter.lastName,
        //   snake: inputs.snake.name,
        //   siteOfBite: inputs.bite.siteOfBite,
        //   circumstance: inputs.bite.circumstance,
        //   environment: inputs.bite.environment
        // });
        await EmailService.sendEmail({
          from: sails.config.appSettings.from,
          // to: doctors.join(","),
          to: inputs.reporter.email,
          subject: "[New] Snake Bite Report",
          body: emailBody
          // text: emailBody
        });
        return done(undefined, "Bite report has been sent.");
      } catch (err) {
        return done(ErrorService.createCustomError(err.code, err.message));
      }
    });
    return deferred;
  },
  respond: function(inputs, db) {
    var deferred = parley(async function(done) {
      try {
        let { id, responder } = inputs;
        let countResponder = await User.count({id: responder,status: "active",type: "doctor"}).usingConnection(db);
        if (countResponder < 1) {
          throw ErrorService.createError(ErrorService.errorList.E_NOT_EXIST,"responder");
        }
        let countBite = await Bite.count({ id: id }).usingConnection(db);
        if (countBite < 1) {
          throw ErrorService.createError(ErrorService.errorList.E_NOT_EXIST,"id");
        }
        let updatedBite = Bite.update({ id: id }).set({ responder: responder }).usingConnection(db).meta({ fetch: true });
        return done(undefined, updatedBite);
      } catch (err) {
        return done(ErrorService.createCustomError(err.code, err.message));
      }
    });
    return deferred;
  },
};
