var parley = require('parley');

module.exports = {
    register: function (inputs, db) { return imp_userInfo.register(inputs, db); },
    find: function (inputs, db) { return imp_userInfo.find(inputs, db); },
    update: function (inputs, db) { return imp_userInfo.update(inputs, db); },
    deactivate: function (inputs, db) { return imp_userInfo.deactivate(inputs, db); },
    uploadPhoto: function (inputs, db) { return imp_userPhoto.upload(inputs, db); },
    findPhoto: function (inputs, db) { return imp_userPhoto.find(inputs, db); },
    updatePhoto: function (inputs, db) { return imp_userPhoto.update(inputs, db); },
    deletePhoto: function (inputs, db) { return imp_userPhoto.delete(inputs, db); },
    changePassword: function (inputs, db) { return imp_userSecurity.changePassword(inputs, db); },
    forgotPassword: function (inputs, db) { return imp_userSecurity.forgotPassword(inputs, db); },
};

var imp_userInfo = {
    register: function (inputs, db) {
        var deferred = parley(async function (done) {
            try {
                let { code, type } = inputs;
                if (type === 'doctor') {
                    if (!code) { throw ErrorService.createError(ErrorService.errorList.E_MISSING_DATA, 'code'); }
                    let unusedCode = await VerificationKey.count({ code: code, status: 'unused' }).usingConnection(db);  //check if code has been used
                    if (unusedCode < 1) { throw ErrorService.createError(ErrorService.errorList.E_VERIFICATION_CODE_UNAVAILABLE, code); }
                }
                let createdUser = await User.create(inputs).usingConnection(db).meta({ fetch: true });
                if (type === 'doctor') {
                    await VerificationKey.update({ code: code }).set({ status: 'used', user: createdUser.id }).usingConnection(db);
                }
                return done(undefined, createdUser);
            } catch (err) {
                return done(ErrorService.createCustomError(err.code, err.message));
            }
        });
        return deferred;
    },
    find: function (inputs, db) {
        var deferred = parley(async function (done) {
            try {
                let { id,
                    username,
                    firstName,
                    lastName,
                    birthDate,
                    phoneNumber,
                    email,
                    status,
                    type,
                    sex,
                    occupation,
                    custom } = inputs;

                let { skip,
                    limit,
                    sort,
                    populate,
                    populateAttributes } = inputs;
                //Create the query based on the parameters passed.
                var query = {};
                var where = { or: [] };

                // if request is post, expect custom input data
                if (custom !== undefined && custom.length) {
                    where.or = custom.filter(function (obj) { return Object.keys(obj).length });
                }

                if (id != undefined && id.length) {
                    where.or.push({ id: id.split(',').map(Number).filter(Boolean) });
                }
                if (username !== undefined && username.length) {
                    where.or.push({ username: username.split(',').map(String).filter(Boolean) });
                }
                if (firstName != undefined && firstName.length) {
                    where.or.push({ firstName: firstName.split(',').map(String).filter(Boolean) });
                }
                if (lastName != undefined && lastName.length) {
                    where.or.push({ lastName: lastName.split(',').map(String).filter(Boolean) });
                }
                if (birthDate != undefined && birthDate.length) {
                    where.or.push({ birthDate: birthDate.split(',').map(Number).filter(Boolean) });
                }
                if (phoneNumber != undefined && phoneNumber.length) {
                    where.or.push({ phoneNumber: phoneNumber.split(',').map(String).filter(Boolean) });
                }
                if (email != undefined && email.length) {
                    where.or.push({ email: email.split(',').map(String).filter(Boolean) });
                }
                if (status != undefined && status.length) {
                    where.or.push({ status: status.split(',').map(String).filter(Boolean) });
                }
                if (type != undefined && type.length) {
                    where.or.push({ type: type.split(',').map(String).filter(Boolean) });
                }
                if (sex != undefined && sex.length) {
                    where.or.push({ sex: sex.split(',').map(String).filter(Boolean) });
                }
                if (occupation != undefined && occupation.length) {
                    where.or.push({ occupation: occupation.split(',').map(String).filter(Boolean) });
                }
                 

                query.where = where.or.length > 0 ? where : {};

                if (skip !== undefined && skip > -1) {
                    query.skip = skip;
                }
                if (limit !== undefined && limit > -1) {
                    query.limit = limit;
                };
                if (sort !== undefined) {
                    if (['ASC', 'DESC'].includes(sort.toUpperCase())) {
                        query.sort = 'id ' + sort.toUpperCase();
                    }
                }

                let queryAction = User.find(query);

                if (populate && populateAttributes) {
                    for (let attribute of populateAttributes) {
                        queryAction.populate(attribute);
                    }
                }

                let foundData = await queryAction;

                return done(undefined, foundData);

            } catch (err) {
                return done(ErrorService.createCustomError(err.code, err.message));
            }
        });
        return deferred;
    },
    update: function (inputs, db) {
        var deferred = parley(async function (done) {
            try {
                //extract info that can be updated
                let [extractedInput] = await RequestService.extractValues(inputs, ['id', 'firstName', 'lastName', 'birthDate', 'phoneNumber', 'email', 'status', 'sex', 'occupation'])

                //find user exist
                let foundUser = await User.count({ id: extractedInput.id }).usingConnection(db);
                if (foundUser < 1) { throw ErrorService.createError(ErrorService.errorList.E_NOT_EXIST, extractedInput.id); }

                //update user info
                let updatedUser = await User.update({ id: extractedInput.id }).set(_.omit(extractedInput, 'id')).usingConnection(db).meta({ fetch: true });
                return done(undefined, updatedUser);
            } catch (err) {
                return done(ErrorService.createCustomError(err.code, err.message));
            }
        });
        return deferred;
    },
    deactivate: function (inputs, db) {
        var deferred = parley(async function (done) {
            try {
                let { id } = inputs

                //check if user exist
                let foundUser = await User.count({ id: id }).usingConnection(db);
                if (foundUser < 1) { throw ErrorService.createError(ErrorService.errorList.E_NOT_EXIST, extractedInput.id); }

                //deactivate user (soft-delete)
                let updatedUser = await User.update({ id: id }).set({ status: 'deactivated' }).usingConnection(db).meta({ fetch: true });
                return done(undefined, updatedUser);
            } catch (err) {
                return done(ErrorService.createCustomError(err.code, err.message));
            }
        });
        return deferred;

    },
}

var imp_userPhoto = {
    upload: function (inputs, db) {

    },
    find: function (inputs, db) {

    },
    update: function (inputs, db) {

    },
    delete: function (inputs, db) {

    },
}

var imp_userSecurity = {
    changePassword: function (inputs, db) {

    },
    forgotPassword: function (inputs, db) {

    },
}
