var flaverr = require('flaverr');

module.exports = {
  errorList: {
    E_MISSING_DATA :  { code: 'E_MISSING_DATA', message: 'Missing content has been identified.'},
    E_NOT_EXIST :     {code: 'E_NOT_EXIST', message: 'Content does not exist.'},
    E_INVALID_DATA :     {code: 'E_INVALID_DATA', message: 'Invalid data.'},
    E_VERIFICATION_CODE_UNAVAILABLE : { code: 'E_VERIFICATION_CODE_UNAVAILABLE', message: 'Verification code is unavailable.'},
    E_IN_USE :        {code: 'E_IN_USE', message: 'Content is in use.'},
    E_IMPLEMENTATION_ERROR:  {code: 'E_IMPLEMENTATION_ERROR', message: 'Possible invalid input or wrong implementation.'},
    E_CRYPT_FAIL:     {code: 'E_CRYPT_FAIL', message: 'Hash failed.'},
    E_ALREADY_EXIST : {code: 'E_ALREADY_EXIST', message: 'Content already exists.'},
    E_FILE_NOT_DELETE: {code: 'E_FILE_NOT_DELETE', message: 'File not deleted.'},
    E_PHOTO_VERIFIED: {code: 'E_PHOTO_VERIFIED', message: 'Photo is already verified.'},
    E_PHOTO_DISABLED: {code: 'E_PHOTO_DISABLED', message: 'Photo is already disabled.'},
    E_INVALID_USER: {code: 'E_INVALID_USER', message: 'User has no right to perform the action.'},    
  },
  createError: function(errorKey, details) {
    let message = errorKey.message + (details? ' '+ details : '');
    return flaverr(errorKey.code, new Error(message));
  },
  createCustomError: function(code, message) {
    return flaverr(code||'E_UNKNOWN', new Error(message ||'Unknown error.'));
  }
};

