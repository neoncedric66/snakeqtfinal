var parley = require('parley');


module.exports = {
    create: function (inputs, db) { return imp_symptom.create(inputs, db); },
    find: function (inputs, db) { return imp_symptom.find(inputs, db); },
    update: function (inputs, db) { return imp_symptom.update(inputs, db); },
    delete: function (inputs, db) { return imp_symptom.delete(inputs, db); },
};

var imp_symptom = {
    create: function (inputs, db) {
        var deferred = parley(async function (done) {
            try {
                inputs.name = inputs.name.toLowerCase()
                let countSymptom = await Symptom.count({ name: inputs.name }).usingConnection(db);
                if (countSymptom > 0) { throw ErrorService.createError(ErrorService.errorList.E_ALREADY_EXIST); }
                //create symptom
                let createdSymptom = await Symptom.create(inputs).usingConnection(db).meta({ fetch: true });
                return done(undefined, createdSymptom);
            } catch (err) {
                return done(ErrorService.createCustomError(err.code, err.message));
            }
        });
        return deferred;
    },
    find: function (inputs, db) {
        var deferred = parley(async function (done) {
            try {
                let { name, id } = inputs;

                let { skip,
                    limit,
                    sort,
                    populate,
                    populateAttributes,
                    custom } = inputs;
                //Create the query based on the parameters passed.
                var query = {};
                var where = { or: [] };

                // if request is post, expect custom input data
                if (custom !== undefined && custom.length) {
                    where.or = custom.filter(function (obj) { return Object.keys(obj).length });
                }

                if (id !== undefined && id.length) {
                    where.or.push({ id: id.split(',').map(Number).filter(Boolean) });
                }

                if (name !== undefined && name.length) {
                    where.or.push({ name: name.split(',').map(String).filter(Boolean) });
                }

                query.where = where.or.length > 0 ? where : {};

                if (skip !== undefined && skip > -1) {
                    query.skip = skip;
                }
                if (limit !== undefined && limit > -1) {
                    query.limit = limit;
                };
                if (sort !== undefined) {
                    if (['ASC', 'DESC'].includes(sort.toUpperCase())) {
                        query.sort = 'name ' + sort.toUpperCase();
                    }
                }

                let queryAction = Symptom.find(query);

                if (populate && populateAttributes) {
                    for (let attribute of populateAttributes) {
                        queryAction.populate(attribute);
                    }
                }

                let foundData = await queryAction;

                return done(undefined, foundData);

            } catch (err) {
                return done(ErrorService.createCustomError(err.code, err.message));
            }
        });
        return deferred;
    },
    update: function (inputs, db) {
        var deferred = parley(async function (done) {
            try {
                //extract info that can be updated
                let [extractedInput] = await RequestService.extractValues(inputs, ['id', 'name'])

                //find symptom exist
                let foundSymptom = await Symptom.count({ id: extractedInput.id }).usingConnection(db);
                if (foundSymptom < 1) { throw ErrorService.createError(ErrorService.errorList.E_NOT_EXIST, extractedInput.id); }

                //update symptom info
                let updatedSymptom = await Symptom.update({ id: extractedInput.id }).set(_.omit(extractedInput, 'id')).usingConnection(db).meta({ fetch: true });
                return done(undefined, updatedSymptom);
            } catch (err) {
                return done(ErrorService.createCustomError(err.code, err.message));
            }
        });
        return deferred;
    },
    delete: function (inputs, db) {
        var deferred = parley(async function (done) {
            try {
                //find symptom exist
                let { id } = inputs;
                let countSymptom = await Symptom.count({ id: id }).usingConnection(db);
                if (countSymptom < 1) { throw ErrorService.createError(ErrorService.errorList.E_NOT_EXIST, 'id'); }
                //find symptom is not being used by snake
                let [countBiteDiagnosis] = await Promise.all([BiteDiagnosis.count({ symptom: id }).usingConnection(db)]);
                if (countBiteDiagnosis > 0) { throw ErrorService.createError(ErrorService.errorList.E_IN_USE); }
                //delete symptom
                let deletedSymptom = await Symptom.destroy({ id: id }).usingConnection(db).meta({ fetch: true });
                return done(undefined, deletedSymptom);
            } catch (err) {
                return done(ErrorService.createCustomError(err.code, err.message));
            }
        });
        return deferred;
    },
}