var parley = require("parley");

module.exports = {
  diagnose: function(inputs, db) {
    return imp_biteDiagnosis.create(inputs, db);
  },
};

var imp_biteDiagnosis = {
  create: function(inputs, db) {
    var deferred = parley(async function(done) {
      try {
        let { diagnoses, doctor } = inputs;

        if (!Array.isArray(diagnoses)) {
            throw ErrorService.createError(ErrorService.errorList.E_INVALID_DATA, "");
        }

        let results = {}
        for (const aDiagnosis of diagnoses) {
            let {bite, symptom} = aDiagnosis
            _.assign(aDiagnosis, { doctor: doctor });

            let [countSymptom, countBite] = await Promise.all([
                Symptom.count({id: symptom}).usingConnection(db),
                Bite.count({id: bite}).usingConnection(db),
            ]);

            if (countSymptom <= 0) {
                throw ErrorService.createError(ErrorService.errorList.E_NOT_EXIST, "symptom");
            }

            if (countBite <= 0) {
                throw ErrorService.createError(ErrorService.errorList.E_NOT_EXIST, "bite");
            }

            let key = bite + ":" + symptom
            let countBiteDiagnosis = await BiteDiagnosis.count({ bite: bite, symptom: symptom }).usingConnection(db);
            if (countBiteDiagnosis > 0) { 
                //update bite diagnosis
                let updatedBiteDiagnosis = _.head(await BiteDiagnosis.update({ bite: bite, symptom: symptom }).set(aDiagnosis).usingConnection(db).meta({ fetch: true }))
                results[key] = updatedBiteDiagnosis
            } else {
                //create bite diagnosis
                let createdBiteDiagnosis = await BiteDiagnosis.create(aDiagnosis).usingConnection(db).meta({ fetch: true })
                results[key] = createdBiteDiagnosis
            }
        }
        return done(undefined, Object.values(results));
      } catch (err) {
        return done(ErrorService.createCustomError(err.code, err.message));
      }
    });
    return deferred;
  },
};
