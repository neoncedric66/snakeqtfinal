var parley = require('parley');


module.exports = {
    create: function (inputs, db) { return imp_tag.create(inputs, db); },
    find: function (inputs, db) { return imp_tag.find(inputs, db); },
    update: function (inputs, db) { return imp_tag.update(inputs, db); },
    delete: function (inputs, db) { return imp_tag.delete(inputs, db); },
};

var imp_tag = {
    create: function (inputs, db) {
        var deferred = parley(async function (done) {
            try {
                inputs.name = inputs.name.toLowerCase()
                let countTag = await Tag.count({ name: inputs.name }).usingConnection(db);
                if (countTag > 0) { throw ErrorService.createError(ErrorService.errorList.E_ALREADY_EXIST); }
                //create tag
                let createdTag = await Tag.create(inputs).usingConnection(db).meta({ fetch: true });
                return done(undefined, createdTag);
            } catch (err) {
                return done(ErrorService.createCustomError(err.code, err.message));
            }
        });
        return deferred;
    },
    find: function (inputs, db) {
        var deferred = parley(async function (done) {
            try {
                let { name,
                        id } = inputs;

                let { skip,
                    limit,
                    sort,
                    populate,
                    populateAttributes,
                    custom } = inputs;
                //Create the query based on the parameters passed.
                var query = {};
                var where = { or: [] };

                // if request is post, expect custom input data
                if (custom !== undefined && custom.length) {
                    where.or = custom.filter(function (obj) { return Object.keys(obj).length });
                }

                if (id !== undefined && id.length) {
                    where.or.push({ id: id.split(',').map(Number).filter(Boolean) });
                }

                if (name !== undefined && name.length) {
                    where.or.push({ name: name.split(',').map(String).filter(Boolean) });
                }

                query.where = where.or.length > 0 ? where : {};

                if (skip !== undefined && skip > -1) {
                    query.skip = skip;
                }
                if (limit !== undefined && limit > -1) {
                    query.limit = limit;
                };
                if (sort !== undefined) {
                    if (['ASC', 'DESC'].includes(sort.toUpperCase())) {
                        query.sort = 'name ' + sort.toUpperCase();
                    }
                }

                let queryAction = Tag.find(query);

                if (populate && populateAttributes) {
                    for (let attribute of populateAttributes) {
                        queryAction.populate(attribute);
                    }
                }

                let foundData = await queryAction;

                return done(undefined, foundData);

            } catch (err) {
                return done(ErrorService.createCustomError(err.code, err.message));
            }
        });
        return deferred;
    },
    update: function (inputs, db) {
        var deferred = parley(async function (done) {
            try {
                //extract info that can be updated
                let [extractedInput] = await RequestService.extractValues(inputs, ['id', 'name'])

                //find tag exist
                let foundTag = await Tag.count({ id: extractedInput.id }).usingConnection(db);
                if (foundTag < 1) { throw ErrorService.createError(ErrorService.errorList.E_NOT_EXIST, extractedInput.id); }

                //update tag info
                let updatedTag = await Tag.update({ id: extractedInput.id }).set(_.omit(extractedInput, 'id')).usingConnection(db).meta({ fetch: true });
                return done(undefined, updatedTag);
            } catch (err) {
                return done(ErrorService.createCustomError(err.code, err.message));
            }
        });
        return deferred;
    },
    delete: function (inputs, db) {
        var deferred = parley(async function (done) {
            try {
                //find tag exist
                let { id } = inputs;
                let countTag = await Tag.count({ id: id }).usingConnection(db);
                if (countTag < 1) { throw ErrorService.createError(ErrorService.errorList.E_NOT_EXIST, 'id'); }
                //find tag is not being used by snake
                let [countSnakeTag] = await Promise.all([SnakeTag.count({ tag: id }).usingConnection(db)]);
                if (countSnakeTag > 0) { throw ErrorService.createError(ErrorService.errorList.E_IN_USE); }
                //delete snake
                let deletedTag = await Tag.destroy({ id: id }).usingConnection(db).meta({ fetch: true });
                return done(undefined, deletedTag);
            } catch (err) {
                return done(ErrorService.createCustomError(err.code, err.message));
            }
        });
        return deferred;
    },
}