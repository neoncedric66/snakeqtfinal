var parley = require('parley');

module.exports = {

    /**
  *  @Description: Uploads file
  *  @Param: request - requested parameters. Contains the file.
  *  @Param: type - type of file to be uploaded
  *  @Error : Default: E_INVALID_DATA
  *  @Custom Error: use useError(<ErrorService error goes here>)
  */
    upload: function (request, type, custom) {
        var invalidError = ErrorService.errorList.E_INVALID_DATA;
        var deferred = parley(function (done) {

            //call filetransfer-hook to upload image
            sails.hooks.filetransfer.upload(request, type, custom,
                function (err, cbresponse) {
                    if (err) {
                        return done(err.message);
                    }
                    return done(undefined, cbresponse);
                });
        }, undefined, {
                useError: function (returnedError) {
                    invalidError = returnedError ? returnedError : invalidError;
                    return deferred;
                },
            });
        return deferred;
    },
    fileExist: function (filePath) {
        var deferred = parley(function (done) {

            //call filetransfer-hook to check file exist
            sails.hooks.filetransfer.fileExist(filePath,
                function (err) {
                    if (err) {
                        return done(undefined, false);
                    }
                    return done(undefined, true);
                });
        });
        return deferred;
    },
    fileDelete: function (filePath) {
        var notDeletedError = ErrorService.errorList.E_FILE_NOT_DELETE;
        var deferred = parley(function (done) {

            //call filetransfer-hook to delete image
            sails.hooks.filetransfer.fileDelete(filePath,
                function (err, cbresponse) {
                    if (err) {
                        return done(undefined, false);
                    }
                    return done(undefined, true);
                });
        }, undefined, {
                useError: function (returnedError) {
                    notDeletedError = returnedError ? returnedError : notDeletedError;
                    return deferred;
                },
            });
        return deferred;
    }
};