const oauth2orize = require('oauth2orize');

/* global Client AuthCode AccessToken RefreshToken User sails */

// Create OAuth 2.0 server
var server = oauth2orize.createServer();

server.serializeClient(function (client, done) {
  return done(null, client.id);
});

server.deserializeClient(async function (id, done) {
  try {
    const client = await Client.findOne(id);
    return done(null, client);
  } catch (err) {
    return done(err);
  }
});

// Generate authorization code
server.grant(oauth2orize.grant.code(async function (client, redirectURI, user, ares, done) {
  try {
    var authCode = await AuthCode.create({
      client_id: client.client_id,
      redirect_uri: redirectURI,
      user_id: user.id,
      scope: ares.scope
    }).fetch();
    if (authCode) {
      return done(null, authCode.code);
    }
  } catch (err) {
    return done(err, null);
  }
}));

// Generate access token for Implicit flow
// Only access token is generated in this flow, no refresh token is issued
server.grant(oauth2orize.grant.token(async function (client, user, ares, done) {
  try {
    await AccessToken.destroy({ user_id: user.id, client_id: client.client_id });
    const accessToken = await AccessToken.create({ user_id: user.id, client_id: client.client_id }).fetch();
    return done(null, accessToken.token);
  } catch (err) {
    return done(err);
  }
}));

// Exchange username & password for access token.
server.exchange(oauth2orize.exchange.password(async function (client, username, password, scope, body, authInfo, done) {
  try {
    console.info('Fetching user');
    const user = await User.findOne({ username: username });
    if (!user) { return done(null, false); }

    console.info('Comparing password');
    const pwdCompare = await CryptoService.bCompare(password, user.password);
    console.info('Logging the pwdCompare');
    console.info(pwdCompare);
    if (!pwdCompare) {

      return done(null, false);
    }

    console.info('Destroying old tokens');
    await RefreshToken.destroy({ user_id: user.id, client_id: client.client_id });
    await AccessToken.destroy({ user_id: user.id, client_id: client.client_id });

    console.info('Creating new tokens');
    const result = await sails.getDatastore().transaction(async (db, proceed) => {
      try {
        const refreshToken = await RefreshToken.create({ user_id: user.id, client_id: client.client_id }).fetch().usingConnection(db);
        const accessToken = await AccessToken.create({ user_id: user.id, client_id: client.client_id }).fetch().usingConnection(db);
        console.info('Success');
        return proceed(null, { refreshToken: refreshToken.token, accessToken: accessToken.token });
      } catch (err) {
        return proceed(err);
      }
    });

    console.info('Finishing exchange');

    //SUCCESS LOGIN LOG
    //await LoggingService.userLogin(user, true, result.accessToken);

    return done(null, result.accessToken, result.refreshToken, { 'expires_in': sails.config.appSettings.tokenLife || sails.config.oauth.tokenLife });
  } catch (err) {
    return done(err);
  }
}));

// Exchange refreshToken for access token.
server.exchange(oauth2orize.exchange.refreshToken(async function (client, refreshToken, scope, done) {

  try {
    const token = RefreshToken.findOne({ token: refreshToken });
    if (!token) { return done(null, false); }

    const user = User.findOne({ id: token.user_id });
    if (!user) { return done(null, false); }

    await RefreshToken.destroy({ user_id: user.id, client_id: client.client_id });
    await AccessToken.destroy({ user_id: user.id, client_id: client.client_id });

    const result = await sails.getDatastore().transaction(async function (db, proceed) {
      try {
        const refreshToken = await RefreshToken.create({ user_id: user.id, client_id: client.client_id }).fetch().usingConnection(db);
        const accessToken = await AccessToken.create({ user_id: user.id, client_id: client.client_id }).fetch().usingConnection(db);
        return proceed(null, { refreshToken: refreshToken.token, accessToken: accessToken.token });
      } catch (err) {
        return proceed(err);
      }
    });

    return done(null, result.accessToken, result.refreshToken, { 'expires_in': sails.config.appSettings.tokenLife || sails.config.oauth.tokenLife });
  } catch (err) {
    return done(err);
  }
}));



server.exchange(oauth2orize.exchange.clientCredentials(async function (client, scope, done) {
  try {
    await RefreshToken.destroy({ client_id: client.client_id });
    await AccessToken.destroy({ client_id: client.client_id });

    var result = await sails.getDatastore().transaction(async (db, proceed) => {
      try {
        var refreshToken = await RefreshToken.create({ client_id: client.client_id }).fetch().usingConnection(db);
        var accessToken = await AccessToken.create({ client_id: client.client_id }).fetch().usingConnection(db);
        return proceed(null, { refreshToken: refreshToken.token, accessToken: accessToken.token });
      } catch (err) {
        return proceed(err);
      }
    });

    return done(null, result.accessToken, result.refreshToken, { 'expires_in': sails.config.appSettings.tokenLife || sails.config.oauth.tokenLife });
  } catch (err) {
    return done(err);
  }
}));

exports = module.exports = server;
