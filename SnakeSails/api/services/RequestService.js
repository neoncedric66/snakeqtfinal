var parley = require('parley');

module.exports = {
  /**
   * @Description: Create array of sub objects containing key/s.
   * @Limitation: Will only work on nested Object but not on Object with nested array unless exact index is known.
   *              For more info on how flatnest works, please visit: https://www.npmjs.com/package/flatnest
   * @Param: input - list of objects / object
   * @Param: keyList - key/s to use for comparison
   * @Error : Default: E_INVALID_DATA
   * @Custom Error: use useError(<ErrorService error goes here>)
   */
  extractValues: function(input, keyList) {
    var invalidError = ErrorService.errorList.E_INVALID_DATA;
    var deferred = parley(function (done) {
      if (input.constructor === Object){
        input = [input];
      }
      sails.helpers.array.extractValues.with({objList:input, keyList:keyList}).switch({
        error: function (err) {
          return done(ErrorService.createError(ErrorService.errorList.E_IMPLEMENTATION_ERROR));
        },
        invalidData: function(){
          return done(ErrorService.createError(invalidError));
        },
        success: function (result) {
          return done(undefined, result.extractedList);
        }
      });
    }, undefined, {
      useError: function (cInvalidError) {
        invalidError = cInvalidError ? cInvalidError : invalidError;
        return deferred;
      },
    });
    return deferred;
  },

  
};
