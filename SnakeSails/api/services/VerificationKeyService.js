var parley = require('parley');
var shortid = require('shortid');

module.exports = {
    create: function (inputs, db) { return imp_verifyKey.create(inputs, db); },
    find: function (inputs, db) { return imp_verifyKey.find(inputs, db); },
    delete: function (inputs, db) { return imp_verifyKey.delete(inputs, db); },
};

var imp_verifyKey = {
    create: function (inputs, db) {
        var deferred = parley(async function (done) {
            try {
                let { count } = inputs;
                if (count < 1) { throw ErrorService.createError(ErrorService.errorList.E_INVALID_DATA, 'count'); }
                let keysToCreate = [...Array(count)].map((_, i) => { return { code: shortid.generate(), status: 'unused' }; });
                let createdKeys = await VerificationKey.createEach(keysToCreate).usingConnection(db).meta({ fetch: true });
                return done(undefined, createdKeys);
            } catch (err) {
                return done(ErrorService.createCustomError(err.code, err.message));
            }
        });
        return deferred;
    },
    find: function (inputs, db) {
        var deferred = parley(async function (done) {
            try {
                let { code,
                    user,
                    status,
                    id,
                    custom } = inputs;
        
                let { skip,
                    limit,
                    sort,
                    populate,
                    populateAttributes } = inputs;
                //Create the query based on the parameters passed.
                var query = {};
                var where = { or: [] };

                // if request is post, expect custom input data
                if (custom !== undefined && custom.length) {
                    where.or = custom.filter(function (obj) { return Object.keys(obj).length });
                }

                if (code !== undefined && code.length) {
                    where.or.push({ code: code.split(',').map(String).filter(Boolean) });
                }
                if (user != undefined && user.length) {
                    where.or.push({ user: user.split(',').map(Number).filter(Boolean) });
                }
                if (status != undefined && status.length) {
                    where.or.push({ status: status.split(',').map(String).filter(Boolean) });
                }
                if (id != undefined && id.length) {
                    where.or.push({ id: id.split(',').map(Number).filter(Boolean) });
                }

                query.where = where.or.length > 0 ? where : {};

                if (skip !== undefined && skip > -1) {
                    query.skip = skip;
                }
                if (limit !== undefined && limit > -1) {
                    query.limit = limit;
                };
                if (sort !== undefined) {
                    if (['ASC', 'DESC'].includes(sort.toUpperCase())) {
                        query.sort = 'code ' + sort.toUpperCase();
                    }
                }

                let queryAction = VerificationKey.find(query);

                if (populate && populateAttributes) {
                    for (let attribute of populateAttributes) {
                        queryAction.populate(attribute);
                    }
                }

                let foundData = await queryAction;

                return done(undefined, foundData);

            } catch (err) {
                return done(ErrorService.createCustomError(err.code, err.message));
            }
        });
        return deferred;
    },
    delete: function (inputs, db) {
        var deferred = parley(async function (done) {
            try {
                let {code} = inputs;
                let unusedKey = await VerificationKey.count({ code:code, status:'unused' }).usingConnection(db);
                if (unusedKey < 1) { throw ErrorService.createError(ErrorService.errorList.E_VERIFICATION_CODE_UNAVAILABLE, code); };
                let deletedKey = await VerificationKey.destroy({ code:code }).usingConnection(db).meta({ fetch: true });
                return done(undefined, deletedKey);
            } catch (err) {
                return done(ErrorService.createCustomError(err.code, err.message));
            }
        });
        return deferred;
    },
}
