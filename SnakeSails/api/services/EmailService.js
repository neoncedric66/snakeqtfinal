
/**
*  @Description: Nodemailer is a module for Node.js applications to allow easy as cake email sending
*  @Usage : let transporter = nodemailer.createTransport(transport[, defaults])
*            * transporter - is going to be an object that is able to send mail
*            * transport - is the transport configuration object, connection url or a transport plugin instance
*            * defaults - is an object that defines default values for mail options
*/


/**
*  Gmail as SMTP Server
*  @Usage :  * service - gmail
*            * auth - your gmail account credential { user, pass }
*/


/* var parley = require('parley');
var nodemailer = require('nodemailer');
let transporter = nodemailer.createTransport({
  service: 'gmail',
  auth: {
    user: 'testaccount@gmail.com',
    pass: '1qaz2wsx'
  },
}); */

var parley = require('parley');
var nodemailer = require('nodemailer');
let transporter = nodemailer.createTransport({
  pool: true,
  host: 'in-v3.mailjet.com',
  port: 465 ,  //alternative port 25, 465, 588, 587, 465
  secure: true,
  auth: {
    user: '879932d434b09c2006e1889421bfee34',
    pass: '3d8247fc7b10eb6734dcd33bfec1ace1'
  },
  tls: {
    rejectUnauthorized: false
  }
});

module.exports = {

  /**
  * sendEmail
  * @Param: options - message configuration
  *           * from - The email address of the sender. All email addresses can be plain ‘sender@server.com’ or formatted ’“Sender Name” sender@server.com‘, see Address object for details
  *   * to - Comma separated list or an array of recipients email addresses that will appear on the To: field
  *   * subject - The subject of the email
  *   * text - The plaintext version of the message.
  *   * html - The HTML version of the message as an Unicode string, Buffer, Stream or an attachment-like object ({path: ‘http://…‘})
  *   * attachments - An array of attachment objects. Attachments can be used for embedding images as well.
  *
  *   note: if you used Gmail at SMTP server you need to add your email account to be able to use as sender
  *   instructions:
  *   1. open your gmail account (https://mail.google.com)
  *   2. go to the "settings tab" > "account and imports"
  *   3. and add your email account to "send mail as"
  *
  */

  sendEmail: function (options) {
    var deferred = parley(function (done) {
      transporter.sendMail({
        from: options.from, // sender address
        to: options.to, // list of receivers separeted by comma's
        subject: options.subject, // Subject line
        text: options.text, // plain text body
        html: options.body, // html body
        attachments: options.attachments

      }, function (err, info) {
        if (err) {
          return done(err);
        }
        //if no error
        return done(undefined, info);
      });
    });
    return deferred;
  },

  getEmailTemplate: function (templateName) {
    var deffered = parley(function (done) {
      var fs = require('fs');
      fs.readFile(templateName, 'utf8', function (err, data) {
        if (err) { return done(err); }
        return done(undefined, data);
      });
    });

    return deffered;
  }
};
