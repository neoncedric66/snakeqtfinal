var parley = require("parley");

module.exports = {
  update: function(inputs, db) {
    return imp_bitePatientStatus.createOrUpdate(inputs, db)
  },
};

var imp_bitePatientStatus = {
    createOrUpdate: function(inputs, db) {
        var deferred = parley(async function(done) {
            try {
                let {bite} = inputs
                let [countBite] = await Promise.all([
                    Bite.count({id: bite}).usingConnection(db),
                ]);
    
                if (countBite <= 0) {
                    throw ErrorService.createError(ErrorService.errorList.E_NOT_EXIST, "bite")
                }

                let countBitePatientStatus = await BitePatientStatus.count({bite: bite}).usingConnection(db)
                let bitePatientStatus
                if (countBitePatientStatus > 0) {
                    // update
                    bitePatientStatus = _.head(await BitePatientStatus.update({bite: bite}).set(inputs).usingConnection(db).meta({fetch: true}))
                } else {
                    // create
                    bitePatientStatus = await BitePatientStatus.create(inputs).usingConnection(db).meta({fetch: true})
                }

                // update Bite
                let updatedBite = _.head(await Bite.update({id: bite}).set({"patientStatus": bitePatientStatus.id}).usingConnection(db).meta({fetch: true}))
               
                return done(undefined, bitePatientStatus)
            } catch (err) {
                return done(ErrorService.createCustomError(err.code, err.message))
            }
        });
        return deferred
    },
};
