var parley = require('parley');

module.exports = {
  create: function(inputs, db) {
    return imp_snake.create(inputs, db);
  },
  find: function(inputs, db) {
    return imp_snake.find(inputs, db);
  },
  update: function(inputs, db) {
    return imp_snake.update(inputs, db);
  },
  delete: function(inputs, db) {
    return imp_snake.delete(inputs, db);
  },
  uploadPhoto: function(inputs, db, env) {
    return imp_snakePhoto.upload(inputs, db, env);
  },
  findPhoto: function(inputs, db) {
    return imp_snakePhoto.find(inputs, db);
  },
  deletePhoto: function(inputs, db) {
    return imp_snakePhoto.delete(inputs, db);
  },
  verifyPhoto: function(inputs, db) {
    return imp_snakePhoto.verify(inputs, db);
  },
};

var imp_snake = {
  create: function(inputs, db) {
    var deferred = parley(async function(done) {
      try {
        let {tags} = inputs;
        let usedInputs = _.omit(inputs, 'tags');

        let countSnake = await Snake.count({
          name: usedInputs.name,
        }).usingConnection(db);
        if (countSnake > 0) {
          throw ErrorService.createError(
            ErrorService.errorList.E_ALREADY_EXIST
          );
        }
        //create snake
        let createdSnake = await Snake.create(usedInputs)
          .usingConnection(db)
          .meta({fetch: true});

        let tagList = tags.split(',');
        let acquiredTags = [];
        for (let aTag of tagList) {
          aTag = aTag.toLowerCase();
          let aTagObj = await Tag.findOrCreate(
            {name: aTag},
            {name: aTag}
          ).usingConnection(db);
          // if (!aTagObj) {
          //   aTagObj = await Tag.create({name: aTag})
          //     .usingConnection(db)
          //     .meta({fetch: true});
          // }

          let aSnakeObj = await SnakeTag.create({
            snake: createdSnake.id,
            tag: aTagObj.id,
          })
            .usingConnection(db)
            .meta({fetch: true});
          aSnakeObj.tag = aTagObj;
          acquiredTags.push(aSnakeObj);
        }
        createdSnake.tags = acquiredTags;
        return done(undefined, createdSnake);
      } catch (err) {
        return done(ErrorService.createCustomError(err.code, err.message));
      }
    });
    return deferred;
  },
  find: function(inputs, db) {
    var deferred = parley(async function(done) {
      try {
        let {name, scientificName, venomous, size, id, tags, custom} = inputs;

        let {skip, limit, sort, populate, populateAttributes} = inputs;
        //Create the query based on the parameters passed.
        var query = {};
        var where = {or: []};

        // if request is post, expect custom input data
        if (custom !== undefined && custom.length) {
          where.or = custom.filter(function(obj) {
            return Object.keys(obj).length;
          });

          for (let customObj in custom) {
            if ('tags' in customObj) {
              let tags = customObj['tags'];
              if (tags.length > 0) {
                let snakes = [];
                for (let aTag in tags) {
                  aTag = aTag.toLowerCase();
                  let snakeTag = await SnakeTag.findOne({
                    tag: aTag,
                  }).usingConnection(db);
                  snakes.push(snakeTag.snake);
                }
                if (snakes.length > 0) {
                  if ('id' in customObj) {
                    customObj['id'].push(snakes);
                  } else {
                    customObj['id'] = snakes;
                  }
                }
              }
            }
          }
        }

        if (name !== undefined && name.length) {
          where.or.push({
            name: name
              .split(',')
              .map(String)
              .filter(Boolean),
          });
        }
        if (scientificName != undefined && scientificName.length) {
          where.or.push({
            scientificName: scientificName
              .split(',')
              .map(String)
              .filter(Boolean),
          });
        }
        if (size != undefined && size.length) {
          where.or.push({
            size: size
              .split(',')
              .map(Number)
              .filter(Boolean),
          });
        }
        if (id != undefined && id.length) {
          where.or.push({
            id: id
              .split(',')
              .map(Number)
              .filter(Boolean),
          });
        }

        if (venomous != undefined && venomous.length) {
          where.or.push({venomous: venomous.split(',').filter(Boolean)});
        }

        if (tags != undefined && tags.length) {
          let tagList = tags.split(',');
          let snakes = [];
          for (let aTag of tagList) {
            aTag = aTag.toLowerCase();
            let aTagObj = await Tag.findOne({name: aTag}).usingConnection(db);
            let snakeTag = await SnakeTag.find({
              tag: aTagObj.id,
            }).usingConnection(db);
            for (let aSnake of snakeTag) {
              snakes.push(aSnake.snake);
            }
          }
          if (snakes.length) {
            where.or.push({id: snakes});
          }
        }

        if (where.or.length < 1 && tags != undefined && tags.length) {
          return done(undefined, []); //since there's nothing to find, we immediately return [] result
        } else {
          query.where = where.or.length > 0 ? where : {};
        }
        if (skip !== undefined && skip > -1) {
          query.skip = skip;
        }
        if (limit !== undefined && limit > -1) {
          query.limit = limit;
        }
        if (sort !== undefined) {
          if (['ASC', 'DESC'].includes(sort.toUpperCase())) {
            query.sort = 'id ' + sort.toUpperCase();
          }
        }

        let queryAction = Snake.find(query);

        if (populate && populateAttributes) {
          for (let attribute of populateAttributes) {
            queryAction.populate(attribute);
          }
        }

        let foundData = await queryAction;

        return done(undefined, foundData);
      } catch (err) {
        return done(ErrorService.createCustomError(err.code, err.message));
      }
    });
    return deferred;
  },
  update: function(inputs, db) {
    var deferred = parley(async function(done) {
      try {
        let {tags} = inputs;
        //extract info that can be updated
        let [extractedInput] = await RequestService.extractValues(inputs, [
          'id',
          'name',
          'scientificName',
          'venomous',
          'size',
          'details',
        ]);

        //find snake exist
        let foundSnake = await Snake.count({
          id: extractedInput.id,
        }).usingConnection(db);
        if (foundSnake < 1) {
          throw ErrorService.createError(
            ErrorService.errorList.E_NOT_EXIST,
            extractedInput.id
          );
        }

        //update snake info
        let updatedSnake = _.head(
          await Snake.update({id: extractedInput.id})
            .set(_.omit(extractedInput, 'id'))
            .usingConnection(db)
            .meta({fetch: true})
        );

        let tagList = tags.split(',');
        let acquiredTags = [];
        //destroy old snaketags
        await SnakeTag.destroy({snake: updatedSnake.id}).usingConnection(db);
        for (let aTag of tagList) {
          aTag = aTag.toLowerCase();
          let aTagObj = await Tag.findOrCreate(
            {name: aTag},
            {name: aTag}
          ).usingConnection(db);
          let aSnakeObj = await SnakeTag.create({
            snake: updatedSnake.id,
            tag: aTagObj.id,
          })
            .usingConnection(db)
            .meta({fetch: true});
          aSnakeObj.tag = aTagObj;
          acquiredTags.push(aSnakeObj);
        }
        updatedSnake.tags = acquiredTags;
        return done(undefined, updatedSnake);
      } catch (err) {
        return done(ErrorService.createCustomError(err.code, err.message));
      }
    });
    return deferred;
  },
  delete: function(inputs, db) {
    var deferred = parley(async function(done) {
      try {
        //find snake exist
        let {id} = inputs;
        let countSnake = await Snake.count({id: id}).usingConnection(db);
        if (countSnake < 1) {
          throw ErrorService.createError(
            ErrorService.errorList.E_NOT_EXIST,
            'id'
          );
        }
        //find snake is not being used by photo / bite
        let [countPhoto, countBite] = await Promise.all([
          Photo.count({snake: id}).usingConnection(db),
          Bite.count({snake: id}).usingConnection(db),
        ]);
        if (countPhoto > 0 || countBite > 0) {
          throw ErrorService.createError(ErrorService.errorList.E_IN_USE);
        }
        //delete snake
        let deletedTags = await SnakeTag.destroy({snake: id})
          .usingConnection(db)
          .meta({fetch: true});
        let deletedSnake = await Snake.destroyOne({id: id})
          .usingConnection(db)
          .meta({fetch: true});
        deletedSnake.tags = deletedTags;
        return done(undefined, deletedSnake);
      } catch (err) {
        return done(ErrorService.createCustomError(err.code, err.message));
      }
    });
    return deferred;
  },
};

var imp_snakePhoto = {
  upload: function(inputs, db, env) {
    var deferred = parley(async function(done) {
      try {
        let {snake, uploader} = inputs;
        let countSnake = await Snake.count({id: snake}).usingConnection(db);
        if (countSnake < 1) {
          throw ErrorService.createError(
            ErrorService.errorList.E_NOT_EXIST,
            'snake'
          );
        }
        let countUser = await User.count({id: uploader}).usingConnection(db);
        if (countUser < 1) {
          throw ErrorService.createError(
            ErrorService.errorList.E_NOT_EXIST,
            'uploader'
          );
        }

        //upload here
        let custom = {
          limitSize: sails.config.appSettings.photoLimitSize,
        };
        let uploadResponse = await FileTransferService.upload(
          env.req,
          sails.config.filetransfer.imageUploadType,
          custom
        );
        _.assign(inputs, {source: uploadResponse.files[0].fd});
        //create Photo object here
        let createdPhoto = await Photo.create(inputs)
          .usingConnection(db)
          .meta({fetch: true});
        return done(undefined, createdPhoto);
      } catch (err) {
        return done(ErrorService.createCustomError(err.code, err.message));
      }
    });
    return deferred;
  },
  find: function(inputs, db) {
    var deferred = parley(async function(done) {
      try {
        let {id, status, uploader, verifier, snakePart, snake, custom} = inputs;

        let {skip, limit, sort, populate, populateAttributes} = inputs;
        //Create the query based on the parameters passed.
        var query = {};
        var where = {or: []};

        // if request is post, expect custom input data
        if (custom !== undefined && custom.length) {
          where.or = custom.filter(function(obj) {
            return Object.keys(obj).length;
          });
        }

        if (id != undefined && id.length) {
          where.or.push({
            id: id
              .split(',')
              .map(Number)
              .filter(Boolean),
          });
        }
        if (status !== undefined && status.length) {
          where.or.push({
            status: status
              .split(',')
              .map(String)
              .filter(Boolean),
          });
        }
        if (uploader != undefined && uploader.length) {
          where.or.push({
            uploader: uploader
              .split(',')
              .map(Number)
              .filter(Boolean),
          });
        }
        if (verifier != undefined && verifier.length) {
          where.or.push({
            verifier: verifier
              .split(',')
              .map(Number)
              .filter(Boolean),
          });
        }
        if (snakePart != undefined && snakePart.length) {
          where.or.push({
            snakePart: snakePart
              .split(',')
              .map(String)
              .filter(Boolean),
          });
        }
        if (snake != undefined && snake.length) {
          where.or.push({
            snake: snake
              .split(',')
              .map(Number)
              .filter(Boolean),
          });
        }

        query.where = where.or.length > 0 ? where : {};

        if (skip !== undefined && skip > -1) {
          query.skip = skip;
        }
        if (limit !== undefined && limit > -1) {
          query.limit = limit;
        }
        if (sort !== undefined) {
          if (['ASC', 'DESC'].includes(sort.toUpperCase())) {
            query.sort = 'id ' + sort.toUpperCase();
          }
        }

        let queryAction = Photo.find(query);

        if (populate && populateAttributes) {
          for (let attribute of populateAttributes) {
            queryAction.populate(attribute);
          }
        }

        let foundData = await queryAction;

        return done(undefined, foundData);
      } catch (err) {
        return done(ErrorService.createCustomError(err.code, err.message));
      }
    });
    return deferred;
  },
  delete: function(inputs, db) {
    var deferred = parley(async function(done) {
      try {
        //find photo exist
        let {id} = inputs;
        let [foundPhoto] = await Photo.find({id: id}).usingConnection(db);
        if (!foundPhoto) {
          throw ErrorService.createError(
            ErrorService.errorList.E_NOT_EXIST,
            'id'
          );
        }
        //delete photo
        let oldFile = foundPhoto.source;
        let fileExist = await FileTransferService.fileExist(oldFile);
        if (fileExist) {
          let deletedFile = await FileTransferService.fileDelete(oldFile);
          if (!deletedFile) {
            throw ErrorService.createError(
              ErrorService.errorList.E_FILE_NOT_DELETE,
              'id'
            );
          }
        }
        let deletedPhoto = await Photo.destroy({id: id})
          .usingConnection(db)
          .meta({fetch: true});
        return done(undefined, deletedPhoto);
      } catch (err) {
        return done(ErrorService.createCustomError(err.code, err.message));
      }
    });
    return deferred;
  },
  verify: function(inputs, db) {
    var deferred = parley(async function(done) {
      try {
        let {id, verifier} = inputs;
        let [foundUser] = await User.find({id: verifier}).usingConnection(db);
        if (!foundUser) {
          throw ErrorService.createError(
            ErrorService.errorList.E_NOT_EXIST,
            'verifier'
          );
        }
        if (foundUser.type !== 'doctor') {
          throw ErrorService.createError(
            ErrorService.errorList.E_INVALID_USER,
            'verifier'
          );
        }
        let [foundPhoto] = await Photo.find({id: id}).usingConnection(db);
        if (!foundPhoto) {
          throw ErrorService.createError(
            ErrorService.errorList.E_NOT_EXIST,
            'id'
          );
        }
        if (foundPhoto.status === 'verified') {
          throw ErrorService.createError(
            ErrorService.errorList.E_PHOTO_VERIFIED,
            'id'
          );
        }
        if (foundPhoto.status === 'disabled') {
          throw ErrorService.createError(
            ErrorService.errorList.E_PHOTO_DISABLED,
            'id'
          );
        }
        let updatedPhoto = await Photo.update({id: id})
          .set({status: 'verified', verifier: verifier})
          .usingConnection(db)
          .meta({fetch: true});
        return done(undefined, updatedPhoto);
      } catch (err) {
        return done(ErrorService.createCustomError(err.code, err.message));
      }
    });
    return deferred;
  },
};
