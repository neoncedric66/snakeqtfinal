var passport = require('passport');

module.exports = function (req, res, next) {
  console.log('Authenticating user / client');
  return passport.authenticate(
    ['clientBasic', 'oauth2-client-password'],
    { session: false }
  )(req, res, next);
};
