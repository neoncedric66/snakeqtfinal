/**
 * oauthBearer policy
 *
 * @module      :: Policy
 * @description :: Simple policy to allow any authenticated user
 *                 Assumes that your login action in one of your controllers sets `req.session.authenticated = true;`
 * @docs        :: http://sailsjs.org/#!documentation/policies
 *
 */


module.exports = (req, res, next) => {
  const { user } = req;
  const { clientInfo } = req;
  // If origin of request is a user, proceed
  if (user) { return res.ok(user); }
  if (clientInfo) { return res.ok(clientInfo); }
  // if not, return forbidden response
  return res.forbidden();
};
