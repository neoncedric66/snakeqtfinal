module.exports = (req, res, next) => {
    const { user } = req;
    console.log(user)
    // If origin of request is a user, proceed
    if (user.type === "doctor") { return next(); }
    // if not, return forbidden response
    return res.forbidden();
  };
  