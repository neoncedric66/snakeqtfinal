/**
 * oauthBearer policy
 *
 * @module      :: Policy
 * @description :: Simple policy to allow any authenticated user
 *                 Assumes that your login action in one of your controllers sets `req.session.authenticated = true;`
 * @docs        :: http://sailsjs.org/#!documentation/policies
 *
 */

var passport = require("passport");

module.exports = function(req, res, next) {
  passport.authenticate("bearer", function(err, object) {
    if (err || !object) {
      return res.sendStatus(401);
      // TODO Remove this afterwards
      // res.redirect('/');
    }

    delete req.query.access_token;
    if (object.email) {
      req.user = object;
    } else {
      req.clientInfo = object;
    }

    return next();
  })(req, res);
};
