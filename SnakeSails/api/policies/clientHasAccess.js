module.exports = async function (req, res, next) {
  try {
    const { clientInfo } = req;
    

    // If origin of request is a client, proceed
    if (clientInfo) {
      return next();
    }
    // if not, return forbidden response
    return res.forbidden();
  } catch(err) {
    return res.forbidden();
  }
};
