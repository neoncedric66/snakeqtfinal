var Oauth = require('../services/Oauth.js');
var series = require('middleware-flow').series;

module.exports = function(req, res, next) {
  // console.log('oauthToken');
  // return Oauth.token()(req, res, next);
  return series(Oauth.token(), Oauth.errorHandler)(req,res,next);
};
