module.exports = (req, res, next) => {
  const { user } = req;
  // If origin of request is a user, proceed
  if (user) { return next(); }
  // if not, return forbidden response
  return res.forbidden();
};
