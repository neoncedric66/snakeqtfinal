module.exports = {
    
    
      friendlyName: 'Extract values',
    
    
      description: 'Return and array of only selected value/s by key/s.',
    
    
      inputs: {
        objList: {
          description: 'The array to be processed.',
          type: 'json',
          required: true
        },
        keyList: {
          description: 'List of keys to be used during extraction.',
          type: 'json',
          required: true
        },
      },
    
    
      exits: {
        invalidData: {
          description: 'Parameter types are not correct.'
        }
      },
    
    
      fn: async function (inputs, exits) {
        let objList = inputs.objList;
        let keyList = inputs.keyList;
    
        if (objList.constructor !== Array || keyList.constructor !== Array) {
          return exits.invalidData('Input is not an array.');
        }
    
        let fn = require("flatnest");
        var customError = undefined;
        let result = [];
        objList.every(function(obj){
          if (obj.constructor !== Object){
            customError = 'Input is not an object.';
            return false;
          }
          var extObj = {};
          keyList.map(function(key){
            if(obj.hasOwnProperty(key)){
              extObj[key] = fn.seek(obj, key);
            }
          })
          result.push(fn.nest(extObj));
          return true;
        })
    
        if (customError !== undefined) {
          return exits.invalidData(customError);
        }
        return exits.success({extractedList:result});
      }
    
    
    };
    
    