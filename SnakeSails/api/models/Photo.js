/**
 * Photo.js
 *
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  schema: true,

  attributes: {

    status: {
      type: 'string',
      columnType: 'VARCHAR(15)',
      defaultsTo: 'pending',
      isIn: ['verified', 'disabled', 'pending']
    },

    uploader: {
      model: 'user',
      required: true,
      columnName: 'uploader_id',
    },

    source: {
      type: 'string',
      columnType: 'VARCHAR(255)',
      required: true,
    },

    verifier: {
      model: 'user',
      columnName: 'verifier_id'
    },

    snakePart: {
      type: 'string',
      columnType: 'VARCHAR(20)',
      required: true,
      isIn: ['general', 'head', 'body', 'tail', 'other']
    },

    snake: {
      model: 'snake',
      columnName: 'snake_id',
      required: true
    },

  },

};

