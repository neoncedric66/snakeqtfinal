/**
 * Snake.js
 *
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  schema: true,

  attributes: {
    name: {
      type: 'string',
      columnType: 'VARCHAR(100)',
      required: true,
      unique: true,
    },

    scientificName: {
      type: 'string',
      columnType: 'VARCHAR(100)',
      required: true,
    },

    venomous: {
      type: 'boolean',
      required: true,
    },

    size: {
      type: 'number',
      columnType: 'INT',
      required: true,
    },

    details: {
      type: 'string',
      columnType: 'TEXT',
      required: true,
    },

    photo_url: {
      type: 'string',
      columnType: 'VARCHAR(500)',
      required: false,
    },

    tags: {
      collection: 'snaketag',
      via: 'snake',
    },
  },
};
