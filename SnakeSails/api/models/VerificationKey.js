/**
 * Verification_Key.js
 *
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  tableName: 'verification_key',
  schema: true,

  attributes: {

    code: {
      type: 'string',
      columnType: 'VARCHAR(100)',
      required: true,
    },

    status: {
      type: 'string',
      columnType: 'VARCHAR(10)',
      required: true,
      isIn: ['used','unused'],
    },

    user: {
      model: 'user',
      columnName: 'user_id',
    },

  },

};

