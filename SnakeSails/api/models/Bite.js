/**
 * Bite.js
 *
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

    schema: true,

    attributes: {

        reporter: {
            model: 'user',
            required: true,
            columnName: 'reporter_id',
        },

        longitude: {
            type: 'string',
            columnType: 'VARCHAR(50)',
            required: true,
        },

        latitude: {
            type: 'string',
            columnType: 'VARCHAR(50)',
            required: true,
        },

        description: {
            type: 'string',
            columnType: 'TEXT',
            required: true,
        },

        timeOfBite: {
            type: 'number',
            required: true,
        },

        snake: {
            model: 'snake',
            columnName: 'snake_id',
            required: false
        },

        environment: {
            type: 'string',
            columnType: 'VARCHAR(20)',
            required: true,
            isIn: ['garden', 'field', 'rice paddy', 'plantation', 'unknown', 'other', 'inside house']
        },

        circumstance: {
            type: 'string',
            columnType: 'VARCHAR(20)',
            required: true,
            isIn: ['walking', 'working', 'recreational', 'catching snakes', 'unknown', 'other']
        },

        siteOfBite: {
            type: 'string',
            columnType: 'VARCHAR(20)',
            required: true,
            isIn: ['hand', 'foot', 'unknown', 'other']
        },

        isAtDominantSide: {
            type: 'boolean',
            defaultsTo: false
        },

        hash: {
            type: 'string',
            columnType: 'VARCHAR(50)',
            required: true
        },

        photos: {
            collection: 'bitephoto',
            via: 'bite'
        },

        description: {
            type: 'string',
            columnType: 'TEXT',
            defaultsTo: "",
        },

        diagnoses: {
            collection: 'BiteDiagnosis',
            via: 'bite',
        },

        patientStatus: {
            model: 'bitepatientstatus',
        },

    },

    customToJSON: function() {
        return _.omit(this, ['hash'])
    },

};

