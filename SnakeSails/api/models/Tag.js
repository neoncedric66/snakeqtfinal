/**
 * Tag.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {
    tableName: 'tag',
    schema: true,
  
    attributes: {
        name: {
            type: 'string',
            columnType: 'VARCHAR(50)',
            required: true,
            unique: true,
          },
    }
  
  };
  
  