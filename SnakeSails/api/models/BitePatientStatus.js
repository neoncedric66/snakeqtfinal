/**
 * BitePatientStatus.js
 *
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

    schema: true,

    attributes: {

        bite: {
            model: 'bite',
            required: true,
        },

        notes: {
            type: 'string',
            columnType: 'TEXT',
            defaultsTo: "",
        },

        isDead: {
            type: 'boolean',
            defaultsTo: false
        },

        isAdmitted: {
            type: 'boolean',
            defaultsTo: false
        },

        admissionHospital: {
            type: 'string',
            columnType: 'VARCHAR(255)',
            defaultsTo: "",
        },

        admissionHospitalType: {
            type: 'string',
            columnType: 'VARCHAR(20)',
            isIn: ['provincial', 'regional', 'national', 'other']
        },

        isReferred: {
            type: 'boolean',
            defaultsTo: false
        },

        referredHospital: {
            type: 'string',
            columnType: 'VARCHAR(255)',
            defaultsTo: "",
        },

        referredHospitalType: {
            type: 'string',
            columnType: 'VARCHAR(20)',
            isIn: ['provincial', 'regional', 'national', 'other']
        },

    },

    customToJSON: function() {
        return _.omit(this, ['id'])
    },

};

