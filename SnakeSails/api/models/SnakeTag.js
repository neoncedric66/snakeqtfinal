/**
 * SnakeTag.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {
    tableName: 'snaketag',
    schema: true,
  
    attributes: {
      snake: {
        model: 'snake',
        required: true,
      },
      tag: {
        model: 'tag',
        required: true,
      },
    }
  
  };
  
  