/**
 * Client.js
 *
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

/* global UtilsService */

module.exports = {

  attributes: {

    name: {
      type: 'string',
      required: true
    },
    redirect_uri: {
      type: 'string',
      required: true
    },
    client_id: {
      type: 'string',
    },
    client_secret: {
      type: 'string',
    },
    trusted: {
      type: 'boolean',
      defaultsTo: false
    },
    status: {
      type: 'string',
      columnType: 'VARCHAR(15)',
      required: true,
      isIn: ['active', 'blocked', 'deactivated']
    },


    //  ╔═╗╦═╗╦╔╦╗╦╔╦╗╦╦  ╦╔═╗╔═╗
    //  ╠═╝╠╦╝║║║║║ ║ ║╚╗╔╝║╣ ╚═╗
    //  ╩  ╩╚═╩╩ ╩╩ ╩ ╩ ╚╝ ╚═╝╚═╝


    //  ╔═╗╔╦╗╔╗ ╔═╗╔╦╗╔═╗
    //  ║╣ ║║║╠╩╗║╣  ║║╚═╗
    //  ╚═╝╩ ╩╚═╝╚═╝═╩╝╚═╝


    //  ╔═╗╔═╗╔═╗╔═╗╔═╗╦╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
    //  ╠═╣╚═╗╚═╗║ ║║  ║╠═╣ ║ ║║ ║║║║╚═╗
    //  ╩ ╩╚═╝╚═╝╚═╝╚═╝╩╩ ╩ ╩ ╩╚═╝╝╚╝╚═╝

  },

  beforeCreate: function(values, next){
    values.client_id = UtilsService.uidLight(10);
    values.client_secret = UtilsService.uid(30);
    next();
  }

};

