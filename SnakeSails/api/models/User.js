/**
 * User.js
 *
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  schema: true,

  attributes: {

    username: {
      type: 'string',
      columnType: 'VARCHAR(20)',
      required: true,
      unique: true,
    },

    password: {
      type: 'string',
      columnType: 'VARCHAR(100)',
      required: true,
    },

    firstName: {
      type: 'string',
      columnType: 'VARCHAR(20)',
      required: true,
    },

    lastName: {
      type: 'string',
      columnType: 'VARCHAR(20)',
      required: true,
    },

    birthDate: {
      type: 'number',
      columnType: 'BIGINT(20)',
      required: true,
    },

    phoneNumber: {
      type: 'string',
      columnType: 'VARCHAR(12)',
      required: true,
    },

    email: {
      type: 'string',
      columnType: 'VARCHAR(40)',
      required: true,
    },

    status: {
      type: 'string',
      columnType: 'VARCHAR(15)',
      required: true,
      isIn: ['active','pending', 'deactivated']
    },

    type: { //Doctor or Regular
      type: 'string',
      columnType: 'VARCHAR(20)',
      required: true,
      isIn: ['doctor','regular']
    },

    sex: {
      type: 'string',
      columnType: 'VARCHAR(20)',
      required: true,
      isIn: ['male','female']
    },

    occupation: {
      type: 'string',
      columnType: 'VARCHAR(100)',
      required: true,
    },

    uploadedPhotos: {
      collection: 'photo',
      via: 'uploader',
    },

    reportedBites: {
      collection: 'bite',
      via: 'reporter'
    },

  },
  customToJSON: function() {
    // Return a shallow copy of this record with the id and password removed.
    return _.omit(this, ['password'])
  },

  // Encrypt the password and set username for storing in the database.
  beforeCreate: async function (values, next) {
    values.password = await CryptoService.bHash(values.password);
    next();
  },

};

