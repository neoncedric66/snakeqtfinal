/**
 * BitePhoto.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {
    tableName: 'bitephoto',
    schema: true,
  
    attributes: {
      bite: {
        model: 'bite',
        required: true,
      },
      photo: {
        model: 'photo',
        required: true,
      },
    }
  
  };
  
  