/**
 * BiteDiagnosis.js
 *
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

    schema: true,

    attributes: {
        symptom: {
            model: 'symptom',
            required: true,
        },

        bite: {
            model: 'bite',
            required: true,
        },

        doctor: {
            model: 'user',
            required: true,
        },

        notes: {
            type: 'string',
            columnType: 'TEXT',
            defaultsTo: "",
        },

        status: {
            type: 'boolean',
            defaultsTo: false
        },

    },

    customToJSON: function() {
        return _.omit(this, ['id'])
    },

};

