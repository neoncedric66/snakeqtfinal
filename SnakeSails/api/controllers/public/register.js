module.exports = {


  friendlyName: 'Register',


  description: 'Register a User',


  inputs: {
    username: {
      description: "Username of the user",
      required: true,
      type: "string",
    },
    password: {
      description: "User's password",
      required: true,
      type: "string",
    },
    firstName: {
      description: "User's first name",
      required: true,
      type: "string",
    },
    lastName: {
      description: "User's last name",
      required: true,
      type: "string",
    },
    birthDate: {
      description: "User's birthday",
      required: true,
      type: "string",
    },
    phoneNumber: {
      description: "User's contact number",
      required: true,
      type: "string",
    },
    email: {
      description: "User's email address",
      required: true,
      type: "string",
    },
    status: {
      description: "Status of the user",
      defaultsTo: 'active',
      type: "string",
    },
    type: {
      description: "Type of user",
      required: true,
      type: "string",
    },
    code: {
      description: "Verification code if user type is doctor",
      type: "string"
    },
    sex: {
      description: "User's sex",
      required: true,
      type: "string"
    },
    occupation: {
      description: "User's occupation",
      required: true,
      type: "string"
    }
  },


  exits: {
    customErr: {
      description: 'Api detected an error with code and description.',
      responseType: 'badRequest'
    }
  },

  fn: async function (inputs, exits) {
    let dataStore = sails.getDatastore();
    var transaction = dataStore.transaction(async function (db, proceed) {
      try {
        let createdUser = await UserService.register(inputs, db);
        return proceed(undefined, { exit: 'SUCCESS', message: createdUser });

      } catch (err) { return proceed(ErrorService.createCustomError(err.code, err.message)); }
    });

    try {
      var result = await transaction;
      switch (result.exit) {
        case 'SUCCESS': return exits.success(result.message);
        default: return exits.error(result.message);
      }
    } catch (err) {
      if (err.code !== undefined) {
        return exits.customErr({ code: err.code, description: err.message });
      }
      return exits.error(err);
    }
  }
};

