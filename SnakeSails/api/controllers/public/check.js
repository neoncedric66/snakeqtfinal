const bcrypt = require('bcrypt');
const parley = require('parley');


module.exports = {

    friendlyName: 'Check Password',

    description: "Compare the user's entered password to the password",

    inputs: {

        password: {
            description: 'Entered password',
            type: 'string',
            required: true,
        },

        username: {
            type: 'string',
            required: true,
        },

    },


    exits: {
        customErr: {
            description: "Api detected an error with code and description.",
            responseType: "badRequest"
          }
    },



    fn: async function (inputs, exits) {
        try {

            let user = await User.findOne({
                username: inputs.username,
            });
            

            if (!user) {
                return exits.customErr({code: "LOGIN_ERROR", message: "Invalid Credentials"});
            };
            
            let result = await CryptoService.bCompare(inputs.password,user.password);
            
            if (!result) {
                return exits.customErr({code: "LOGIN_ERROR", message: "Invalid Credentials"});
            };

            return exits.success(user);
            
        } catch (err) {
            if (err.code !== undefined) {
                return exits.customErr({ code: err.code, description: err.message });
            }
            return exits.error(err);
        }



    },
}