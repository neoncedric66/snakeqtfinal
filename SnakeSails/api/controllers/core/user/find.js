module.exports = {


    friendlyName: 'Find',


    description: 'Find a User',


    inputs: {
        id: {
            description: 'Array of user id',
            type: 'string'
        },
        username: {
            description: 'Array of username',
            type: 'string'
        },
        firstName: {
            description: 'Array of first name',
            type: 'string'
        },
        lastName: {
            description: 'Array of last name',
            type: 'string'
        },
        birthDate: {
            description: 'Array of birthdate',
            type: 'string'
        },
        phoneNumber: {
            description: 'Array of phone numbers',
            type: 'string'
        },
        email: {
            description: 'Array of emails',
            type: 'string'
        },
        status: {
            description: 'Array of statuses',
            type: 'string'
        },
        type: {
            description: 'Array of user type doctor/regular',
            type: 'string'
        },
        sex: {
            description: 'Array of sexes',
            type: 'string'
        },
        occupation: {
            description: 'Array of occupations',
            type: 'string'
        },
        skip: {
            description: 'Number of entries to skip',
            extendedDescription: 'If the value is greater than number of entries, query will return empty array.',
            type: 'number'
        },
        limit: {
            description: 'Max number of results to return',
            extendedDescription: 'If limit is greater than max number of result, query will return everything.',
            type: 'number'
        },
        sort: {
            description: 'Ascending or descending order',
            extendedDescription: 'ASC / DESC',
            type: 'string'
        },
        populate: {
            description: 'Populate associated models or not.',
            type: 'boolean',
            defaultsTo: true
        },
        custom: {
            description: 'Array of custom conditions containing any of the ff: id, username, firstName, lastName, birthDate, phoneNumber, email, status, type, sex, occupation.',
            extendedDescription: 'May only be used if request is POST. Example body: {custom:[{type:doctor, id:[1,2]}, {username: ausername, id: 2}]}',
            type: 'json'
        }
    },
    exits: {
        customErr: {
            description: 'Api detected an error with code and description.',
            responseType: 'badRequest'
        }
    },

    fn: async function (inputs, exits) {
        let dataStore = sails.getDatastore();
        var transaction = dataStore.transaction(async function (db, proceed) {
            try {
                let foundUsers = await UserService.find(inputs, db);
                return proceed(undefined, { exit: 'SUCCESS', message: foundUsers });

            } catch (err) { return proceed(ErrorService.createCustomError(err.code, err.message)); }
        });

        try {
            var result = await transaction;
            switch (result.exit) {
                case 'SUCCESS': return exits.success(result.message);
                default: return exits.error(result.message);
            }
        } catch (err) {
            if (err.code !== undefined) {
                return exits.customErr({ code: err.code, description: err.message });
            }
            return exits.error(err);
        }
    }
};

