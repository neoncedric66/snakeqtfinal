module.exports = {


      friendlyName: 'Update',


      description: 'Update a User',


      inputs: {
        firstName: {
          description: "User's first name",
          type: "string",
        },
        lastName: {
          description: "User's last name",
          type: "string",
        },
        birthDate: {
          description: "User's birthday",
          type: "string",
        },
        phoneNumber: {
          description: "User's contact number",
          type: "string",
        },
        email: {
          description: "User's email address",
          type: "string",
        },
        status: {
          description: "Status of the user",
          type: "string",
        },
        sex: {
          description: "User's sex",
          type: "string"
        },
        occupation: {
          description: "User's occupation",
          type: "string"
        }
      },

      exits: {
        customErr: {
          description: 'Api detected an error with code and description.',
          responseType: 'badRequest'
        }
      },

      fn: async function (inputs, exits, env) {
        _.assign(inputs, { id: env.req.user.id });
        let dataStore = sails.getDatastore();
        var transaction = dataStore.transaction(async function (db, proceed) {
          try {
            let updatedUser = await UserService.update(inputs, db);
            return proceed(undefined, { exit: 'SUCCESS', message: updatedUser });

          } catch (err) { return proceed(ErrorService.createCustomError(err.code, err.message)); }
        });

        try {
          var result = await transaction;
          switch (result.exit) {
            case 'SUCCESS': return exits.success(result.message);
            default: return exits.error(result.message);
          }
        } catch (err) {
          if (err.code !== undefined) {
            return exits.customErr({ code: err.code, description: err.message });
          }
          return exits.error(err);
        }
      }
    };

