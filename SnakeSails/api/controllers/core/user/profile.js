module.exports = {
    
    
        friendlyName: 'Profile',
    
    
        description: 'Profile a User',
    
    
        inputs: {
        },
        exits: {
            customErr: {
                description: 'Api detected an error with code and description.',
                responseType: 'badRequest'
            }
        },
    
        fn: async function (inputs, exits, env) {
            _.assign(inputs, { id: env.req.user.id });
            let dataStore = sails.getDatastore();
            var transaction = dataStore.transaction(async function (db, proceed) {
                try {
                    let foundUsers = await UserService.find(inputs, db);
                    return proceed(undefined, { exit: 'SUCCESS', message: _.head(foundUsers) });
    
                } catch (err) { return proceed(ErrorService.createCustomError(err.code, err.message)); }
            });
    
            try {
                var result = await transaction;
                switch (result.exit) {
                    case 'SUCCESS': return exits.success(result.message);
                    default: return exits.error(result.message);
                }
            } catch (err) {
                if (err.code !== undefined) {
                    return exits.customErr({ code: err.code, description: err.message });
                }
                return exits.error(err);
            }
        }
    };
    
    