module.exports = {


    friendlyName: 'Update bite',


    description: 'Update bite',


    inputs: {
        id: {
            description: 'id of bite',
            required: true,
            type: 'number',
        },
        description: {
            description: "bite details",
            required: true,
            type: "string",
        },
        environment: {
            description: "where the bite happened",
            extendedDescription: "garden, field, rice paddy, plantation, unknown, other",
            required: true,
            type: "string"
        },
        circumstance: {
            description: "what was the victim doing when the bite happened",
            extendedDescription: "walking, working, recreational, catching snakes, unknown, other",
            required: true,
            type: "string"
        },
        siteOfBite: {
            description: "part of the body the snake bit",
            extendedDescription: "hand, foot, unknown, other",
            required: true,
            type: "string"
        },
        isAtDominantSide: {
            description: "If the bite is at the dominant side of the site of bite",
            required: true,
            type: "boolean"
        },
    },


    exits: {
      customErr: {
        description: 'Api detected an error with code and description.',
        responseType: 'badRequest'
      }
    },

    fn: async function (inputs, exits, env) {
      _.assign(inputs, { user: env.req.user.id });
      let dataStore = sails.getDatastore();
      var transaction = dataStore.transaction(async function (db, proceed) {
        try {
          let udpatedBite = await BiteService.update(inputs, db);
          return proceed(undefined, { exit: 'SUCCESS', message: udpatedBite });

        } catch (err) { return proceed(ErrorService.createCustomError(err.code, err.message)); }
      });

      try {
        var result = await transaction;
        switch (result.exit) {
          case 'SUCCESS': return exits.success(result.message);
          default: return exits.error(result.message);
        }
      } catch (err) {
        if (err.code !== undefined) {
          return exits.customErr({ code: err.code, description: err.message });
        }
        return exits.error(err);
      }
    }
  };

