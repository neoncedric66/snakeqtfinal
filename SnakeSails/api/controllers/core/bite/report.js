module.exports = {


      friendlyName: 'Report',


      description: 'Report bite',


      inputs: {
        longitude: {
            description: "coordinate of bite",
            required: true,
            type: "string",
        },
        latitude: {
            description: "coordinate of bite",
            required: true,
            type: "string",
        },
        description: {
            description: "bite details",
            required: true,
            type: "string",
        },
        timeOfBite: {
            description: "time of the bite",
            required: true,
            type: "number",
        },
        snake: {
            description: "biter",
            required: false,
            type: "number"
        },
        environment: {
          description: "where the bite happened",
          extendedDescription: "garden, field, rice paddy, plantation, unknown, other",
          required: true,
          type: "string"
        },
        circumstance: {
          description: "what was the victim doing when the bite happened",
          extendedDescription: "walking, working, recreational, catching snakes, unknown, other",
          required: true,
          type: "string"
        },
        siteOfBite: {
          description: "part of the body the snake bit",
          extendedDescription: "hand, foot, unknown, other",
          required: true,
          type: "string"
        },
        isAtDominantSide: {
          description: "If the bite is at the dominant side of the site of bite",
          required: true,
          type: "boolean"
        },
        photos: {
          description: "photo ids",
          extendedDescription: "[photo ids] / snakePart + snake + image; NOTE: If 'photo' exists, other attributes are ignored.",
          required: false,
          type: "string"
        },
        snakePart: {
          description: "part of the snake in the photo that is required if user will be uploading an 'image'",
          extendedDescription: "'general', 'head', 'body', 'tail', 'other'",
          required: false,
          type: "string"
        }
      },


      exits: {
        customErr: {
          description: 'Api detected an error with code and description.',
          responseType: 'badRequest'
        }
      },

      fn: async function (inputs, exits, env) {
        _.assign(inputs, { reporter: env.req.user.id });
        let dataStore = sails.getDatastore();
        var transaction = dataStore.transaction(async function (db, proceed) {
          try {
            if (env.req._fileparser && inputs.snakePart && inputs.snake && !inputs.photos){
              if (env.req._fileparser.upstreams.length){
                let uploadInputs = {snake:inputs.snake, snakePart:inputs.snakePart, uploader:inputs.reporter};
                let uploadedPhoto = await SnakeService.uploadPhoto(uploadInputs, db, env);
                _.assign(inputs, {photos:String(uploadedPhoto.id)});
              }
            }
            let createdBite = await BiteService.report(inputs, db);
            return proceed(undefined, { exit: 'SUCCESS', message: createdBite });

          } catch (err) { return proceed(ErrorService.createCustomError(err.code, err.message)); }
        });

        try {
          var result = await transaction;
          switch (result.exit) {
            case 'SUCCESS': return exits.success(result.message);
            default: return exits.error(result.message);
          }
        } catch (err) {
          if (err.code !== undefined) {
            return exits.customErr({ code: err.code, description: err.message });
          }
          return exits.error(err);
        }
      }
    };

