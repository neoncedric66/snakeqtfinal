module.exports = {


    friendlyName: 'Find',


    description: 'Find bite.',


    inputs: {
        id: {
            description: 'Array of bite id',
            type: 'string'
        },
        reporter: {
            description: 'Array of reporter id',
            type: 'string'
        },
        timeOfBite: {
            description: 'Array of time of bite',
            type: 'string'
        },
        snake: {
            description: 'Array of snake id',
            type: 'string'
        },
        environment: {
            description: 'Array of environment',
            type: 'string'
        },
        circumstance: {
            description: 'Array of circumstance',
            type: 'string'
        },
        siteOfBite: {
            description: 'Array of site of bite',
            type: 'string'
        },
        coordinate: {
            description: 'Array of longitude-latitude paired by :',
            extendedDescription: '1.2111:13.222',
            type: 'string'
        },
        skip: {
            description: 'Number of entries to skip',
            extendedDescription: 'If the value is greater than number of entries, query will return empty array.',
            type: 'number'
        },
        limit: {
            description: 'Max number of results to return',
            extendedDescription: 'If limit is greater than max number of result, query will return everything.',
            type: 'number'
        },
        sort: {
            description: 'Ascending or descending order',
            extendedDescription: 'ASC / DESC',
            type: 'string'
        },
        populate: {
            description: 'Populate associated models or not.',
            type: 'boolean',
            defaultsTo: true
        },
        custom: {
            description: 'Array of custom conditions containing any of the ff: responder, reporter, longitude, latitude, id, snake, timeOfBite, environment, circumstance, siteOfBite.',
            extendedDescription: 'May only be used if request is POST. Example body: {custom:[{responder:1, id:[1,2]}, {longitude: 1.2, latitude: 2.2}]}',
            type: 'json'
        }
    },


    exits: {
        customErr: {
            description: 'Api detected an error with code and description.',
            responseType: 'badRequest'
        }
    },


    fn: async function (inputs, exits, env) {
        let newInputs = inputs;
        if (env.req.method !== 'POST') {
            newInputs = _.omit(newInputs, ['custom']);
        }
        _.assign(newInputs, { populateAttributes: ['reporter', 'snake', 'photos', 'diagnoses', 'patientStatus'] });

        //Find and filter result based on query.
        try {
            let foundData = await BiteService.find(newInputs);
            return exits.success(foundData);
        } catch (err) {
            if (err.code !== undefined) {
                return exits.customErr({ code: err.code, description: err.message });
            }
            return exits.error(err);
        }
    }
};
