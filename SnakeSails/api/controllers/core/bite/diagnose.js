module.exports = {


    friendlyName: 'Diagnose bite',


    description: 'Diagnose bite',


    inputs: {
        diagnoses: {
            description: 'Array of diagnosis containing the ff: bite, symptom, notes, status.',
            extendedDescription: 'May only be used if request is POST. Example body: {diagnoses:[{bite: 1, symptom: 1, notes:"a note.", status:true}]}',
            type: 'json'
        }
    },


    exits: {
      customErr: {
        description: 'Api detected an error with code and description.',
        responseType: 'badRequest'
      }
    },

    fn: async function (inputs, exits, env) {
      _.assign(inputs, { doctor: env.req.user.id });
      let dataStore = sails.getDatastore();
      var transaction = dataStore.transaction(async function (db, proceed) {
        try {
          let biteDiagnoses = await BiteDiagnosisService.diagnose(inputs, db);
          return proceed(undefined, { exit: 'SUCCESS', message: biteDiagnoses });

        } catch (err) { return proceed(ErrorService.createCustomError(err.code, err.message)); }
      });

      try {
        var result = await transaction;
        switch (result.exit) {
          case 'SUCCESS': return exits.success(result.message);
          default: return exits.error(result.message);
        }
      } catch (err) {
        if (err.code !== undefined) {
          return exits.customErr({ code: err.code, description: err.message });
        }
        return exits.error(err);
      }
    }
  };

