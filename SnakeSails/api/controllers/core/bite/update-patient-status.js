module.exports = {


    friendlyName: 'Update bite patient status',


    description: 'Update bite patient status',


    inputs: {
        bite: {
            description: 'id of bite',
            required: true,
            type:  'number',
        },
        notes: {
            description: "bite patient status note",
            required: false,
            type: "string",
        },
        isDead: {
            description: "if patient died",
            required: false,
            type: "boolean"
        },
        isAdmitted: {
            description: "if patient is admitted to hospital",
            required: false,
            type: "boolean"
        },
        admissionHospital: {
            description: "name of admission hospital",
            required: false,
            type: "string",
        },
        admissionHospitalType: {
            description: "type of admission hospital ",
            extendedDescription: "provincial, regional, national, other",
            required: false,
            type: "string"
        },
        isReferred: {
            description: "if patient is referred to another hospital",
            required: false,
            type: "boolean"
        },
        referredHospital: {
            description: "name of admission hospital",
            required: false,
            type: "string",
        },
        referredHospitalType: {
            description: "type of admission hospital ",
            extendedDescription: "provincial, regional, national, other",
            required: false,
            type: "string"
        }
    },


    exits: {
      customErr: {
        description: 'Api detected an error with code and description.',
        responseType: 'badRequest'
      }
    },

    fn: async function (inputs, exits, env) {
      _.assign(inputs, { user: env.req.user.id });
      let dataStore = sails.getDatastore();
      var transaction = dataStore.transaction(async function (db, proceed) {
        try {
          let udpatedBitePatientStatus = await BitePatientStatusService.update(inputs, db);
          return proceed(undefined, { exit: 'SUCCESS', message: udpatedBitePatientStatus });

        } catch (err) { return proceed(ErrorService.createCustomError(err.code, err.message)); }
      });

      try {
        var result = await transaction;
        switch (result.exit) {
          case 'SUCCESS': return exits.success(result.message);
          default: return exits.error(result.message);
        }
      } catch (err) {
        if (err.code !== undefined) {
          return exits.customErr({ code: err.code, description: err.message });
        }
        return exits.error(err);
      }
    }
  };

