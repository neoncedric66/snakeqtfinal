module.exports = {
  friendlyName: "Upload",

  description: "Upload Snake Photo",

  inputs: {
    snake: {
      description: "snake id",
      required: true,
      type: "number"
    },
    snakePart: {
      description: "part of the snake in the photo",
      extendedDescription: "'general', 'head', 'body', 'tail', 'other'",
      required: true,
      type: "string"
    }
  },

  exits: {
    customErr: {
      description: "Api detected an error with code and description.",
      responseType: "badRequest"
    }
  },

  fn: async function(inputs, exits, env) {
    _.assign(inputs, { uploader: env.req.user.id });
    let dataStore = sails.getDatastore();
    var transaction = dataStore.transaction(async function(db, proceed) {
      try {
        let uploadedPhoto = await SnakeService.uploadPhoto(inputs, db, env);
        return proceed(undefined, { exit: "SUCCESS", message: uploadedPhoto });
      } catch (err) {
        return proceed(ErrorService.createCustomError(err.code, err.message));
      }
    });

    try {
      var result = await transaction;
      switch (result.exit) {
        case "SUCCESS":
          return exits.success(result.message);
        default:
          return exits.error(result.message);
      }
    } catch (err) {
      if (err.code !== undefined) {
        return exits.customErr({ code: err.code, description: err.message });
      }
      return exits.error(err);
    }
  }
};
