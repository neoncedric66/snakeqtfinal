module.exports = {


    friendlyName: 'Find',


    description: 'Find a Snake',

    inputs: {
        id: {
            description: 'Array of photo id',
            type: 'string'
        },
        status: {
            description: 'Array of photo status',
            extendedDescription: 'verified, disabled, pending',
            type: 'string'
        },
        uploader: {
            description: 'Array of user id',
            type: 'string'
        },
        verifier: {
            description: 'Array of user id',
            type: 'string'
        },
        snakePart: {
            description:'Array of snake parts',
            extendedDescription: 'general, head, body, tail, other',
            type: 'string'
        },
        snake: {
            description: 'Array of snake id',
            type: 'string'
        },
        skip: {
            description: 'Number of entries to skip',
            extendedDescription: 'If the value is greater than number of entries, query will return empty array.',
            type: 'number'
        },
        limit: {
            description: 'Max number of results to return',
            extendedDescription: 'If limit is greater than max number of result, query will return everything.',
            type: 'number'
        },
        sort: {
            description: 'Ascending or descending order',
            extendedDescription: 'ASC / DESC',
            type: 'string'
        },
        populate: {
            description: 'Populate associated models or not.',
            type: 'boolean',
            defaultsTo: true
        },
        custom: {
            description: 'Array of custom conditions containing any of the ff: id, status, uploader, verifier, snakePart, snake.',
            extendedDescription: 'May only be used if request is POST. Example body: {custom:[{status:[active], id:[1,2]}, {snake: [2]}]}',
            type: 'json'
        }
    },
    exits: {
        customErr: {
            description: 'Api detected an error with code and description.',
            responseType: 'badRequest'
        }
    },

    fn: async function (inputs, exits, env) {
        let newInputs = inputs;
        if (env.req.method !== 'POST') {
            newInputs = _.omit(newInputs, ['custom']);
        }
        _.assign(newInputs, { populateAttributes: ['verifier', 'uploader', 'snake'] });
        let dataStore = sails.getDatastore();
        var transaction = dataStore.transaction(async function (db, proceed) {
            try {
                let foundSnakes = await SnakeService.findPhoto(newInputs, db);
                return proceed(undefined, { exit: 'SUCCESS', message: foundSnakes });

            } catch (err) { return proceed(ErrorService.createCustomError(err.code, err.message)); }
        });

        try {
            var result = await transaction;
            switch (result.exit) {
                case 'SUCCESS': return exits.success(result.message);
                default: return exits.error(result.message);
            }
        } catch (err) {
            if (err.code !== undefined) {
                return exits.customErr({ code: err.code, description: err.message });
            }
            return exits.error(err);
        }
    }
};

