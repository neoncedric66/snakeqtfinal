module.exports = {
  friendlyName: "Verify",

  description: "Verify Snake Photo",

  inputs: {
    id: {
      description: "photo id",
      required: true,
      type: "number"
    }
  },

  exits: {
    customErr: {
      description: "Api detected an error with code and description.",
      responseType: "badRequest"
    }
  },

  fn: async function(inputs, exits, env) {
    _.assign(inputs, { verifier: env.req.user.id });
    let dataStore = sails.getDatastore();
    var transaction = dataStore.transaction(async function(db, proceed) {
      try {
        let verifiedPhoto = await SnakeService.verifyPhoto(inputs, db);
        return proceed(undefined, { exit: "SUCCESS", message: verifiedPhoto });
      } catch (err) {
        return proceed(ErrorService.createCustomError(err.code, err.message));
      }
    });

    try {
      var result = await transaction;
      switch (result.exit) {
        case "SUCCESS":
          return exits.success(result.message);
        default:
          return exits.error(result.message);
      }
    } catch (err) {
      if (err.code !== undefined) {
        return exits.customErr({ code: err.code, description: err.message });
      }
      return exits.error(err);
    }
  }
};
