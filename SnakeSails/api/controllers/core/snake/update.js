module.exports = {
  friendlyName: 'Update',

  description: 'Update snakes',

  inputs: {
    id: {
      description: 'id of snake',
      required: true,
      type: 'number',
    },
    name: {
      description: 'name of snake',
      type: 'string',
    },
    scientificName: {
      description: 'scientific name of snake',
      type: 'string',
    },
    venomous: {
      description: 'is nake venomous',
      type: 'boolean',
    },
    size: {
      description: 'average size of snake',
      type: 'number',
    },
    details: {
      description: 'description of the snake',
      type: 'string',
    },
    tags: {
      description: 'tag names',
      extendedDescription: 'tag names separated by comma',
      required: false,
      type: 'string',
    },
  },

  exits: {
    customErr: {
      description: 'Api detected an error with code and description.',
      responseType: 'badRequest',
    },
  },

  fn: async function(inputs, exits) {
    let dataStore = sails.getDatastore();
    var transaction = dataStore.transaction(async function(db, proceed) {
      try {
        let updatedSnake = await SnakeService.update(inputs, db);
        return proceed(undefined, {exit: 'SUCCESS', message: updatedSnake});
      } catch (err) {
        return proceed(ErrorService.createCustomError(err.code, err.message));
      }
    });

    try {
      var result = await transaction;
      switch (result.exit) {
        case 'SUCCESS':
          return exits.success(result.message);
        default:
          return exits.error(result.message);
      }
    } catch (err) {
      if (err.code !== undefined) {
        return exits.customErr({code: err.code, description: err.message});
      }
      return exits.error(err);
    }
  },
};
