module.exports = {
  friendlyName: 'Create',

  description: 'Create snakes',

  inputs: {
    name: {
      description: 'name of snake',
      required: true,
      type: 'string',
    },
    scientificName: {
      description: 'scientific name of snake',
      required: true,
      type: 'string',
    },
    venomous: {
      description: 'is nake venomous',
      required: true,
      type: 'boolean',
    },
    size: {
      description: 'average size of snake',
      required: true,
      type: 'number',
    },
    details: {
      description: 'description of snake',
      required: true,
      type: 'string',
    },
    photo_url: {
      description: 'photo url of the snake',
      required: false,
      type: 'string',
      defaultsTo: '',
    },
    tags: {
      description: 'tag names',
      extendedDescription: 'tag names separated by comma',
      required: false,
      type: 'string',
    },
  },

  exits: {
    customErr: {
      description: 'Api detected an error with code and description.',
      responseType: 'badRequest',
    },
  },

  fn: async function(inputs, exits, env) {
    let dataStore = sails.getDatastore();
    var transaction = dataStore.transaction(async function(db, proceed) {
      try {
        //upload here
        if (env.req._fileparser) {
          let custom = {
            limitSize: sails.config.appSettings.photoLimitSize,
          };
          if (env.req._fileparser) {
            if (env.req._fileparser.upstreams.length) {
              let uploadResponse = await FileTransferService.upload(
                env.req,
                sails.config.filetransfer.imageUploadType,
                custom
              );
              inputs.photo_url = uploadResponse.files[0].fd;
            }
          }
        }
        let createdSnake = await SnakeService.create(inputs, db);
        return proceed(undefined, {exit: 'SUCCESS', message: createdSnake});
      } catch (err) {
        return proceed(ErrorService.createCustomError(err.code, err.message));
      }
    });

    try {
      var result = await transaction;
      switch (result.exit) {
        case 'SUCCESS':
          return exits.success(result.message);
        default:
          return exits.error(result.message);
      }
    } catch (err) {
      if (err.code !== undefined) {
        return exits.customErr({code: err.code, description: err.message});
      }
      return exits.error(err);
    }
  },
};
