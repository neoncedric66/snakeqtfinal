module.exports = {
  friendlyName: 'Find',

  description: 'Find a Snake',

  inputs: {
    id: {
      description: 'Array of snake id',
      type: 'string',
    },
    name: {
      description: 'Array of snake names',
      type: 'string',
    },
    scientificName: {
      description: 'Array of snake scientific names',
      type: 'string',
    },
    venomous: {
      description:
        'Array of true and or false to find venomous/non venomous snake',
      type: 'string',
    },
    size: {
      description: 'Array of snake sizes',
      type: 'string',
    },
    tags: {
      description: 'tag names',
      extendedDescription: 'tag names separated by comma',
      required: false,
      type: 'string',
    },
    skip: {
      description: 'Number of entries to skip',
      extendedDescription:
        'If the value is greater than number of entries, query will return empty array.',
      type: 'number',
    },
    limit: {
      description: 'Max number of results to return',
      extendedDescription:
        'If limit is greater than max number of result, query will return everything.',
      type: 'number',
    },
    sort: {
      description: 'Ascending or descending order',
      extendedDescription: 'ASC / DESC',
      type: 'string',
    },
    populate: {
      description: 'Populate associated models or not.',
      type: 'boolean',
      defaultsTo: true,
    },
    custom: {
      description:
        'Array of custom conditions containing any of the ff: id, name, scientificName, venomous, size.',
      extendedDescription:
        'May only be used if request is POST. Example body: {custom:[{venomous:[true], id:[1,2]}, {name: boa, id: 2}]}',
      type: 'json',
    },
  },
  exits: {
    customErr: {
      description: 'Api detected an error with code and description.',
      responseType: 'badRequest',
    },
  },

  fn: async function(inputs, exits, env) {
    let newInputs = inputs;
    if (env.req.method !== 'POST') {
      newInputs = _.omit(newInputs, ['custom']);
    }
    _.assign(newInputs, {populateAttributes: ['tags']});
    let dataStore = sails.getDatastore();
    var transaction = dataStore.transaction(async function(db, proceed) {
      try {
        let foundSnakes = await SnakeService.find(newInputs, db);
        return proceed(undefined, {exit: 'SUCCESS', message: foundSnakes});
      } catch (err) {
        return proceed(ErrorService.createCustomError(err.code, err.message));
      }
    });

    try {
      var result = await transaction;
      switch (result.exit) {
        case 'SUCCESS':
          return exits.success(result.message);
        default:
          return exits.error(result.message);
      }
    } catch (err) {
      if (err.code !== undefined) {
        return exits.customErr({code: err.code, description: err.message});
      }
      return exits.error(err);
    }
  },
};
