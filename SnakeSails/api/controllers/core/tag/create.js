module.exports = {
    
    
        friendlyName: 'Create',
    
    
        description: 'Create tag',
    
    
        inputs: {
            name: {
                description: "name of tag",
                required: true,
                type: "string",
            },
        },
    
    
        exits: {
            customErr: {
                description: 'Api detected an error with code and description.',
                responseType: 'badRequest'
            }
        },
    
        fn: async function (inputs, exits, env) {
            let dataStore = sails.getDatastore();
            var transaction = dataStore.transaction(async function (db, proceed) {
                try {
                    let createdTag = await TagService.create(inputs, db);
                    return proceed(undefined, { exit: 'SUCCESS', message: createdTag });
    
                } catch (err) { return proceed(ErrorService.createCustomError(err.code, err.message)); }
            });
    
            try {
                var result = await transaction;
                switch (result.exit) {
                    case 'SUCCESS': return exits.success(result.message);
                    default: return exits.error(result.message);
                }
            } catch (err) {
                if (err.code !== undefined) {
                    return exits.customErr({ code: err.code, description: err.message });
                }
                return exits.error(err);
            }
        }
    };
    
    