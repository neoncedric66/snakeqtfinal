module.exports = {
    
    
        friendlyName: 'Update',
    
    
        description: 'Update tag',
    
    
        inputs: {
            id: {
                description: "id of tag",
                required: true,
                type: "number"
            },
            name: {
                description: "name of tag", 
                required: true,
                type: "string",
            },
        },
    
    
        exits: {
            customErr: {
                description: 'Api detected an error with code and description.',
                responseType: 'badRequest'
            }
        },
    
        fn: async function (inputs, exits) {
            let dataStore = sails.getDatastore();
            var transaction = dataStore.transaction(async function (db, proceed) {
                try {
                    let updatedTag = await TagService.update(inputs, db);
                    return proceed(undefined, { exit: 'SUCCESS', message: updatedTag });
    
                } catch (err) { return proceed(ErrorService.createCustomError(err.code, err.message)); }
            });
    
            try {
                var result = await transaction;
                switch (result.exit) {
                    case 'SUCCESS': return exits.success(result.message);
                    default: return exits.error(result.message);
                }
            } catch (err) {
                if (err.code !== undefined) {
                    return exits.customErr({ code: err.code, description: err.message });
                }
                return exits.error(err);
            }
        }
    };
    
    