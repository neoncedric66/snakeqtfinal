module.exports = {


    friendlyName: 'Find',


    description: 'Find verification key.',


    inputs: {
        code: {
            description: 'Array of code',
            type: 'string'
        },
        id: {
            description: 'Array of code id',
            type: 'string'
        },
        user: {
            description: 'Array of user id',
            type: 'string'
        },
        status: {
            description: 'Array of status: unused/used',
            type: 'string'
        },
        skip: {
            description: 'Number of entries to skip',
            extendedDescription: 'If the value is greater than number of entries, query will return empty array.',
            type: 'number'
        },
        limit: {
            description: 'Max number of results to return',
            extendedDescription: 'If limit is greater than max number of result, query will return everything.',
            type: 'number'
        },
        sort: {
            description: 'Ascending or descending order',
            extendedDescription: 'ASC / DESC',
            type: 'string'
        },
        populate: {
            description: 'Populate associated models or not.',
            type: 'boolean',
            defaultsTo: true
        },
        custom: {
            description: 'Array of custom conditions containing any of the ff: code, id, status, user.',
            extendedDescription: 'May only be used if request is POST. Example body: {custom:[{code:[A2werf, wweq3Qr], id:[1,2]},{status:[active]}]}',
            type: 'json'
        }
    },


    exits: {
        customErr: {
            description: 'Api detected an error with code and description.',
            responseType: 'badRequest'
        }
    },


    fn: async function (inputs, exits, env) {
        let newInputs = inputs;
        if (env.req.method !== 'POST') {
            newInputs = _.omit(newInputs, ['custom']);
        }
        _.assign(newInputs, { populateAttributes: ['user'] });

        //Find and filter result based on query.
        try {
            let foundData = await VerificationKeyService.find(newInputs);
            return exits.success(foundData);
        } catch (err) {
            if (err.code !== undefined) {
                return exits.customErr({ code: err.code, description: err.message });
            }
            return exits.error(err);
        }
    }
};
