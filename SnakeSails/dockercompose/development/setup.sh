#!/bin/bash
down="false"
count=1
mode="basic" #!/bare - basic - full
ignoredb="false"
while getopts d:c:b:x: option
do
 case "${option}"
 in
 d) down=${OPTARG};;
 c) count=${OPTARG};;
 b) mode=${OPTARG};;
 x) ignoredb=${OPTARG};;
 esac
done


if [ "$down" == "true" ]
then
  docker-compose down -v
fi

if [ "$ignoredb" == "true" ]
then
  docker-compose stop snake-core nginx
  docker-compose rm -v -f
  docker-compose build snake-core nginx
  COMPOSE_HTTP_TIMEOUT=200 docker-compose up -d --no-deps --no-recreate --remove-orphans --scale snake-core=$count
  # docker-compose up -d --no-deps -V --build --scale okto-core-faux=$count
else
  docker-compose build
  COMPOSE_HTTP_TIMEOUT=200 docker-compose up -d --remove-orphans --scale snake-core=$count
  sleep 20
  if [ "$mode" == "bare" ]
  then
    docker exec -it snake-core-db_dev bash -c "mysql -uroot -p1qaz2wsx snake < ./docker-entrypoint-initdb.d/init-bare.sql"
  elif [ "$mode" == "full" ]
  then
    docker exec -it snake-core-db_dev bash -c "mysql -uroot -p1qaz2wsx snake < ./docker-entrypoint-initdb.d/init.sql"
  else [ "$mode" == "basic" ]
    docker exec -it snake-core-db_dev bash -c "mysql -uroot -p1qaz2wsx snake < ./docker-entrypoint-initdb.d/init-basic.sql"
  fi
fi
