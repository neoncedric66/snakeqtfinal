-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 10, 2019 at 08:05 PM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `snake`
--

-- --------------------------------------------------------

--
-- Table structure for table `accesstoken`
--

CREATE TABLE `accesstoken` (
  `createdAt` bigint(20) DEFAULT NULL,
  `updatedAt` bigint(20) DEFAULT NULL,
  `id` int(11) NOT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  `client_id` varchar(255) DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL,
  `scope` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `accesstoken`
--

INSERT INTO `accesstoken` (`createdAt`, `updatedAt`, `id`, `user_id`, `client_id`, `token`, `scope`) VALUES
(1529325523215, 1529325523215, 82, '', '5XKSMNDT5T', 'IdGXY7kfCEradGkvcpm4iMUBqFYFcVzTy0UtiZBMIJuzQH4JvkqnkPlBD4iyn1mC7cn18jpV04nrX4RLoR763J06fQMfxqfnLHCy5V9CVTJHNgunwXEqYING8QLVmXgezHIFhPfIBj9aYOlI9RSPI6FPkaXNwK8aIhYHlosKHl7Hlv3QIg1ikZirrglVeQsyxrIpmyiQpqYT00SiXdvDYjLGSkuuuGYapX0s20BALLsx6KoQJaP4o0Q0OKR03d9', ''),
(1529325523488, 1529325523635, 83, '2', '5XKSMNDT5T', 'PRrMjlbwyjnp4Zh7mqbsOBXSh2H9r1ilz6Pjwx3u1KAUkUXQVg9mtBKxQSVk0YsAnZCV5noFRaNHOrm0GMiVn2dPiXtKfuGDDQk8PljaqCtOj6qkMLRL9pGFoH9QZKyrBYmQY6WDw9bTgiGUFt7qlD7xxmViXlsB8u9SJT5xKVO7NSTfn3XaWlFfoE7dtsPKexXp6yxuS1DI55vUDGKEddE2uOBOU4YEtnHigxfaAsmzC5rQ00lbrHZ7c4Tf6J4', ''),
(1575117848372, 1575118022775, 109, '', 'FTYBFCJHRY', 'oBATDqL9lPM3Wpy7q3GXGKIkA870ypjlGpOtioaHLV2dpB7beG5sbfig2GEma0AI77dlIh28GuznVGajWOYFAhtsw38wpcu2guJ7x83gmrP0eQG3eP2Fi2LUqhIwJURAU2zZ6KSbFUojB92IKXf3NsbIfszaehc7CQ2NSVbaUjx7JNchV7krsxdzR0bSEoeF9Q3PYgZTqARJ2NhzMLqYcIclPo7ApkHljOBRcGxl36K9BhxXQQkzcFPOqUv9lO7', '');

-- --------------------------------------------------------

--
-- Table structure for table `archive`
--

CREATE TABLE `archive` (
  `id` int(11) NOT NULL,
  `createdAt` bigint(20) DEFAULT NULL,
  `fromModel` varchar(255) DEFAULT NULL,
  `originalRecord` longtext,
  `originalRecordId` longtext
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `authcode`
--

CREATE TABLE `authcode` (
  `createdAt` bigint(20) DEFAULT NULL,
  `updatedAt` bigint(20) DEFAULT NULL,
  `id` int(11) NOT NULL,
  `code` varchar(255) DEFAULT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  `client_id` varchar(255) DEFAULT NULL,
  `redirect_uri` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bite`
--

CREATE TABLE `bite` (
  `createdAt` bigint(20) DEFAULT NULL,
  `updatedAt` bigint(20) DEFAULT NULL,
  `id` int(11) NOT NULL,
  `longitude` varchar(50) DEFAULT NULL,
  `latitude` varchar(50) DEFAULT NULL,
  `description` text,
  `timeOfBite` double DEFAULT NULL,
  `environment` varchar(20) DEFAULT NULL,
  `circumstance` varchar(20) DEFAULT NULL,
  `siteOfBite` varchar(20) DEFAULT NULL,
  `isAtDominantSide` tinyint(1) DEFAULT NULL,
  `hash` varchar(50) DEFAULT NULL,
  `reporter_id` int(11) DEFAULT NULL,
  `snake_id` int(11) DEFAULT NULL,
  `patientStatus` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bite`
--

INSERT INTO `bite` (`createdAt`, `updatedAt`, `id`, `longitude`, `latitude`, `description`, `timeOfBite`, `environment`, `circumstance`, `siteOfBite`, `isAtDominantSide`, `hash`, `reporter_id`, `snake_id`, `patientStatus`) VALUES
(1555893297198, 1564883710761, 2, '51.3333222', '10.222222222', 'The snake was huge', 1723923962, 'plantation', 'working', 'hand', 1, '3fe62067f2b6c5fe973b7e0956c7a4b5bd76807f', 2, 1, NULL),
(1561893583361, 1561893583361, 3, '51.3333222', '10.222222222', 'Hmmmm', 1723923962, 'plantation', 'walking', 'unknown', 1, 'c1f1aae71e1e50e816ce2370c7a1bb3e7f39fe43', 2, 1, NULL),
(1564883545065, 1564883545065, 4, '51.3333222', '10.222222222', 'Hmmmm', 1723923962, 'plantation', 'walking', 'unknown', 1, '4260bde9cf63b222d8f8eabf7156d90951fa5979', 3, 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `bitediagnosis`
--

CREATE TABLE `bitediagnosis` (
  `createdAt` bigint(20) DEFAULT NULL,
  `updatedAt` bigint(20) DEFAULT NULL,
  `id` int(11) NOT NULL,
  `notes` text,
  `status` tinyint(1) DEFAULT NULL,
  `symptom` int(11) DEFAULT NULL,
  `bite` int(11) DEFAULT NULL,
  `doctor` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bitediagnosis`
--

INSERT INTO `bitediagnosis` (`createdAt`, `updatedAt`, `id`, `notes`, `status`, `symptom`, `bite`, `doctor`) VALUES
(1564945913392, 1564946587647, 1, 'Patient is in pain', 1, 1, 2, 3),
(1564946519705, 1564946563345, 2, 'Patient is in pain', 1, 1, 4, 3);

-- --------------------------------------------------------

--
-- Table structure for table `bitepatientstatus`
--

CREATE TABLE `bitepatientstatus` (
  `createdAt` bigint(20) DEFAULT NULL,
  `updatedAt` bigint(20) DEFAULT NULL,
  `id` int(11) NOT NULL,
  `notes` text,
  `isDead` tinyint(1) DEFAULT NULL,
  `isAdmitted` tinyint(1) DEFAULT NULL,
  `admissionHospital` varchar(255) DEFAULT NULL,
  `admissionHospitalType` varchar(20) DEFAULT NULL,
  `isReferred` tinyint(1) DEFAULT NULL,
  `referredHospital` varchar(255) DEFAULT NULL,
  `referredHospitalType` varchar(20) DEFAULT NULL,
  `bite` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bitephoto`
--

CREATE TABLE `bitephoto` (
  `createdAt` bigint(20) DEFAULT NULL,
  `updatedAt` bigint(20) DEFAULT NULL,
  `id` int(11) NOT NULL,
  `bite` int(11) DEFAULT NULL,
  `photo` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bitephoto`
--

INSERT INTO `bitephoto` (`createdAt`, `updatedAt`, `id`, `bite`, `photo`) VALUES
(1555893297234, 1555893297234, 1, 2, 1),
(1561893583382, 1561893583382, 2, 3, 1),
(1564883545113, 1564883545113, 3, 4, 1);

-- --------------------------------------------------------

--
-- Table structure for table `client`
--

CREATE TABLE `client` (
  `createdAt` bigint(20) DEFAULT NULL,
  `updatedAt` bigint(20) DEFAULT NULL,
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `redirect_uri` varchar(255) DEFAULT NULL,
  `client_id` varchar(255) DEFAULT NULL,
  `client_secret` varchar(255) DEFAULT NULL,
  `trusted` tinyint(1) DEFAULT NULL,
  `status` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `client`
--

INSERT INTO `client` (`createdAt`, `updatedAt`, `id`, `name`, `redirect_uri`, `client_id`, `client_secret`, `trusted`, `status`) VALUES
(1538672532382, 1538672532402, 1, 'Snake App', 'https://google.com', 'FTYBFCJHRY', 'KFqpxpSidY5llJYciSpMk4L9lQZTAA', 1, 'active');

-- --------------------------------------------------------

--
-- Table structure for table `generateuser`
--

CREATE TABLE `generateuser` (
  `createdAt` bigint(20) DEFAULT NULL,
  `updatedAt` bigint(20) DEFAULT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `photo`
--

CREATE TABLE `photo` (
  `createdAt` bigint(20) DEFAULT NULL,
  `updatedAt` bigint(20) DEFAULT NULL,
  `id` int(11) NOT NULL,
  `status` varchar(15) DEFAULT NULL,
  `source` varchar(255) DEFAULT NULL,
  `snakePart` varchar(20) DEFAULT NULL,
  `uploader_id` int(11) DEFAULT NULL,
  `verifier_id` int(11) DEFAULT NULL,
  `snake_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `photo`
--

INSERT INTO `photo` (`createdAt`, `updatedAt`, `id`, `status`, `source`, `snakePart`, `uploader_id`, `verifier_id`, `snake_id`) VALUES
(1555893287908, 1555893287908, 1, 'pending', 'C:\\Users\\Princess Carabeo\\Documents\\Projects\\Sails\\Snake\\SnakeSails\\assets\\uploads\\images\\155589328790685b6f89b41cae26786ac72365fff771b.jpg', 'general', 2, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `refreshtoken`
--

CREATE TABLE `refreshtoken` (
  `createdAt` bigint(20) DEFAULT NULL,
  `updatedAt` bigint(20) DEFAULT NULL,
  `id` int(11) NOT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  `client_id` varchar(255) DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `refreshtoken`
--

INSERT INTO `refreshtoken` (`createdAt`, `updatedAt`, `id`, `user_id`, `client_id`, `token`) VALUES
(1575117848320, 1575117848320, 23, '', 'FTYBFCJHRY', 'vs5S7nRvlm79ISEdoevG4PWkpURS5V6KoWCyhrSnOTSnJvxZuQ1aS5hMsoEOdvZEIfDbj9BMnGSmkuNWww5AxBP0keMG0KmLYPu9e9YfT5Z7cRtFFfscFmBamDwrJTHZlYcst1JORsPvTd1esYvmAxitunxtjQucvXebyVhCtLhppEu8aIOkBuKIKCETeDsaepnd4vcYPfFveiKiOSuFsT2HMxy7To3pjy42Z57pTMXS9rqivy0tiGdXJxzvWB1');

-- --------------------------------------------------------

--
-- Table structure for table `snake`
--

CREATE TABLE `snake` (
  `createdAt` bigint(20) DEFAULT NULL,
  `updatedAt` bigint(20) DEFAULT NULL,
  `id` int(11) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `scientificName` varchar(100) DEFAULT NULL,
  `venomous` tinyint(1) DEFAULT NULL,
  `size` int(11) DEFAULT NULL,
  `details` text,
  `photo_url` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `snake`
--

INSERT INTO `snake` (`createdAt`, `updatedAt`, `id`, `name`, `scientificName`, `venomous`, `size`, `details`, `photo_url`) VALUES
(1555892964075, 1555892964075, 1, 'rattle snake', 'a venomous snake', 1, 20, 'This is a new snake', 'C:\\Users\\Princess Carabeo\\Documents\\Projects\\Sails\\Snake\\SnakeSails\\assets\\uploads\\images\\155589296403485b6f89b41cae26786ac72365fff771b.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `snaketag`
--

CREATE TABLE `snaketag` (
  `createdAt` bigint(20) DEFAULT NULL,
  `updatedAt` bigint(20) DEFAULT NULL,
  `id` int(11) NOT NULL,
  `snake` int(11) DEFAULT NULL,
  `tag` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `snaketag`
--

INSERT INTO `snaketag` (`createdAt`, `updatedAt`, `id`, `snake`, `tag`) VALUES
(1538672532382, 1538672532402, 1, 1, 1),
(1538672532382, 1538672532402, 2, 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `symptom`
--

CREATE TABLE `symptom` (
  `createdAt` bigint(20) DEFAULT NULL,
  `updatedAt` bigint(20) DEFAULT NULL,
  `id` int(11) NOT NULL,
  `name` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `symptom`
--

INSERT INTO `symptom` (`createdAt`, `updatedAt`, `id`, `name`) VALUES
(1564938202581, 1564938452692, 1, 'bruising');

-- --------------------------------------------------------

--
-- Table structure for table `tag`
--

CREATE TABLE `tag` (
  `createdAt` bigint(20) DEFAULT NULL,
  `updatedAt` bigint(20) DEFAULT NULL,
  `id` int(11) NOT NULL,
  `name` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tag`
--

INSERT INTO `tag` (`createdAt`, `updatedAt`, `id`, `name`) VALUES
(1555893375033, 1555893375033, 1, 'big snake'),
(1555893387050, 1564883191840, 2, 'very dangerous');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `createdAt` bigint(20) DEFAULT NULL,
  `updatedAt` bigint(20) DEFAULT NULL,
  `id` int(11) NOT NULL,
  `username` varchar(20) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `firstName` varchar(20) DEFAULT NULL,
  `lastName` varchar(20) DEFAULT NULL,
  `birthDate` bigint(20) DEFAULT NULL,
  `phoneNumber` varchar(12) DEFAULT NULL,
  `email` varchar(40) DEFAULT NULL,
  `status` varchar(15) DEFAULT NULL,
  `type` varchar(20) DEFAULT NULL,
  `sex` varchar(20) DEFAULT NULL,
  `occupation` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`createdAt`, `updatedAt`, `id`, `username`, `password`, `firstName`, `lastName`, `birthDate`, `phoneNumber`, `email`, `status`, `type`, `sex`, `occupation`) VALUES
(1555680496374, 1569638811207, 1, 'cedric123', '$2b$10$7g5CGAzgkXPkemrBn/KWVOvPybnHu1x6OxRpeSAZGAOnjDIWyakQG', 'Colson', 'Chua', 839289600000, '1234567890', 'cchua@bisvietnam.net', 'active', 'doctor', 'male', 'general doctor');

-- --------------------------------------------------------

--
-- Table structure for table `verification_key`
--

CREATE TABLE `verification_key` (
  `createdAt` bigint(20) DEFAULT NULL,
  `updatedAt` bigint(20) DEFAULT NULL,
  `id` int(11) NOT NULL,
  `code` varchar(100) DEFAULT NULL,
  `status` varchar(10) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `verification_key`
--

INSERT INTO `verification_key` (`createdAt`, `updatedAt`, `id`, `code`, `status`, `user_id`) VALUES
(1575117851441, 1575117851441, 1, 'VLyMILAK_', 'used', 1),
(1575117851441, 1575117851441, 2, 'xH2GwXPXK3', 'unused', NULL),
(1575117851441, 1575117851441, 3, 'SER2iB42hJ', 'unused', NULL),
(1575117851441, 1575117851441, 4, 'CxTL9j9FZs', 'unused', NULL),
(1575117851441, 1575117851441, 5, 'B6uT5rebVM', 'unused', NULL),
(1575117851441, 1575117851441, 6, 'Fho1OE3jbc', 'unused', NULL),
(1575117851441, 1575117851441, 7, 'HByCWgrC1M', 'unused', NULL),
(1575117851441, 1575117851441, 8, 'tz8QYRtbX1', 'unused', NULL),
(1575117851441, 1575117851441, 9, '5kalvtQ4ap', 'unused', NULL),
(1575117851441, 1575117851441, 10, 'e_m3fVLM1Q', 'unused', NULL),
(1575118009453, 1575118009453, 11, 'YOZ0_TuIf', 'unused', NULL),
(1575118009453, 1575118009453, 12, 'aac6SV55tC', 'unused', NULL),
(1575118009453, 1575118009453, 13, 'E4U9iYcZ8r', 'unused', NULL),
(1575118009453, 1575118009453, 14, 'dhPJIH6DK_', 'unused', NULL),
(1575118009453, 1575118009453, 15, 'eS0d38CUsz', 'unused', NULL),
(1575118009453, 1575118009453, 16, 'x8Tyo47cng', 'unused', NULL),
(1575118009453, 1575118009453, 17, 'A-4PeuF6zw', 'unused', NULL),
(1575118009453, 1575118009453, 18, 'vFtzaDm-L9', 'unused', NULL),
(1575118009453, 1575118009453, 19, 'ia9ud-Rm7Y', 'unused', NULL),
(1575118009453, 1575118009453, 20, 'SAiOpD4t5r', 'unused', NULL),
(1575118014832, 1575118014832, 21, '03CHoaetP', 'unused', NULL),
(1575118014832, 1575118014832, 22, 'oakW1uWpiW', 'unused', NULL),
(1575118014832, 1575118014832, 23, 'axwKsiNL7C', 'unused', NULL),
(1575118014832, 1575118014832, 24, 'Kc2VHx9jLV', 'unused', NULL),
(1575118014832, 1575118014832, 25, '_IZNuyJO09', 'unused', NULL),
(1575118014832, 1575118014832, 26, 'CQ9tx0a3v6', 'unused', NULL),
(1575118014832, 1575118014832, 27, '-ZZ6RzDKxm', 'unused', NULL),
(1575118014832, 1575118014832, 28, 'Z_UwpJKTyU', 'unused', NULL),
(1575118014832, 1575118014832, 29, 'N7lz1wEQMN', 'unused', NULL),
(1575118014832, 1575118014832, 30, 'FD4VCukz2m', 'unused', NULL),
(1575118016495, 1575118016495, 31, 'zVposHRVM', 'unused', NULL),
(1575118016495, 1575118016495, 32, 'HAkm_BvyYF', 'unused', NULL),
(1575118016495, 1575118016495, 33, 'jZCVRaNDo_', 'unused', NULL),
(1575118016495, 1575118016495, 34, 'Y-TZXStRbk', 'unused', NULL),
(1575118016495, 1575118016495, 35, '7LUgJUasmv', 'unused', NULL),
(1575118016495, 1575118016495, 36, 'bCPn64Mf28', 'unused', NULL),
(1575118016495, 1575118016495, 37, '_lQL7CkaPO', 'unused', NULL),
(1575118016495, 1575118016495, 38, 'bquvBqdU8L', 'unused', NULL),
(1575118016495, 1575118016495, 39, 'VyKh-xWx6f', 'unused', NULL),
(1575118016495, 1575118016495, 40, 'hL6S4HHeiR', 'unused', NULL),
(1575118018058, 1575118018058, 41, 'oyvMjS_Xp', 'unused', NULL),
(1575118018058, 1575118018058, 42, '2Dsb3pGezS', 'unused', NULL),
(1575118018058, 1575118018058, 43, 'rUmRxPd-_J', 'unused', NULL),
(1575118018058, 1575118018058, 44, 'EMSIlVr8Be', 'unused', NULL),
(1575118018058, 1575118018058, 45, 'dwv75tcLZE', 'unused', NULL),
(1575118018058, 1575118018058, 46, 'fFs96KTsY_', 'unused', NULL),
(1575118018058, 1575118018058, 47, 'dDgBROOvPB', 'unused', NULL),
(1575118018058, 1575118018058, 48, '6MVX05cJ24', 'unused', NULL),
(1575118018058, 1575118018058, 49, 'i_FdScLfos', 'unused', NULL),
(1575118018058, 1575118018058, 50, 'PWPHdn8llh', 'unused', NULL),
(1575118022831, 1575118022831, 51, 'gt1OQlcaW', 'unused', NULL),
(1575118022831, 1575118022831, 52, 'eejnUSlYaS', 'unused', NULL),
(1575118022831, 1575118022831, 53, 'KxbVdDPALE', 'unused', NULL),
(1575118022831, 1575118022831, 54, 'Qwx4BALa87', 'unused', NULL),
(1575118022831, 1575118022831, 55, 'BjmMtdMoC1', 'unused', NULL),
(1575118022831, 1575118022831, 56, 'iVFWuIwXIT', 'unused', NULL),
(1575118022831, 1575118022831, 57, 'uD86wWmqPU', 'unused', NULL),
(1575118022831, 1575118022831, 58, 'HYiVcUAYo_', 'unused', NULL),
(1575118022831, 1575118022831, 59, 'g0EggHL6fU', 'unused', NULL),
(1575118022831, 1575118022831, 60, 'VtIyzo5cfv', 'unused', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accesstoken`
--
ALTER TABLE `accesstoken`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `archive`
--
ALTER TABLE `archive`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `authcode`
--
ALTER TABLE `authcode`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `bite`
--
ALTER TABLE `bite`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `bitediagnosis`
--
ALTER TABLE `bitediagnosis`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `bitepatientstatus`
--
ALTER TABLE `bitepatientstatus`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `bitephoto`
--
ALTER TABLE `bitephoto`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `client`
--
ALTER TABLE `client`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `generateuser`
--
ALTER TABLE `generateuser`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `photo`
--
ALTER TABLE `photo`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `refreshtoken`
--
ALTER TABLE `refreshtoken`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `snake`
--
ALTER TABLE `snake`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `snaketag`
--
ALTER TABLE `snaketag`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `symptom`
--
ALTER TABLE `symptom`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `tag`
--
ALTER TABLE `tag`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `verification_key`
--
ALTER TABLE `verification_key`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `accesstoken`
--
ALTER TABLE `accesstoken`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=110;

--
-- AUTO_INCREMENT for table `archive`
--
ALTER TABLE `archive`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `authcode`
--
ALTER TABLE `authcode`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bite`
--
ALTER TABLE `bite`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `bitediagnosis`
--
ALTER TABLE `bitediagnosis`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `bitepatientstatus`
--
ALTER TABLE `bitepatientstatus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bitephoto`
--
ALTER TABLE `bitephoto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `client`
--
ALTER TABLE `client`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `generateuser`
--
ALTER TABLE `generateuser`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `photo`
--
ALTER TABLE `photo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `refreshtoken`
--
ALTER TABLE `refreshtoken`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `snake`
--
ALTER TABLE `snake`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `snaketag`
--
ALTER TABLE `snaketag`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `symptom`
--
ALTER TABLE `symptom`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tag`
--
ALTER TABLE `tag`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `verification_key`
--
ALTER TABLE `verification_key`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
