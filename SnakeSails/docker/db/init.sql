-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Nov 16, 2018 at 03:16 AM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `snake`
--

-- --------------------------------------------------------

--
-- Table structure for table `accesstoken`
--

CREATE TABLE `accesstoken` (
  `createdAt` bigint(20) DEFAULT NULL,
  `updatedAt` bigint(20) DEFAULT NULL,
  `id` int(11) NOT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  `client_id` varchar(255) DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL,
  `scope` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `accesstoken`
--

INSERT INTO `accesstoken` (`createdAt`, `updatedAt`, `id`, `user_id`, `client_id`, `token`, `scope`) VALUES
(1542330329594, 1542330810944, 1, '', '1U895T6L3Y', 'TCpdLqCr8KMYzgvh2lf4Ef6G4MIjtVlBz41JmjStUz6kGL5jXmNnnDlbHPOAmgmrmdKEzfHOu3yxhC0wYKV1r0tSEQvK1uVKT7CTSNHp55NnjBVgVSfzTixEy1W2MYogDyCIfLVlZtcgbyIBVXQO2kFAetHj7w1ZNT5rNKkiD6AzbUanRvK7v80ycWWDTgLbLZFnFVuC19b5pKibNaiPiQYiauZGxUqP4Oag78kGuZ7N9eKLyGXVJiqEKI9yK8B', '');

-- --------------------------------------------------------

--
-- Table structure for table `archive`
--

CREATE TABLE `archive` (
  `id` int(11) NOT NULL,
  `createdAt` bigint(20) DEFAULT NULL,
  `fromModel` varchar(255) DEFAULT NULL,
  `originalRecord` longtext,
  `originalRecordId` longtext
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `authcode`
--

CREATE TABLE `authcode` (
  `createdAt` bigint(20) DEFAULT NULL,
  `updatedAt` bigint(20) DEFAULT NULL,
  `id` int(11) NOT NULL,
  `code` varchar(255) DEFAULT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  `client_id` varchar(255) DEFAULT NULL,
  `redirect_uri` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bite`
--

CREATE TABLE `bite` (
  `createdAt` bigint(20) DEFAULT NULL,
  `updatedAt` bigint(20) DEFAULT NULL,
  `id` int(11) NOT NULL,
  `longitude` varchar(50) DEFAULT NULL,
  `latitude` varchar(50) DEFAULT NULL,
  `description` text,
  `timeOfBite` double DEFAULT NULL,
  `environment` varchar(20) DEFAULT NULL,
  `circumstance` varchar(20) DEFAULT NULL,
  `siteOfBite` varchar(20) DEFAULT NULL,
  `hash` varchar(50) DEFAULT NULL,
  `reporter_id` int(11) DEFAULT NULL,
  `snake_id` int(11) DEFAULT NULL,
  `photo_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `client`
--

CREATE TABLE `client` (
  `createdAt` bigint(20) DEFAULT NULL,
  `updatedAt` bigint(20) DEFAULT NULL,
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `redirect_uri` varchar(255) DEFAULT NULL,
  `client_id` varchar(255) DEFAULT NULL,
  `client_secret` varchar(255) DEFAULT NULL,
  `trusted` tinyint(1) DEFAULT NULL,
  `status` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `client`
--

INSERT INTO `client` (`createdAt`, `updatedAt`, `id`, `name`, `redirect_uri`, `client_id`, `client_secret`, `trusted`, `status`) VALUES
(1542330273714, 1542330273714, 1, 'Snake App', 'www.test.com', '1U895T6L3Y', 'Qj6r5uCfyZqS3i2lt8gOqX2wAb6QM5', 1, 'active');

-- --------------------------------------------------------

--
-- Table structure for table `photo`
--

CREATE TABLE `photo` (
  `createdAt` bigint(20) DEFAULT NULL,
  `updatedAt` bigint(20) DEFAULT NULL,
  `id` int(11) NOT NULL,
  `status` varchar(15) DEFAULT NULL,
  `source` varchar(255) DEFAULT NULL,
  `snakePart` varchar(20) DEFAULT NULL,
  `uploader_id` int(11) DEFAULT NULL,
  `verifier_id` int(11) DEFAULT NULL,
  `snake_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `refreshtoken`
--

CREATE TABLE `refreshtoken` (
  `createdAt` bigint(20) DEFAULT NULL,
  `updatedAt` bigint(20) DEFAULT NULL,
  `id` int(11) NOT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  `client_id` varchar(255) DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `refreshtoken`
--

INSERT INTO `refreshtoken` (`createdAt`, `updatedAt`, `id`, `user_id`, `client_id`, `token`) VALUES
(1542330329587, 1542330329587, 1, '', '1U895T6L3Y', 'QHifRLVPQncpItSN28Tfxdwh4CSiaX3NaVsekAXoz2IwDEq0puT74rpyQ6kC6NTzKPSAm8JqLER3tnacE1Zq9YJxUcKHIXQnogEV0px9UGKoK9Ug2F2cMpU1O4uPvHcuiENJxziNb63yjtO9MzvHDijKiJvT51v5MB7HjOb3xdBcB21bXPczwGXUYsluHF4WkyNfhnUYv32h0a0CPFoPrCG81o2XMXPCiORoAJ3EyDNWX2Jqha1Pf72NrEr7fqI');

-- --------------------------------------------------------

--
-- Table structure for table `snake`
--

CREATE TABLE `snake` (
  `createdAt` bigint(20) DEFAULT NULL,
  `updatedAt` bigint(20) DEFAULT NULL,
  `id` int(11) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `scientificName` varchar(100) DEFAULT NULL,
  `venomous` tinyint(1) DEFAULT NULL,
  `size` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `createdAt` bigint(20) DEFAULT NULL,
  `updatedAt` bigint(20) DEFAULT NULL,
  `id` int(11) NOT NULL,
  `username` varchar(20) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `firstName` varchar(20) DEFAULT NULL,
  `lastName` varchar(20) DEFAULT NULL,
  `birthDate` bigint(20) DEFAULT NULL,
  `phoneNumber` varchar(12) DEFAULT NULL,
  `email` varchar(40) DEFAULT NULL,
  `status` varchar(15) DEFAULT NULL,
  `type` varchar(20) DEFAULT NULL,
  `sex` varchar(20) DEFAULT NULL,
  `occupation` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`createdAt`, `updatedAt`, `id`, `username`, `password`, `firstName`, `lastName`, `birthDate`, `phoneNumber`, `email`, `status`, `type`, `sex`, `occupation`) VALUES
(1542330825701, 1542330825701, 1, 'cedric123', '$2b$10$wvPxP23adiDo6UCDQmDIB.nepfBfe0Lwlg/n/KpqEMbYVgWkZPlCu', 'Cedric', 'Chua', 839289600000, '1234567890', 'cchua@bisvietnam.net', 'active', 'doctor', 'male', 'general doctor');

-- --------------------------------------------------------

--
-- Table structure for table `verification_key`
--

CREATE TABLE `verification_key` (
  `createdAt` bigint(20) DEFAULT NULL,
  `updatedAt` bigint(20) DEFAULT NULL,
  `id` int(11) NOT NULL,
  `code` varchar(100) DEFAULT NULL,
  `status` varchar(10) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `verification_key`
--

INSERT INTO `verification_key` (`createdAt`, `updatedAt`, `id`, `code`, `status`, `user_id`) VALUES
(1542330384505, 1542330825789, 1, 'rkdM_qoam', 'used', 1),
(1542330384505, 1542330384505, 2, 'BJeOf_qiaX', 'unused', NULL),
(1542330384505, 1542330384505, 3, 'HybuGO9jTX', 'unused', NULL),
(1542330384505, 1542330384505, 4, 'Hkz_fu9i6Q', 'unused', NULL),
(1542330384505, 1542330384505, 5, 'HJXdfO5s6m', 'unused', NULL),
(1542330384505, 1542330384505, 6, 'SkNuf_5opQ', 'unused', NULL),
(1542330384505, 1542330384505, 7, 'S1SuMO9sam', 'unused', NULL),
(1542330384505, 1542330384505, 8, 'BJLdGdcjpm', 'unused', NULL),
(1542330384505, 1542330384505, 9, 'rkvufdcjTX', 'unused', NULL),
(1542330384505, 1542330384505, 10, 'BJuOGu5o6m', 'unused', NULL),
(1542330394006, 1542330394006, 11, 'SkMXOciTX', 'unused', NULL),
(1542330394006, 1542330394006, 12, 'r1gfmd5iaQ', 'unused', NULL),
(1542330394006, 1542330394006, 13, 'H1Zfmdqja7', 'unused', NULL),
(1542330394006, 1542330394006, 14, 'SkMfXO9oaX', 'unused', NULL),
(1542330394006, 1542330394006, 15, 'H1XzXuqi67', 'unused', NULL),
(1542330394006, 1542330394006, 16, 'HJVGXdqiaQ', 'unused', NULL),
(1542330394006, 1542330394006, 17, 'r1rfm_5saQ', 'unused', NULL),
(1542330394006, 1542330394006, 18, 'HkLG7_cj6m', 'unused', NULL),
(1542330394006, 1542330394006, 19, 'rywGQd9jTQ', 'unused', NULL),
(1542330394006, 1542330394006, 20, 'B1OfQOco67', 'unused', NULL),
(1542330399105, 1542330399105, 21, 'Skwm_qiaQ', 'unused', NULL),
(1542330399105, 1542330399105, 22, 'S1gvm_qsTm', 'unused', NULL),
(1542330399105, 1542330399105, 23, 'SkWPQdcoTX', 'unused', NULL),
(1542330399105, 1542330399105, 24, 'BkGDXu9i67', 'unused', NULL),
(1542330399105, 1542330399105, 25, 'BJXwQ_qoTX', 'unused', NULL),
(1542330399105, 1542330399105, 26, 'SJ4wm_cjpm', 'unused', NULL),
(1542330399105, 1542330399105, 27, 'r1Bv7u5ia7', 'unused', NULL),
(1542330399105, 1542330399105, 28, 'S18PQuqiTQ', 'unused', NULL),
(1542330399105, 1542330399105, 29, 'ryPw7d9j6Q', 'unused', NULL),
(1542330399105, 1542330399105, 30, 'BJuD7dqsTQ', 'unused', NULL),
(1542330405788, 1542330405788, 31, 'rJR7d9jpX', 'unused', NULL),
(1542330405788, 1542330405788, 32, 'SJeCXucia7', 'unused', NULL),
(1542330405788, 1542330405788, 33, 'r1b07_9o6Q', 'unused', NULL),
(1542330405788, 1542330405788, 34, 'HJMA7u5o6X', 'unused', NULL),
(1542330405788, 1542330405788, 35, 'HJXRQ_qoaX', 'unused', NULL),
(1542330405788, 1542330405788, 36, 'HkVC7uqipX', 'unused', NULL),
(1542330405788, 1542330405788, 37, 'BJH0XOqiTm', 'unused', NULL),
(1542330405788, 1542330405788, 38, 'HJL0muqoam', 'unused', NULL),
(1542330405788, 1542330405788, 39, 'Byw0mOqiam', 'unused', NULL),
(1542330405788, 1542330405788, 40, 'BJdAQu5sTQ', 'unused', NULL),
(1542330408336, 1542330408336, 41, 'rkxVuqsa7', 'unused', NULL),
(1542330408336, 1542330408336, 42, 'BJgg4_9opQ', 'unused', NULL),
(1542330408336, 1542330408336, 43, 'S1bgVO5iTm', 'unused', NULL),
(1542330408336, 1542330408336, 44, 'ByfxVd9jaQ', 'unused', NULL),
(1542330408336, 1542330408336, 45, 'rymgVO9saX', 'unused', NULL),
(1542330408336, 1542330408336, 46, 'HkNlN_9sam', 'unused', NULL),
(1542330408336, 1542330408336, 47, 'SyBl4O5jT7', 'unused', NULL),
(1542330408336, 1542330408336, 48, 'Sy8l4_5i6X', 'unused', NULL),
(1542330408336, 1542330408336, 49, 'HyDx4O9o6Q', 'unused', NULL),
(1542330408336, 1542330408336, 50, 'BJOlEu9opm', 'unused', NULL),
(1542330416777, 1542330416777, 51, 'SJFV_5saX', 'unused', NULL),
(1542330416777, 1542330416777, 52, 'ByetEOcspX', 'unused', NULL),
(1542330416777, 1542330416777, 53, 'rkWt4dcsam', 'unused', NULL),
(1542330416777, 1542330416777, 54, 'S1zFVO5iaQ', 'unused', NULL),
(1542330416777, 1542330416777, 55, 'BJXYEO9o67', 'unused', NULL),
(1542330416777, 1542330416777, 56, 'S1VYE_qsaQ', 'unused', NULL),
(1542330416777, 1542330416777, 57, 'S1BtE_5j6X', 'unused', NULL),
(1542330416777, 1542330416777, 58, 'BJIKEu5jTX', 'unused', NULL),
(1542330416777, 1542330416777, 59, 'HywtNuciTQ', 'unused', NULL),
(1542330416777, 1542330416777, 60, 'SyOFNdcspQ', 'unused', NULL),
(1542330418281, 1542330418281, 61, 'rk5NdqopX', 'unused', NULL),
(1542330418281, 1542330418281, 62, 'B1xcVdciT7', 'unused', NULL),
(1542330418281, 1542330418281, 63, 'HJb9Eucja7', 'unused', NULL),
(1542330418281, 1542330418281, 64, 'BJf9E_9sp7', 'unused', NULL),
(1542330418281, 1542330418281, 65, 'B1Qc4_9oa7', 'unused', NULL),
(1542330418281, 1542330418281, 66, 'H1Nq4O5iaX', 'unused', NULL),
(1542330418281, 1542330418281, 67, 'HJSqEuqia7', 'unused', NULL),
(1542330418281, 1542330418281, 68, 'ByL54dqs6Q', 'unused', NULL),
(1542330418281, 1542330418281, 69, 'B1P94d9op7', 'unused', NULL),
(1542330418281, 1542330418281, 70, 'rkdcNu5s67', 'unused', NULL),
(1542330421853, 1542330421853, 71, 'B1CEdqs6Q', 'unused', NULL),
(1542330421853, 1542330421853, 72, 'ByeRV_qjTX', 'unused', NULL),
(1542330421853, 1542330421853, 73, 'Hk-CNOcop7', 'unused', NULL),
(1542330421853, 1542330421853, 74, 'Hyf04O5oTQ', 'unused', NULL),
(1542330421853, 1542330421853, 75, 'HymR4O5s67', 'unused', NULL),
(1542330421853, 1542330421853, 76, 'HJNREu5oam', 'unused', NULL),
(1542330421853, 1542330421853, 77, 'r1BCNdcoTQ', 'unused', NULL),
(1542330421853, 1542330421853, 78, 'B1LA4O9o6Q', 'unused', NULL),
(1542330421853, 1542330421853, 79, 'HJDRN_qjpX', 'unused', NULL),
(1542330421853, 1542330421853, 80, 'S1_0NOqia7', 'unused', NULL),
(1542330422617, 1542330422617, 81, 'HJJBO5s6m', 'unused', NULL),
(1542330422617, 1542330422617, 82, 'HkgJBO5sTX', 'unused', NULL),
(1542330422617, 1542330422617, 83, 'rJW1Su5jpQ', 'unused', NULL),
(1542330422617, 1542330422617, 84, 'SyMkruciTX', 'unused', NULL),
(1542330422617, 1542330422617, 85, 'SJ71SO5sp7', 'unused', NULL),
(1542330422617, 1542330422617, 86, 'ByVkS_csp7', 'unused', NULL),
(1542330422617, 1542330422617, 87, 'HJrySu5sTX', 'unused', NULL),
(1542330422617, 1542330422617, 88, 'SkIkSdcsTm', 'unused', NULL),
(1542330422617, 1542330422617, 89, 'S1wySd9ipX', 'unused', NULL),
(1542330422617, 1542330422617, 90, 'r1uyS_5iT7', 'unused', NULL),
(1542330423348, 1542330423348, 91, 'HJKJBuqsam', 'unused', NULL),
(1542330423348, 1542330423348, 92, 'r1qJB_cop7', 'unused', NULL),
(1542330423348, 1542330423348, 93, 'r1iJH_9sam', 'unused', NULL),
(1542330423348, 1542330423348, 94, 'Hyn1rd5ia7', 'unused', NULL),
(1542330423348, 1542330423348, 95, 'SJpyHOqj6X', 'unused', NULL),
(1542330423348, 1542330423348, 96, 'H1R1r_9s6Q', 'unused', NULL),
(1542330423348, 1542330423348, 97, 'B1JgkSO5oaX', 'unused', NULL),
(1542330423348, 1542330423348, 98, 'B1eeJHO5jpQ', 'unused', NULL),
(1542330423348, 1542330423348, 99, 'SJ-eJSd9spX', 'unused', NULL),
(1542330423348, 1542330423348, 100, 'B1Ge1ru5opm', 'unused', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accesstoken`
--
ALTER TABLE `accesstoken`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `archive`
--
ALTER TABLE `archive`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `authcode`
--
ALTER TABLE `authcode`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `bite`
--
ALTER TABLE `bite`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `client`
--
ALTER TABLE `client`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `photo`
--
ALTER TABLE `photo`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `refreshtoken`
--
ALTER TABLE `refreshtoken`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `snake`
--
ALTER TABLE `snake`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `verification_key`
--
ALTER TABLE `verification_key`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `accesstoken`
--
ALTER TABLE `accesstoken`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `archive`
--
ALTER TABLE `archive`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `authcode`
--
ALTER TABLE `authcode`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `bite`
--
ALTER TABLE `bite`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `client`
--
ALTER TABLE `client`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `photo`
--
ALTER TABLE `photo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `refreshtoken`
--
ALTER TABLE `refreshtoken`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `snake`
--
ALTER TABLE `snake`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `verification_key`
--
ALTER TABLE `verification_key`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=101;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
