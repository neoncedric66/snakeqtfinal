/**
 * Application Settings Configuration
 * (sails.config.appSettings)
 */

module.exports.appSettings = {

  // EMAIL CREDENTIALS
  baseUrl: 'http://localhost:1337',
  from: 'Snake App <princess.carabeo@godigitalcorp.com>', //value of /from/ in sendactivation

  // ENCRYPTION CONFIG
  algorithmType: 'aes-128-cbc',
  secretKey: 'b7690d2a64ecd0ee6ea95ebe229e0f08',
  ivKey: '48dea3668ca6bb26ae5ccb6e36fbd854',
  tokenDelimiter: '_&@_',
  arraySeparator: '_$',

  // EMAIL TEMPLATES
  templateChangePassword: 'assets/templates/change_password.html',  //template of change password
  templateForgotPassword: 'assets/templates/forgot_password.html',  //template of forgot password
  templateBiteReport: 'assets/templates/bite-report.html',

  // RESET PASSWORD
  resetPasswordExpiry: 3000000, //in milliseconds
  resetPasswordView: '/reset-password',

  // TOKEN
  tokenLife: 3600000,

  // UPLOAD PHOTO SIZE LIMIT
  photoLimitSize: 10000000,

  // EMAIL BITE
  shouldEmailBite: true
};
