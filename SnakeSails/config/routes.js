/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes tell Sails what to do each time it receives a request.
 *
 * For more information on configuring custom routes, check out:
 * https://sailsjs.com/anatomy/config/routes-js
 */

module.exports.routes = {


  //  ╦ ╦╔═╗╔╗ ╔═╗╔═╗╔═╗╔═╗╔═╗
  //  ║║║║╣ ╠╩╗╠═╝╠═╣║ ╦║╣ ╚═╗
  //  ╚╩╝╚═╝╚═╝╩  ╩ ╩╚═╝╚═╝╚═╝

  /***************************************************************************
  *                                                                          *
  * Make the view located at `views/homepage.ejs` your home page.            *
  *                                                                          *
  * (Alternatively, remove this and add an `index.html` file in your         *
  * `assets` directory)                                                      *
  *                                                                          *
  ***************************************************************************/

  '/': {
    view: 'pages/homepage'
  },

  /***************************************************************************
  *                                                                          *
  * More custom routes here...                                               *
  * (See https://sailsjs.com/config/routes for examples.)                    *
  *                                                                          *
  * If a request to a URL doesn't match any of the routes in this file, it   *
  * is matched against "shadow routes" (e.g. blueprint routes).  If it does  *
  * not match any of those, it is matched against static assets.             *
  *                                                                          *
  ***************************************************************************/


  //  ╔═╗╔═╗╦  ╔═╗╔╗╔╔╦╗╔═╗╔═╗╦╔╗╔╔╦╗╔═╗
  //  ╠═╣╠═╝║  ║╣ ║║║ ║║╠═╝║ ║║║║║ ║ ╚═╗
  //  ╩ ╩╩  ╩  ╚═╝╝╚╝═╩╝╩  ╚═╝╩╝╚╝ ╩ ╚═╝

  // USER
  "POST /user/register": { action: "public/register" },
  "POST /user/update": { action: "core/user/update" },
  "POST /user/deactivate": { action: "core/user/deactivate" },
  // "GET /user/find": { action: "core/user/find" },
  // "POST /user/find": { action: "core/user/find" },
  "GET /user/find": { action: "core/user/find" },
  "POST /user/find": { action: "core/user/find" },
  "GET /user/profile": {action: "core/user/profile"},
  //Sends password to encrypt and compare
  "POST /user/password": { action: "public/check"},

  // VERIFICATION KEY
  "POST /code/generate": { action: "core/verificationKey/generate" },
  "POST /code/delete": { action: "core/verificationKey/delete" },
  "GET /code/find": { action: "core/verificationKey/find" },
  "POST /code/find": { action: "core/verificationKey/find" },

  // BITE
  "POST /bite/report": { action: "core/bite/report" },
  "POST /bite/delete" : {action: "core/bite/delete"},
  "GET /bite/find" : {action: "core/bite/find"},
  "POST /bite/find" : {action: "core/bite/find"},
  "POST /bite/update" : {action: "core/bite/update"},
  "POST /bite/diagnose" : {action: "core/bite/diagnose"},
  "POST /bite/update-patient-status" : {action: "core/bite/update-patient-status"},

  // TAG
  "POST /tag/create": { action: "core/tag/create" },
  "POST /tag/update": { action: "core/tag/update" },
  "POST /tag/delete": { action: "core/tag/delete" },
  "GET /tag/find": { action: "core/tag/find" },
  "POST /tag/find": { action: "core/tag/find" },

  // SYMPTOM
  "POST /symptom/create": { action: "core/symptom/create" },
  "POST /symptom/update": { action: "core/symptom/update" },
  "POST /symptom/delete": { action: "core/symptom/delete" },
  "GET /symptom/find": { action: "core/symptom/find" },
  "POST /symptom/find": { action: "core/symptom/find" },

  // SNAKE
  "POST /snake/create": { action: "core/snake/create" },
  "POST /snake/update": { action: "core/snake/update" },
  "POST /snake/delete": { action: "core/snake/delete" },
  "GET /snake/find": { action: "core/snake/find" },
  "POST /snake/find": { action: "core/snake/find" },

  // SNAKE PHOTO
  "POST /snake/photo/upload": { action: "core/snake/photo/upload" },
  "POST /snake/photo/verify": { action: "core/snake/photo/verify" },
  "POST /snake/photo/delete": { action: "core/snake/photo/delete" },
  "GET /snake/photo/find": { action: "core/snake/photo/find" },
  "POST /snake/photo/find": { action: "core/snake/photo/find" },

    /** OAUTH2 **/
    'POST /oauth/token': [
      { policy: 'grantPassword' },
      { policy: 'requestPolicy' },
      { policy: 'oauthToken' },
    ],
    'POST /oauth/token/validate': [
      { policy: 'oauthBearer'},
      { policy: 'oauthBearerHandler' }
    ],
    'POST /oauth/clients': [
      { policy: 'oauthBearer' },
      { action: 'oauth-client/findall' }
    ],
    'POST /oauth/client/create': [
      { action: 'oauth-client/create' }
    ],

  //  ╦ ╦╔═╗╔╗ ╦ ╦╔═╗╔═╗╦╔═╔═╗
  //  ║║║║╣ ╠╩╗╠═╣║ ║║ ║╠╩╗╚═╗
  //  ╚╩╝╚═╝╚═╝╩ ╩╚═╝╚═╝╩ ╩╚═╝


  //  ╔╦╗╦╔═╗╔═╗
  //  ║║║║╚═╗║
  //  ╩ ╩╩╚═╝╚═╝


};
