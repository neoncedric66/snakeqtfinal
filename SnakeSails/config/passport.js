var bcrypt = require('bcrypt');
var moment = require('moment');
var passport = require('passport');
var BearerStrategy = require('passport-http-bearer').Strategy;
var BasicStrategy = require('passport-http').BasicStrategy;
var LocalStrategy = require('passport-local').Strategy;
var ClientPasswordStrategy = require('passport-oauth2-client-password').Strategy;

/* global User Client AccessToken sails */

/**
 * LocalStrategy
 *
 * This strategy is used to authenticate users based on a username and password.
 * Anytime a request is made to authorize an application, we must ensure that
 * a user is logged in before asking them to approve the request.
 */

passport.use(new LocalStrategy(
  function (username, password, done) {
    process.nextTick(
      async function () {
        try {
          var user = await User.findOne({email: username});
          if (!user) {
            return done(null, false,{message: 'Unknown user ' + username});
          }
          bcrypt.compare(password, user.password, function (err, res) {
            if (err) {
              return done(err, null);
            } else {
              if (!res) {
                return done(null, false, { message: 'Invalid password' });
              } else {
                return done(null, user);
              }
            }
          });
        } catch (err) {
          return done(err);
        }

      }
    );
  }
));

/**
 * BasicStrategy & ClientPasswordStrategy
 *
 * These strategies are used to authenticate registered OAuth clients.  They are
 * employed to protect the `token` endpoint, which consumers use to obtain
 * access tokens.  The OAuth 2.0 specification suggests that clients use the
 * HTTP Basic scheme to authenticate.  Use of the client password strategy
 * allows clients to send the same credentials in the request body (as opposed
 * to the `Authorization` header).  While this approach is not recommended by
 * the specification, in practice it is quite common.
 */

passport.use('clientBasic', new BasicStrategy(
  async function (clientId, clientSecret, done) {
    try {
      var client = await Client.findOne({client_id: clientId});
      if (!client) { return done(null, false); }
      if (_.includes(['deactivated', 'blocked'], client.status)) { return done(null, false); }
      if (client.client_secret !== clientSecret) {
        return done(null, false);
      }
      return done(null, client);
    } catch (err) {
      return done(err);
    }
  }
));

passport.use('basic', new BasicStrategy(
  async function (username, password, done) {
    try {
      var user = await User.findOne({ email: username });
      if (!user) { return done(null, false); }
      bcrypt.compare(password, user.password, function (err, res) {
        if (err) {
          return done(err, null);
        } else {
          if (!res) {
            return done(null, false, { message: 'Invalid password' });
          } else {
            return done(null, user);
          }
        }
      });
    } catch(err) {
      return done(err);
    }
  }
));

passport.use(new ClientPasswordStrategy(
  async function (clientId, clientSecret, done) {
    try {
      var client = await Client.findOne({client_id: clientId});
      if (!client) {
        return done(null, false);
      }
      if (_.includes(['deactivated', 'blocked'], client.status)) { return done(null, false); }
      if (client.client_secret !== clientSecret) {
        return done(null, false);
      }
      return done(null, client);
    } catch (err) {
      return done(err);
    }
  }
));

/**
 * BearerStrategy
 *
 * This strategy is used to authenticate users based on an access token (aka a
 * bearer token).  The user must have previously authorized a client
 * application, which is issued an access token to make requests on behalf of
 * the authorizing user.
 */
passport.use(new BearerStrategy(
  async function (accessToken, done) {
    try {
      var token = await AccessToken.findOne({token: accessToken});
      if (!token) {
        return done(null, false);
      }

      var now = moment().unix();
      var creationDate = moment(token.updatedAt).unix();

      if (now - creationDate > (sails.config.appSettings.tokenLife || sails.config.oauth.tokenLife)) {
        await AccessToken.destroy({ token: accessToken });
        return done(null, false, { message: 'Token expired' });
      }

      await AccessToken.update({ token: accessToken }).set({ updatedAt: new Date() });

      // var info = { scope: '*' };
      if (token.user_id) {
        const user = await User.findOne({ id: token.user_id });
        return done(null, user);
      }
      var client = await Client.findOne({client_id: token.client_id});
      return done(null, client);
    } catch(err) {
      return done(err);
    }
  }
));
