/**
 * Policy Mappings
 * (sails.config.policies)
 *
 * Policies are simple functions which run **before** your actions.
 *
 * For more information on configuring policies, check out:
 * https://sailsjs.com/docs/concepts/policies
 */

module.exports.policies = {

  /***************************************************************************
  *                                                                          *
  * Default policy for all controllers and actions, unless overridden.       *
  * (`true` allows public access)                                            *
  *                                                                          *
  ***************************************************************************/

  '*': true,
  //BITE
  "core/bite/delete"                      : [ 'oauthBearer', 'userHasAccess', 'doctorHasAccess' ],
  "core/bite/update"                      : [ 'oauthBearer', 'userHasAccess', 'doctorHasAccess' ],
  "core/bite/diagnose"                    : [ 'oauthBearer', 'userHasAccess', 'doctorHasAccess' ],
  "core/bite/update-patient-status"       : [ 'oauthBearer', 'userHasAccess', 'doctorHasAccess' ],
  "core/bite/*"                           : [ 'oauthBearer', 'userHasAccess' ],

  //SNAKE PHOTO
  "core/snake/photo/delete"     :[ 'oauthBearer', 'clientHasAccess' ],
  "core/snake/photo/*"          :[ 'oauthBearer', 'userHasAccess' ],
  
  //SNAKE
  "core/snake/find"             :[ 'oauthBearer', 'userHasAccess'],
  "core/snake/*"                :[ 'oauthBearer', 'clientHasAccess' ],

  //TAG
  "core/tag/find"               :[ 'oauthBearer', 'userHasAccess'],
  "core/tag/*"                  :[ 'oauthBearer', 'clientHasAccess' ],  

  //SYMPTOM
  "core/symptom/find"               :[ 'oauthBearer', 'userHasAccess'],
  "core/symptom/*"                  :[ 'oauthBearer', 'clientHasAccess' ],  

  //USER
  "core/user/*"                 : [ 'oauthBearer', 'userHasAccess' ],

  //VERIFICATION KEY
  "core/verificationKey/*"      : [ 'oauthBearer', 'clientHasAccess' ],

  //PUBLIC
  "public/*"                    : true,


};
