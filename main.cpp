#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QScreen>
#include <QQmlComponent>
#include <QtNetwork>
#include <QNetworkConfiguration>
#include <QQmlNetworkAccessManagerFactory>
//#include "netreq.h"
#include <QPointF>

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;
    QQmlContext *context = engine.rootContext();

    context->setContextProperty("IMG_LOC", "qrc:/Icons/");
    context->setContextProperty("SCENE_LOC", "qrc:/Views/Scenes/");
    context->setContextProperty("FONT_LOC", "qrc:/Font/");
    context->setContextProperty("COMPONENT_LOC", "qrc:/Views/Components/");
    context->setContextProperty("APP_VERSION", "v1.0.0");
    context->setContextProperty("SIDE_LOC", "qrc:/Views/Scenes/SidebarScenes/");
    context->setContextProperty("LOGIN_LOC", "qrc:/Icons/LoginPage/");
    context->setContextProperty("SNAKE_LOC", "qrc:/Icons/Snakes/");
    context->setContextProperty("LOAD_LOC", "qrc:/Icons/LoadingPage/");
    context->setContextProperty("SLIDE_LOC", "qrc:/Icons/Slider/");
    context->setContextProperty("TREAT_LOC", "qrc:/Icons/Treatment/");
//    context->setContextProperty("MAP_point", point);
    context->setContextProperty("SnakeChosen", "");
    


//    //example
//    NetReq *netreq = new NetReq(); //this is the instance of your cpp class
//    context->setContextProperty("NET_REQ", netreq); //you use NET_REQ in qml files to acess the functions of MYCPPClass.
//    //test download
//    //to download image place the file in the build folder then call on the file name
//    netreq->downloadImage("http://thepublicvoice.org/wordpress/wp-content/uploads/2016/01/TPV-people-hp.jpg","TPV_people.jpg");

    engine.load(QUrl(QLatin1String("qrc:/main.qml")));

    return app.exec();
}
