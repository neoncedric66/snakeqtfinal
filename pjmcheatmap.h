#ifndef PJMCHEATMAP_H
#define PJMCHEATMAP_H

#include <QObject>
#include <QVector>
#include <QImage>
#include "PJMCGradientPalette.h"

class PJMCGradientPalette;

class PJMCHeatMap : public QObject
{
    Q_OBJECT
    Q_PROPERTY(int opacity READ opacity WRITE setOpacity NOTIFY opacityChanged)
    Q_PROPERTY(int radius READ radius WRITE setRadius NOTIFY radiusChanged)
    Q_PROPERTY(QImage* mainCanvas READ mainCanvas NOTIFY mainCanvasChanged)
    Q_PROPERTY(PJMCGradientPalette* palette READ palette WRITE setPalette NOTIFY paletteChanged)
public:
    explicit PJMCHeatMap(QObject *parent = nullptr);
    ~PJMCHeatMap();

    int opacity();
    void setOpacity(int opacity);
    int radius();
    void setRadius(int radius);
    QImage* mainCanvas();
    PJMCGradientPalette* palette();
    void setPalette(PJMCGradientPalette *palette);

    Q_INVOKABLE void addPoint(int x, int y);
    Q_INVOKABLE void createMainCanvas(int width, int height);

    int  getCount(int x, int y);
    void colorize(int x, int y);
    void colorize();
    virtual void drawAlpha(int x, int y, int count, bool colorize_now = true);

protected:
    virtual void colorize(int left, int top, int right, int bottom);
    void redraw();

private:
    int increase(int x, int y, int delta = 1);

private:
    QVector<int> data_;
    QImage *alphaCanvas_;
    QImage *mainCanvas_;
    PJMCGradientPalette *palette_;
    int radius_ = 60;
    int opacity_ = 0.5;
    qreal max_;
    int width_;
    int height_;

signals:
    void opacityChanged();
    void radiusChanged();
    void mainCanvasChanged();
    void paletteChanged();

public slots:
};

#endif // PJMCHEATMAP_H
